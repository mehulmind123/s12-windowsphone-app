﻿using System;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Converters
{
    public class StatusToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SolidColorBrush foregroundSolidColorBrush;
            string val = System.Convert.ToString(value);
            if (val == "3")
            {
                foregroundSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 72, 88);
                return foregroundSolidColorBrush;
            }
            else if (val == "2")
            {
                foregroundSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 186, 0);
                return foregroundSolidColorBrush;
            }
            else if (val == "1")
            {
                foregroundSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 0, 216, 0);
                return foregroundSolidColorBrush;
            }
            else
            {
                foregroundSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);
                return foregroundSolidColorBrush;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            SolidColorBrush foregroundSolidColorBrush;
            if (parameter != null && parameter is string &&
                parameter as string == "1")
            {
                foregroundSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 0, 216, 0);
                return foregroundSolidColorBrush;
            }

            foregroundSolidColorBrush = new SolidColorBrush();
            foregroundSolidColorBrush.Color = Color.FromArgb(255, 0, 216, 0);
            return foregroundSolidColorBrush;
        }
    }
}
