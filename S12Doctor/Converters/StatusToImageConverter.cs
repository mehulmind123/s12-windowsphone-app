﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace S12Doctor.Converters
{
    public class StatusToImageConverter : IValueConverter
    {
        
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string val = System.Convert.ToString(value);
            if (val == "3")
            {
                return new BitmapImage(new Uri("ms-appx:///Assets/AMHP/HomeScreen/red.png", UriKind.RelativeOrAbsolute));
            }
            else if (val == "2")
            {
                return new BitmapImage(new Uri("ms-appx:///Assets/AMHP/HomeScreen/umber.png", UriKind.RelativeOrAbsolute));
            }
            else if (val == "1")
            {
                return new BitmapImage(new Uri("ms-appx:///Assets/AMHP/HomeScreen/green.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                return new BitmapImage(new Uri("ms-appx:///Assets/AMHP/HomeScreen/grey.png", UriKind.RelativeOrAbsolute));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (parameter != null && parameter is string &&
                parameter as string == "1")
            {
                return new BitmapImage(new Uri("ms-appx:///Assets/AMHP/HomeScreen/grey.png", UriKind.RelativeOrAbsolute));
            }

            return new BitmapImage(new Uri("ms-appx:///Assets/AMHP/HomeScreen/grey.png", UriKind.RelativeOrAbsolute));
        }

    }
}
