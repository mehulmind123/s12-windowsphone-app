﻿using System;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Converters
{
    public class BoolToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            bool boolValue = (bool)value;

            if (parameter != null && parameter is string &&
                parameter as string == "1")
            {
                boolValue ^= true;
            }
            return boolValue ? new SolidColorBrush(Colors.Cyan) : new SolidColorBrush(Colors.White) ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (parameter != null && parameter is string &&
                parameter as string == "1")
            {
                return new SolidColorBrush(Colors.Cyan);
            }

            return new SolidColorBrush(Colors.White);
        }
    }
}
