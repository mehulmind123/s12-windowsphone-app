﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace S12Doctor.Converters
{
    public class DateTimeToRelativeTimeConverter : IValueConverter
    {
        const int SECOND = 1;
        const int MINUTE = 60 * SECOND;
        const int HOUR = 60 * MINUTE;
        const int DAY = 24 * HOUR;
        const int MONTH = 30 * DAY;

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                if (value != null && !string.IsNullOrEmpty(System.Convert.ToString(value)))
                {
                    DateTime val = System.Convert.ToDateTime(value);

                    var ts = new TimeSpan(DateTime.UtcNow.Ticks - val.Ticks);
                    double delta = Math.Abs(ts.TotalSeconds);

                    if (delta < 1 * MINUTE)
                        return ts.Seconds == 1 ? "Updated Just now" : "Updated Just now";

                    if (delta < 2 * MINUTE)
                        return "Updated "+ ts.Minutes +" minute ago";

                    if (delta < 45 * MINUTE)
                        return "Updated " + ts.Minutes + " minutes ago";

                    if (delta < 90 * MINUTE)
                        return "Updated an hour ago";

                    if (delta < 24 * HOUR)
                        return "Updated " + ts.Hours + " hours ago";

                    if (delta < 48 * HOUR)
                        return "Updated 1 day ago";

                    if (delta < 30 * DAY)
                    {
                        if (ts.Days < 7)
                            return "Updated " + ts.Days + " days ago";
                        else if(ts.Days < 14)
                            return "Updated " + 1 + " week ago";
                        else
                            return "Updated " + (int)(ts.Days / 7) + " weeks ago";
                    }

                    if (delta < 12 * MONTH)
                    {
                        int months = System.Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                        return months <= 1 ? "Updated 1 month ago" : "Updated " + months + " months ago";
                    }
                    else
                    {
                        int years = System.Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                        return years <= 1 ? "Updated 1 year ago" : "Updated " + years + " years ago";
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (parameter != null && parameter is string &&
                parameter as string == "1")
            {
                return string.Empty;
            }

            return string.Empty;
        }
    }
}
