﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace S12Doctor.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            bool boolValue = (bool)value;

            if (parameter != null && parameter is string &&
                parameter as string == "1")
            {
                boolValue ^= true;
            }

            return boolValue ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (parameter != null && parameter is string &&
                parameter as string == "1")
            {
                return (Visibility)value != Visibility.Visible;
            }

            return (Visibility)value == Visibility.Visible;
        }
    }
}
