﻿using S12Doctor.Model.Common;
using S12Doctor.Utilities;
using System;
using System.Text.RegularExpressions;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace S12Doctor
{
    public sealed partial class Login : UserControl
    {
        WebService.CommonServices common;

        public Login()
        {
            this.InitializeComponent();
            btnLogin.Click += BtnLogin_Click;
            this.Loaded += Login_Loaded;
        }

        private async void Login_Loaded(object sender, RoutedEventArgs e)
        {
            if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
            {
                if (string.IsNullOrEmpty(Helpers.SettingHelper.IsS12LoginFirstTime))
                {
                    flipViewS12.Visibility = Visibility.Visible;
                    stkTextBoxs.Visibility = Visibility.Collapsed;
                }
                else
                {
                    flipViewS12.Visibility = Visibility.Collapsed;
                    stkTextBoxs.Visibility = Visibility.Visible;
                }
                    

            }
            else
            {
                if (string.IsNullOrEmpty(Helpers.SettingHelper.IsAMHPLoginFirstTime))
                {
                    flipViewAMHP.Visibility = Visibility.Visible;
                    stkTextBoxs.Visibility = Visibility.Collapsed;
                }
                else
                {
                    flipViewAMHP.Visibility = Visibility.Collapsed;
                    stkTextBoxs.Visibility = Visibility.Visible;

                }
            }

            if (common == null)
                common = new WebService.CommonServices();

            if (Utilities.SessionData.CurrntAppUser != null)
            {
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).AddControl(Pages.Home);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).AddControl(Pages.Home);
            }

            
            //txtEmailAddress.Text = "alisha.mind@gmail.com";
            //txtPassword.Password = "tiBOfiw";

            SolidColorBrush headerStatusTextSolidColorBrush;
            headerStatusTextSolidColorBrush = new SolidColorBrush();

            if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
            {
                headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 19, 113, 185);
                btnLogin.Background = headerStatusTextSolidColorBrush;
                btnFPSubmit.Background = headerStatusTextSolidColorBrush;
                textBlock.Foreground = headerStatusTextSolidColorBrush;
                textBlock.Text = "Section 12 Doctor";
            }
            else
            {
                headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 192, 181);
                btnLogin.Background = headerStatusTextSolidColorBrush;
                btnFPSubmit.Background = headerStatusTextSolidColorBrush;
                textBlock.Foreground = headerStatusTextSolidColorBrush;
                textBlock.Text = "Approved Mental Health Professional";
            }

            if ( !string.IsNullOrEmpty(Helpers.SettingHelper.IsAMHPLoginFirstTime) && string.IsNullOrEmpty(Helpers.SettingHelper.IsS12LoginFirstTime))
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    txtEmailAddress.Focus(FocusState.Keyboard));
            }
                

            try
            {
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                {
                    object result = await common.cms();
                    if (result is CMS)
                        SessionData.CMSData = ((CMS)result);
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                else
                {
                    object result = await common.cms();
                    if (result is CMS)
                        SessionData.CMSData = ((CMS)result);
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }

            }
            catch (Exception ex) { }
        }

        private void txtEmailAddress_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                FocusManager.TryMoveFocus(FocusNavigationDirection.Next);

                // Make sure to set the Handled to true, otherwise the RoutedEvent might fire twice
                e.Handled = true;
            }
        }

        private async void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;
            if (string.IsNullOrWhiteSpace(txtEmailAddress.Text))
                error = "Email address can not be blank.";
            else if (!Regex.IsMatch(txtEmailAddress.Text, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
                error = "Please enter a valid email address.";
            else if (string.IsNullOrWhiteSpace(txtPassword.Password))
                error = "Password cannot be blank.";

            if (!string.IsNullOrWhiteSpace(error))
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
                try
                {
                    if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    {
                        object result = await common.login(txtEmailAddress.Text, txtPassword.Password, "1");
                        if (result is AppUser)
                        {
                            if (Utilities.SessionData.CurrntAppUser.Data.AcceptTermsCondition == 0)
                            {
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).AddControl(Pages.TermsAndConditions);
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).TermsAndConditionsControl.SetTermsConditionsVisibility(0);
                            }
                            else if (Utilities.SessionData.CurrntAppUser.Data.ChangePassword == 0)
                            {
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).AddControl(Pages.ChangePassword);
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                            }
                            else
                            {
                                Helpers.SettingHelper.EmailId = txtEmailAddress.Text;
                                Helpers.SettingHelper.Password = txtPassword.Password;
                                Helpers.SettingHelper.LoginType = "1";

                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).AddControl(Pages.Home);
                            }
                        }
                        else
                        {
                            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                        }
                    }
                    else
                    {
                        object result = await common.login(txtEmailAddress.Text, txtPassword.Password, "2");
                        if (result is AppUser)
                        {
                            if (Utilities.SessionData.CurrntAppUser.Data.AcceptTermsCondition == 0)
                            {
                                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).AddControl(Pages.TermsAndConditions);
                                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).TermsAndConditionsControl.SetTermsConditionsVisibility(0);
                            }
                            else if (Utilities.SessionData.CurrntAppUser.Data.ChangePassword == 0)
                            {
                                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).AddControl(Pages.ChangePassword);
                                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                            }
                            else
                            {
                                Helpers.SettingHelper.EmailId = txtEmailAddress.Text;
                                Helpers.SettingHelper.Password = txtPassword.Password;
                                Helpers.SettingHelper.LoginType = "2";

                                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).AddControl(Pages.Home);
                            }
                        }
                        else
                        {
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                        }
                    }
                }
                catch { }
        }

        async private void btnFPSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;
            if (string.IsNullOrWhiteSpace(txtFPEmailAddress.Text))
                error = "Email address can not be blank.";
            else if (!Regex.IsMatch(txtFPEmailAddress.Text, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
                error = "Please enter a valid email address.";

            if (!string.IsNullOrWhiteSpace(error))
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
            {
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                {
                    object result = await common.forgotPassword(txtFPEmailAddress.Text, "1");
                    if (result is MetaInfo)
                    {
                        txtFPEmailAddress.Text = string.Empty;
                        gridForgotPassword.Visibility = Visibility.Collapsed;
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                else
                {
                    object result = await common.forgotPassword(txtFPEmailAddress.Text, "2");
                    if (result is MetaInfo)
                    {
                        txtFPEmailAddress.Text = string.Empty;
                        gridForgotPassword.Visibility = Visibility.Collapsed;
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
            }
        }

        private void txtForgotPassword_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridForgotPassword.Visibility = Visibility.Visible;
        }

        public void DisposeMemory()
        {
            try
            {

                if (Doctor12Logo != null)
                {
                    Doctor12Logo.Resources.Clear();
                    Doctor12Logo.Resources = null;
                    Doctor12Logo.Source = null;
                    Doctor12Logo = null;
                }
                if (textBlock != null)
                {
                    textBlock.Text = null;
                    textBlock = null;
                }
                if (brEmailAddress != null)
                {
                    brEmailAddress.Resources.Clear();
                    brEmailAddress.Resources = null;
                    brEmailAddress = null;
                }
                if (imgEmailAddress != null)
                {
                    imgEmailAddress.Resources.Clear();
                    imgEmailAddress.Resources = null;
                    imgEmailAddress.Source = null;
                    imgEmailAddress = null;
                }
                if (txtEmailAddress != null)
                {
                    txtEmailAddress.Text = null;
                    txtEmailAddress = null;
                }

                if (brPassword != null)
                {
                    brPassword.Resources.Clear();
                    brPassword.Resources = null;
                    brPassword = null;
                }
                if (imgPassword != null)
                {
                    imgPassword.Resources.Clear();
                    imgPassword.Resources = null;
                    imgPassword.Source = null;
                    imgPassword = null;
                }
                if (txtPassword != null)
                {
                    txtPassword.Password = null;
                    txtPassword = null;
                }
                if (btnLogin != null)
                {
                    btnLogin.Click -= BtnLogin_Click;
                    btnLogin.Resources.Clear();
                    btnLogin.Resources = null;
                    btnLogin = null;
                }
                if (txtForgotPassword != null)
                {
                    txtForgotPassword.Text = null;
                    txtForgotPassword = null;
                }
                if (gridPassword != null)
                {
                    gridPassword.Resources.Clear();
                    gridPassword.Resources = null;
                    gridPassword.Children.Clear();
                    gridPassword = null;
                }

                if (gridEmailAddress != null)
                {
                    gridEmailAddress.Resources.Clear();
                    gridEmailAddress.Resources = null;
                    gridEmailAddress.Children.Clear();
                    gridEmailAddress = null;
                }
                if (stkTextBoxs != null)
                {
                    stkTextBoxs.Resources.Clear();
                    stkTextBoxs.Resources = null;
                    stkTextBoxs.Children.Clear();
                    stkTextBoxs = null;
                }
                if (LayoutRoot != null)
                {
                    LayoutRoot.Resources.Clear();
                    LayoutRoot.Resources = null;
                    LayoutRoot.Children.Clear();
                    LayoutRoot = null;
                }
                if (DoctorLogin != null)
                {
                    DoctorLogin.Resources.Clear();
                    DoctorLogin.Resources = null;
                    DoctorLogin = null;
                }
                GC.Collect();
            }
            catch (Exception)
            { }
        }

        private void HeaderText_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            flipViewAMHP.Visibility = Visibility.Collapsed;
            flipViewS12.Visibility = Visibility.Collapsed;
            stkTextBoxs.Visibility = Visibility.Visible;
        }

        private void flipViewAMHP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FlipView fp = sender as FlipView;
            if (btnDone != null)
            {
                if (fp.SelectedIndex == 4)
                    btnDone.Visibility = Visibility.Visible;
                else
                    btnDone.Visibility = Visibility.Collapsed;
            }
        }
    }
}
