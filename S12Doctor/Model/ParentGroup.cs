﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace S12Doctor.Model
{
    public class ParentGroup : ObservableCollection<ChildGroup>
    {
        public ParentGroup(IEnumerable<ChildGroup> items)
            : base(items)
        {
        }

        public string GroupID { get; set; }
    }
}
