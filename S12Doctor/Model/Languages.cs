﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace S12Doctor.Model
{
    public class Languages : BindableBase
    {
        public Languages()
        {
            _Items = new ObservableCollection<Language>()
            {
               new Language() { Text = "English", IsChecked = true },
               new Language() { Text = "Arabic", IsChecked = false },
               new Language() { Text = "Greek", IsChecked = false },
               new Language() { Text = "Hawaiian", IsChecked = true },
               new Language() { Text = "Elvish", IsChecked = false },
               new Language() { Text = "Hebrew", IsChecked = true },
               new Language() { Text = "Chinese", IsChecked = false }
            };

            //foreach (var item in this.Items)
            //    item.PropertyChanged += (s, e) => base.RaisePropertyChanged("SelectedLocalAuthorities");
        }

        public void AddLanguage(string language)
        {
            _Items.Add(new Language() { Text = language, IsChecked = false });
        }

        public string SelectedLanguage
        {
            get
            {
                var array = this.Items
                    .Where(x => x.IsChecked)
                    .Select(x => x.Text).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        ObservableCollection<Language> _Items;
        public ObservableCollection<Language> Items { get { return _Items; } }
    }

    public class Language : BindableBase
    {
        public string Text { get; set; }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }
    }

    public class Lang : BindableBase
    {
        [JsonProperty("language_id")]
        public int LanguageID { get; set; }

        [JsonProperty("language_name")]
        public string LanguageName { get; set; }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }
    }
}
