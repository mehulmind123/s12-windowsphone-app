﻿using S12Doctor.Base;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace S12Doctor.Model
{
    public class Specialities : BindableBase
    {
        public Specialities()
        {
            _Items = new ObservableCollection<Specialty>()
            {
               new Specialty() { Text = "Specialty 1", IsChecked = false },
               new Specialty() { Text = "Specialty 2", IsChecked = false },
               new Specialty() { Text = "Specialty 3", IsChecked = false },
               new Specialty() { Text = "Specialty 4", IsChecked = false },
               new Specialty() { Text = "Specialty 5", IsChecked = false },
               new Specialty() { Text = "Specialty 6", IsChecked = false },
               new Specialty() { Text = "Specialty 7", IsChecked = false },
               new Specialty() { Text = "Specialty 8", IsChecked = false }
            };

            //foreach (var item in this.Items)
            //    item.PropertyChanged += (s, e) => base.RaisePropertyChanged("SelectedLocalAuthorities");
        }

        public string SelectedSpeciality
        {
            get
            {
                var array = this.Items
                    .Where(x => x.IsChecked)
                    .Select(x => x.Text).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        ObservableCollection<Specialty> _Items;
        public ObservableCollection<Specialty> Items { get { return _Items; } }
    }

    public class Specialty : BindableBase
    {
        public string Text { get; set; }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }
    }
}
