﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.S12Doctor
{
    public partial class CompletedClaimForm
    {
        [JsonProperty("data")]
        public List<ClaimFormData> Data { get; set; }

        [JsonProperty("meta")]
        public CompletedClaimFormMeta Meta { get; set; }
    }

    public partial class CompletedClaimFormMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("draft_count")]
        public long DraftCount { get; set; }

        [JsonProperty("new_offset")]
        public long NewOffset { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class CompletedClaimForm
    {
        public static CompletedClaimForm FromJson(string json) => JsonConvert.DeserializeObject<CompletedClaimForm>(json, ConverterCompletedClaimForm.Settings);
    }

    public static class SerializeCompletedClaimForm
    {
        public static string ToJson(this CompletedClaimForm self) => JsonConvert.SerializeObject(self, ConverterCompletedClaimForm.Settings);
    }

    public class ConverterCompletedClaimForm
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
