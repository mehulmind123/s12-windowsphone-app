﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.S12Doctor
{
    
    public partial class CalanderAvailibility
    {
        [JsonProperty("data")]
        public CalanderAvailibilityInformation Data { get; set; }

        [JsonProperty("meta")]
        public CalanderAvailibilityMeta Meta { get; set; }
    }

    public partial class CalanderAvailibilityInformation
    {
        [JsonProperty("weekly_availability_status")]
        public WeeklyAvailabilityStatusinfo WeeklyAvailabilityStatus { get; set; }
        
    }

    public partial class WeeklyAvailabilityStatusinfo
    {
        [JsonProperty("am")]
        public string am { get; set; }

        [JsonProperty("lunch")]
        public string lunch { get; set; }

        [JsonProperty("pm")]
        public string pm { get; set; }

        [JsonProperty("evening")]
        public string evening { get; set; }

        [JsonProperty("night")]
        public string night { get; set; }
    }

    public partial class CalanderAvailibilityMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("draft_count")]
        public long DraftCount { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class CalanderAvailibility
    {
        public static CalanderAvailibility FromJson(string json) => JsonConvert.DeserializeObject<CalanderAvailibility>(json, ConverterCalanderAvailibility.Settings);
    }

    public static class SerializeCalanderAvailibility
    {
        public static string ToJson(this CalanderAvailibility self) => JsonConvert.SerializeObject(self, ConverterCalanderAvailibility.Settings);
    }

    public class ConverterCalanderAvailibility
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
