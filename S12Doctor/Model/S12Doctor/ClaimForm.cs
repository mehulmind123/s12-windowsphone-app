﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;

namespace S12Doctor.Model.S12Doctor
{


    public partial class ClaimForm
    {
        [JsonProperty("data")]
        public ClaimFormData Data { get; set; }

        [JsonProperty("meta")]
        public ClaimFormMeta Meta { get; set; }
    }

    public partial class ClaimFormData : BindableBase
    {
        public string Title { get; set; }

        [JsonProperty("claim_id")]
        public long ClaimId { get; set; }

        [JsonProperty("assessment_location_type_description")]
        public string AssessmentLocationTypeDescription { get; set; }

        [JsonProperty("amhp_name_for_display")]
        public string AmhpNameForDisplay { get; set; }

        [JsonProperty("amhp_id")]
        public long AmhpId { get; set; }

        [JsonProperty("additional_information")]
        public string AdditionalInformation { get; set; }

        [JsonProperty("amhp_name")]
        public string AmhpName { get; set; }

        [JsonProperty("assessment_date_time")]
        private string _AssessmentDateTime = string.Empty;
        public string AssessmentDateTime {
            get
            {
                try
                {
                    return Convert.ToDateTime(_AssessmentDateTime).ToString("yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    return _AssessmentDateTime;
                }
            }
            set { _ClaimCreationTimestamp = value; }
        }

        [JsonProperty("approval_status")]

        private long approvalStatus;
        public long ApprovalStatus
        {
            get { return this.approvalStatus; }
            set
            {
                this.approvalStatus = value;
            }
        }

        public string ApprovalStatusName
        {
            get
            {
                if (ApprovalStatus == 1)
                    return "Draft";
                if (ApprovalStatus == 2)
                    return "Pending";
                if (ApprovalStatus == 3)
                    return "Approved";
                if (ApprovalStatus == 4)
                    return "Rejected";
                else
                    return "N/A";
            }
        }

        [JsonProperty("assessment_location_type")]
        public long AssessmentLocationType { get; set; }

        [JsonProperty("car_model")]
        public string CarModel { get; set; }

        [JsonProperty("assessment_postcode")]
        public string AssessmentPostcode { get; set; }

        [JsonProperty("assessment_location_type_text")]
        public string AssessmentLocationTypeText { get; set; }

        [JsonProperty("car_make")]
        public string CarMake { get; set; }

        [JsonProperty("ccg_name")]
        public string CcgName { get; set; }

        [JsonProperty("car_registration_plate")]
        public string CarRegistrationPlate { get; set; }

        [JsonProperty("claim_creation_timestamp")]
        private string _ClaimCreationTimestamp;
        public string ClaimCreationTimestamp
        {
            get
            {
                System.DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
                dt = dt.AddSeconds(Convert.ToInt64(_ClaimCreationTimestamp)).ToLocalTime();
                return dt.ToString("dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            set { _ClaimCreationTimestamp = value; }
        }

        [JsonProperty("miles_traveled")]
        public string MilesTraveled { get; set; }

        [JsonProperty("engine_size")]
        public long EngineSize { get; set; }

        [JsonProperty("doctor_address")]
        public string DoctorAddress { get; set; }

        [JsonProperty("disclaimer")]
        public long Disclaimer { get; set; }

        [JsonProperty("doctor_name")]
        public string DoctorName { get; set; }

        [JsonProperty("local_authority")]
        public string LocalAuthority { get; set; }

        [JsonProperty("from_postcode")]
        public string FromPostcode { get; set; }

        [JsonProperty("local_authority_id")]
        public long LocalAuthorityId { get; set; }

        [JsonProperty("rejection_reason")]
        public string RejectionReason { get; set; }

        [JsonProperty("patient_nhs_number")]
        public string PatientNhsNumber { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("patient_postcode")]
        public string PatientPostcode { get; set; }

        [JsonProperty("section_implemented_text")]
        public string SectionImplementedText { get; set; }

        [JsonProperty("sync_id")]
        public long SyncId { get; set; }

        [JsonProperty("section_implemented")]
        public string SectionImplemented { get; set; }

        [JsonProperty("section_type_description")]
        public string SectionTypeDescription { get; set; }

        [JsonProperty("to_postcode")]
        public string ToPostcode { get; set; }

        bool _IsDeleteMode = default(bool);
        public bool IsDeleteMode { get { return _IsDeleteMode; } set { SetProperty(ref _IsDeleteMode, value); } }
    }

    public partial class ClaimFormMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("draft_count")]
        public long DraftCount { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class ClaimForm
    {
        public static ClaimForm FromJson(string json) => JsonConvert.DeserializeObject<ClaimForm>(json, ConverterClaimForm.Settings);
    }

    public static class SerializeClaimForm
    {
        public static string ToJson(this ClaimForm self) => JsonConvert.SerializeObject(self, ConverterClaimForm.Settings);
    }

    public class ConverterClaimForm
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
