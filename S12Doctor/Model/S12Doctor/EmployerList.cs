﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace S12Doctor.Model.S12Doctor
{
    public partial class EmployerList
    {
        [JsonProperty("data")]
        public ObservableCollection<Employer> Data { get; set; }

        [JsonProperty("meta")]
        public EmployerListMeta Meta { get; set; }
       
        public string SelectedEmployerByName
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.OrganisationName).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        public string SelectedEmployerByID
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.OrganisationId).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }
    }

    public partial class Employer : BindableBase
    {
        [JsonProperty("organisation_id")]
        public long OrganisationId { get; set; }

        [JsonProperty("organisation_name")]
        public string OrganisationName { get; set; }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }
    }

    public partial class EmployerListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class EmployerList
    {
        public static EmployerList FromJson(string json) => JsonConvert.DeserializeObject<EmployerList>(json, ConverterEmployerList.Settings);
    }

    public static class SerializeEmployerList
    {
        public static string ToJson(this EmployerList self) => JsonConvert.SerializeObject(self, ConverterEmployerList.Settings);
    }

    public class ConverterEmployerList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
