﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.S12Doctor
{
    public partial class AssessmentLocationTypeList
    {
        [JsonProperty("data")]
        public List<AssessmentLocationType> Data { get; set; }

        [JsonProperty("meta")]
        public AssessmentLocationTypeListMeta Meta { get; set; }
    }

    public partial class AssessmentLocationType
    {
        [JsonProperty("assessment_location_type_id")]
        public long AssessmentLocationTypeId { get; set; }

        [JsonProperty("assessment_location_type_name")]
        public string AssessmentLocationTypeName { get; set; }
    }

    public partial class AssessmentLocationTypeListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class AssessmentLocationTypeList
    {
        public static AssessmentLocationTypeList FromJson(string json) => JsonConvert.DeserializeObject<AssessmentLocationTypeList>(json, ConverterAssessmentLocationTypeList.Settings);
    }

    public static class SerializeAssessmentLocationTypeList
    {
        public static string ToJson(this AssessmentLocationTypeList self) => JsonConvert.SerializeObject(self, ConverterAssessmentLocationTypeList.Settings);
    }

    public class ConverterAssessmentLocationTypeList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
