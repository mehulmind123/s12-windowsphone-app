﻿using Newtonsoft.Json;

namespace S12Doctor.Model.S12Doctor
{
    public partial class AvailabilityStatus
    {
        [JsonProperty("data")]
        public AvailabilityStatusData Data { get; set; }

        [JsonProperty("meta")]
        public AvailabilityStatusMeta Meta { get; set; }
    }

    public partial class AvailabilityStatusData
    {
        [JsonProperty("availability_message")]
        public string AvailabilityMessage { get; set; }

        [JsonProperty("availability_status")]
        public long AvailabilityStatus { get; set; }
    }

    public partial class AvailabilityStatusMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class AvailabilityStatus
    {
        public static AvailabilityStatus FromJson(string json) => JsonConvert.DeserializeObject<AvailabilityStatus>(json, ConverterAvailabilityStatus.Settings);
    }

    public static class SerializeAvailabilityStatus
    {
        public static string ToJson(this AvailabilityStatus self) => JsonConvert.SerializeObject(self, ConverterAvailabilityStatus.Settings);
    }

    public class ConverterAvailabilityStatus
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {            
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
