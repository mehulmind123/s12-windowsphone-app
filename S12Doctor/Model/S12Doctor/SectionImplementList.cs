﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.S12Doctor
{
    public partial class SectionImplementList
    {
        [JsonProperty("data")]
        public List<SectionImplement> Data { get; set; }

        [JsonProperty("meta")]
        public SectionImplementListMeta Meta { get; set; }
    }

    public partial class SectionImplement
    {
        [JsonProperty("section_implement_id")]
        public long SectionImplementId { get; set; }

        [JsonProperty("section_implement_name")]
        public string SectionImplementName { get; set; }
    }

    public partial class SectionImplementListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class SectionImplementList
    {
        public static SectionImplementList FromJson(string json) => JsonConvert.DeserializeObject<SectionImplementList>(json, ConverterSectionImplementList.Settings);
    }

    public static class SerializeSectionImplementList
    {
        public static string ToJson(this SectionImplementList self) => JsonConvert.SerializeObject(self, ConverterSectionImplementList.Settings);
    }

    public class ConverterSectionImplementList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
