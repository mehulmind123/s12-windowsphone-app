﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.S12Doctor
{
    public partial class UsefulInformationList
    {
        [JsonProperty("data")]
        public List<UsefulInformation> Data { get; set; }

        [JsonProperty("meta")]
        public UsefulInformationListMeta Meta { get; set; }
    }

    public partial class UsefulInformation
    {
        [JsonProperty("amhps")]
        public List<Amhp> Amhps { get; set; }

        [JsonProperty("contact_number")]
        public string ContactNumber { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("ccgs")]
        public List<Ccg> Ccgs { get; set; }

        [JsonProperty("local_authority")]
        public string LocalAuthority { get; set; }

        [JsonProperty("local_authority_id")]
        public long LocalAuthorityId { get; set; }
    }

    public partial class Amhp
    {
        [JsonProperty("amhp_contact_number")]
        public string AmhpContactNumber { get; set; }

        [JsonProperty("amhp_name")]
        public string AmhpName { get; set; }
    }

    public partial class Ccg
    {
        [JsonProperty("ccg_contact_number")]
        public string CcgContactNumber { get; set; }

        [JsonProperty("ccg_address")]
        public string CcgAddress { get; set; }

        [JsonProperty("ccg_name")]
        public string CcgName { get; set; }
    }

    public partial class UsefulInformationListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("draft_count")]
        public long DraftCount { get; set; }

        [JsonProperty("new_offset")]
        public long NewOffset { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class UsefulInformationList
    {
        public static UsefulInformationList FromJson(string json) => JsonConvert.DeserializeObject<UsefulInformationList>(json, ConverterUsefulInformationList.Settings);
    }

    public static class SerializeUsefulInformationList
    {
        public static string ToJson(this UsefulInformationList self) => JsonConvert.SerializeObject(self, ConverterUsefulInformationList.Settings);
    }

    public class ConverterUsefulInformationList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
