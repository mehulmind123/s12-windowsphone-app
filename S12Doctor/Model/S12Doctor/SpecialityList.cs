﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S12Doctor.Model.S12Doctor
{
    public partial class SpecialityList
    {
        [JsonProperty("data")]
        public List<Speciality> Data { get; set; }

        [JsonProperty("meta")]
        public SpecialityListMeta Meta { get; set; }


        public string SelectedSpecialityByName
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.SpecialtiesName).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        public string SelectedSpecialityByID
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.SpecialtiesId).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }
    }

    public partial class Speciality : BindableBase
    {
        [JsonProperty("specialties_id")]
        public long SpecialtiesId { get; set; }

        [JsonProperty("specialties_name")]
        public string SpecialtiesName { get; set; }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }
    }

    public partial class SpecialityListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class SpecialityList
    {
        public static SpecialityList FromJson(string json) => JsonConvert.DeserializeObject<SpecialityList>(json, ConverterSpecialityList.Settings);
    }

    public static class SerializeSpecialityList
    {
        public static string ToJson(this SpecialityList self) => JsonConvert.SerializeObject(self, ConverterSpecialityList.Settings);
    }

    public class ConverterSpecialityList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
