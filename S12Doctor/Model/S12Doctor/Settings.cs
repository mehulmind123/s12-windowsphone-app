﻿using Newtonsoft.Json;

namespace S12Doctor.Model.S12Doctor
{
    public partial class Settings
    {
        [JsonProperty("data")]
        public SettingsData Data { get; set; }

        [JsonProperty("meta")]
        public SettingsMeta Meta { get; set; }
    }

    public partial class SettingsData
    {
        [JsonProperty("gps_enable")]
        public long GpsEnable { get; set; }

        [JsonProperty("notification_reminder")]
        public long NotificationReminder { get; set; }
    }

    public partial class SettingsMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class Settings
    {
        public static Settings FromJson(string json) => JsonConvert.DeserializeObject<Settings>(json, ConverterSettings.Settings);
    }

    public static class SerializeSettings
    {
        public static string ToJson(this Settings self) => JsonConvert.SerializeObject(self, ConverterSettings.Settings);
    }

    public class ConverterSettings
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
