﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace S12Doctor.Model.S12Doctor
{
    public partial class LanguageList
    {
        [JsonProperty("data")]
        public ObservableCollection<Language> Data { get; set; }

        [JsonProperty("meta")]
        public LanguageListMeta Meta { get; set; }

        public void AddLanguage(string language)
        {
            Data.Add(new Language() { LanguageId = -1, LanguageName = language, IsChecked = true, LanguageType = "new" });
        }

        public string SelectedLanguageByName
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.LanguageName).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        public string NewLanguageByName
        {
            get
            {
                var array = this.Data
                    .Where(x => x.LanguageType == "new")
                    .Select(x => x.LanguageName).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        public string SelectedLanguageByID
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked && x.LanguageId != -1)
                    .Select(x => x.LanguageId).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }
    }

    public partial class Language : BindableBase
    {
        [JsonProperty("language_id")]
        public long LanguageId { get; set; }

        [JsonProperty("language_name")]
        public string LanguageName { get; set; }

        string _LanguageType = "old";
        public string LanguageType { get { return _LanguageType; } set { SetProperty(ref _LanguageType, value); } }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }
    }

    public partial class LanguageListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class LanguageList
    {
        public static LanguageList FromJson(string json) => JsonConvert.DeserializeObject<LanguageList>(json, ConverterLanguageList.Settings);
    }

    public static class SerializeLanguageList
    {
        public static string ToJson(this LanguageList self) => JsonConvert.SerializeObject(self, ConverterLanguageList.Settings);
    }

    public class ConverterLanguageList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
