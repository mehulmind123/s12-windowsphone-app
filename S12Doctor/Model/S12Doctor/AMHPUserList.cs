﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.S12Doctor
{
    public partial class AMHPUserList
    {
        [JsonProperty("data")]
        public List<AMHPUser> Data { get; set; }

        [JsonProperty("meta")]
        public AMHPUserListMeta Meta { get; set; }
    }

    public partial class AMHPUser
    {
        [JsonProperty("amhp_name")]
        public string AmhpName { get; set; }

        [JsonProperty("amhp_id")]
        public long AmhpId { get; set; }

        [JsonProperty("authority_id")]
        public long AuthorityId { get; set; }
    }

    public partial class AMHPUserListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class AMHPUserList
    {
        public static AMHPUserList FromJson(string json) => JsonConvert.DeserializeObject<AMHPUserList>(json, ConverterAMHPUserList.Settings);
    }

    public static class SerializeAMHPUserList
    {
        public static string ToJson(this AMHPUserList self) => JsonConvert.SerializeObject(self, ConverterAMHPUserList.Settings);
    }

    public class ConverterAMHPUserList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
