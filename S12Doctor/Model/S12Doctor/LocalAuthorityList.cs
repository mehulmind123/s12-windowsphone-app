﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace S12Doctor.Model.S12Doctor
{
    public partial class LocalAuthorityList
    {
        [JsonProperty("data")]
        public ObservableCollection<LocalAuthority> Data { get; set; }

        [JsonProperty("meta")]
        public LocalAuthorityListMeta Meta { get; set; }
       
        public string SelectedLocalAuthorityByName
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.AuthorityName).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        public string SelectedLocalAuthorityByID
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.AuthorityId).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }
    }

    public partial class LocalAuthority : BindableBase
    {
        [JsonProperty("authority_id")]
        public long AuthorityId { get; set; }

        [JsonProperty("authority_name")]
        public string AuthorityName { get; set; }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }
    }

   
    public partial class LocalAuthorityListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class LocalAuthorityList
    {
        public static LocalAuthorityList FromJson(string json) => JsonConvert.DeserializeObject<LocalAuthorityList>(json, ConverterLocalAuthorityList.Settings);
    }

    public static class SerializeLocalAuthorityList
    {
        public static string ToJson(this LocalAuthorityList self) => JsonConvert.SerializeObject(self, ConverterLocalAuthorityList.Settings);
    }

    public class ConverterLocalAuthorityList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
