﻿using System;
using S12Doctor.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace S12Doctor.Model.S12Doctor
{
    public partial class OrganisationList
    {
        [JsonProperty("data")]
        public List<Organisation> Data { get; set; }

        [JsonProperty("meta")]
        public OrganisationListMeta Meta { get; set; }

        public string SelectedOrganisationByName
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.OrganisationName).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        public string SelectedOrganisationByID
        {
            get
            {
                var array = this.Data
                    .Where(x => x.IsChecked)
                    .Select(x => x.OrganisationId).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

    }

    public partial class Organisation : BindableBase
    {
        [JsonProperty("organisation_id")]
        public long OrganisationId { get; set; }

        [JsonProperty("organisation_name")]
        public string OrganisationName { get; set; }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }

    }

    public partial class OrganisationListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class OrganisationList
    {
        public static OrganisationList FromJson(string json) => JsonConvert.DeserializeObject<OrganisationList>(json, ConverterOrganisationList.Settings);
    }

    public static class SerializeOrganisationList
    {
        public static string ToJson(this OrganisationList self) => JsonConvert.SerializeObject(self, ConverterOrganisationList.Settings);
    }

    public class ConverterOrganisationList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
