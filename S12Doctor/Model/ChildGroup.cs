﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace S12Doctor.Model
{
    public class ChildGroup : ObservableCollection<SourceData>
    {
        public ChildGroup(IEnumerable<SourceData> items)
            : base(items)
        {
        }

        public string GroupHeader { get; set; }
        public bool HeaderVisibility { get; set; }
    }
}
