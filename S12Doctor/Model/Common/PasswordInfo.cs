﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S12Doctor.Model.Common
{
    public partial class MetaInfo
    {
        [JsonProperty("data")]
        public MetaData Data { get; set; }

        [JsonProperty("meta")]
        public MetaMeta Meta { get; set; }
    }

    public partial class MetaData
    {
    }

    public partial class MetaMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("status_code")]
        public long StatusCode { get; set; }
    }

    public partial class MetaInfo
    {
        public static MetaInfo FromJson(string json) => JsonConvert.DeserializeObject<MetaInfo>(json, ConverterMetaInfo.Settings);
    }

    public partial class MetaMeta
    {
        public static MetaMeta FromJson(string json) => JsonConvert.DeserializeObject<MetaMeta>(json, ConverterMetaInfo.Settings);
    }

    public static class SerializeMetaInfo
    {
        public static string ToJson(this MetaInfo self) => JsonConvert.SerializeObject(self, ConverterMetaInfo.Settings);
    }

    public class ConverterMetaInfo
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
