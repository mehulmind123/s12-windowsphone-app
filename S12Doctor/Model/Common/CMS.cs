﻿using Newtonsoft.Json;

namespace S12Doctor.Model.Common
{
    public partial class CMS
    {
        [JsonProperty("data")]
        public CMSData Data { get; set; }

        [JsonProperty("meta")]
        public CMSMeta Meta { get; set; }
    }

    public partial class CMSData
    {
        [JsonProperty("privacy_content")]
        public string PrivacyContent { get; set; }

        [JsonProperty("about_us_content")]
        public string AboutUsContent { get; set; }

        [JsonProperty("terms_and_conditions_content")]
        public string TermsAndConditionsContent { get; set; }
    }

    public partial class CMSMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class CMS
    {
        public static CMS FromJson(string json) => JsonConvert.DeserializeObject<CMS>(json, ConverterCMS.Settings);
    }

    public static class SerializeCMS
    {
        public static string ToJson(this CMS self) => JsonConvert.SerializeObject(self, ConverterCMS.Settings);
    }

    public class ConverterCMS
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
