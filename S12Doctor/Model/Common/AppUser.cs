﻿using S12Doctor.Model.S12Doctor;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace S12Doctor.Model.Common
{
    public partial class AppUser
    {
        [JsonProperty("data")]
        public UserData Data { get; set; }

        [JsonProperty("meta")]
        public UserMeta Meta { get; set; }
    }
    public partial class UserData
    {
        [JsonProperty("default_claim_address")]
        public string DefaultClaimAddress { get; set; }

        [JsonProperty("gps_enable")]
        public long GpsEnable { get; set; }

        [JsonProperty("region_name")]
        public string RegionName { get; set; }

        [JsonProperty("region_id")]
        public long RegionId { get; set; }

        [JsonProperty("car_make")]
        public string CarMake { get; set; }

        [JsonProperty("availability_message")]
        public string AvailabilityMessage { get; set; }

        [JsonProperty("accept_terms_condition")]
        public long AcceptTermsCondition { get; set; }

        [JsonProperty("availability_status")]
        public long AvailabilityStatus { get; set; }

        [JsonProperty("car_registration_plate")]
        public string CarRegistrationPlate { get; set; }

        [JsonProperty("car_model")]
        public string CarModel { get; set; }

        [JsonProperty("change_password")]
        public long ChangePassword { get; set; }

        [JsonProperty("engine_size")]
        public string EngineSize { get; set; }

        [JsonProperty("employer_id")]
        public long EmployerId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("employer_name")]
        public string EmployerName { get; set; }

        [JsonProperty("general_availability")]
        public string GeneralAvailability { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("gmc_referance_number")]
        public string GmcReferanceNumber { get; set; }

        [JsonProperty("login_type")]
        public long LoginType { get; set; }

        [JsonProperty("office_base_postcode")]
        public string OfficeBasePostcode { get; set; }

        [JsonProperty("language_spoken")]
        public List<S12Doctor.Language> LanguageSpoken { get; set; }

        [JsonProperty("landline_number")]
        public string LandlineNumber { get; set; }

        [JsonProperty("employer")]
        public List<S12Doctor.Employer> Employer { get; set; }

        public string SelectedEmployerById
        {
            get
            {
                var array = this.Employer
                    .Select(x => x.OrganisationId).ToArray();
                if (!array.Any())
                    return "";
                return string.Join(", ", array);
            }
        }

        public string SelectedEmployerByName
        {
            get
            {
                var array = this.Employer
                    .Select(x => x.OrganisationName).ToArray();
                if (!array.Any())
                    return "N/A";
                return string.Join(", ", array);
            }
        }

        [JsonProperty("local_authority")]
        public List<S12Doctor.LocalAuthority> LocalAuthority { get; set; }

        public string SelectedLocalAuthorityById
        {
            get
            {
                var array = this.LocalAuthority
                    .Select(x => x.AuthorityId).ToArray();
                if (!array.Any())
                    return "";
                return string.Join(", ", array);
            }
        }

        public string SelectedLocalAuthorityByName
        {
            get
            {
                var array = this.LocalAuthority
                    .Select(x => x.AuthorityName).ToArray();
                if (!array.Any())
                    return "N/A";
                return string.Join(", ", array);
            }
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("mobile_number")]
        public string MobileNumber { get; set; }

        [JsonProperty("notification_reminder")]
        public long NotificationReminder { get; set; }

        [JsonProperty("specialties")]
        public List<Speciality> Specialties { get; set; }

        [JsonProperty("office_base_team")]
        public string OfficeBaseTeam { get; set; }

        [JsonProperty("user_id")]
        public long UserId { get; set; }
    }
    public partial class UserMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("draft_count")]
        public long DraftCount { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }
    }
    public partial class AppUser
    {
        public static AppUser FromJson(string json) => JsonConvert.DeserializeObject<AppUser>(json, ConverterAppUser.Settings);
    }
    public static class SerializeAppUser
    {
        public static string ToJson(this AppUser self) => JsonConvert.SerializeObject(self, ConverterAppUser.Settings);
    }
    public class ConverterAppUser
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }

}
