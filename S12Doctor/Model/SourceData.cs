﻿using S12Doctor.Model.S12Doctor;
using System.Collections.Generic;

namespace S12Doctor.Model
{
    public class Details
    {
        public string Name { get; set; }

        public bool NameVisibility { get; set; }

        public string Address { get; set; }

        public string AddressImagePath { get; set; }

        public bool AddressVisibility { get; set; }

        public string Contact { get; set; }

        public string ContactImagePath { get; set; }

        public string Information { get; set; }
    }

    public class SourceData
    {
        public string GroupID { get; set; }

        public string GroupHeader { get; set; }

        public bool HeaderVisibility { get; set; }

        public int GroupHeaderHight { get; set; }

        public Details GroupData { get; set; }
        public static object UIApplication { get; private set; }

        public static IEnumerable<SourceData> GetData(UsefulInformationList usefulInformationList)
        {
            List<SourceData> sourceData = new List<Model.SourceData>();

            foreach (var item in usefulInformationList.Data)
            {
                sourceData.Add(new SourceData
                {
                    GroupID = item.LocalAuthority,
                    HeaderVisibility = false,
                    GroupData = new Details
                    {
                        AddressVisibility = true,
                        NameVisibility = false,
                        Address = string.IsNullOrEmpty(item.Address) ? "N/A" : item.Address,
                        Contact = string.IsNullOrEmpty(item.ContactNumber) ? "N/A" : item.ContactNumber,
                        AddressImagePath = "ms-appx:///Assets/SideMenu/address.png",
                        ContactImagePath = "ms-appx:///Assets/SideMenu/c_number.png"
                    }
                });

                foreach (var amhp in item.Amhps)
                {
                    sourceData.Add(new SourceData
                    {
                        GroupID = item.LocalAuthority,
                        HeaderVisibility = true,
                        GroupHeader = "AMHPs",
                        GroupData = new Details
                        {
                            AddressVisibility = false,
                            NameVisibility = true,
                            Name = string.IsNullOrEmpty(amhp.AmhpName) ? "N/A" : amhp.AmhpName,
                            Address = "",
                            Contact = string.IsNullOrEmpty(amhp.AmhpContactNumber) ? "N/A" : amhp.AmhpContactNumber
                        }
                    });
                }

                foreach (var ccg in item.Ccgs)
                {
                    sourceData.Add(new SourceData
                    {
                        GroupID = item.LocalAuthority,
                        HeaderVisibility = true,
                        GroupHeader = "CCGs",
                        GroupData = new Details
                        {
                            AddressVisibility = true,
                            NameVisibility = true,
                            Name = string.IsNullOrEmpty(ccg.CcgName) ? "N/A" : ccg.CcgName,
                            Address = string.IsNullOrEmpty(ccg.CcgAddress) ? "N/A" : ccg.CcgAddress,
                            Contact = string.IsNullOrEmpty(ccg.CcgContactNumber) ? "N/A" : ccg.CcgContactNumber
                        }
                    });
                }
            }

            //var handMadeData = new[]
            //{
            //    new SourceData { GroupID = "David Wilson", HeaderVisibility = false, GroupData = new Details { AddressVisibility = true, NameVisibility = false, Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898", AddressImagePath="ms-appx:///Assets/SideMenu/address.png",  ContactImagePath = "ms-appx:///Assets/SideMenu/c_number.png" } },
            //    new SourceData { GroupID = "David Wilson", HeaderVisibility = true, GroupHeader = "AMHPs", GroupData = new Details { AddressVisibility = false, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Address = "UK", Contact="9898989898" } },
            //    new SourceData { GroupID = "David Wilson", HeaderVisibility = true, GroupHeader = "AMHPs", GroupData = new Details { AddressVisibility = false, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Address = "UK", Contact="9898989898" } },
            //    new SourceData { GroupID = "David Wilson", HeaderVisibility = true, GroupHeader = "CCGs", GroupData = new Details { AddressVisibility = true, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898" }  },
            //    new SourceData { GroupID = "David Wilson", HeaderVisibility = true, GroupHeader = "CCGs", GroupData = new Details { AddressVisibility = true, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898" }  },


            //    new SourceData { GroupID = "David Wilson 1", HeaderVisibility = false, GroupData = new Details { AddressVisibility = true, NameVisibility = false, Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898", AddressImagePath="ms-appx:///Assets/SideMenu/address.png",  ContactImagePath = "ms-appx:///Assets/SideMenu/c_number.png" } },
            //    new SourceData { GroupID = "David Wilson 1", HeaderVisibility = true, GroupHeader = "AMHPs", GroupData = new Details { AddressVisibility = false, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Contact="9898989898" } },
            //    new SourceData { GroupID = "David Wilson 1", HeaderVisibility = true, GroupHeader = "AMHPs", GroupData = new Details { AddressVisibility = false, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Contact="9898989898" } },
            //    new SourceData { GroupID = "David Wilson 1", HeaderVisibility = true, GroupHeader = "CCGs", GroupData = new Details { AddressVisibility = true, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898" }  },
            //    new SourceData { GroupID = "David Wilson 1", HeaderVisibility = true, GroupHeader = "CCGs", GroupData = new Details { AddressVisibility = true, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898" }  },


            //    new SourceData { GroupID = "David Wilson 2", HeaderVisibility = false, GroupData = new Details { AddressVisibility = true, NameVisibility = false, Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898", AddressImagePath="ms-appx:///Assets/SideMenu/address.png",  ContactImagePath = "ms-appx:///Assets/SideMenu/c_number.png" } },
            //    new SourceData { GroupID = "David Wilson 2", HeaderVisibility = true, GroupHeader = "AMHPs", GroupData = new Details { AddressVisibility = false, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Contact="9898989898" } },
            //    new SourceData { GroupID = "David Wilson 2", HeaderVisibility = true, GroupHeader = "AMHPs", GroupData = new Details { AddressVisibility = false, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Contact="9898989898" } },
            //    new SourceData { GroupID = "David Wilson 2", HeaderVisibility = true, GroupHeader = "CCGs", GroupData = new Details { AddressVisibility = true, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898" }  },
            //    new SourceData { GroupID = "David Wilson 2", HeaderVisibility = true, GroupHeader = "CCGs", GroupData = new Details { AddressVisibility = true, NameVisibility = true, Name ="NHS Airedale, Wharfedale and Creaven CCG", Address = "UK HealthCare 800 Rose Street, Room C101 Lexington KY 40536", Contact="9898989898" }  },

            //};

            return sourceData;
        }


        public static IEnumerable<SourceData> GetUseFullInfoData()
        {
            var handMadeData = new[]
            {
                new SourceData { GroupID = "David Wilson", GroupData = new Details { Information = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printe to a gallery of type and scrembled it to make a type specimen book. it has survived not only five centuries, but also the leap into electronic typesetting in the 1960s with the release of Letraset with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." } },

                new SourceData { GroupID = "Marry Rose", GroupData = new Details { Information = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printe to a gallery of type and scrembled it to make a type specimen book. it has survived not only five centuries, but also the leap into electronic typesetting in the 1960s with the release of Letraset with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." } },

                new SourceData { GroupID = "Willem Zohn", GroupData = new Details { Information = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printe to a gallery of type and scrembled it to make a type specimen book. it has survived not only five centuries, but also the leap into electronic typesetting in the 1960s with the release of Letraset with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." } },
            };

            return handMadeData;
        }
    }
}
