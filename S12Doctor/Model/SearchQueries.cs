﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WPGoogleApi;
using WPGoogleApi.Entities.Maps.Geocode.Request;
using WPGoogleApi.Entities.Maps.Geocode.Response;
using WPGoogleApi.Entities.Maps.Geolocation.Request;
using WPGoogleApi.Entities.Maps.Geolocation.Response;
using WPGoogleApi.Entities.Places.AutoComplete.Request;

namespace S12Doctor.Model
{
    public class SearchQueries
    {
        public string PlacesApiKey { get; private set; } = "AIzaSyADs3FT11SVsmASrgd6BfKENio8wzFKc_M";
        public string LocationApiKey { get; private set; } = "AIzaSyBauqxotjsQ9yUWG5eDSklr7ubzmGfkehM";

        public async Task<IEnumerable<Continents>> GetMatchingContinents(string query)
        {
            List<Continents> list = new List<Continents>();

            if (!string.IsNullOrEmpty(query))
            {
                var request = new PlacesAutoCompleteRequest
                {
                    Key = PlacesApiKey,
                    Input = query
                };

                try
                {

                    var response = await GooglePlaces.AutoComplete.QueryAsync(request);

                    if (response.Predictions.Count() > 0)
                    {
                        foreach (var item in response.Predictions.ToList())
                        {
                            list.Add(new Continents { name = item.Description });
                        }
                    }

                }
                catch (ArgumentException aex)
                { }
                catch (Exception ex)
                { }
            }

            return list;
        }

        public async Task<GeolocationResponse> GetCurrentLocation()
        {

            var locationRequest = new GeolocationRequest
            {
                Key = LocationApiKey,
            };


            GeolocationResponse locationResponse = await GoogleMaps.Geolocation.QueryAsync(locationRequest);

            //return String.Format("lat : {0} -> long : {1} -> address : {2}", a.Location.Latitude, a.Location.Longitude, a.Location.Address);
            return locationResponse;
        }

        public async Task<GeocodingResponse> GetLatLong(string query)
        {
            var geocodeRequest = new GeocodingRequest
            {
                Key = LocationApiKey,
                Address = query,
            };

            GeocodingResponse geocodingResponse = await GoogleMaps.Geocode.QueryAsync(geocodeRequest);

            return geocodingResponse;
        }
    }

    public class Continents
    {
        public string name;

        public override string ToString()
        {
            return string.Format("{0}", name);
        }
    }
}
