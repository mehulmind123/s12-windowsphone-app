﻿using S12Doctor.Base;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace S12Doctor.Model
{
    public class LocalAuthorities : BindableBase
    {
        public LocalAuthorities()
        {
            _Items = new ObservableCollection<LocalAuthority>()
            {
               new LocalAuthority() { Text = "LA12", IsChecked = false },
               new LocalAuthority() { Text = "LA11", IsChecked = false },
               new LocalAuthority() { Text = "Devid Wilson", IsChecked = false }
            };

            //foreach (var item in this.Items)
            //    item.PropertyChanged += (s, e) => base.RaisePropertyChanged("SelectedLocalAuthorities");
        }

        public string SelectedLocalAuthority
        {
            get
            {
                var array = this.Items
                    .Where(x => x.IsChecked)
                    .Select(x => x.Text).ToArray();
                if (!array.Any())
                    return String.Empty;
                return string.Join(", ", array);
            }
        }

        ObservableCollection<LocalAuthority> _Items;
        public ObservableCollection<LocalAuthority> Items { get { return _Items; } }
    }

    public class LocalAuthority : BindableBase
    {
        public string Text { get; set; }

        bool _IsChecked = default(bool);
        public bool IsChecked { get { return _IsChecked; } set { SetProperty(ref _IsChecked, value); } }
    }
}
