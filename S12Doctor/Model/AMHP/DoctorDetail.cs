﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System.Collections.Generic;
using System.Linq;

namespace S12Doctor.Model.AMHP
{
    public partial class DoctorDetail
    {
        [JsonProperty("data")]
        public DoctorDetailData Data { get; set; }

        [JsonProperty("meta")]
        public DoctorDetailMeta Meta { get; set; }
    }
    public partial class Organisation
    {
        [JsonProperty("organisation_id")]
        public long OrganisationId { get; set; }

        [JsonProperty("organisation_name")]
        public string OrganisationName { get; set; }
        
    }

    public partial class Employer
    {
        [JsonProperty("employer_id")]
        public long EmployerId { get; set; }

        [JsonProperty("employer_name")]
        public string EmployerName { get; set; }

    }

    public partial class DoctorDetailData : BindableBase
    {
        [JsonProperty("general_availability")]
        private string _GeneralAvailability;
        public string GeneralAvailability
        {
            get { return string.IsNullOrEmpty(_GeneralAvailability) ? "N/A" : _GeneralAvailability; }
            set { SetProperty(ref _GeneralAvailability, value); }
        }

        [JsonProperty("doctor_name")]
        private string _DoctorName;
        public string DoctorName
        {
            get { return string.IsNullOrEmpty(_DoctorName) ? "N/A" : _DoctorName; }
            set { SetProperty(ref _DoctorName, value); }
        }

        long? _AvailabilityStatus;
        [JsonProperty("availability_status")]
        public long? AvailabilityStatus
        {
            get { return _AvailabilityStatus; }
            set
            {
                SetProperty(ref _AvailabilityStatus, value);
                if (value == 1)
                    _StatusText = "Available";
                else if (value == 2)
                    _StatusText = "May be available";
                else if (value == 3)
                    _StatusText = "Unavailable";
                else
                    _StatusText = "Unknown";
            }
        }

        string _StatusText;
        public string StatusText { get { return _StatusText; } set { SetProperty(ref _StatusText, value); } }

        [JsonProperty("availability_message")]
        private string _AvailabilityMessage;
        public string AvailabilityMessage
        {
            get { return string.IsNullOrEmpty(_AvailabilityMessage) ? "N/A" : _AvailabilityMessage; }
            set { SetProperty(ref _AvailabilityMessage, value); }
        }

        [JsonProperty("doctor_id")]
        public long DoctorId { get; set; }

        //[JsonProperty("employer")]
        //private string _Employer;
        //public string Employer
        //{
        //    get { return string.IsNullOrEmpty(_Employer) ? "N/A" : _Employer; }
        //    set { SetProperty(ref _Employer, value); }
        //}

        [JsonProperty("email")]
        private string _Email;
        public string Email
        {
            get { return string.IsNullOrEmpty(_Email) ? "N/A" : _Email; }
            set { SetProperty(ref _Email, value); }
        }

        [JsonProperty("gender")]
        public long _Gender { get; set; }
        public string Gender
        {
            get { return _Gender.ToString() =="1" ? "Prefer no type" : _Gender.ToString() == "2" ? "Male" : "Female"; }
        }
        [JsonProperty("employer")]
        public List<Employer> employerData { get; set; }

        public string EmployersText
        {
            get
            {
                var array = this.employerData
                    .Select(x => x.EmployerName).ToArray();
                if (!array.Any())
                    return "N/A";
                return string.Join(", ", array);
            }
        }


        [JsonProperty("local_authorities")]
        public List<LocalAuthority> LocalAuthorities { get; set; }

        public string LocalAuthoritiesText
        {
            get
            {
                var array = this.LocalAuthorities
                    .Select(x => x.AuthorityName).ToArray();
                if (!array.Any())
                    return "N/A";
                return string.Join(", ", array);
            }
        }

        [JsonProperty("landline_number")]
        private string _LandlineNumber;
        public string LandlineNumber
        {
            get { return string.IsNullOrEmpty(_LandlineNumber) ? "N/A" : _LandlineNumber; }
            set { SetProperty(ref _LandlineNumber, value); }
        }

        [JsonProperty("gmc_referance_number")]
        private string _GmcReferanceNumber;
        public string GmcReferanceNumber
        {
            get { return string.IsNullOrEmpty(_GmcReferanceNumber) ? "N/A" : _GmcReferanceNumber; }
            set { SetProperty(ref _GmcReferanceNumber, value); }
        }

        [JsonProperty("languages_spoken")]
        public List<LanguagesSpoken> LanguagesSpoken { get; set; }

        public string LanguagesSpokenText
        {
            get
            {
                var array = this.LanguagesSpoken
                    .Select(x => x.LanguageName).ToArray();
                if (!array.Any())
                    return "N/A";
                return string.Join(", ", array);
            }
        }

        [JsonProperty("office_base_postcode")]
        private string _OfficeBasePostcode;
        public string OfficeBasePostcode
        {
            get { return string.IsNullOrEmpty(_OfficeBasePostcode) ? "N/A" : _OfficeBasePostcode; }
            set { SetProperty(ref _OfficeBasePostcode, value); }
        }

        [JsonProperty("specialties")]
        public List<Specialty> Specialties { get; set; }

        public string SpecialtiesText
        {
            get
            {
                var array = this.Specialties
                    .Select(x => x.SpecialtiesName).ToArray();
                if (!array.Any())
                    return "N/A";
                return string.Join(", ", array);
            }
        }

        [JsonProperty("mobile_number")]
        private string _MobileNumber;
        public string MobileNumber
        {
            get { return string.IsNullOrEmpty(_MobileNumber) ? "N/A" : _MobileNumber; }
            set { SetProperty(ref _MobileNumber, value); }
        }

        [JsonProperty("office_base_team")]
        private string _OfficeBaseTeam;
        public string OfficeBaseTeam
        {
            get { return string.IsNullOrEmpty(_OfficeBaseTeam) ? "N/A" : _OfficeBaseTeam; }
            set { SetProperty(ref _OfficeBaseTeam, value); }
        }

        [JsonProperty("updated_status_timestamp")]
        public string UpdatedStatusTimestamp { get; set; }

        [JsonProperty("weekly_availability_status")]
        public WeeklyAvailabilityStatusinfo WeeklyAvailabilityStatus { get; set; }
    }

    public partial class LanguagesSpoken
    {
        [JsonProperty("language_id")]
        public long LanguageId { get; set; }

        [JsonProperty("language_name")]
        public string LanguageName { get; set; }
    }

    public partial class Specialty
    {
        [JsonProperty("specialties_id")]
        public long SpecialtiesId { get; set; }

        [JsonProperty("specialties_name")]
        public string SpecialtiesName { get; set; }
    }

    public partial class WeeklyAvailabilityStatusinfo
    {
        [JsonProperty("am")]
        public string am { get; set; }

        [JsonProperty("lunch")]
        public string lunch { get; set; }

        [JsonProperty("pm")]
        public string pm { get; set; }

        [JsonProperty("evening")]
        public string evening { get; set; }

        [JsonProperty("night")]
        public string night { get; set; }
    }

    public partial class DoctorDetailMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class DoctorDetail
    {
        public static DoctorDetail FromJson(string json) => JsonConvert.DeserializeObject<DoctorDetail>(json, ConverterDoctorDetail.Settings);
    }

    public static class SerializeDoctorDetail
    {
        public static string ToJson(this DoctorDetail self) => JsonConvert.SerializeObject(self, ConverterDoctorDetail.Settings);
    }

    public class ConverterDoctorDetail
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
