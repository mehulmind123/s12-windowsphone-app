﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S12Doctor.Model.AMHP
{
    public partial class AddressBookDoctorList
    {
        [JsonProperty("data")]
        public List<AddressBookDoctorData> Data { get; set; }

        [JsonProperty("meta")]
        public AddressBookDoctorMeta Meta { get; set; }
    }

    public partial class AddressBookDoctorData : BindableBase
    {
        long _FavouriteStatus;
        [JsonProperty("favourite_status")]
        public long FavouriteStatus
        {
            get { return _FavouriteStatus; }
            set
            {
                SetProperty(ref _FavouriteStatus, value);
                if (value == 1)
                    _IsFavourite = true;
                else
                    _IsFavourite = false;
            }
        }

        bool _IsMilesVisible = false;
        public bool IsMilesVisible { get { return _IsMilesVisible; } set { SetProperty(ref _IsMilesVisible, value); } }

        bool _IsFavourite;
        public bool IsFavourite { get { return _IsFavourite; } set { SetProperty(ref _IsFavourite, value); } }

        [JsonProperty("doctor_id")]
        public long DoctorId { get; set; }

        long _AvailabilityStatus;
        [JsonProperty("availability_status")]
        public long AvailabilityStatus
        {
            get { return _AvailabilityStatus; }
            set
            {
                SetProperty(ref _AvailabilityStatus, value);
                if (value == 1)
                    _StatusText = "Available";
                else if (value == 2)
                    _StatusText = "May be available";
                else if (value == 3)
                    _StatusText = "Unavailable";
                else
                    _StatusText = "Unknown";
            }
        }

        string _StatusText;
        public string StatusText { get { return _StatusText; } set { SetProperty(ref _StatusText, value); } }

        string _AvailabilityMessage;
        [JsonProperty("availability_message")]
        public string AvailabilityMessage
        {
            get { return string.IsNullOrEmpty(_AvailabilityMessage) ? "N/A" : _AvailabilityMessage; }
            set { SetProperty(ref _AvailabilityMessage, value); }
        }

        [JsonProperty("distance")]
        public string Distance { get; set; }

        public string Miles { get { return String.Format("{0,2:N1} mi", Distance); } }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("doctor_name")]
        public string DoctorName { get; set; }

        [JsonProperty("employer_name")]
        public string EmployerName { get; set; }

        [JsonProperty("mobile_number")]
        public string MobileNumber { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("landline_number")]
        public string LandlineNumber { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("new_organisation_id")]
        public long NewOrganisationId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        string _UpdatedStatusTimestamp;
        [JsonProperty("updated_status_timestamp")]
        public string UpdatedStatusTimestamp { get { return _StatusText == "Unknown" ? string.Empty : _UpdatedStatusTimestamp; } set { SetProperty(ref _UpdatedStatusTimestamp, value); } }
    }

    public partial class AddressBookDoctorMeta
    {
        [JsonProperty("new_offset")]
        public long NewOffset { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class AddressBookDoctorList
    {
        public static AddressBookDoctorList FromJson(string json) => JsonConvert.DeserializeObject<AddressBookDoctorList>(json, ConverterAddressBookDoctorList.Settings);
    }

    public static class SerializeAddressBookDoctorList
    {
        public static string ToJson(this AddressBookDoctorList self) => JsonConvert.SerializeObject(self, ConverterAddressBookDoctorList.Settings);
    }

    public class ConverterAddressBookDoctorList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
