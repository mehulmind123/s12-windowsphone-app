﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.AMHP
{
    public partial class LocalAuthorityList
    {
        [JsonProperty("data")]
        public List<LocalAuthority> Data { get; set; }

        [JsonProperty("meta")]
        public LocalAuthorityListMeta Meta { get; set; }
    }

    public partial class LocalAuthority
    {
        [JsonProperty("authority_id")]
        public long AuthorityId { get; set; }

        [JsonProperty("authority_name")]
        public string AuthorityName { get; set; }
    }

    public partial class LocalAuthorityListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class LocalAuthorityList
    {
        public static LocalAuthorityList FromJson(string json) => JsonConvert.DeserializeObject<LocalAuthorityList>(json, ConverterLocalAuthorityList.Settings);
    }

    public static class SerializeLocalAuthorityList
    {
        public static string ToJson(this LocalAuthorityList self) => JsonConvert.SerializeObject(self, ConverterLocalAuthorityList.Settings);
    }

    public class ConverterLocalAuthorityList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
