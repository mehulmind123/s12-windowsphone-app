﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.AMHP
{
    public partial class UsefulInformationList
    {
        [JsonProperty("data")]
        public List<UsefulInformation> Data { get; set; }

        [JsonProperty("meta")]
        public UsefulInformationListMeta Meta { get; set; }
    }

    public partial class UsefulInformation
    {
        [JsonProperty("local_authority")]
        public string LocalAuthority { get; set; }

        [JsonProperty("useful_info")]
        public string UsefulInfo { get; set; }
    }

    public partial class UsefulInformationListMeta
    {
        [JsonProperty("new_offset")]
        public long NewOffset { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class UsefulInformationList
    {
        public static UsefulInformationList FromJson(string json) => JsonConvert.DeserializeObject<UsefulInformationList>(json, ConverterUsefulInformationList.Settings);
    }

    public static class SerializeUsefulInformationList
    {
        public static string ToJson(this UsefulInformationList self) => JsonConvert.SerializeObject(self, ConverterUsefulInformationList.Settings);
    }

    public class ConverterUsefulInformationList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
