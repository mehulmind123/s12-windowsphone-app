﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.AMHP
{
    public partial class DoctorsList
    {
        [JsonProperty("data")]
        public List<DoctorDetailData> Data { get; set; }

        [JsonProperty("meta")]
        public DoctorsListMeta Meta { get; set; }
    }

    public partial class DoctorsListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("new_offset")]
        public long NewOffset { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class DoctorsList
    {
        public static DoctorsList FromJson(string json) => JsonConvert.DeserializeObject<DoctorsList>(json, ConverterDoctorsList.Settings);
    }

    public static class SerializeDoctorsList
    {
        public static string ToJson(this DoctorDetail self) => JsonConvert.SerializeObject(self, ConverterDoctorsList.Settings);
    }

    public class ConverterDoctorsList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
