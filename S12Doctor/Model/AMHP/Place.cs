﻿namespace S12Doctor.Model.AMHP
{
    public class Place
    {

        public Place(string pName)
        {
            PlaceName = pName;
        }

        public string PlaceName { get; set; }
    }
}
