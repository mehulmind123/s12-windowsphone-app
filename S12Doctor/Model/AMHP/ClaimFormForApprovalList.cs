﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S12Doctor.Model.AMHP
{
    public partial class ClaimFormForApprovalList
    {
        [JsonProperty("data")]
        public List<ClaimFormForApprovalData> Data { get; set; }

        [JsonProperty("meta")]
        public ClaimFormForApprovalMeta Meta { get; set; }
    }

    public partial class ClaimFormForApprovalData : BindableBase
    {
        [JsonProperty("claim_id")]
        public long ClaimId { get; set; }

        [JsonProperty("assessment_location_type")]
        public string AssessmentLocationType { get; set; }

        [JsonProperty("assessment_date_time")]
        private string _AssessmentDateTime;
        public string AssessmentDateTime {
            get
            {
                try
                {
                    return Convert.ToDateTime(_AssessmentDateTime).ToString("yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    return string.IsNullOrEmpty(_AssessmentDateTime) ? "N/A" : _AssessmentDateTime; ;
                }
            }
            set { SetProperty(ref _AssessmentDateTime, value); }
        }

        [JsonProperty("claim_creation_timestamp")]
        public string ClaimCreationTimestamp { get; set; }

        [JsonProperty("doctor_name")]
        public string DoctorName { get; set; }

        [JsonProperty("doctor_address")]
        public string DoctorAddress { get; set; }

        [JsonProperty("sync_id")]
        public long SyncId { get; set; }
    }

    public partial class ClaimFormForApprovalMeta
    {
        [JsonProperty("new_offset")]
        public long NewOffset { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class ClaimFormForApprovalList
    {
        public static ClaimFormForApprovalList FromJson(string json) => JsonConvert.DeserializeObject<ClaimFormForApprovalList>(json, ConverterClaimFormForApproval.Settings);
    }

    public static class SerializeClaimFormForApproval
    {
        public static string ToJson(this ClaimFormForApprovalList self) => JsonConvert.SerializeObject(self, ConverterClaimFormForApproval.Settings);
    }

    public class ConverterClaimFormForApproval
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
