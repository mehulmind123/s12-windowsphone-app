﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S12Doctor.Model.AMHP
{
    public partial class FavouriteDoctorList
    {
        [JsonProperty("data")]
        public List<FavouriteDoctorData> Data { get; set; }

        [JsonProperty("meta")]
        public FavouriteDoctorMeta Meta { get; set; }
    }

    public partial class FavouriteDoctorData : BindableBase
    {
        [JsonProperty("doctor_id")]
        public long DoctorId { get; set; }
        
        long _IsFavourite;
        [JsonProperty("is_favourite")]
        public long IsFavourite
        {
            get { return _IsFavourite; }
            set
            {
                SetProperty(ref _IsFavourite, value);
                if (value == 1)
                    _IsDoctorFavourite = true;
                else
                    _IsDoctorFavourite = false;
            }
        }

        long _AvailabilityStatus;
        [JsonProperty("availability_status")]
        public long AvailabilityStatus
        {
            get { return _AvailabilityStatus; }
            set
            {
                SetProperty(ref _AvailabilityStatus, value);
                if (value == 1)
                    _StatusText = "Available";
                else if (value == 2)
                    _StatusText = "May be available";
                else if (value == 3)
                    _StatusText = "Unavailable";
                else
                    _StatusText = "Unknown";
            }
        }

        string _StatusText;
        public string StatusText { get { return _StatusText; } set { SetProperty(ref _StatusText, value); } }

        [JsonProperty("doctor_name")]
        public string DoctorName { get; set; }
        
        string _UpdatedStatusTimestamp;
        [JsonProperty("updated_status_timestamp")]
        public string UpdatedStatusTimestamp { get { return _StatusText == "Unknown" ? string.Empty : _UpdatedStatusTimestamp; } set { SetProperty(ref _UpdatedStatusTimestamp, value); } }

        bool _IsMilesVisible = false;
        public bool IsMilesVisible { get { return _IsMilesVisible; } set { SetProperty(ref _IsMilesVisible, value); } }

        bool _IsDoctorFavourite;
        public bool IsDoctorFavourite { get { return _IsDoctorFavourite; } set { SetProperty(ref _IsDoctorFavourite, value); } }
    }

    public partial class FavouriteDoctorMeta
    {
        [JsonProperty("new_offset")]
        public long NewOffset { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class FavouriteDoctorList
    {
        public static FavouriteDoctorList FromJson(string json) => JsonConvert.DeserializeObject<FavouriteDoctorList>(json, FavouriteDoctorListConverter.Settings);
    }
    
    public static class FavouriteDoctorListSerialize
    {
        public static string ToJson(this FavouriteDoctorList self) => JsonConvert.SerializeObject(self, FavouriteDoctorListConverter.Settings);
    }

    public class FavouriteDoctorListConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
