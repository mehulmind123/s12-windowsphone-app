﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace S12Doctor.Model.AMHP
{
    public partial class CCGList
    {
        [JsonProperty("data")]
        public List<CCG> Data { get; set; }

        [JsonProperty("meta")]
        public CCGListMeta Meta { get; set; }
    }

    public partial class CCG
    {
        [JsonProperty("organisation_id")]
        public long OrganisationId { get; set; }

        [JsonProperty("organisation_name")]
        public string OrganisationName { get; set; }
    }

    public partial class CCGListMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class CCGList
    {
        public static CCGList FromJson(string json) => JsonConvert.DeserializeObject<CCGList>(json, ConverterCCGList.Settings);
    }

    public static class SerializeCCGList
    {
        public static string ToJson(this CCGList self) => JsonConvert.SerializeObject(self, ConverterCCGList.Settings);
    }

    public class ConverterCCGList
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
