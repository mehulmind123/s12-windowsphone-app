﻿using Newtonsoft.Json;
using S12Doctor.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S12Doctor.Model.AMHP
{
    public partial class ClaimForm
    {
        [JsonProperty("data")]
        public ClaimFormData Data { get; set; }

        [JsonProperty("meta")]
        public ClaimFormMeta Meta { get; set; }
    }

    public partial class ClaimFormData : BindableBase
    {
        [JsonProperty("claim_id")]
        public long ClaimId { get; set; }

        [JsonProperty("assessment_location_type_description")]
        private string _AssessmentLocationTypeDescription;
        public string AssessmentLocationTypeDescription
        {
            get { return string.IsNullOrEmpty(_AssessmentLocationTypeDescription) ? "N/A" : _AssessmentLocationTypeDescription; }
            set { SetProperty(ref _AssessmentLocationTypeDescription, value); }
        }

        [JsonProperty("amhp_name_for_display")]
        private string _AmhpNameForDisplay;
        public string AmhpNameForDisplay
        {
            get { return string.IsNullOrEmpty(_AmhpNameForDisplay) ? "N/A" : _AmhpNameForDisplay; }
            set { SetProperty(ref _AmhpNameForDisplay, value); }
        }

        [JsonProperty("amhp_id")]
        public long AmhpId { get; set; }

        [JsonProperty("additional_information")]
        private string _AdditionalInformation;
        public string AdditionalInformation
        {
            get { return string.IsNullOrEmpty(_AdditionalInformation) ? "N/A" : _AdditionalInformation; }
            set { SetProperty(ref _AdditionalInformation, value); }
        }

        [JsonProperty("amhp_name")]
        private string _AmhpName;
        public string AmhpName
        {
            get { return string.IsNullOrEmpty(_AmhpName) ? "N/A" : _AmhpName; }
            set { SetProperty(ref _AmhpName, value); }
        }

        [JsonProperty("assessment_date_time")]
        private string _AssessmentDateTime;
        public string AssessmentDateTime
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(_AssessmentDateTime))
                        return "N/A";

                    return Convert.ToDateTime(_AssessmentDateTime).ToString("yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    return string.IsNullOrEmpty(_AssessmentDateTime) ? "N/A" : _AssessmentDateTime; ;
                }
            }
            set { SetProperty(ref _AssessmentDateTime, value); }
        }

        [JsonProperty("approval_status")]
        public long ApprovalStatus { get; set; }

        [JsonProperty("assessment_location_type")]
        public long AssessmentLocationType { get; set; }

        [JsonProperty("car_model")]
        private string _CarModel;
        public string CarModel
        {
            get { return string.IsNullOrEmpty(_CarModel) ? "N/A" : _CarModel; }
            set { SetProperty(ref _CarModel, value); }
        }

        [JsonProperty("assessment_postcode")]
        private string _AssessmentPostcode;
        public string AssessmentPostcode
        {
            get { return string.IsNullOrEmpty(_AssessmentPostcode) ? "N/A" : _AssessmentPostcode; }
            set { SetProperty(ref _AssessmentPostcode, value); }
        }

        [JsonProperty("assessment_location_type_text")]
        private string _AssessmentLocationTypeText;
        public string AssessmentLocationTypeText
        {
            get { return string.IsNullOrEmpty(_AssessmentLocationTypeText) ? "N/A" : _AssessmentLocationTypeText; }
            set { SetProperty(ref _AssessmentLocationTypeText, value); }
        }

        [JsonProperty("car_make")]
        private string _CarMake;
        public string CarMake
        {
            get { return string.IsNullOrEmpty(_CarMake) ? "N/A" : _CarMake; }
            set { SetProperty(ref _CarMake, value); }
        }

        [JsonProperty("ccg_name")]
        private string _CcgName;
        public string CcgName
        {
            get { return string.IsNullOrEmpty(_CcgName) ? "N/A" : _CcgName; }
            set { SetProperty(ref _CcgName, value); }
        }

        [JsonProperty("car_registration_plate")]
        private string _CarRegistrationPlate;
        public string CarRegistrationPlate
        {
            get { return string.IsNullOrEmpty(_CarRegistrationPlate) ? "N/A" : _CarRegistrationPlate; }
            set { SetProperty(ref _CarRegistrationPlate, value); }
        }

        [JsonProperty("claim_creation_timestamp")]
        private string _ClaimCreationTimestamp;
        public string ClaimCreationTimestamp
        {
            get { return string.IsNullOrEmpty(_ClaimCreationTimestamp) ? "N/A" : _ClaimCreationTimestamp; }
            set { SetProperty(ref _ClaimCreationTimestamp, value); }
        }

        [JsonProperty("miles_traveled")]
        private string _MilesTraveled;
        public string MilesTraveled
        {
            get { return string.IsNullOrEmpty(_MilesTraveled) ? "N/A" : _MilesTraveled + " mi"; }
            set { SetProperty(ref _MilesTraveled, value); }
        }

        [JsonProperty("engine_size")]
        private string _EngineSize;
        public string EngineSize
        {
            get { return string.IsNullOrEmpty(_EngineSize) ? "N/A" : _EngineSize; }
            set { SetProperty(ref _EngineSize, value); }
        }

        [JsonProperty("doctor_address")]
        private string _DoctorAddress;
        public string DoctorAddress
        {
            get { return string.IsNullOrEmpty(_DoctorAddress) ? "N/A" : _DoctorAddress; }
            set { SetProperty(ref _DoctorAddress, value); }
        }

        [JsonProperty("disclaimer")]
        public long Disclaimer { get; set; }

        [JsonProperty("doctor_name")]
        private string _DoctorName;
        public string DoctorName
        {
            get { return string.IsNullOrEmpty(_DoctorName) ? "N/A" : _DoctorName; }
            set { SetProperty(ref _DoctorName, value); }
        }

        [JsonProperty("local_authority")]
        private string _LocalAuthority;
        public string LocalAuthority
        {
            get { return string.IsNullOrEmpty(_LocalAuthority) ? "N/A" : _LocalAuthority; }
            set { SetProperty(ref _LocalAuthority, value); }
        }

        [JsonProperty("from_postcode")]
        private string _FromPostcode;
        public string FromPostcode
        {
            get { return string.IsNullOrEmpty(_FromPostcode) ? "N/A" : _FromPostcode; }
            set { SetProperty(ref _FromPostcode, value); }
        }

        [JsonProperty("local_authority_id")]
        public long LocalAuthorityId { get; set; }

        [JsonProperty("rejection_reason")]
        private string _RejectionReason;
        public string RejectionReason
        {
            get { return string.IsNullOrEmpty(_RejectionReason) ? "N/A" : _RejectionReason; }
            set { SetProperty(ref _RejectionReason, value); }
        }

        [JsonProperty("patient_nhs_number")]
        private string _PatientNhsNumber;
        public string PatientNhsNumber
        {
            get { return string.IsNullOrEmpty(_PatientNhsNumber) ? "N/A" : _PatientNhsNumber; }
            set { SetProperty(ref _PatientNhsNumber, value); }
        }

        [JsonProperty("notes")]
        private string _Notes;
        public string Notes
        {
            get { return string.IsNullOrEmpty(_Notes) ? "N/A" : _Notes; }
            set { SetProperty(ref _Notes, value); }
        }

        [JsonProperty("patient_postcode")]
        private string _PatientPostcode;
        public string PatientPostcode
        {
            get { return string.IsNullOrEmpty(_PatientPostcode) ? "N/A" : _PatientPostcode; }
            set { SetProperty(ref _PatientPostcode, value); }
        }

        [JsonProperty("section_implemented_text")]
        private string _SectionImplementedText;
        public string SectionImplementedText
        {
            get { return string.IsNullOrEmpty(_SectionImplementedText) ? "N/A" : _SectionImplementedText; }
            set { SetProperty(ref _SectionImplementedText, value); }
        }

        [JsonProperty("sync_id")]
        public long SyncId { get; set; }

        [JsonProperty("section_implemented")]
        public long SectionImplemented { get; set; }

        [JsonProperty("section_type_description")]
        private string _SectionTypeDescription;
        public string SectionTypeDescription
        {
            get { return string.IsNullOrEmpty(_SectionTypeDescription) ? "N/A" : _SectionTypeDescription; }
            set { SetProperty(ref _SectionTypeDescription, value); }
        }

        [JsonProperty("to_postcode")]
        private string _ToPostcode;
        public string ToPostcode
        {
            get { return string.IsNullOrEmpty(_ToPostcode) ? "N/A" : _ToPostcode; }
            set { SetProperty(ref _ToPostcode, value); }
        }
    }

    public partial class ClaimFormMeta
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }

    public partial class ClaimForm
    {
        public static ClaimForm FromJson(string json) => JsonConvert.DeserializeObject<ClaimForm>(json, ConverterClaimForm.Settings);
    }

    public static class SerializeClaimForm
    {
        public static string ToJson(this ClaimForm self) => JsonConvert.SerializeObject(self, ConverterClaimForm.Settings);
    }

    public class ConverterClaimForm
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
