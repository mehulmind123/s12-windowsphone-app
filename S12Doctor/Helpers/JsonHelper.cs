﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S12Doctor.Helpers
{
    public class JsonHelper
    {
        public static T Get<T>(string json) where T : new()
        {
            if (string.IsNullOrWhiteSpace(json))
                return new T();

            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                //HASConnector.TrackEvent(Global.EVENT_HELPER, ex);
                return new T();
            }
        }

        public static string Convert(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        //public static TanksJson GetTanks(string tanksJson)
        //{
        //    return Get<TanksJson>(tanksJson);
        //}

        //public static EntityJson GetEntity(string entityJson)
        //{
        //    return Get<EntityJson>(entityJson);
        //}

        //public static ChildrenJson GetRouteParent(string childrenJson)
        //{
        //    return Get<ChildrenJson>(childrenJson);
        //}

        //public static ShowPropertiesJson GetSiteShowProperties(string json)
        //{
        //    return Get<ShowPropertiesJson>(json);
        //}

        //public static List<Children> GetChildrens(string json)
        //{
        //    return Get<List<Children>>(json);
        //}

        //public static PropertiesJson GetSiteProperties(string json)
        //{
        //    return Get<PropertiesJson>(json);
        //}

        //public static WellDetailsJson GetWellDetails(string json)
        //{
        //    return Get<WellDetailsJson>(json);
        //}

        //public static Dictionary<string, string> GetWellSettings(string json)
        //{
        //    var wellinfo = GetWellDetails(json);
        //    return wellinfo != null ? wellinfo.details.ToDictionaryEx(d => d.typeId, d => d.value) : new Dictionary<string, string>();
        //}

        //public static string GetWellSettingValue(string json, string key)
        //{
        //    var ws = GetWellSettings(json);
        //    return ws.ContainsKey(key) ? ws[key] : null;
        //}

        //public static bool GetHasFlow(string json)
        //{
        //    return !GetWellDetails(json).details.Where(x => x.typeId == "-80" && x.value == "1").Any();
        //}

        //public static MeasurementJson GetMeasurementProperties(string json)
        //{
        //    return Get<MeasurementJson>(json);
        //}

        //public static TreatmentServiceDataJson GetTreatmentServiceDataProperties(string json)
        //{
        //    return Get<TreatmentServiceDataJson>(json);
        //}

        //public static Dictionary<string, decimal?> FlattenPoints(string json)
        //{
        //    try
        //    {
        //        var result = Get<EntryJson>(json);
        //        var points = result.data.ToDictionaryEx(d => d.pointId, d => d.accumValue ?? d.value);
        //        var readings = result.data
        //            .SelectMany(s => s.readings)
        //            .ToDictionaryEx(d => d.readingId, d => d.value);

        //        return points.Concat(readings.Where(kvp => !points.ContainsKey(kvp.Key)))
        //            .ToDictionaryEx(d => d.Key, d => d.Value);
        //    }
        //    catch (Exception ex)
        //    {
        //        HASConnector.TrackEvent(Global.EVENT_HELPER, ex);
        //        return new Dictionary<string, decimal?>();
        //    }
        //}
    }
}
