﻿using S12Doctor.Helpers;
using System;
using System.Collections.Generic;

namespace S12Doctor.Constants
{
    public static partial class SettingConstant
    {
        public static List<string> GetAllKeys()
        {
            return 
                typeof(SettingConstant)
                .GetAllPublicConstantValues<string>();
        }        

        public const string S12USERINFO_KEY = "s12userinfo_key";
        public static readonly string S12USERINFO_DEFAULT = string.Empty;

        public const string AMHPUSERINFO_KEY = "amhpuserinfo_key";
        public static readonly string AMHPUSERINFO_DEFAULT = string.Empty;

        public const string EMAILID_KEY = "emailid_key";
        public static readonly string EMAILID_DEFAULT = string.Empty;

        public const string PASSWORD_KEY = "password_key";
        public static readonly string PASSWORD_DEFAULT = string.Empty;

        public const string LOGINTYPE_KEY = "logintype_key";
        public static readonly string LOGINTYPE_DEFAULT = string.Empty;

        public const string ISS12LOGINFIRSTTIME_KEY = "iss12loginfirsttime_key";
        public static readonly string ISS12LOGINFIRSTTIME_DEFAULT = string.Empty;

        public const string ISAMHPLOGINFIRSTTIME_KEY = "isamhploginfirsttime_key";
        public static readonly string ISAMHPLOGINFIRSTTIME_DEFAULT = string.Empty;

        //public const string USERID_KEY = "userid_key";
        //public static readonly int USERID_DEFAULT = 0;

        //public const string USERFIRSTNAME_KEY = "userfirstname_key";
        //public static readonly string USERFIRSTNAME_DEFAULT = string.Empty;

        //public const string USERLASTNAME_KEY = "userlastname_key";
        //public static readonly string USERLASTNAME_DEFAULT = string.Empty;

        //public const string COMPANYCODE_KEY = "companycode_key";
        //public static readonly string COMPANYCODE_DEFAULT = string.Empty;

        //public const string COMPANYID_KEY = "companyid_key";
        //public static readonly string COMPANYID_DEFAULT = string.Empty;

        //public const string DEVICETOKEN_KEY = "devicetoken_key";
        //public static readonly string DEVICETOKEN_DEFAULT = string.Empty;

        //public const string INITIALIZEDATE_KEY = "initializedate_key";
        //public static readonly string INITIALIZEDATE_DEFAULT = string.Empty;

        //public const string OIL_VENDOR_KEY = "default_oil_vendor_key";
        //public static readonly int OIL_VENDOR_DEFAULT = 0;

        //public const string WATER_VENDOR_KEY = "default_water_vendor_key";
        //public static readonly int WATER_VENDOR_DEFAULT = 0;

        //public const string SYNCDATE_KEY = "syncdate_key";
        //public static readonly DateTime SYNCDATE_DEFAULT = DateTime.Now;

        //public const string ISAUTOSYNC_KEY = "isautosync_key";
        //public static readonly bool ISAUTOSYNC_DEFAULT = true;

        //public const string ISADMIN_KEY = "isadmin_key";
        //public static readonly bool ISADMIN_DEFAULT = false;

        //public const string ENVIRONMENT_KEY = "environment_key";
        //public static readonly string ENVIRONMENT_DEFAULT = string.Empty;

        //public const string ISRULEENGINEENABLED_KEY = "isruleengineenabled_key";
        //public static readonly bool ISRULEENGINEENABLED_DEFAULT = false;

        //public const string ISLOGGEDTOTABLE_KEY = "isloggedtotable_key";
        //public static readonly bool ISLOGGEDTOTABLE_DEFAULT = false;

        //public const string SOURCENAME_KEY = "sourcename_key";
        //public static readonly string SOURCENAME_DEFAULT = string.Empty;

        //public const string SESSIONID_KEY = "sessionid_key";
        //public static readonly string SESSIONID_DEFAULT = string.Empty;

        //public const string ISSITEPROPERTYEXPAND_KEY = "issitepropertyexpand_key";
        //public static readonly bool ISSITEPROPERTYEXPAND_DEFAULT = false;

        //public const string LASTVISITEDSITES_KEY = "lastvisitedsites_key";
        //public static readonly string LASTVISITEDSITES_DEFAULT = string.Empty;
    }
}
