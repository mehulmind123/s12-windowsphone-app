using S12Doctor.Constants;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;

namespace S12Doctor.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class SettingHelper
  {
        #region // Static Instance
        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }
        #endregion

        #region // Properties
        public static string S12UserInfo
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingConstant.S12USERINFO_KEY, SettingConstant.S12USERINFO_DEFAULT);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingConstant.S12USERINFO_KEY, value);
            }
        }

        public static string AmhpUserInfo
        {
            get { return AppSettings.GetValueOrDefault<string>(SettingConstant.AMHPUSERINFO_KEY, SettingConstant.AMHPUSERINFO_DEFAULT); }
            set { AppSettings.AddOrUpdateValue<string>(SettingConstant.AMHPUSERINFO_KEY, value); }
        }

        public static string EmailId
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingConstant.EMAILID_KEY, SettingConstant.EMAILID_DEFAULT);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingConstant.EMAILID_KEY, value);
            }
        }

        public static string Password
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingConstant.PASSWORD_KEY, SettingConstant.PASSWORD_DEFAULT);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingConstant.PASSWORD_KEY, value);
            }
        }

        public static string LoginType
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingConstant.LOGINTYPE_KEY, SettingConstant.LOGINTYPE_DEFAULT);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingConstant.LOGINTYPE_KEY, value);
            }
        }

        public static string IsS12LoginFirstTime
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingConstant.ISS12LOGINFIRSTTIME_KEY, SettingConstant.ISS12LOGINFIRSTTIME_DEFAULT);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingConstant.ISS12LOGINFIRSTTIME_KEY, value);
            }
        }

        public static string IsAMHPLoginFirstTime
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingConstant.ISAMHPLOGINFIRSTTIME_KEY, SettingConstant.ISAMHPLOGINFIRSTTIME_DEFAULT);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingConstant.ISAMHPLOGINFIRSTTIME_KEY, value);
            }
        }

        //public static int UserId
        //{
        //    get { return AppSettings.GetValueOrDefault<int>(SettingConstant.USERID_KEY, SettingConstant.USERID_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<int>(SettingConstant.USERID_KEY, value); }
        //}

        //public static string FirstName
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.USERFIRSTNAME_KEY, SettingConstant.USERFIRSTNAME_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.USERFIRSTNAME_KEY, value); }
        //}

        //public static string LastName
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.USERLASTNAME_KEY, SettingConstant.USERLASTNAME_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.USERLASTNAME_KEY, value); }
        //}

        //public static string CompanyCode
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.COMPANYCODE_KEY, SettingConstant.COMPANYCODE_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.COMPANYCODE_KEY, value); }
        //}

        //public static string CompanyId
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.COMPANYID_KEY, SettingConstant.COMPANYID_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.COMPANYID_KEY, value); }
        //}

        //public static string DeviceToken
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.DEVICETOKEN_KEY, SettingConstant.DEVICETOKEN_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.DEVICETOKEN_KEY, value); }
        //}

        //public static string InitializeDate
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.INITIALIZEDATE_KEY, SettingConstant.INITIALIZEDATE_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.INITIALIZEDATE_KEY, value); }
        //}

        //public static int OilVendor
        //{
        //    get { return AppSettings.GetValueOrDefault<int>(SettingConstant.OIL_VENDOR_KEY, SettingConstant.OIL_VENDOR_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<int>(SettingConstant.OIL_VENDOR_KEY, value); }
        //}

        //public static int WaterVendor
        //{
        //    get { return AppSettings.GetValueOrDefault<int>(SettingConstant.WATER_VENDOR_KEY, SettingConstant.WATER_VENDOR_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<int>(SettingConstant.WATER_VENDOR_KEY, value); }
        //}

        //public static DateTime SyncDate
        //{
        //    get { return AppSettings.GetValueOrDefault<DateTime>(SettingConstant.SYNCDATE_KEY, SettingConstant.SYNCDATE_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<DateTime>(SettingConstant.SYNCDATE_KEY, value); }
        //}

        //public static bool IsAutoSync
        //{
        //    get { return AppSettings.GetValueOrDefault<bool>(SettingConstant.ISAUTOSYNC_KEY, SettingConstant.ISAUTOSYNC_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<bool>(SettingConstant.ISAUTOSYNC_KEY, value); }
        //}

        //public static bool IsAdminMode
        //{
        //    get { return AppSettings.GetValueOrDefault<bool>(SettingConstant.ISADMIN_KEY, SettingConstant.ISADMIN_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<bool>(SettingConstant.ISADMIN_KEY, value); }
        //}

        //public static string Environment
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.ENVIRONMENT_KEY, SettingConstant.ENVIRONMENT_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.ENVIRONMENT_KEY, value); }
        //}

        //public static bool IsRuleEngineEnabled
        //{
        //    get { return AppSettings.GetValueOrDefault<bool>(SettingConstant.ISRULEENGINEENABLED_KEY, SettingConstant.ISRULEENGINEENABLED_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<bool>(SettingConstant.ISRULEENGINEENABLED_KEY, value); }
        //}

        //public static bool IsLoggedToTable
        //{
        //    get { return AppSettings.GetValueOrDefault<bool>(SettingConstant.ISLOGGEDTOTABLE_KEY, SettingConstant.ISLOGGEDTOTABLE_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<bool>(SettingConstant.ISLOGGEDTOTABLE_KEY, value); }
        //}

        //public static string SourceName
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.SOURCENAME_KEY, SettingConstant.SOURCENAME_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.SOURCENAME_KEY, value); }
        //}

        //public static string SessionId
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.SESSIONID_KEY, SettingConstant.SESSIONID_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.SESSIONID_KEY, value); }
        //}

        //public static bool IsSitePropertyExpanded
        //{
        //    get { return AppSettings.GetValueOrDefault<bool>(SettingConstant.ISSITEPROPERTYEXPAND_KEY, SettingConstant.ISSITEPROPERTYEXPAND_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<bool>(SettingConstant.ISSITEPROPERTYEXPAND_KEY, value); }
        //}

        //public static string LastVisitedSites
        //{
        //    get { return AppSettings.GetValueOrDefault<string>(SettingConstant.LASTVISITEDSITES_KEY, SettingConstant.LASTVISITEDSITES_DEFAULT); }
        //    set { AppSettings.AddOrUpdateValue<string>(SettingConstant.LASTVISITEDSITES_KEY, value); }
        //}

        #endregion

        #region // Methods
        public static void Purge(List<string> keep = null)
        {
            keep = keep ?? new List<string>();
            foreach (var key in SettingConstant.GetAllKeys())
            {
                if (!keep.Contains(key))
                    AppSettings.Remove(key);
            }
        } 
        #endregion
    }
}