﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace S12Doctor.Helpers
{
    public static class TypeExtension
    {
        public static List<T> GetAllPublicConstantValues<T>(this Type type)
        {
            return type
                .GetRuntimeFields()
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(T))
                .Select(x => (T)x.GetValue(null))
                .ToList();
        }
    }
}
