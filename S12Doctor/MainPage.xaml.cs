﻿using S12Doctor.Utilities;
using S12Doctor.WebService;
using System;
using Windows.ApplicationModel.Email;
using Windows.Phone.UI.Input;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using S12Doctor.Model.S12Doctor;

namespace S12Doctor
{
    public sealed partial class MainPage : Page
    {
        private bool isLeftMenuOpen = false;

        DispatcherTimer dptTimer;
        DispatcherTimer s12dptTimer;
        DispatcherTimer dptTimerForLocation;

        public Login LoginControl;
        public Views.Home HomeControl;
        public Views.UsefulInfo UsefulInfoControl;
        public Views.ClaimForms ClaimFormsControl;
        public Views.ViewClaimForm ViewClaimFormControl;
        public Views.CreateClaimForm CreateClaimFormControl;
        public Views.EditProfile EditProfileControl;
        public Views.DraftClaimForm DraftClaimFormControl;
        public Views.Setting SettingControl;
        public Views.ChangePassword ChangePasswordControl;
        public Views.TermsAndConditions TermsAndConditionsControl;
        public Views.UserGuide userGuideControl;
        public Views.AboutUs AboutUsControl;
        public Views.PrivacyPolicy PrivacyPolicyControl;

        S12DoctorServices s12DoctorServices;

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Disabled;
            this.Loaded += MainPage_Loaded;
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //Show the status bar
            var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
            ////statusBar.BackgroundColor = Windows.UI.Colors.White;
            statusBar.ForegroundColor = Windows.UI.Colors.Black;
            //statusBar.BackgroundOpacity = 1;
            await statusBar.ShowAsync();

            dptTimer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(500) };
            dptTimer.Tick += DptTimer_Tick;
            dptTimer.Start();

            s12dptTimer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(100) };
            s12dptTimer.Tick += s12DisbleEnableTime_Tick;
            s12dptTimer.Start();


            RightMenuStorybord_Close.Completed += RightMenuStorybord_Close_Completed;

            AddControl(Pages.Login);

            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

        }

        bool isSet = true;
        private void DptTimerForLocation_Tick(object sender, object e)
        {
            try
            {
                if (s12DoctorServices != null && Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                    if (isSet)
                    {
                        isSet = false;
                        setCurrentLatLong(Common.LocationService.Instance.Latitude, Common.LocationService.Instance.Longitude);
                    }
            }
            catch (Exception) { }
        }

        private async void Instance_OnPositionChanged(Windows.Devices.Geolocation.Geoposition obj)
        {
            try
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                            isSet = true);
            }
            catch (Exception) { }
        }

        private async void setCurrentLatLong(string latitude, string longitude)
        {
            try
            {
                var data = await s12DoctorServices.setCurrentLatLong(latitude, longitude);
            }
            catch (Exception) { }
        }
        private async void s12DisbleEnableTime_Tick(object sender, object e)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                s12EnableLinks();
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

                ////// Sync Calendar
                Windows.Storage.ApplicationDataCompositeValue composite = (Windows.Storage.ApplicationDataCompositeValue)localSettings.Values["calanderData"];
                //object value = localSettings.Values["calanderAvailibility"];
                string isSynced = (composite!=null)?composite["isSynced"].ToString():string.Empty;
                if (composite != null && isSynced == "0")
                {
                    try
                    {
                        ////sync data
                            string am = composite["am"].ToString();
                            string lunch = composite["lunch"].ToString();
                            string evening = composite["evening"].ToString();
                            string night = composite["night"].ToString();
                            string pm = composite["pm"].ToString();
                            
                            object result = await s12DoctorServices.availabilityCalander(am, lunch, pm, evening, night, false);
                            composite.Clear();
                            //if (result is CalanderAvailibility)
                    }
                    catch (Exception ex)
                    {

                    }                        
                }
                ///// sync avaibility status
                Windows.Storage.ApplicationDataCompositeValue composite1 = (Windows.Storage.ApplicationDataCompositeValue)localSettings.Values["Avaibility"];
                //object value = localSettings.Values["calanderAvailibility"];
                string isStatusSynced = (composite1 != null) ? composite1["isStatusSynced"].ToString() : string.Empty;
                if (composite1 != null && isStatusSynced == "0")
                {
                    try
                    {
                        ////sync data
                        string status = composite1["status"].ToString();
                        string statusText = composite1["EditStatusText"].ToString() != "" ? composite1["EditStatusText"].ToString() : "";
                        object result = await s12DoctorServices.changeAvailabilityStatus(status.ToString(), statusText,true);
                        composite1.Clear();
           
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                s12disableLinks();
            }
        }
        private void DptTimer_Tick(object sender, object e)
        {
            try
            {
                    // Gets the app's current memory usage   
                    ulong AppMemoryUsageUlong = MemoryManager.AppMemoryUsage;

                // Gets the app's memory usage limit   
                ulong AppMemoryUsageLimitUlong = MemoryManager.AppMemoryUsageLimit;

                AppMemoryUsageUlong /= 1024 * 1024;
                AppMemoryUsageLimitUlong /= 1024 * 1024;
                AppMemoryUsage.Text = "App memory uage - " + AppMemoryUsageUlong.ToString();
                AppMemoryUsageLimit.Text = "App memory usage limit - " + AppMemoryUsageLimitUlong.ToString();

                // Gets the app's memory usage level whether low or medium or high   
                AppMemoryUsageLevels.Text = "App memory usage level - " + MemoryManager.AppMemoryUsageLevel.ToString();

                if (SessionData.CurrntAppUser != null && SessionData.CurrntAppUser.Meta != null)
                {
                    textDraftClaimFormsCount.Text = SessionData.CurrntAppUser.Meta.DraftCount.ToString();
                    if (SessionData.CurrntAppUser.Meta.DraftCount <= 0)
                        gridDraftCount.Visibility = Visibility.Collapsed;
                    else
                        gridDraftCount.Visibility = Visibility.Visible;
                }

                GC.Collect();
            }
            catch { }
        }

        private void RightMenuStorybord_Close_Completed(object sender, object e)
        {
            RightMenuStorybord.Stop();
            RightMenuStorybord_Close.Stop();
        }

        private void Hamburger_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MenubarAction();
        }

        private void CloseLeftMenu_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MenubarAction();
        }

        public void RedirectToHomePage()
        {
            AddControl(Pages.Home);
            HomeControl = new Views.Home();
            ViewContainer.Children.Add(HomeControl);
        }

        public void s12disableLinks()
        {
            if (SessionData.CurrentView == Pages.Login)
            {
                InternetCon.Visibility = Visibility.Collapsed;
            }
            else
            {
                InternetCon.Visibility = Visibility.Visible;
            }
            SolidColorBrush foregroundSolidColorBrush;
            foregroundSolidColorBrush = new SolidColorBrush();
            foregroundSolidColorBrush.Color = Color.FromArgb(255, 169, 169, 169);

            textEditProfile.Foreground = foregroundSolidColorBrush;
            textCompletedClaimForms.Foreground = foregroundSolidColorBrush;
            textDraftClaimForms.Foreground = foregroundSolidColorBrush;
            textSettings.Foreground = foregroundSolidColorBrush;
            textChangePassword.Foreground = foregroundSolidColorBrush;
            
        }

        public void s12EnableLinks()
        {
            SolidColorBrush foregroundSolidColorBrush;
            foregroundSolidColorBrush = new SolidColorBrush();
            foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);

            textEditProfile.Foreground = foregroundSolidColorBrush;
            textCompletedClaimForms.Foreground = foregroundSolidColorBrush;
            textDraftClaimForms.Foreground = foregroundSolidColorBrush;
            textSettings.Foreground = foregroundSolidColorBrush;
            textChangePassword.Foreground = foregroundSolidColorBrush;
            InternetCon.Visibility = Visibility.Collapsed;
        }

        private void MenubarAction()
        {
            isLeftMenuOpen = !isLeftMenuOpen;

            if (isLeftMenuOpen)
            {
                RightMenuStorybord.Begin();
                CloseLeftMenu.Visibility = Visibility.Visible;
                if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    s12disableLinks();
                }
                else
                {
                    s12EnableLinks();
                }

            }
            else
            {
                RightMenuStorybord_Close.Begin();
                CloseLeftMenu.Visibility = Visibility.Collapsed;
            }
        }

        public void LoaderVisibility(Visibility visibility)
        {
            LoadingWindow.Visibility = visibility;
        }

        public void OpenPopUpBox(string msg)
        {
            txtPopUpMessage.Text = msg;
            GridPopUpWindow.Visibility = Visibility.Visible;
        }

        public void SetHeaderText(string text)
        {
            HeaderText.Text = text;
        }

        public void SetSaveButtonVisibility(Visibility visibility)
        {
            btnSave.Visibility = visibility;
        }

        public void SetHamburgerVisibility(Visibility visibility)
        {
            Hamburger.Visibility = visibility;
        }

        public void SetUserControlOnHeader()
        {
            Grid.SetRow(ViewContainer, 0);
            Grid.SetRowSpan(ViewContainer, 2);
        }

        public void RemoveUserControlFromHeader()
        {
            Grid.SetRow(ViewContainer, 1);
        }

        async private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (SessionData.CurrentView == Pages.Home)
            {
                if (HomeControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    string result = await HomeControl.createClaimForm.SaveClaimForm();
                    if ("error".Equals(result))
                    {
                        HomeControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        HomeControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                        HomeControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        btnSave.Visibility = Visibility.Collapsed;
                        HeaderText.Text = "Section 12 doctor home";
                        SetHamburgerVisibility(Visibility.Visible);
                        OpenPopUpBox("Partially filled claim form saved in draft list");
                    }
                }
            }
            else if (SessionData.CurrentView == Pages.ClaimForms)
            {
                if (ClaimFormsControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    string result = await ClaimFormsControl.createClaimForm.SaveClaimForm();
                    if ("error".Equals(result))
                    {
                        ClaimFormsControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        ClaimFormsControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                        ClaimFormsControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        btnSave.Visibility = Visibility.Collapsed;
                        HeaderText.Text = "Claim Forms";
                        SetHamburgerVisibility(Visibility.Visible);
                        OpenPopUpBox("Claim form saved successfully");
                        ClaimFormsControl.ReloadData();
                    }
                }
            }
            else if (SessionData.CurrentView == Pages.DraftClaimForm)
            {
                if (DraftClaimFormControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    string result = await DraftClaimFormControl.createClaimForm.SaveClaimForm();
                    if ("error".Equals(result))
                    {
                        DraftClaimFormControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        DraftClaimFormControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                        DraftClaimFormControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        btnSave.Visibility = Visibility.Collapsed;
                        HeaderText.Text = "Drafts";
                        SetHamburgerVisibility(Visibility.Visible);
                        OpenPopUpBox("Claim form saved successfully");
                        DraftClaimFormControl.ReloadData();
                    }
                }
            }
        }

        async public void SaveAsDraftClaimFormOnBackKeyPress(bool isSaveAsDraft)
        {
            if (SessionData.CurrentView == Pages.Home)
            {
                if (HomeControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    if (isSaveAsDraft)
                    {
                        string result = await HomeControl.createClaimForm.SaveClaimForm();
                        if ("error".Equals(result))
                        {
                            SetSaveButtonVisibility(Visibility.Visible);
                            HomeControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            HomeControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                            HomeControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                            btnSave.Visibility = Visibility.Collapsed;
                            HeaderText.Text = "Section 12 doctor home";
                            SetHamburgerVisibility(Visibility.Visible);
                            OpenPopUpBox("Partially filled claim form saved in draft list");
                        }
                    }
                    else
                    {
                        HomeControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                        HomeControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        btnSave.Visibility = Visibility.Collapsed;
                        HeaderText.Text = "Section 12 doctor home";
                        SetHamburgerVisibility(Visibility.Visible);
                    }
                }
            }
            else if (SessionData.CurrentView == Pages.ClaimForms)
            {
                if (ClaimFormsControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    if (isSaveAsDraft)
                    {
                        string result = await ClaimFormsControl.createClaimForm.SaveClaimForm();
                        if ("error".Equals(result))
                        {
                            SetSaveButtonVisibility(Visibility.Visible);
                            ClaimFormsControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            ClaimFormsControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                            ClaimFormsControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                            btnSave.Visibility = Visibility.Collapsed;
                            HeaderText.Text = "Claim Forms";
                            SetHamburgerVisibility(Visibility.Visible);
                            OpenPopUpBox("Claim form saved successfully");
                            ClaimFormsControl.ReloadData();
                        }
                    }
                    else
                    {
                        ClaimFormsControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                        ClaimFormsControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        btnSave.Visibility = Visibility.Collapsed;
                        HeaderText.Text = "Claim Forms";
                        SetHamburgerVisibility(Visibility.Visible);
                    }
                }
            }
            else if (SessionData.CurrentView == Pages.DraftClaimForm)
            {
                if (DraftClaimFormControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    if (isSaveAsDraft)
                    {
                        string result = await DraftClaimFormControl.createClaimForm.SaveClaimForm();
                        if ("error".Equals(result))
                        {
                            SetSaveButtonVisibility(Visibility.Visible);
                            DraftClaimFormControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            DraftClaimFormControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                            DraftClaimFormControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                            btnSave.Visibility = Visibility.Collapsed;
                            HeaderText.Text = "Drafts";
                            SetHamburgerVisibility(Visibility.Visible);
                            OpenPopUpBox("Claim form saved successfully");
                            DraftClaimFormControl.ReloadData();
                        }
                    }
                    else
                    {
                        DraftClaimFormControl.gridCreateClaimForm.Visibility = Visibility.Collapsed;
                        DraftClaimFormControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                        btnSave.Visibility = Visibility.Collapsed;
                        HeaderText.Text = "Drafts";
                        SetHamburgerVisibility(Visibility.Visible);
                    }
                }
            }
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            if (SessionData.CurrentView == Pages.EditProfile)
            {
                AddControl(Pages.Home);
            }
        }

        #region ADD & DISPOSE Applications Functions...

        #region Pages

        public void AddControl(Pages view)
        {
            if (SessionData.CurrentView != view)
            {
                SessionData.DisposableView = SessionData.CurrentView;
                SessionData.CurrentView = view;

                DisposeControl(SessionData.DisposableView);

                switch (view)
                {
                    case Pages.Login:
                        if (LoginControl == null)
                        {
                            LoginControl = new Login();
                            SetUserControlOnHeader();
                            ViewContainer.Children.Add(LoginControl);
                        }
                        break;
                    case Pages.Home:

                        if (HomeControl == null)
                        {
                            HeaderText.Text = "Section 12 doctor home";
                            HomeControl = new Views.Home();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(HomeControl);
                            if (s12DoctorServices == null)
                                s12DoctorServices = new S12DoctorServices();
                            if (dptTimerForLocation == null)
                            {
                                dptTimerForLocation = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(60) };
                                dptTimerForLocation.Tick += DptTimerForLocation_Tick;
                                dptTimerForLocation.Start();

                                Common.LocationService.Instance.OnPositionChanged += Instance_OnPositionChanged;
                            }
                        }

                        break;
                    case Pages.UsefulInfo:

                        if (UsefulInfoControl == null)
                        {
                            HeaderText.Text = "Useful Information";
                            UsefulInfoControl = new Views.UsefulInfo();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(UsefulInfoControl);
                        }

                        break;

                    case Pages.ClaimForms:

                        if (ClaimFormsControl == null)
                        {
                            HeaderText.Text = "Claim Forms";
                            ClaimFormsControl = new Views.ClaimForms();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(ClaimFormsControl);
                        }

                        break;

                    case Pages.ViewClaimForm:

                        if (ViewClaimFormControl == null)
                        {
                            HeaderText.Text = "View Claim Form";
                            ViewClaimFormControl = new Views.ViewClaimForm();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(ViewClaimFormControl);
                        }

                        break;
                    case Pages.CreateClaimForm:

                        if (CreateClaimFormControl == null)
                        {
                            HeaderText.Text = "Create Claim Form";
                            CreateClaimFormControl = new Views.CreateClaimForm();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(CreateClaimFormControl);
                        }

                        break;

                    case Pages.EditProfile:

                        if (EditProfileControl == null)
                        {
                            btnDone.Visibility = Visibility.Collapsed;
                            HeaderText.Text = "Edit Profile";
                            EditProfileControl = new Views.EditProfile();
                            EditProfileControl.SetDefaultData(false);
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(EditProfileControl);
                        }

                        break;

                    case Pages.DraftClaimForm:

                        if (DraftClaimFormControl == null)
                        {
                            HeaderText.Text = "Drafts";
                            DraftClaimFormControl = new Views.DraftClaimForm();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(DraftClaimFormControl);
                        }

                        break;

                    case Pages.Setting:

                        if (SettingControl == null)
                        {
                            HeaderText.Text = "Settings";
                            SettingControl = new Views.Setting();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(SettingControl);
                        }

                        break;

                    case Pages.ChangePassword:

                        if (ChangePasswordControl == null)
                        {
                            HeaderText.Text = "Change Password";
                            ChangePasswordControl = new Views.ChangePassword();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(ChangePasswordControl);
                        }

                        break;

                    case Pages.TermsAndConditions:

                        if (TermsAndConditionsControl == null)
                        {
                            HeaderText.Text = "Terms & Conditions";
                            TermsAndConditionsControl = new Views.TermsAndConditions();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(TermsAndConditionsControl);
                        }

                        break;

                    case Pages.UserGuide:
                        if (userGuideControl == null)
                        {
                            HeaderText.Text = "User Guide";
                            userGuideControl = new Views.UserGuide();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(userGuideControl);
                        }
                        break;
                    case Pages.AboutUs:

                        if (AboutUsControl == null)
                        {
                            HeaderText.Text = "About Us";
                            AboutUsControl = new Views.AboutUs();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(AboutUsControl);
                        }

                        break;

                    case Pages.PrivacyPolicy:

                        if (PrivacyPolicyControl == null)
                        {
                            HeaderText.Text = "Privacy Policy";
                            PrivacyPolicyControl = new Views.PrivacyPolicy();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(PrivacyPolicyControl);
                        }

                        break;

                    default:
                        break;
                }
            }
        }

        public void DisposeControl(Pages view)
        {
            switch (view)
            {
                case Pages.Login:

                    if (LoginControl != null)
                    {
                        LoginControl.DisposeMemory();
                        LoginControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.Home:

                    if (HomeControl != null)
                    {
                        HomeControl.DisposeMemory();
                        HomeControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.UsefulInfo:

                    if (UsefulInfoControl != null)
                    {
                        //UsefulInfoControl.DisposeMemory();
                        UsefulInfoControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.ClaimForms:

                    if (ClaimFormsControl != null)
                    {
                        //ClaimFormsControl.DisposeMemory();
                        ClaimFormsControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.ViewClaimForm:

                    if (ViewClaimFormControl != null)
                    {
                        //ViewClaimFormControl.DisposeMemory();
                        ViewClaimFormControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.CreateClaimForm:

                    if (CreateClaimFormControl != null)
                    {
                        //CreateClaimFormControl.DisposeMemory();
                        CreateClaimFormControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.EditProfile:

                    if (EditProfileControl != null)
                    {
                        btnDone.Visibility = Visibility.Collapsed;
                        //EditProfileControl.DisposeMemory();
                        EditProfileControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.DraftClaimForm:

                    if (DraftClaimFormControl != null)
                    {
                        //DraftClaimFormControl.DisposeMemory();
                        DraftClaimFormControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.Setting:

                    if (SettingControl != null)
                    {
                        //SettingControl.DisposeMemory();
                        SettingControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.ChangePassword:

                    if (ChangePasswordControl != null)
                    {
                        //ChangePasswordControl.DisposeMemory();
                        ChangePasswordControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.TermsAndConditions:

                    if (TermsAndConditionsControl != null)
                    {
                        //TermsAndConditionsControl.DisposeMemory();
                        TermsAndConditionsControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;
                case Pages.UserGuide:

                    if (userGuideControl != null)
                    {
                        //TermsAndConditionsControl.DisposeMemory();
                        userGuideControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;
                case Pages.AboutUs:

                    if (AboutUsControl != null)
                    {
                        //AboutUsControl.DisposeMemory();
                        AboutUsControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.PrivacyPolicy:

                    if (PrivacyPolicyControl != null)
                    {
                        //PrivacyPolicyControl.DisposeMemory();
                        PrivacyPolicyControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                default:
                    break;
            }
        }

        #endregion

        #endregion

        private void HeaderText_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MemoryWindow.Visibility = Visibility.Visible;
        }

        private void btnOk_Click(object sender, TappedRoutedEventArgs e)
        {
            GridPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void gridShadow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GridPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void gridHalpSupportBack_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridHalpSupportPopUp.Visibility = Visibility.Collapsed;
        }

        private void btnHalpSupportCancel_Click(object sender, TappedRoutedEventArgs e)
        {
            gridHalpSupportPopUp.Visibility = Visibility.Collapsed;
        }

        private void lblUserGuide_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridHalpSupportPopUp.Visibility = Visibility.Collapsed;
            AddControl(Pages.UserGuide);
            if (gridHalpSupportPopUp.Visibility != Visibility.Visible)
                MenubarAction();

        }

        private async void lblEmailUs_Tapped(object sender, TappedRoutedEventArgs e)
        {
            
            try
            {
                gridHalpSupportPopUp.Visibility = Visibility.Collapsed;

                EmailRecipient sendTo = new EmailRecipient()
                {
                    Address = "support@s12solutions.com",
                };

                //generate mail object
                EmailMessage mail = new EmailMessage();
                mail.Subject = "";
                mail.Body = "";
                //add recipients to the mail object
                mail.To.Add(sendTo);

                //open the share contract with Mail only:
                await EmailManager.ShowComposeNewEmailAsync(mail);
            }
            catch (Exception ex)
            {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        private void gridLogOutShadow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GridLogOutPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void btnLogOutNo_Click(object sender, TappedRoutedEventArgs e)
        {
            GridLogOutPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void btnLogOutYes_Click(object sender, TappedRoutedEventArgs e)
        {
            GridLogOutPopUpWindow.Visibility = Visibility.Collapsed;
            DisposeControl(SessionData.DisposableView);
            DisposeControl(SessionData.CurrentView);
            MenubarAction();

            SessionData.ClearSessionData();

            var lst = new System.Collections.Generic.List<string>();
            lst.Add("iss12loginfirsttime_key");
            lst.Add("isamhploginfirsttime_key");

            Helpers.SettingHelper.Purge(lst);

            if (Frame.CanGoBack)
                Frame.GoBack();
            else
                Frame.Navigate(typeof(WelcomePage));
        }

        private void MemoryWindow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MemoryWindow.Visibility = Visibility.Collapsed;
        }

        #region SidebarMenuButtons

        private void gridLogout_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GridLogOutPopUpWindow.Visibility = Visibility.Visible;
        }

        private void SideMenuButton_Click(object sender, TappedRoutedEventArgs e)
        {
            switch (((Grid)sender).Name)
            {
                case "gridHome":
                    AddControl(Pages.Home);
                    break;
                case "gridUserInfo":
                    AddControl(Pages.UsefulInfo);
                    break;
                case "gridEditProfile":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.EditProfile);
                        }
                        else
                            return;
                    }
                    break;
                case "gridSettings":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.Setting);
                        }
                        else
                            return;
                    }
                    break;
                case "gridChangePassword":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.ChangePassword);
                        }
                        else
                            return;
                    }
                    
                    break;
                case "gridDraftClaimForms":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.DraftClaimForm);
                        }
                        else
                            return;
                    }
                    break;
                case "gridCompletedClaimForms":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.ClaimForms);

                        }
                        else
                            return;
                    }
                    break;
                case "gridAboutUs":
                     AddControl(Pages.AboutUs);
                     break;
                case "gridTermsConditions":
                    AddControl(Pages.TermsAndConditions);
                    break;
                case "gridPrivecyPolicy":
                    AddControl(Pages.PrivacyPolicy);
                    break;
                case "gridHelpSupport":
                    gridHalpSupportPopUp.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }

            if (gridHalpSupportPopUp.Visibility != Visibility.Visible)
                MenubarAction();
        }

        #endregion

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (GridPopUpWindow.Visibility == Visibility.Visible)
            {
                GridPopUpWindow.Visibility = Visibility.Collapsed;
                e.Handled = true;
            }
            else if (gridHalpSupportPopUp.Visibility == Visibility.Visible)
            {
                gridHalpSupportPopUp.Visibility = Visibility.Collapsed;
                e.Handled = true;
            }
            else if (GridLogOutPopUpWindow.Visibility == Visibility.Visible)
            {
                GridLogOutPopUpWindow.Visibility = Visibility.Collapsed;
                e.Handled = true;
            }
            else if (SessionData.CurrentView == Pages.Login)
            {
                if (LoginControl.gridForgotPassword.Visibility == Visibility.Visible)
                {
                    LoginControl.txtFPEmailAddress.Text = string.Empty;
                    LoginControl.gridForgotPassword.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else
                {
                    e.Handled = true;
                    btnLogOutYes_Click(null, null);
                }
            }
            else if (SessionData.CurrentView == Pages.Home)
            {
                if (HomeControl.gridEditPopUp.Visibility == Visibility.Visible)
                {
                    HomeControl.SetStatus(HomeControl.CurrentStatus);
                    HomeControl.gridEditPopUp.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }

                else if (HomeControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    if (HomeControl.createClaimForm.GridEnterPasswordPopUp.Visibility == Visibility.Visible)
                    {
                        SetSaveButtonVisibility(Visibility.Visible);
                        HomeControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                    }
                    else if (HomeControl.createClaimForm.gridAdditionalInformationMsg.Visibility == Visibility.Visible)
                    {
                        HomeControl.createClaimForm.gridAdditionalInformationMsg.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        HomeControl.createClaimForm.GridSaveAsDraftPopUpWindow.Visibility = Visibility.Visible;
                        SetSaveButtonVisibility(Visibility.Collapsed);
                    }
                    e.Handled = true;
                }
                else if (HomeControl.gridEditProfile.Visibility == Visibility.Visible)
                {
                    if (HomeControl.EditProfile.gridAddLanguagePopUp.Visibility == Visibility.Visible)
                    {
                        HomeControl.EditProfile.gridAddLanguagePopUp.Visibility = Visibility.Collapsed;
                    }
                    else if (HomeControl.EditProfile.gridOfficeBasePostCodeInfoMsg.Visibility == Visibility.Visible)
                    {
                        HomeControl.EditProfile.gridOfficeBasePostCodeInfoMsg.Visibility = Visibility.Collapsed;
                    }
                    else if (HomeControl.EditProfile.gridLocalAuthorities.Visibility == Visibility.Visible)
                    {
                        HomeControl.EditProfile.gridLocalAuthorities.Visibility = Visibility.Collapsed;
                        RemoveUserControlFromHeader();
                    }
                    else if (HomeControl.EditProfile.gridEmployerList.Visibility == Visibility.Visible)
                    {
                        HomeControl.EditProfile.gridEmployerList.Visibility = Visibility.Collapsed;
                        RemoveUserControlFromHeader();
                    }
                    else if (HomeControl.EditProfile.gridLanguages.Visibility == Visibility.Visible)
                    {
                        HomeControl.EditProfile.gridLanguages.Visibility = Visibility.Collapsed;
                        RemoveUserControlFromHeader();
                    }
                    else if (HomeControl.EditProfile.gridSpecialities.Visibility == Visibility.Visible)
                    {
                        HomeControl.EditProfile.gridSpecialities.Visibility = Visibility.Collapsed;
                        RemoveUserControlFromHeader();
                    }
                    else
                    {
                        SetHamburgerVisibility(Visibility.Visible);
                        HomeControl.gridEditProfile.Visibility = Visibility.Collapsed;
                        HeaderText.Text = "Section 12 doctor home";
                    }
                    e.Handled = true;
                }
            }
            else if (SessionData.CurrentView == Pages.EditProfile)
            {
                if (EditProfileControl.gridAddLanguagePopUp.Visibility == Visibility.Visible)
                {
                    EditProfileControl.gridAddLanguagePopUp.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else if (EditProfileControl.gridOfficeBasePostCodeInfoMsg.Visibility == Visibility.Visible)
                {
                    EditProfileControl.gridOfficeBasePostCodeInfoMsg.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else if (EditProfileControl.gridEmployerList.Visibility == Visibility.Visible)
                {
                    EditProfileControl.gridEmployerList.Visibility = Visibility.Collapsed;
                    RemoveUserControlFromHeader();
                    e.Handled = true;
                }
                else if (EditProfileControl.gridLocalAuthorities.Visibility == Visibility.Visible)
                {
                    EditProfileControl.gridLocalAuthorities.Visibility = Visibility.Collapsed;
                    RemoveUserControlFromHeader();
                    e.Handled = true;
                }
                else if (EditProfileControl.gridLanguages.Visibility == Visibility.Visible)
                {
                    EditProfileControl.gridLanguages.Visibility = Visibility.Collapsed;
                    RemoveUserControlFromHeader();
                    e.Handled = true;
                }
                else if (EditProfileControl.gridSpecialities.Visibility == Visibility.Visible)
                {
                    EditProfileControl.gridSpecialities.Visibility = Visibility.Collapsed;
                    RemoveUserControlFromHeader();
                    e.Handled = true;
                }
            }
            else if (SessionData.CurrentView == Pages.ClaimForms)
            {
                if (ClaimFormsControl.gridViewClaimForm.Visibility == Visibility.Visible)
                {
                    SetHamburgerVisibility(Visibility.Visible);
                    ClaimFormsControl.gridViewClaimForm.Visibility = Visibility.Collapsed;
                    HeaderText.Text = "Claim Forms";
                    e.Handled = true;
                }
                else if (ClaimFormsControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    if (ClaimFormsControl.createClaimForm.GridEnterPasswordPopUp.Visibility == Visibility.Visible)
                    {
                        SetSaveButtonVisibility(Visibility.Visible);
                        ClaimFormsControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                    }
                    else if (ClaimFormsControl.createClaimForm.gridAdditionalInformationMsg.Visibility == Visibility.Visible)
                    {
                        ClaimFormsControl.createClaimForm.gridAdditionalInformationMsg.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        ClaimFormsControl.createClaimForm.GridSaveAsDraftPopUpWindow.Visibility = Visibility.Visible;
                        SetSaveButtonVisibility(Visibility.Collapsed);
                    }
                    e.Handled = true;
                }
            }
            else if (SessionData.CurrentView == Pages.DraftClaimForm)
            {
                if (DraftClaimFormControl.gridCreateClaimForm.Visibility == Visibility.Visible)
                {
                    if (DraftClaimFormControl.createClaimForm.GridEnterPasswordPopUp.Visibility == Visibility.Visible)
                    {
                        SetSaveButtonVisibility(Visibility.Visible);
                        DraftClaimFormControl.createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                    }
                    else if (DraftClaimFormControl.createClaimForm.gridAdditionalInformationMsg.Visibility == Visibility.Visible)
                    {
                        DraftClaimFormControl.createClaimForm.gridAdditionalInformationMsg.Visibility = Visibility.Collapsed;
                    }
                    else
                    {

                        DraftClaimFormControl.createClaimForm.GridSaveAsDraftPopUpWindow.Visibility = Visibility.Visible;
                        SetSaveButtonVisibility(Visibility.Collapsed);
                    }
                    e.Handled = true;
                }
            }
            //else if (Frame.CanGoBack)
            //{
            //    e.Handled = true;
            //    Frame.GoBack();
            //}
        }
    }
}
