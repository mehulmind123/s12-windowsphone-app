﻿using System;
using Windows.Devices.Geolocation;

namespace S12Doctor.Common
{
    public class LocationService
    {
        private Geolocator locationservice = new Geolocator();

        private static LocationService _locationService;
        public static LocationService Instance
        {
            get { return _locationService ?? (_locationService = new LocationService()); }
            set { _locationService = value; }
        }

        private bool isLocationEnabled;
        public event Action<bool> OnLocationStatusChange = delegate { };

        public event Action<Geoposition> OnPositionChanged = delegate { };

        public bool IsLocationEnabled
        {
            get
            {
                return isLocationEnabled;
            }
            protected set
            {
                if (isLocationEnabled == value) return;
                isLocationEnabled = value;
                OnLocationStatusChange(value);
            }
        }

        private string latitude;
        public string Latitude
        {
            get
            {
                return latitude;
            }
            protected set
            {
                if (latitude == value) return;
                latitude = value;
            }
        }

        private string longitude;
        public string Longitude
        {
            get
            {
                return longitude;
            }
            protected set
            {
                if (longitude == value) return;
                longitude = value;
            }
        }

        private LocationService()
        {
            locationservice.MovementThreshold = 1;
            locationservice.DesiredAccuracy = PositionAccuracy.High;
            locationservice.StatusChanged += Locationservice_StatusChanged;
            locationservice.PositionChanged += Locationservice_PositionChanged;
        }

        private void Locationservice_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            try
            {
                if (isLocationEnabled && NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    latitude = args.Position.Coordinate.Point.Position.Latitude.ToString();
                    longitude = args.Position.Coordinate.Point.Position.Longitude.ToString();
                    OnPositionChanged(args.Position);
                }
            }
            catch (Exception) { }
        }

        private void Locationservice_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            switch (args.Status)
            {
                case PositionStatus.Ready:
                    // Location platform is providing valid data.
                    IsLocationEnabled = true;
                    break;

                case PositionStatus.Initializing:
                    // Location platform is attempting to acquire a fix.
                    IsLocationEnabled = true;
                    break;

                case PositionStatus.NoData:
                    // Location platform could not obtain location data.
                    IsLocationEnabled = false;
                    break;

                case PositionStatus.Disabled:
                    // The permission to access location data is denied by the user or other policies.
                    IsLocationEnabled = false;
                    // Show message to the user to go to location settings.

                    // Clear any cached location data.
                    //UpdateLocationData(null);
                    break;

                case PositionStatus.NotInitialized:
                    // The location platform is not initialized. This indicates that the application
                    // has not made a request for location data.
                    IsLocationEnabled = false;
                    break;

                case PositionStatus.NotAvailable:
                    // The location platform is not available on this version of the OS.
                    IsLocationEnabled = false;
                    break;

                default:
                    IsLocationEnabled = false;
                    // Unknown;
                    break;
            }
        }
    }
}
