﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace S12Doctor.Common
{
    public class Loader
    {
        public static void Show()
        {
            if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).LoaderVisibility(Visibility.Visible);
            else if(S12Doctor.Utilities.SessionData.CurrentApplication == "AMHP")
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).LoaderVisibility(Visibility.Visible);
            else
                ((S12Doctor.WelcomePage)(Window.Current.Content as Frame).Content).LoaderVisibility(Visibility.Visible);
        }

        public static void Hode()
        {
            if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).LoaderVisibility(Visibility.Collapsed);
            else if (S12Doctor.Utilities.SessionData.CurrentApplication == "AMHP")
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).LoaderVisibility(Visibility.Collapsed);
            else
                ((S12Doctor.WelcomePage)(Window.Current.Content as Frame).Content).LoaderVisibility(Visibility.Collapsed);
        }
    }
}
