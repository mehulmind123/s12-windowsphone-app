﻿using S12Doctor.Model.Common;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

namespace S12Doctor
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class WelcomePage : Page
    {
        WebService.CommonServices common;
        public WelcomePage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Show the status bar
            var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
            //statusBar.BackgroundColor = Windows.UI.Colors.Blue;
            statusBar.ForegroundColor = Windows.UI.Colors.White;
            //statusBar.BackgroundOpacity = 1;
            await statusBar.ShowAsync();

            if (!string.IsNullOrEmpty(Helpers.SettingHelper.EmailId) &&
                !string.IsNullOrEmpty(Helpers.SettingHelper.Password) &&
                !string.IsNullOrEmpty(Helpers.SettingHelper.LoginType))
            {
                if (common == null)
                    common = new WebService.CommonServices();

                //MessageDialog msgbox = new MessageDialog("Message to be displayed.");
                //await msgbox.ShowAsync();

                object result = await common.login(Helpers.SettingHelper.EmailId, Helpers.SettingHelper.Password, Helpers.SettingHelper.LoginType);
                if (result is AppUser)
                {
                    if (Helpers.SettingHelper.LoginType == "1")
                        img12Doctor_Tapped(null, null);
                    else
                        imgAMHP_Tapped(null, null);
                }
            }
        }

        public void LoaderVisibility(Visibility visibility)
        {
            LoadingWindow.Visibility = visibility;
        }

        private void imgAMHP_Tapped(object sender, TappedRoutedEventArgs e)
        {
            S12Doctor.Utilities.SessionData.CurrentApplication = "AMHP";
            Frame.Navigate(typeof(AMHPMainPage));
        }

        private void img12Doctor_Tapped(object sender, TappedRoutedEventArgs e)
        {
            S12Doctor.Utilities.SessionData.CurrentApplication = "S12";
            Frame.Navigate(typeof(MainPage));
        }
    }
}
