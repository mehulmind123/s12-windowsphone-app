﻿using System.Collections.Generic;

namespace S12Doctor.Utilities
{
    public class SessionData
    {
        public static string UserName { get; set; }

        public static string Email { get; set; }

        public static Pages CurrentView { get; set; }

        public static Pages DisposableView { get; set; }

        public static string CurrentApplication { get; set; }

        public static string LoginType { get; set; }

        public static string ApiHeaderScheme { get; set; }

        public static string ApiHeaderParameter { get; set; }

        public static Model.Common.AppUser CurrntAppUser { get; set; }

        public static Model.Common.CMS CMSData { get; set; }

        public static Model.Common.CMS CMSTermsConditionData { get; set; }

        public static Model.S12Doctor.UsefulInformationList usefulInfoList {get; set;}

        public static Model.AMHP.UsefulInformationList amhpUsefulInfoList { get; set; }

        public static string amhpUsefulInfo { get; set; }
        public static void ClearSessionData()
        {
            UserName = string.Empty;
            Email = string.Empty;
            CurrentView = Pages.None;
            CurrentView = Pages.None;
            DisposableView = Pages.None;
            CurrentApplication = string.Empty;
            LoginType = string.Empty;
            ApiHeaderScheme = string.Empty;
            ApiHeaderParameter = string.Empty;
            CurrntAppUser = null;
        }
    }
}
