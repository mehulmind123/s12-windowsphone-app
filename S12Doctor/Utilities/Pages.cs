﻿namespace S12Doctor.Utilities
{
    public enum Pages
    {
        None = 0,
        Login = 1,
        Home = 2,
        UsefulInfo = 3,
        ClaimForms = 4,
        ViewClaimForm = 5,
        CreateClaimForm = 6,
        EditProfile = 7,
        DraftClaimForm = 8,
        Setting = 9,
        ChangePassword = 10,
        TermsAndConditions = 11,
        MyFavorites = 12,
        AboutUs = 13,
        PrivacyPolicy = 14,
        HelpAndSupport = 15,
        UserGuide = 16,
    }
}
