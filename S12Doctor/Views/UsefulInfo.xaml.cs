﻿using S12Doctor.Model;
using S12Doctor.Model.S12Doctor;
using S12Doctor.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Windows.ApplicationModel.Calls;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Views
{
    public sealed partial class UsefulInfo : UserControl
    {
        WebService.S12DoctorServices s12DoctorServices;
        UsefulInformationList usefulinfo;
        List<ParentGroup> usefulInfoData;
        private bool incall = false, endoflist = false, fatchData = false;
        private int offset = 0;
        public UsefulInfo()
        {
            this.InitializeComponent();
            this.Loaded += UsefulInfo_Loaded;
            
        }

        private void UsefulInfo_Loaded(object sender, RoutedEventArgs e)
        {
            if (s12DoctorServices == null)
                s12DoctorServices = new WebService.S12DoctorServices();

            LoadData(offset);
        }

        //CollectionViewSource cvs;
        public async void LoadData(int _offset, int _limit = 10)
        {
            if(Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                object result = await s12DoctorServices.usefulInformation(_offset.ToString(), _limit.ToString());
                
                if (result is UsefulInformationList)
                {
                    if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    {
                        usefulinfo = (UsefulInformationList)result;
                        if(usefulinfo.Data.Count != 0)
                        {
                            SessionData.usefulInfoList = ((UsefulInformationList)result);
                        }
                    }
                    else
                    {
                        SessionData.usefulInfoList = ((UsefulInformationList)result);
                    }
                    IEnumerable<ParentGroup> groups =
                    from item in SourceData.GetData((UsefulInformationList)result)
                    group item by item.GroupID
                        into parentGroup
                    let parentGroupItems =
                        from item2 in parentGroup
                        group item2 by new { item2.GroupHeader, item2.HeaderVisibility }
                        into childGroup
                        select new ChildGroup(childGroup)
                        {
                            GroupHeader = childGroup.Key.GroupHeader,
                            HeaderVisibility = childGroup.Key.HeaderVisibility,
                        }
                    select new ParentGroup(parentGroupItems)
                    {
                        GroupID = parentGroup.Key
                    };

                    if (usefulInfoData == null)
                        usefulInfoData = new List<ParentGroup>();

                    usefulInfoData.AddRange(groups);

                    offset = (int)((UsefulInformationList)result).Meta.NewOffset;
                    fatchData = false;

                    var cvs = (CollectionViewSource)Resources["src"];
                    cvs.Source = usefulInfoData;
                }
                else
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
            } else
            {
                try
                {
                    object infoResult = S12Doctor.Utilities.SessionData.usefulInfoList;
                    if (infoResult is UsefulInformationList)
                    {
                        IEnumerable<ParentGroup> groups =
                        from item in SourceData.GetData((UsefulInformationList)infoResult)
                        group item by item.GroupID
                            into parentGroup
                        let parentGroupItems =
                            from item2 in parentGroup
                            group item2 by new { item2.GroupHeader, item2.HeaderVisibility }
                            into childGroup
                            select new ChildGroup(childGroup)
                            {
                                GroupHeader = childGroup.Key.GroupHeader,
                                HeaderVisibility = childGroup.Key.HeaderVisibility,
                            }
                        select new ParentGroup(parentGroupItems)
                        {
                            GroupID = parentGroup.Key
                        };

                        if (usefulInfoData == null)
                            usefulInfoData = new List<ParentGroup>();

                        usefulInfoData.AddRange(groups);

                        //offset = (int)((UsefulInformationList)infoResult).Meta.NewOffset;
                        //fatchData = false;

                        var cvs = (CollectionViewSource)Resources["src"];
                        cvs.Source = usefulInfoData;
                    }
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                }catch(Exception ex) { }
            }
            
        }

        private void listUseFullInfo_Loaded(object sender, RoutedEventArgs e)
        {
            ScrollViewer viewer = GetScrollViewer(this.listUseFullInfo);
            viewer.ViewChanged += MainPage_ViewChanged;
        }

        private void MainPage_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer view = (ScrollViewer)sender;
            double progress = view.VerticalOffset / view.ScrollableHeight;
            System.Diagnostics.Debug.WriteLine(progress);

            if (progress == 1 & !incall && !endoflist)
            {
                incall = true;
                fetchCountries(offset);
            }
        }

        private void reqHp_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            TextBlock obj = (TextBlock)sender;
            if(obj.Text != "N/A")
            PhoneCallManager.ShowPhoneCallUI(obj.Text, "");
        }

        private void fetchCountries(int offset)
        {
            if (!fatchData)
            {
                if (offset > 0)
                {
                    LoadData(offset);
                    fatchData = true;
                }
                else
                {
                    endoflist = true;
                }
            }
            incall = false;
        }

        // method to pull out a ScrollViewer
        private ScrollViewer GetScrollViewer(DependencyObject depObj)
        {
            if (depObj is ScrollViewer) return depObj as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);
                var result = GetScrollViewer(child);
                if (result != null) return result;
            }

            return null;
        }
    }
}
