﻿using S12Doctor.Common;
using System;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace S12Doctor.Views
{
    public sealed partial class Setting : UserControl
    {
        public Setting()
        {
            this.InitializeComponent();

            this.Loaded += Setting_Loaded;
            LocationService.Instance.OnLocationStatusChange += Instance_OnLocationStatusChange;
        }

        private async void Instance_OnLocationStatusChange(bool obj)
        {
            string uri = "ms-appx:///Assets/Setting/gps_enabled.png";

            if (!obj)
                uri = "ms-appx:///Assets/Setting/gps_disabled.png";

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                 imgGPS.Source = new BitmapImage(new Uri(
                           uri, UriKind.Absolute)));
        }

        private void Setting_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            string uri = "ms-appx:///Assets/Setting/gps_enabled.png";

            if (!LocationService.Instance.IsLocationEnabled)
                uri = "ms-appx:///Assets/Setting/gps_disabled.png";

            imgGPS.Source = new BitmapImage(new Uri(
                           uri, UriKind.Absolute));
        }
    }
}
