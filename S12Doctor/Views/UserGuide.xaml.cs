﻿using S12Doctor.Model.Common;
using S12Doctor.WebService;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media;
using System.IO;
using System.Diagnostics;
using System;
using Windows.Storage;

namespace S12Doctor.Views
{
    public sealed partial class UserGuide : UserControl
    {
        private S12DoctorServices s12DoctorServices;
        private AMHPServices amhpServices;

        public UserGuide()
        {
            this.InitializeComponent();
            this.Loaded += UserGuide_Loaded;
        }

        private  void UserGuide_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (s12DoctorServices == null)
                s12DoctorServices = new S12DoctorServices();

            if (amhpServices == null)
                amhpServices = new AMHPServices();
           
            SolidColorBrush headerStatusTextSolidColorBrush;
            headerStatusTextSolidColorBrush = new SolidColorBrush();

            if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
            {
                headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 19, 113, 185);
                btnAccept.Background = headerStatusTextSolidColorBrush;
                
                webViewer.Source = new Uri("ms-appx-web:///Assets/S12-App-Doctor-User-Guide.html");
                
            
            }
            else
            {
                webViewer.Source = new Uri("ms-appx-web:///Assets/S12-App-AMHP-User-Guide.html");

                headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 192, 181);
                btnAccept.Background = headerStatusTextSolidColorBrush;
            }
        }

        public void SetTermsConditionsVisibility(Visibility visibility)
        {
            btnAccept.Visibility = visibility;
        }

        async private void btnAccept_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            try
            {
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                {
                    object result = await s12DoctorServices.acceptTermsConditions();
                    if (result == null)
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).AddControl(Utilities.Pages.ChangePassword);
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                    }
                    else
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }
                }
                else
                {
                    object result = await amhpServices.acceptTermsConditions();
                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).AddControl(Utilities.Pages.ChangePassword);
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                        }
                        else
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }
                }
            }
            catch { }
        }
    }
}
