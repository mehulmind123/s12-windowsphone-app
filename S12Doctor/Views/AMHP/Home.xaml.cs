﻿using S12Doctor.Model;
using S12Doctor.Model.AMHP;
using S12Doctor.Model.S12Doctor;
using S12Doctor.WebService;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Windows.ApplicationModel.Email;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;

namespace S12Doctor.Views.AMHP
{
    public sealed partial class Home : UserControl
    {
        private const string addressBookFile = "MyAddressBookData";
        private const string LocationDataFile = "MyLocationData";

        SearchQueries searchQuerie;
        //AddressBook addressBook;
        AMHPServices amhpServices;

        public double Latitude = 0.0;
        public double Longitude = 0.0;

        public bool IsAddressBookVisible = true;
        public bool IsSearch = false;
        public bool IsLocationSearch = false;

        ObservableCollection<AddressBookDoctorData> addressBookList;
        ObservableCollection<AddressBookDoctorData> addressBookSearchList;

        ObservableCollection<AddressBookDoctorData> locationSearchList;
        ObservableCollection<AddressBookDoctorData> locationSearchListSearchData;

        private bool incallAddressBook = false, endoflistAddressBook = false, fatchDataAddressBook = false;
        private int offsetAddressBook = 0;

        private bool incallLocation = false, endoflistLocation = false, fatchDataLocation = false;
        private int offsetLocation = 0;
        //string gPath = "";
        //SQLiteConnection conn;

        public Home()
        {
            this.InitializeComponent();
            this.Loaded += Home_Loaded;

            ResetFilters();
            ResetSortby();
            addressBookSortHiglight();
            addressBookFilterHiglight();
            locationSortHiglight();
            locationFilterHiglight();
        }

        private void Home_Loaded(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Helpers.SettingHelper.IsAMHPLoginFirstTime))
            {
                Helpers.SettingHelper.IsAMHPLoginFirstTime = "1";
            }

            //try
            //{
            //    string dpPath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "Person.db3");
            //    var db = new SQLiteConnection(dpPath);
            //    db.CreateTable<AddressBookDoctor>();
            //    AddressBookDoctor tbl = new AddressBookDoctor();
            //    tbl.Name = "Abc";
            //    tbl.Address = "Test";
            //    db.Insert(tbl);
            //}
            //catch (Exception ex)
            //{
            //}

            if (amhpServices == null)
                amhpServices = new AMHPServices();

            if (searchQuerie == null)
                searchQuerie = new SearchQueries();

            if (addressBookList == null)
                addressBookList = new ObservableCollection<AddressBookDoctorData>();

            if (locationSearchList == null)
                locationSearchList = new ObservableCollection<AddressBookDoctorData>();

            listAddressBook.ItemsSource = addressBookList;

            gridViewTabs.Visibility = Visibility.Collapsed;
            Grid.SetRow(gridAddressBookData, 0);
            Grid.SetRowSpan(gridAddressBookData, 2);

            GetLocalAuthority();
            GetSpeciality();
            GetLanguage();

            SetMapData();

            GetOffsetDataForAddressBook(0);
            //try
            //{
            //    conn.Insert(new AddressBookDoctor()
            //    {
            //        Name = "Abc",
            //        Address = "Pqr"
            //    });
            //}
            //catch(Exception) { }
        }

        async private void GetLanguage()
        {
            try
            {
                object result = await amhpServices.languageList();

                if (result is LanguageList)
                    cbLanguage.DataContext = ((LanguageList)result).Data;
                else
                {
                    bool isFileExist;

                    try
                    {
                        var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(addressBookFile);
                        string content = string.Empty;
                        using (StreamReader reader = new StreamReader(myStream))
                        {
                            content = await reader.ReadToEndAsync();
                            isFileExist = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        isFileExist = false;
                    }
                    if (!isFileExist)
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        async private void GetSpeciality()
        {
            try
            {
                object result = await amhpServices.specialityList();

                if (result is SpecialityList)
                    cbSpeciality.DataContext = ((SpecialityList)result).Data;
                else
                {
                    bool isFileExist;

                    try
                    {
                        var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(addressBookFile);
                        string content = string.Empty;
                        using (StreamReader reader = new StreamReader(myStream))
                        {
                            content = await reader.ReadToEndAsync();
                            isFileExist = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        isFileExist = false;
                    }
                    if (!isFileExist)
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }
                }
                 
            }
            catch (Exception ex)
            {
            }
        }

        async private void GetLocalAuthority()
        {
            try
            {
                object result = await amhpServices.localAuthorityList();

                if (result is Model.AMHP.LocalAuthorityList)
                    cbLocalAuthority.DataContext = ((Model.AMHP.LocalAuthorityList)result).Data;
                else
                {
                    bool isFileExist;

                    try
                    {
                        var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(addressBookFile);
                        string content = string.Empty;
                        using (StreamReader reader = new StreamReader(myStream))
                        {
                            content = await reader.ReadToEndAsync();
                            isFileExist = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        isFileExist = false;
                    }
                    if (!isFileExist)
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }
                }
                    
            }
            catch (Exception ex)
            {
            }
        }

        string addressBookFilter_availability = string.Empty;
        string addressBookFilter_weeklyAvailability = string.Empty;
        string addressBookFilter_timeSlotAvailability = string.Empty;
        string addressBookFilter_local_authority = string.Empty;
        string addressBookFilter_distance = string.Empty;
        string addressBookFilter_employer = string.Empty;
        string addressBookFilter_specialty = string.Empty;
        string addressBookFilter_gender = string.Empty;
        string addressBookFilter_language = string.Empty;

        string addressBookSortby_availability = string.Empty;
        string addressBookSortby_employer = string.Empty;
        string addressBookSortby_distance = string.Empty;
        string addressBookSortby_name = string.Empty;

        bool isAddressBookFilterd = false;
        bool isAddressBookSorted = false;

        private void addressBookFilterHiglight()
        {
            if (isAddressBookFilterd)
                imgFilter.Source = new BitmapImage(new Uri(
                       "ms-appx:///Assets/AMHP/HomeScreen/filterSelected.png", UriKind.Absolute));
            else
                imgFilter.Source = new BitmapImage(new Uri(
                       "ms-appx:///Assets/AMHP/HomeScreen/filter.png", UriKind.Absolute));

            SetDefaultAddressBookFilter();
        }

        private void addressBookSortHiglight()
        {
            if (isAddressBookSorted)
                imgSort.Source = new BitmapImage(new Uri(
                       "ms-appx:///Assets/AMHP/HomeScreen/sortbySelected.png", UriKind.Absolute));
            else
                imgSort.Source = new BitmapImage(new Uri(
                       "ms-appx:///Assets/AMHP/HomeScreen/sortby.png", UriKind.Absolute));

            SetDefaultAddressBookSort();
        }

        private void addressBookSort()
        {
            addressBookSortby_availability = addressBookSortby_employer = addressBookSortby_name = string.Empty;

            if (chkAvailability.IsChecked == true)
                addressBookSortby_availability = "1";

            if (rbtnTtoI.IsChecked == true)
                addressBookSortby_employer = "1";
            if (rbtnItoT.IsChecked == true)
                addressBookSortby_employer = "2";

            if (rbtnAtoZ.IsChecked == true)
                addressBookSortby_name = "1";
            if (rbtnZtoA.IsChecked == true)
                addressBookSortby_name = "2";

            if (string.IsNullOrEmpty(addressBookSortby_availability) &&
                string.IsNullOrEmpty(addressBookSortby_employer) &&
                string.IsNullOrEmpty(addressBookSortby_name))
            {
                isAddressBookSorted = false;
            }
            else
                isAddressBookSorted = true;

            addressBookSortHiglight();
        }

        private void addressBookFilter()
        {
            addressBookFilter_availability = string.Empty;
            addressBookFilter_weeklyAvailability = string.Empty;
            addressBookFilter_timeSlotAvailability = string.Empty;
            addressBookFilter_local_authority = string.Empty;
            addressBookFilter_distance = string.Empty;
            addressBookFilter_employer = string.Empty;
            addressBookFilter_specialty = string.Empty;
            addressBookFilter_gender = string.Empty;
            addressBookFilter_language = string.Empty;

            if (btnAvailable.Tag != null && btnAvailable.Tag.ToString() == "2")
                addressBookFilter_availability = "1";
            if (btnMaybeavailable.Tag != null && btnMaybeavailable.Tag.ToString() == "2")
                addressBookFilter_availability = string.IsNullOrEmpty(addressBookFilter_availability) ? "2" : addressBookFilter_availability + ",2";
            if (btnUnavailable.Tag != null && btnUnavailable.Tag.ToString() == "2")
                addressBookFilter_availability = string.IsNullOrEmpty(addressBookFilter_availability) ? "3" : addressBookFilter_availability + ",3";
            if (btnUnknown.Tag != null && btnUnknown.Tag.ToString() == "2")
                addressBookFilter_availability = string.IsNullOrEmpty(addressBookFilter_availability) ? "4" : addressBookFilter_availability + ",4";
            //WeeklyAvailability
            if (btnMon.Tag != null && btnMon.Tag.ToString() == "2")
                addressBookFilter_weeklyAvailability = "1";
            if (btnTue.Tag != null && btnTue.Tag.ToString() == "2")
                addressBookFilter_weeklyAvailability = string.IsNullOrEmpty(addressBookFilter_weeklyAvailability) ? "2" : addressBookFilter_weeklyAvailability + ",2";
            if (btnWed.Tag != null && btnWed.Tag.ToString() == "2")
                addressBookFilter_weeklyAvailability = string.IsNullOrEmpty(addressBookFilter_weeklyAvailability) ? "3" : addressBookFilter_weeklyAvailability + ",3";
            if (btnThu.Tag != null && btnThu.Tag.ToString() == "2")
                addressBookFilter_weeklyAvailability = string.IsNullOrEmpty(addressBookFilter_weeklyAvailability) ? "4" : addressBookFilter_weeklyAvailability + ",4";
            if (btnFri.Tag != null && btnFri.Tag.ToString() == "2")
                addressBookFilter_weeklyAvailability = string.IsNullOrEmpty(addressBookFilter_weeklyAvailability) ? "5" : addressBookFilter_weeklyAvailability + ",5";
            if (btnSat.Tag != null && btnSat.Tag.ToString() == "2")
                addressBookFilter_weeklyAvailability = string.IsNullOrEmpty(addressBookFilter_weeklyAvailability) ? "6" : addressBookFilter_weeklyAvailability + ",6";
            if (btnSun.Tag != null && btnSun.Tag.ToString() == "2")
                addressBookFilter_weeklyAvailability = string.IsNullOrEmpty(addressBookFilter_weeklyAvailability) ? "7" : addressBookFilter_weeklyAvailability + ",7";

            //weeklyTimeSlote
            if (btnMorning.Tag != null && btnMorning.Tag.ToString() == "2")
                addressBookFilter_timeSlotAvailability = "1";
            if (btnLunch.Tag != null && btnLunch.Tag.ToString() == "2")
                addressBookFilter_timeSlotAvailability = string.IsNullOrEmpty(addressBookFilter_timeSlotAvailability) ? "2" : addressBookFilter_timeSlotAvailability + ",2";
            if (btnAfternoon.Tag != null && btnAfternoon.Tag.ToString() == "2")
                addressBookFilter_timeSlotAvailability = string.IsNullOrEmpty(addressBookFilter_timeSlotAvailability) ? "3" : addressBookFilter_timeSlotAvailability + ",3";
            if (btnEvening.Tag != null && btnEvening.Tag.ToString() == "2")
                addressBookFilter_timeSlotAvailability = string.IsNullOrEmpty(addressBookFilter_timeSlotAvailability) ? "4" : addressBookFilter_timeSlotAvailability + ",4";
            if (btnNight.Tag != null && btnNight.Tag.ToString() == "2")
                addressBookFilter_timeSlotAvailability = string.IsNullOrEmpty(addressBookFilter_timeSlotAvailability) ? "5" : addressBookFilter_timeSlotAvailability + ",5";


            if (cbLocalAuthority.SelectedIndex > -1)
                addressBookFilter_local_authority = ((Model.AMHP.LocalAuthority)cbLocalAuthority.SelectedItem).AuthorityId.ToString();

            if (btnTrust.Tag != null && btnTrust.Tag.ToString() == "1")
                addressBookFilter_employer = "1";
            if (btnIndependent.Tag != null && btnIndependent.Tag.ToString() == "1")
                addressBookFilter_employer = "2";

            if (cbSpeciality.SelectedIndex > -1)
                addressBookFilter_specialty = ((Speciality)cbSpeciality.SelectedItem).SpecialtiesId.ToString();

            if (btnFemale.Tag != null && btnFemale.Tag.ToString() == "1")
                addressBookFilter_gender = "1";
            if (btnMale.Tag != null && btnMale.Tag.ToString() == "1")
                addressBookFilter_gender = "2";

            if (cbLanguage.SelectedIndex > -1)
                addressBookFilter_language = ((Language)cbLanguage.SelectedItem).LanguageId.ToString();

            if (string.IsNullOrEmpty(addressBookFilter_availability) &&
                string.IsNullOrEmpty(addressBookFilter_weeklyAvailability) &&
                string.IsNullOrEmpty(addressBookFilter_timeSlotAvailability) &&
                string.IsNullOrEmpty(addressBookFilter_local_authority) &&
                string.IsNullOrEmpty(addressBookFilter_distance) &&
                string.IsNullOrEmpty(addressBookFilter_employer) &&
                string.IsNullOrEmpty(addressBookFilter_specialty) &&
                string.IsNullOrEmpty(addressBookFilter_gender) &&
                string.IsNullOrEmpty(addressBookFilter_language))
            {
                isAddressBookFilterd = false;
            }
            else
                isAddressBookFilterd = true;

            addressBookFilterHiglight();
        }

        public void SetDefaultAddressBookFilter()
        {
            SetGenderStatus(3);
            SetEmployerStatus(3);
            btnAvailable.Tag = "2";
            btnMaybeavailable.Tag = "2";
            btnUnavailable.Tag = "2";
            btnUnknown.Tag = "2";
            SetStatus(1);
            SetStatus(2);
            SetStatus(3);
            SetStatus(4);

            btnMon.Tag = "2";
            btnTue.Tag = "2";
            btnWed.Tag = "2";
            btnThu.Tag = "2";
            btnFri.Tag = "2";
            btnSat.Tag = "2";
            btnSun.Tag = "2";

            SetWeeklyStatus(1, btnMon);
            SetWeeklyStatus(2, btnTue);
            SetWeeklyStatus(3, btnWed);
            SetWeeklyStatus(4, btnThu);
            SetWeeklyStatus(5, btnFri);
            SetWeeklyStatus(6, btnSat);
            SetWeeklyStatus(7, btnSun);

            btnMorning.Tag = "2";
            btnLunch.Tag = "2";
            btnAfternoon.Tag = "2";
            btnEvening.Tag = "2";
            btnNight.Tag = "2";

            SetWeeklyStatus(1, btnMorning);
            SetWeeklyStatus(2, btnLunch);
            SetWeeklyStatus(3, btnAfternoon);
            SetWeeklyStatus(4, btnEvening);
            SetWeeklyStatus(5, btnNight);

            cbLocalAuthority.SelectedIndex = -1;
            cbSpeciality.SelectedIndex = -1;
            cbLanguage.SelectedIndex = -1;
            sliderDistance.Value = 10;

            if (addressBookFilter_availability.Contains("1"))
                SetStatus(1);
            if (addressBookFilter_availability.Contains("2"))
                SetStatus(2);
            if (addressBookFilter_availability.Contains("3"))
                SetStatus(3);
            if (addressBookFilter_availability.Contains("4"))
                SetStatus(4);

            if (addressBookFilter_weeklyAvailability.Contains("1"))
                SetWeeklyStatus(1, btnMon);
            if (addressBookFilter_weeklyAvailability.Contains("2"))
                SetWeeklyStatus(2, btnTue);
            if (addressBookFilter_weeklyAvailability.Contains("3"))
                SetWeeklyStatus(3, btnWed);
            if (addressBookFilter_weeklyAvailability.Contains("4"))
                SetWeeklyStatus(4, btnThu);
            if (addressBookFilter_weeklyAvailability.Contains("5"))
                SetWeeklyStatus(5, btnFri);
            if (addressBookFilter_weeklyAvailability.Contains("6"))
                SetWeeklyStatus(6, btnSat);
            if (addressBookFilter_weeklyAvailability.Contains("7"))
                SetWeeklyStatus(7, btnSun);

            if (addressBookFilter_timeSlotAvailability.Contains("1"))
                SetWeeklyStatus(1, btnMorning);
            if (addressBookFilter_timeSlotAvailability.Contains("2"))
                SetWeeklyStatus(2, btnLunch);
            if (addressBookFilter_timeSlotAvailability.Contains("3"))
                SetWeeklyStatus(3, btnAfternoon);
            if (addressBookFilter_timeSlotAvailability.Contains("4"))
                SetWeeklyStatus(4, btnEvening);
            if (addressBookFilter_timeSlotAvailability.Contains("5"))
                SetWeeklyStatus(5, btnNight);

            if (!string.IsNullOrEmpty(addressBookFilter_local_authority))
                cbLocalAuthority.SelectedItem = cbLocalAuthority.Items.Where(e => ((Model.AMHP.LocalAuthority)e).AuthorityId == Convert.ToInt64(addressBookFilter_local_authority)).FirstOrDefault();

            //if (!string.IsNullOrEmpty(addressBookFilter_distance))
            //    sliderDistance.Value = Convert.ToDouble(addressBookFilter_distance);

            if (!string.IsNullOrEmpty(addressBookFilter_employer))
                SetEmployerStatus(Convert.ToInt32(addressBookFilter_employer));

            if (!string.IsNullOrEmpty(addressBookFilter_specialty))
                cbSpeciality.SelectedItem = cbSpeciality.Items.Where(e => ((Speciality)e).SpecialtiesId == Convert.ToInt64(addressBookFilter_specialty)).FirstOrDefault();

            if (!string.IsNullOrEmpty(addressBookFilter_gender))
                SetGenderStatus(Convert.ToInt32(addressBookFilter_gender));

            if (!string.IsNullOrEmpty(addressBookFilter_language))
                cbLanguage.SelectedItem = cbLanguage.Items.Where(e => ((Language)e).LanguageId == Convert.ToInt64(addressBookFilter_language)).FirstOrDefault();
        }

        internal void DisposeMemory()
        {
            mapControl.MapElements.Clear();
            mapControl = null;
        }

        public void SetDefaultAddressBookSort()
        {
            chkAvailability.IsChecked = false;
            //chkDistance.IsChecked = false;
            rbtnTtoI.IsChecked = false;
            rbtnItoT.IsChecked = false;
            rbtnAtoZ.IsChecked = false;
            rbtnZtoA.IsChecked = false;

            if (!string.IsNullOrEmpty(addressBookSortby_availability))
                chkAvailability.IsChecked = true;

            if (addressBookSortby_employer == "1")
                rbtnTtoI.IsChecked = true;
            else if (addressBookSortby_employer == "2")
                rbtnItoT.IsChecked = true;

            //if (addressBookSortby_distance == "1")
            //    chkDistance.IsChecked = true;

            if (addressBookSortby_name == "1")
                rbtnAtoZ.IsChecked = true;
            else if (addressBookSortby_name == "2")
                rbtnZtoA.IsChecked = true;
        }

        //type:1/2 (1-Address Book Tab, 2-Location Tab)
        //search_text:
        //filter_availability:comma separated availability status id(1,2,3,4)
        //filter_local_authority:Local authority id
        //filter_distance: pass selected distance range
        //filter_employer: 1/2 (1-trust, 2-independent)
        //filter_specialty:speciality id
        //filter_gender: 1/2 (1-female, 2-male)
        //filter_language: language id
        //sortby_availability: 1 (1-sort by availability --> Unknown ASC)
        //sortby_employer: 1/2 (1- Sort by Trust, 2-Sort by independent)
        //sortby_distance: 1/0 (1- Nearest First, 0- DESC by distance)
        //sortby_name:1/2 (1- A to Z, 2- Z to A)
        //longitude: Pass current longitude(for location tab)
        //latitude: Pass current latitude(for location tab)
        ScrollViewer viewer;
        private async void GetOffsetDataForAddressBook(int _offset, int count = 20, bool isReload = false)
        {
            try
            {
                addressBookSort();

                addressBookFilter();
                object result=null;

                bool isFileExist;
                if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {

                    try
                    {
                        var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(addressBookFile);
                        string content = string.Empty;
                        using (StreamReader reader = new StreamReader(myStream))
                        {
                            content = await reader.ReadToEndAsync();
                            isFileExist = true;
                        }
                        //((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                    }
                    catch (Exception ex)
                    {
                        isFileExist = false;
                    }

                }
                else
                {
                    isFileExist = false;
                }

                if (isFileExist)
                {
                    /// read from file
                    //var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(XMLFILENAME);

                    //DataContractSerializer stuffSerializer = new DataContractSerializer(typeof(AddressBookDoctorList));

                    //var setResult = (AddressBookDoctorList)stuffSerializer.ReadObject(myStream);
                    //result = setResult;

                  
                    var Serializer = new DataContractSerializer(typeof(ObservableCollection<AddressBookDoctorData>));
                    using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(addressBookFile))
                    {
                        if(addressBookList.Count==0)
                        {
                            addressBookList = (ObservableCollection<AddressBookDoctorData>)Serializer.ReadObject(stream);
                            listAddressBook.ItemsSource = addressBookList;

                        }

                    }
                }
                else
                {
                    result = await amhpServices.homeScreen("1", _offset.ToString(), count.ToString(),
                                                 "", addressBookFilter_availability,
                                                 addressBookFilter_local_authority, addressBookFilter_distance,
                                                 addressBookFilter_employer, addressBookFilter_specialty,
                                                 addressBookFilter_gender, addressBookFilter_language,
                                                 addressBookSortby_availability, addressBookSortby_employer,
                                                 addressBookSortby_distance, addressBookSortby_name,
                                                 "", "", addressBookFilter_weeklyAvailability,addressBookFilter_timeSlotAvailability);

                }
                if (result is AddressBookDoctorList)
                {
              
                    var data = new ObservableCollection<AddressBookDoctorData>(((AddressBookDoctorList)result).Data);
                    offsetAddressBook = 0;
                    if (addressBookList == null)
                        addressBookList = new ObservableCollection<AddressBookDoctorData>();

                    if (isReload)
                        addressBookList.Clear();

                    int i = offsetAddressBook + 1;
                    foreach (var item in data)
                    {
                        addressBookList.Add(item);
                    }
                    ////write to file
                    StorageFile userdetailsfile = await ApplicationData.Current.LocalFolder.CreateFileAsync(addressBookFile, CreationCollisionOption.ReplaceExisting);
                    IRandomAccessStream raStream = await userdetailsfile.OpenAsync(FileAccessMode.ReadWrite);

                    using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
                    {

                        // Serialize the Session State. 

                        DataContractSerializer serializer = new DataContractSerializer(typeof(ObservableCollection<AddressBookDoctorData>));

                        serializer.WriteObject(outStream.AsStreamForWrite(), addressBookList);

                        await outStream.FlushAsync();
                        outStream.Dispose(); //  
                        raStream.Dispose();
                    }


                    if (viewer == null)
                    {
                        viewer = GetScrollViewer(this.listAddressBook);
                        viewer.ViewChanged += MainPage_ViewChanged;
                    }

                    if (addressBookList.Count <= 0)
                    {
                        txtNoResultsFound.Visibility = Visibility.Visible;
                        listAddressBook.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        txtNoResultsFound.Visibility = Visibility.Collapsed;
                        listAddressBook.Visibility = Visibility.Visible;
                    }


                    offsetAddressBook = (int)((AddressBookDoctorList)result).Meta.NewOffset;
                    fatchDataAddressBook = false;

                    if (isReload)
                        incallAddressBook = endoflistAddressBook = fatchDataAddressBook = false;
                }
                else
                {
                    if (viewer == null)
                    {
                        viewer = GetScrollViewer(this.listAddressBook);
                        viewer.ViewChanged += MainPage_ViewChanged;
                    }

                    if (addressBookList.Count <= 0)
                    {
                        txtNoResultsFound.Visibility = Visibility.Visible;
                        listAddressBook.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        txtNoResultsFound.Visibility = Visibility.Collapsed;
                        listAddressBook.Visibility = Visibility.Visible;
                    }
                        

                    offsetAddressBook = addressBookList.Count;
                    fatchDataAddressBook = false;

                    if (isReload)
                        incallAddressBook = endoflistAddressBook = fatchDataAddressBook = false;

                }
                //((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");

            }
            catch (Exception ex)
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

       
       
        string locationFilter_availability = string.Empty;
        string locationFilter_weeklyAvailability = string.Empty;
        string locationFilter_timeSlotAvailability = string.Empty;
        string locationFilter_local_authority = string.Empty;
        string locationFilter_distance = string.Empty;
        string locationFilter_employer = string.Empty;
        string locationFilter_specialty = string.Empty;
        string locationFilter_gender = string.Empty;
        string locationFilter_language = string.Empty;

        string locationSortby_availability = string.Empty;
        string locationSortby_employer = string.Empty;
        string locationSortby_distance = string.Empty;
        string locationSortby_name = string.Empty;

        bool isLocationFilterd = false;
        bool isLocationSorted = false;

        private void locationFilterHiglight()
        {
            if (isLocationFilterd)
                imgFilter.Source = new BitmapImage(new Uri(
                       "ms-appx:///Assets/AMHP/HomeScreen/filterSelected.png", UriKind.Absolute));
            else
                imgFilter.Source = new BitmapImage(new Uri(
                       "ms-appx:///Assets/AMHP/HomeScreen/filter.png", UriKind.Absolute));

            SetDefaultLocationFilter();
        }

        private void locationSortHiglight()
        {
            if (isLocationSorted)
                imgSort.Source = new BitmapImage(new Uri(
                       "ms-appx:///Assets/AMHP/HomeScreen/sortbySelected.png", UriKind.Absolute));
            else
                imgSort.Source = new BitmapImage(new Uri(
                       "ms-appx:///Assets/AMHP/HomeScreen/sortby.png", UriKind.Absolute));

            SetDefaultLocationSort();
        }

        private void locationSort()
        {
            locationSortby_availability = string.Empty;
            locationSortby_employer = string.Empty;
            locationSortby_distance = string.Empty;
            locationSortby_name = string.Empty;

            if (chkAvailability.IsChecked == true)
                locationSortby_availability = "1";

            if (rbtnTtoI.IsChecked == true)
                locationSortby_employer = "1";
            if (rbtnItoT.IsChecked == true)
                locationSortby_employer = "2";

            locationSortby_distance = chkDistance.IsChecked.HasValue ? chkDistance.IsChecked.Value == true ? "1" : "0" : "0";

            if (rbtnAtoZ.IsChecked == true)
                locationSortby_name = "1";
            if (rbtnZtoA.IsChecked == true)
                locationSortby_name = "2";

            if (string.IsNullOrEmpty(locationSortby_availability) &&
                string.IsNullOrEmpty(locationSortby_employer) &&
                locationSortby_distance == "0" &&
                string.IsNullOrEmpty(locationSortby_name))
            {
                isLocationSorted = false;
            }
            else
                isLocationSorted = true;

            locationSortHiglight();
        }

        private void locationFilter()
        {
            locationFilter_availability = string.Empty;
            locationFilter_weeklyAvailability = string.Empty;
            locationFilter_timeSlotAvailability = string.Empty;
            locationFilter_local_authority = string.Empty;
            locationFilter_distance = string.Empty;
            locationFilter_employer = string.Empty;
            locationFilter_specialty = string.Empty;
            locationFilter_gender = string.Empty;
            locationFilter_language = string.Empty;

            if (btnAvailable.Tag != null && btnAvailable.Tag.ToString() == "2")
                locationFilter_availability = "1";
            if (btnMaybeavailable.Tag != null && btnMaybeavailable.Tag.ToString() == "2")
                locationFilter_availability = string.IsNullOrEmpty(locationFilter_availability) ? "2" : locationFilter_availability + ",2";
            if (btnUnavailable.Tag != null && btnUnavailable.Tag.ToString() == "2")
                locationFilter_availability = string.IsNullOrEmpty(locationFilter_availability) ? "3" : locationFilter_availability + ",3";
            if (btnUnknown.Tag != null && btnUnknown.Tag.ToString() == "2")
                locationFilter_availability = string.IsNullOrEmpty(locationFilter_availability) ? "4" : locationFilter_availability + ",4";

            //WeeklyAvailability
            if (btnMon.Tag != null && btnMon.Tag.ToString() == "2")
                locationFilter_weeklyAvailability = "1";
            if (btnTue.Tag != null && btnTue.Tag.ToString() == "2")
                locationFilter_weeklyAvailability = string.IsNullOrEmpty(locationFilter_weeklyAvailability) ? "2" : locationFilter_weeklyAvailability + ",2";
            if (btnWed.Tag != null && btnWed.Tag.ToString() == "2")
                locationFilter_weeklyAvailability = string.IsNullOrEmpty(locationFilter_weeklyAvailability) ? "3" : locationFilter_weeklyAvailability + ",3";
            if (btnThu.Tag != null && btnThu.Tag.ToString() == "2")
                locationFilter_weeklyAvailability = string.IsNullOrEmpty(locationFilter_weeklyAvailability) ? "4" : locationFilter_weeklyAvailability + ",4";
            if (btnFri.Tag != null && btnFri.Tag.ToString() == "2")
                locationFilter_weeklyAvailability = string.IsNullOrEmpty(locationFilter_weeklyAvailability) ? "5" : locationFilter_weeklyAvailability + ",5";
            if (btnSat.Tag != null && btnSat.Tag.ToString() == "2")
                locationFilter_weeklyAvailability = string.IsNullOrEmpty(locationFilter_weeklyAvailability) ? "6" : locationFilter_weeklyAvailability + ",6";
            if (btnSun.Tag != null && btnSun.Tag.ToString() == "2")
                locationFilter_weeklyAvailability = string.IsNullOrEmpty(locationFilter_weeklyAvailability) ? "7" : locationFilter_weeklyAvailability + ",7";

            //weeklyTimeSlote
            if (btnMorning.Tag != null && btnMorning.Tag.ToString() == "2")
                locationFilter_timeSlotAvailability = "1";
            if (btnLunch.Tag != null && btnLunch.Tag.ToString() == "2")
                locationFilter_timeSlotAvailability = string.IsNullOrEmpty(locationFilter_timeSlotAvailability) ? "2" : locationFilter_timeSlotAvailability + ",2";
            if (btnAfternoon.Tag != null && btnAfternoon.Tag.ToString() == "2")
                locationFilter_timeSlotAvailability = string.IsNullOrEmpty(locationFilter_timeSlotAvailability) ? "3" : locationFilter_timeSlotAvailability + ",3";
            if (btnEvening.Tag != null && btnEvening.Tag.ToString() == "2")
                locationFilter_timeSlotAvailability = string.IsNullOrEmpty(locationFilter_timeSlotAvailability) ? "4" : locationFilter_timeSlotAvailability + ",4";
            if (btnNight.Tag != null && btnNight.Tag.ToString() == "2")
                locationFilter_timeSlotAvailability = string.IsNullOrEmpty(locationFilter_timeSlotAvailability) ? "5" : locationFilter_timeSlotAvailability + ",5";

            if (cbLocalAuthority.SelectedIndex > 0)
                locationFilter_local_authority = ((Model.AMHP.LocalAuthority)cbLocalAuthority.SelectedItem).AuthorityId.ToString();

            locationFilter_distance = sliderDistance.Value.ToString();

            if (btnTrust.Tag != null)
                if (btnTrust.Tag.ToString() == "1")
                    locationFilter_employer = "1";
            if (btnIndependent.Tag != null)
                if (btnIndependent.Tag.ToString() == "1")
                    locationFilter_employer = "2";

            if (cbSpeciality.SelectedIndex > -1)
                locationFilter_specialty = ((Speciality)cbSpeciality.SelectedItem).SpecialtiesId.ToString();

            if (btnFemale.Tag != null)
                if (btnFemale.Tag.ToString() == "1")
                    locationFilter_gender = "1";
            if (btnMale.Tag != null)
                if (btnMale.Tag.ToString() == "1")
                    locationFilter_gender = "2";

            if (cbLanguage.SelectedIndex > -1)
                locationFilter_language = ((Language)cbLanguage.SelectedItem).LanguageId.ToString();

            if (string.IsNullOrEmpty(locationFilter_availability) &&
                string.IsNullOrEmpty(locationFilter_weeklyAvailability) &&
                string.IsNullOrEmpty(locationFilter_timeSlotAvailability) &&
                string.IsNullOrEmpty(locationFilter_local_authority) &&
                locationFilter_distance == "10" &&
                string.IsNullOrEmpty(locationFilter_employer) &&
                string.IsNullOrEmpty(locationFilter_specialty) &&
                string.IsNullOrEmpty(locationFilter_gender) &&
                string.IsNullOrEmpty(locationFilter_language))
            {
                isLocationFilterd = false;
            }
            else
                isLocationFilterd = true;

            locationFilterHiglight();
        }

        public void SetDefaultLocationFilter()
        {
            SetGenderStatus(3);
            SetEmployerStatus(3);
            btnAvailable.Tag = "2";
            btnMaybeavailable.Tag = "2";
            btnUnavailable.Tag = "2";
            btnUnknown.Tag = "2";
            SetStatus(1);
            SetStatus(2);
            SetStatus(3);
            SetStatus(4);

            btnMon.Tag = "2";
            btnTue.Tag = "2";
            btnWed.Tag = "2";
            btnThu.Tag = "2";
            btnFri.Tag = "2";
            btnSat.Tag = "2";
            btnSun.Tag = "2";

            SetWeeklyStatus(1, btnMon);
            SetWeeklyStatus(2, btnTue);
            SetWeeklyStatus(3, btnWed);
            SetWeeklyStatus(4, btnThu);
            SetWeeklyStatus(5, btnFri);
            SetWeeklyStatus(6, btnSat);
            SetWeeklyStatus(7, btnSun);

            btnMorning.Tag = "2";
            btnLunch.Tag = "2";
            btnAfternoon.Tag = "2";
            btnEvening.Tag = "2";
            btnNight.Tag = "2";

            SetWeeklyStatus(1, btnMorning);
            SetWeeklyStatus(2, btnLunch);
            SetWeeklyStatus(3, btnAfternoon);
            SetWeeklyStatus(4, btnEvening);
            SetWeeklyStatus(5, btnNight);

            cbLocalAuthority.SelectedIndex = -1;
            cbSpeciality.SelectedIndex = -1;
            cbLanguage.SelectedIndex = -1;
            sliderDistance.Value = 10;

            if (locationFilter_availability.Contains("1"))
                SetStatus(1);
            if (locationFilter_availability.Contains("2"))
                SetStatus(2);
            if (locationFilter_availability.Contains("3"))
                SetStatus(3);
            if (locationFilter_availability.Contains("4"))
                SetStatus(4);

            if (locationFilter_weeklyAvailability.Contains("1"))
                SetWeeklyStatus(1, btnMon);
            if (locationFilter_weeklyAvailability.Contains("2"))
                SetWeeklyStatus(2, btnTue);
            if (locationFilter_weeklyAvailability.Contains("3"))
                SetWeeklyStatus(3, btnWed);
            if (locationFilter_weeklyAvailability.Contains("4"))
                SetWeeklyStatus(4, btnThu);
            if (locationFilter_weeklyAvailability.Contains("5"))
                SetWeeklyStatus(5, btnFri);
            if (locationFilter_weeklyAvailability.Contains("6"))
                SetWeeklyStatus(6, btnSat);
            if (locationFilter_weeklyAvailability.Contains("7"))
                SetWeeklyStatus(7, btnSun);

            if (locationFilter_timeSlotAvailability.Contains("1"))
                SetWeeklyStatus(1, btnMorning);
            if (locationFilter_timeSlotAvailability.Contains("2"))
                SetWeeklyStatus(2, btnLunch);
            if (locationFilter_timeSlotAvailability.Contains("3"))
                SetWeeklyStatus(3, btnAfternoon);
            if (locationFilter_timeSlotAvailability.Contains("4"))
                SetWeeklyStatus(4, btnEvening);
            if (locationFilter_timeSlotAvailability.Contains("5"))
                SetWeeklyStatus(5, btnNight);

            if (!string.IsNullOrEmpty(locationFilter_local_authority))
                cbLocalAuthority.SelectedItem = cbLocalAuthority.Items.Where(e => ((Model.AMHP.LocalAuthority)e).AuthorityId == Convert.ToInt64(locationFilter_local_authority)).FirstOrDefault();

            if (!string.IsNullOrEmpty(locationFilter_distance))
                sliderDistance.Value = Convert.ToDouble(locationFilter_distance);

            if (!string.IsNullOrEmpty(locationFilter_employer))
                SetEmployerStatus(Convert.ToInt32(locationFilter_employer));

            if (!string.IsNullOrEmpty(locationFilter_specialty))
                cbSpeciality.SelectedItem = cbSpeciality.Items.Where(e => ((Speciality)e).SpecialtiesId == Convert.ToInt64(locationFilter_specialty)).FirstOrDefault();

            if (!string.IsNullOrEmpty(locationFilter_gender))
                SetGenderStatus(Convert.ToInt32(locationFilter_gender));

            if (!string.IsNullOrEmpty(locationFilter_language))
                cbLanguage.SelectedItem = cbLanguage.Items.Where(e => ((Language)e).LanguageId == Convert.ToInt64(locationFilter_language)).FirstOrDefault();
        }

        public void SetDefaultLocationSort()
        {
            chkAvailability.IsChecked = false;
            chkDistance.IsChecked = false;
            rbtnTtoI.IsChecked = false;
            rbtnItoT.IsChecked = false;
            rbtnAtoZ.IsChecked = false;
            rbtnZtoA.IsChecked = false;

            if (!string.IsNullOrEmpty(locationSortby_availability))
                chkAvailability.IsChecked = true;

            if (locationSortby_employer == "1")
                rbtnTtoI.IsChecked = true;
            else if (locationSortby_employer == "2")
                rbtnItoT.IsChecked = true;

            if (locationSortby_distance == "1")
                chkDistance.IsChecked = true;

            if (locationSortby_name == "1")
                rbtnAtoZ.IsChecked = true;
            else if (locationSortby_name == "2")
                rbtnZtoA.IsChecked = true;
        }

        private async void GetOffsetDataForLocation(int _offset, int count = 20, bool isReload = false)
        {
            try
            {
                locationSort();

                locationFilter();
                object result = null;

                bool isFileExist;
                if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    try
                    {
                        var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(LocationDataFile);
                        string content = string.Empty;
                        using (StreamReader reader = new StreamReader(myStream))
                        {
                            content = await reader.ReadToEndAsync();
                            isFileExist = true;
                        }
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                    }
                    catch (Exception ex)
                    {
                        isFileExist = false;
                    }
                } else
                {
                    isFileExist = false;
                }

                if (isFileExist)
                {
                    var Serializer = new DataContractSerializer(typeof(ObservableCollection<AddressBookDoctorData>));
                    using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(LocationDataFile))
                    {
                        if (locationSearchList.Count == 0)
                        {
                            locationSearchList = (ObservableCollection<AddressBookDoctorData>)Serializer.ReadObject(stream);
                            listAddressBook.ItemsSource = locationSearchList;
                            AddPinsInMapControl();
                        }
                    }
                } else
                {
                    result = await amhpServices.homeScreen("2", _offset.ToString(), count.ToString(),
                    "", locationFilter_availability,
                    locationFilter_local_authority, locationFilter_distance,
                    locationFilter_employer, locationFilter_specialty,
                    locationFilter_gender, locationFilter_language,
                    locationSortby_availability, locationSortby_employer,
                    locationSortby_distance, locationSortby_name,
                    Longitude.ToString(), Latitude.ToString(), locationFilter_weeklyAvailability, locationFilter_timeSlotAvailability);
                }
                

                if (result is AddressBookDoctorList)
                {
                    var data = new ObservableCollection<AddressBookDoctorData>(((AddressBookDoctorList)result).Data);

                    offsetLocation = 0;
                    if (locationSearchList == null)
                        locationSearchList = new ObservableCollection<AddressBookDoctorData>();

                    if (isReload)
                        locationSearchList.Clear();

                    int i = offsetLocation + 1;
                    foreach (var item in data)
                    {
                        item.IsMilesVisible = true;
                        locationSearchList.Add(item);
                    }

                    ////write to file
                    StorageFile userdetailsfile = await ApplicationData.Current.LocalFolder.CreateFileAsync(LocationDataFile, CreationCollisionOption.ReplaceExisting);
                    IRandomAccessStream raStream = await userdetailsfile.OpenAsync(FileAccessMode.ReadWrite);

                    using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
                    {

                        // Serialize the Session State. 

                        DataContractSerializer serializer = new DataContractSerializer(typeof(ObservableCollection<AddressBookDoctorData>));

                        serializer.WriteObject(outStream.AsStreamForWrite(), locationSearchList);

                        await outStream.FlushAsync();
                        outStream.Dispose(); //  
                        raStream.Dispose();
                    }

                    AddPinsInMapControl();

                    if (locationSearchList.Count <= 0)
                    {
                        txtNoResultsFound.Visibility = Visibility.Visible;
                        listAddressBook.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        txtNoResultsFound.Visibility = Visibility.Collapsed;
                        listAddressBook.Visibility = Visibility.Visible;
                    }


                    offsetLocation = (int)((AddressBookDoctorList)result).Meta.NewOffset;
                    fatchDataLocation = false;

                    if (isReload)
                        incallLocation = endoflistLocation = fatchDataLocation = false;
                } else
                {
                    if (viewer == null)
                    {
                        viewer = GetScrollViewer(this.listAddressBook);
                        viewer.ViewChanged += MainPage_ViewChanged;
                    }

                    if (addressBookList.Count <= 0)
                    {
                        txtNoResultsFound.Visibility = Visibility.Visible;
                        listAddressBook.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        txtNoResultsFound.Visibility = Visibility.Collapsed;
                        listAddressBook.Visibility = Visibility.Visible;
                    }


                    offsetAddressBook = locationSearchList.Count;
                    fatchDataAddressBook = false;

                    if (isReload)
                        incallAddressBook = endoflistAddressBook = fatchDataAddressBook = false;
                }  
            }
            catch (Exception ex)
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        private async void AutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            // Only get results when it was a user typing, 
            // otherwise assume the value got filled in by TextMemberPath 
            // or the handler for SuggestionChosen.
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                //Set the ItemsSource to be your filtered dataset
                var matchingContinents = await searchQuerie.GetMatchingContinents(sender.Text);
                sender.ItemsSource = matchingContinents.ToList();
            }
        }

        private async void AutoSuggestBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            try
            {
                var continents = args.SelectedItem as Continents;

                var data = await searchQuerie.GetLatLong(continents.name);
                Latitude = 0.0;
                Longitude = 0.0;
                //var location = new Geopoint(new BasicGeoposition()
                //{

                //    Latitude = data.Results.FirstOrDefault().Geometry.Location.Latitude,
                //    Longitude = data.Results.FirstOrDefault().Geometry.Location.Longitude
                //});

                Latitude = data.Results.FirstOrDefault().Geometry.Location.Latitude;
                Longitude = data.Results.FirstOrDefault().Geometry.Location.Longitude;

                var location = new Geopoint(new BasicGeoposition()
                {
                    //Latitude = data.Results.FirstOrDefault().Geometry.Location.Latitude,
                    //Longitude = data.Results.FirstOrDefault().Geometry.Location.Longitude
                    Latitude = Latitude,
                    Longitude = Longitude
                });

                // Set the map center point.
                mapControl.Center = location;
                mapControl.ZoomLevel = 15;

                // Various display options
                mapControl.LandmarksVisible = false;
                mapControl.TrafficFlowVisible = false;
                mapControl.PedestrianFeaturesVisible = false;

                GetOffsetDataForLocation(0, isReload: true);

                //string.Format("{0}", continents.name);
            }
            catch (Exception ex) {
                message(ex.Message, "ERROR!");
            }
        }

        private void btnOpenDoctorDetailView_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    if (IsAddressBookVisible)
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenDoctorDetailPage((IsSearch)? (addressBookSearchList[listAddressBook.SelectedIndex].DoctorId) : (addressBookList[listAddressBook.SelectedIndex].DoctorId));
                        listAddressBook.SelectedIndex = -1;
                    }
                    else
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenDoctorDetailPage((IsLocationSearch)? (locationSearchListSearchData[listAddressBook.SelectedIndex].DoctorId) : (locationSearchList[listAddressBook.SelectedIndex].DoctorId));
                        listAddressBook.SelectedIndex = -1;
                    }
                }   
            }
            catch { }
        }

        AddressBookDoctorData addressBookData;
        private void btnOpenActionView_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                if (listAddressBook.SelectedIndex != -1)
                {
                    if (IsAddressBookVisible)
                        addressBookData = addressBookList[listAddressBook.SelectedIndex];
                    else
                        addressBookData = locationSearchList[listAddressBook.SelectedIndex];

                    gridAction.Visibility = Visibility.Visible;

                    txtSelectedDoctorName.Text = addressBookData.Name;

                    if (addressBookData.IsFavourite)
                        lblMarkAsFavouritOrUnfavourite.Text = "Mark as Unfavourite";
                    else
                        lblMarkAsFavouritOrUnfavourite.Text = "Mark as Favourite";
                }
            }
            catch { }
        }

        private void btnAvailable_Click(object sender, RoutedEventArgs e)
        {
            SetStatus(1);
        }

        private void btnMaybeavailable_Click(object sender, RoutedEventArgs e)
        {
            SetStatus(2);
        }

        private void btnUnavailable_Click(object sender, RoutedEventArgs e)
        {
            SetStatus(3);
        }

        private void btnUnknown_Click(object sender, RoutedEventArgs e)
        {
            SetStatus(4);
        }

        private void btnTrust_Click(object sender, RoutedEventArgs e)
        {
            SetEmployerStatus(1);
        }

        private void btnIndependent_Click(object sender, RoutedEventArgs e)
        {
            SetEmployerStatus(2);
        }

        private void btnMale_Click(object sender, RoutedEventArgs e)
        {
            SetGenderStatus(2);
        }

        private void btnFemale_Click(object sender, RoutedEventArgs e)
        {
            SetGenderStatus(1);
        }

        public void ListBoxMultipleSelectionMode(bool isMulti)
        {
            try
            {
                if (isMulti)
                    listAddressBook.SelectionMode = ListViewSelectionMode.Multiple;
                else
                {
                    if (listAddressBook.SelectedItems.Count > 0)
                        listAddressBook.SelectedItems.Clear();
                    listAddressBook.SelectionMode = ListViewSelectionMode.Single;
                }
            }
            catch (Exception) { }
        }

        async public void SendSelectedContectsSMS()
        {
            if (listAddressBook.SelectedItems.Count > 0)
            {
                Windows.ApplicationModel.Chat.ChatMessage msg
                         = new Windows.ApplicationModel.Chat.ChatMessage();

                foreach (AddressBookDoctorData item in listAddressBook.SelectedItems)
                {
                    msg.Recipients.Add(item.MobileNumber);
                    amhpServices.contactDoctor(item.DoctorId.ToString(), "4");
                }
                
                msg.Body = string.Format("Dear Dr," + Environment.NewLine + "" + Environment.NewLine + "I am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]." + Environment.NewLine + "Are you available to attend? if so, please reply to this message or phone [number] for more information." + Environment.NewLine + Environment.NewLine + "Kind regards,");

                await Windows.ApplicationModel.Chat.ChatMessageManager.ShowComposeSmsMessageAsync(msg);
            }

            ListBoxMultipleSelectionMode(false);
        }

        private void bdrAddressBook_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (addressBookList.Count <= 0)
                txtNoResultsFound.Visibility = Visibility.Visible;
            else
                txtNoResultsFound.Visibility = Visibility.Collapsed;

            ListBoxMultipleSelectionMode(false);
            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).MultiSelectButtonVisible();

            listAddressBook.ItemsSource = addressBookList;

            addressBookSortHiglight();
            addressBookFilterHiglight();

            IsAddressBookVisible = true;
            //gridMap.Visibility = Visibility.Collapsed;
            imgSort.Visibility = Visibility.Visible;
            autoSuggerstBoxLocationSearch.Visibility = Visibility.Collapsed;
            txtsearchs12address.Visibility = Visibility.Visible;
            gridAddressBookData.Visibility = Visibility.Visible;

            gridViewTabs.Visibility = Visibility.Collapsed;
            Grid.SetRow(gridAddressBookData, 0);
            Grid.SetRowSpan(gridAddressBookData, 2);

            SetAddressLocationStatus(1);
            txtsearchs12address.Text = "";
            gridMiles.Visibility = Visibility.Collapsed;
            gridDistance.Visibility = Visibility.Collapsed;
            if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
            }
        }

        private void bdrLocationSearch_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (locationSearchList.Count <= 0)
                txtNoResultsFound.Visibility = Visibility.Visible;
            else
                txtNoResultsFound.Visibility = Visibility.Collapsed;

            ListBoxMultipleSelectionMode(false);
            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).MultiSelectButtonCollabpsed();

            locationSortHiglight();
            locationFilterHiglight();

            IsAddressBookVisible = false;
            //bdrListView_Tapped(null, null);
            bdrMapView_Tapped(null, null);
            gridViewTabs.Visibility = Visibility.Visible;
            Grid.SetRow(gridAddressBookData, 1);
            Grid.SetRowSpan(gridAddressBookData, 1);

            SetAddressLocationStatus(2);

            gridMiles.Visibility = Visibility.Visible;
            gridDistance.Visibility = Visibility.Visible;
        }

        public async void SetMapData()
        {

            // Get the user's current location.
            Geolocator geolocator = new Geolocator();
            Geoposition geoposition = null;
            try
            {
                geoposition = await geolocator.GetGeopositionAsync();
            }
            catch (Exception ex)
            {

                //message(ex.Message, "ERROR!");

                // Make sure you have defined ID_CAP_LOCATION in the application manifest 
                // and that on your phone, you have turned on location by checking 
                // Settings > Location.

                // Handle errors like unauthorized access to location services
            }

            geoposition = null;

            try
            {
                var currentlocation = await searchQuerie.GetCurrentLocation();
                Latitude = currentlocation.Location.Latitude;
                Longitude = currentlocation.Location.Longitude;
            }
            catch (Exception)
            { }

            //var data = await searchQuerie.GetLatLong("London");

            var location = new Geopoint(new BasicGeoposition()
            {
                //Latitude = data.Results.FirstOrDefault().Geometry.Location.Latitude,
                //Longitude = data.Results.FirstOrDefault().Geometry.Location.Longitude
                Latitude = Latitude,
                Longitude = Longitude
            });

            // Set the map center point.
            try
            {
                mapControl.Center = location;
                mapControl.ZoomLevel = 15;

                // Various display options
                mapControl.LandmarksVisible = false;
                mapControl.TrafficFlowVisible = false;
                mapControl.PedestrianFeaturesVisible = false;
            }
            catch(Exception ex)
            {

            }
            GetOffsetDataForLocation(0);


            // zoom level

        }

        private async void AddPinsInMapControl()
        {
            if (locationSearchList.Count > 0)
            {
                int i = 0;
                foreach (var item in locationSearchList)
                {
                    //// Create the map icon
                    //MapIcon mapIcon = new MapIcon();

                    //if (item.StatusText == "Available")
                    //    mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/AMHP/LocationSearch/green_pin.png"));
                    //else if (item.StatusText == "May be available")
                    //    mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/AMHP/LocationSearch/yellow_pin.png"));
                    //else if (item.StatusText == "Unavailable")
                    //    mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/AMHP/LocationSearch/red_pin.png"));
                    //else
                    //    mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/AMHP/LocationSearch/grey_pin.png"));

                    //mapIcon.NormalizedAnchorPoint = new Point(0.5, 0.5);

                    //var loc = new Geopoint(new BasicGeoposition()
                    //{
                    //    Latitude = Convert.ToDouble(item.Latitude),
                    //    Longitude = Convert.ToDouble(item.Longitude)
                    //});

                    //mapIcon.Location = loc;
                    //// Get the text to display above the map icon from the resource files.
                    //string title = item.DoctorName;
                    //mapIcon.Title = title;

                    //// Add the map icon to the map elements collection.
                    //mapControl.MapElements.Add(mapIcon);
                    BasicGeoposition location = new BasicGeoposition();
                    location.Latitude = Convert.ToDouble(item.Latitude);
                    location.Longitude = Convert.ToDouble(item.Longitude);
                    Uri imgSource;
                    if (item.StatusText == "Available")
                        imgSource = new Uri("ms-appx:///Assets/AMHP/LocationSearch/green_pin.png");
                    else if (item.StatusText == "May be available")
                        imgSource = new Uri("ms-appx:///Assets/AMHP/LocationSearch/yellow_pin.png");
                    else if (item.StatusText == "Unavailable")
                        imgSource =new Uri("ms-appx:///Assets/AMHP/LocationSearch/red_pin.png");
                    else
                        imgSource =new Uri("ms-appx:///Assets/AMHP/LocationSearch/grey_pin.png");

                    StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(imgSource);
                    IRandomAccessStream fileStream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                    BitmapImage image = new BitmapImage();
                    i = i + 1;
                    image.SetSource(fileStream);
                    var pin = new Image()
                    {
                        Source = image,
                        Width = 50,
                        Height = 50,
                    };
                    pin.Tag = item.DoctorId;
                    pin.Tapped += pin_Tapped;

                    Windows.UI.Xaml.Controls.Maps.MapControl.SetLocation(pin, new Geopoint(location));
                    mapControl.Children.Add(pin);
                }
            }
        }
        private void pin_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                Image img = (Image)sender;
                int tag = Convert.ToInt16(img.Tag);
                AddressBookDoctorData data = locationSearchList.Where(d => d.DoctorId == tag).FirstOrDefault();

                if (data != null && data.IsFavourite)
                    imgFavoritePin.Source = new BitmapImage(new Uri("ms-appx:///Assets/AMHP/HomeScreen/selected_star.png", UriKind.RelativeOrAbsolute));
                else
                    imgFavoritePin.Source = new BitmapImage(new Uri("ms-appx:///Assets/AMHP/HomeScreen/unselect_star.png", UriKind.RelativeOrAbsolute));

                gridMapPinViewer.Visibility = Visibility.Visible;
                
                gridMapPinViewerData.DataContext = data;
            }
            catch (Exception ex)
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        private async void message(string body, string title)
        {
            var dlg = new MessageDialog(
                    string.Format(body), title);
            try
            {
                await dlg.ShowAsync();
            }
            catch (Exception) { }
        }

        public void SetAddressLocationStatus(int status)
        {
            SolidColorBrush foregroundSolidColorBrush;
            SolidColorBrush backgroundStatusTextSolidColorBrush;

            backgroundStatusTextSolidColorBrush = new SolidColorBrush();
            foregroundSolidColorBrush = new SolidColorBrush();
            backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
            foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);

            bdrAddressBook.Background = bdrLocationSearch.Background = backgroundStatusTextSolidColorBrush;

            lblAddtessBook.Foreground = bdrAddressBook.BorderBrush =
                lblLocationSearch.Foreground = bdrLocationSearch.BorderBrush = foregroundSolidColorBrush;

            switch (status)
            {
                case 1:
                    backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush = new SolidColorBrush();
                    backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                    bdrAddressBook.Background = bdrAddressBook.BorderBrush = backgroundStatusTextSolidColorBrush;
                    lblAddtessBook.Foreground = foregroundSolidColorBrush;
                    break;
                case 2:
                    backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush = new SolidColorBrush();
                    backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                    bdrLocationSearch.Background = bdrLocationSearch.BorderBrush = backgroundStatusTextSolidColorBrush;
                    lblLocationSearch.Foreground = foregroundSolidColorBrush;
                    break;
                default:
                    break;
            }
        }

        public void SetGenderStatus(int status)
        {
            SolidColorBrush foregroundSolidColorBrush;
            SolidColorBrush backgroundStatusTextSolidColorBrush;

            backgroundStatusTextSolidColorBrush = new SolidColorBrush();
            foregroundSolidColorBrush = new SolidColorBrush();
            backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
            foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);

            btnMale.Background = btnFemale.Background = backgroundStatusTextSolidColorBrush;

            btnMale.Foreground = btnMale.BorderBrush =
                btnFemale.Foreground = btnFemale.BorderBrush = foregroundSolidColorBrush;

            btnMale.Tag = "0";
            btnFemale.Tag = "0";

            switch (status)
            {
                case 1:
                    backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush = new SolidColorBrush();
                    backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                    btnFemale.Background = btnFemale.BorderBrush = backgroundStatusTextSolidColorBrush;
                    btnFemale.Foreground = foregroundSolidColorBrush;
                    btnFemale.Tag = "1";
                    break;
                case 2:
                    backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush = new SolidColorBrush();
                    backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                    btnMale.Background = btnMale.BorderBrush = backgroundStatusTextSolidColorBrush;
                    btnMale.Foreground = foregroundSolidColorBrush;
                    btnMale.Tag = "1";
                    break;
                default:
                    break;
            }
        }

        public void SetEmployerStatus(int status)
        {
            SolidColorBrush foregroundSolidColorBrush;
            SolidColorBrush backgroundStatusTextSolidColorBrush;

            backgroundStatusTextSolidColorBrush = new SolidColorBrush();
            foregroundSolidColorBrush = new SolidColorBrush();
            backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
            foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);

            btnTrust.Background = btnIndependent.Background = backgroundStatusTextSolidColorBrush;

            btnTrust.Foreground = btnTrust.BorderBrush =
                btnIndependent.Foreground = btnIndependent.BorderBrush = foregroundSolidColorBrush;

            btnTrust.Tag = "0";
            btnIndependent.Tag = "0";

            switch (status)
            {
                case 1:
                    backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush = new SolidColorBrush();
                    backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                    btnTrust.Background = btnTrust.BorderBrush = backgroundStatusTextSolidColorBrush;
                    btnTrust.Foreground = foregroundSolidColorBrush;
                    btnTrust.Tag = "1";
                    break;
                case 2:
                    backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush = new SolidColorBrush();
                    backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                    btnIndependent.Background = btnIndependent.BorderBrush = backgroundStatusTextSolidColorBrush;
                    btnIndependent.Foreground = foregroundSolidColorBrush;
                    btnIndependent.Tag = "1";
                    break;
                default:
                    break;
            }
        }

        public void SetStatus(int status)
        {
            SolidColorBrush foregroundSolidColorBrush;
            SolidColorBrush backgroundStatusTextSolidColorBrush;

            switch (status)
            {
                case 1:
                    if (btnAvailable.Tag.ToString() == "1")
                    {
                        backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush = new SolidColorBrush();
                        backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                        btnAvailable.Background = backgroundStatusTextSolidColorBrush;
                        btnAvailable.BorderBrush = backgroundStatusTextSolidColorBrush;
                        btnAvailable.Foreground = foregroundSolidColorBrush;
                        btnAvailable.Tag = "2";
                    }
                    else
                    {
                        backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush = new SolidColorBrush();
                        backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);
                        btnAvailable.Background = backgroundStatusTextSolidColorBrush;
                        btnAvailable.Foreground =
                        btnAvailable.BorderBrush = foregroundSolidColorBrush;
                        btnAvailable.Tag = "1";
                    }
                    break;
                case 2:
                    if (btnMaybeavailable.Tag.ToString() == "1")
                    {
                        backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush = new SolidColorBrush();
                        backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                        btnMaybeavailable.Background = btnMaybeavailable.BorderBrush = backgroundStatusTextSolidColorBrush;
                        btnMaybeavailable.Foreground = foregroundSolidColorBrush;
                        btnMaybeavailable.Tag = "2";
                    }
                    else
                    {
                        backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush = new SolidColorBrush();
                        backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);
                        btnMaybeavailable.Background = backgroundStatusTextSolidColorBrush;
                        btnMaybeavailable.Foreground =
                        btnMaybeavailable.BorderBrush = foregroundSolidColorBrush;
                        btnMaybeavailable.Tag = "1";
                    }
                    break;
                case 3:
                    if (btnUnavailable.Tag.ToString() == "1")
                    {
                        backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush = new SolidColorBrush();
                        backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                        btnUnavailable.Background = btnUnavailable.BorderBrush = backgroundStatusTextSolidColorBrush;
                        btnUnavailable.Foreground = foregroundSolidColorBrush;
                        btnUnavailable.Tag = "2";
                    }
                    else
                    {
                        backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush = new SolidColorBrush();
                        backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);
                        btnUnavailable.Background = backgroundStatusTextSolidColorBrush;
                        btnUnavailable.Foreground =
                        btnUnavailable.BorderBrush = foregroundSolidColorBrush;
                        btnUnavailable.Tag = "1";
                    }

                    break;
                case 4:
                    if (btnUnknown.Tag.ToString() == "1")
                    {
                        backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush = new SolidColorBrush();
                        backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                        btnUnknown.Background = btnUnknown.BorderBrush = backgroundStatusTextSolidColorBrush;
                        btnUnknown.Foreground = foregroundSolidColorBrush;
                        btnUnknown.Tag = "2";
                    }
                    else
                    {
                        backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush = new SolidColorBrush();
                        backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);
                        btnUnknown.Background = backgroundStatusTextSolidColorBrush;
                        btnUnknown.Foreground =
                        btnUnknown.BorderBrush = foregroundSolidColorBrush;
                        btnUnknown.Tag = "1";
                    }
                    break;
                default:
                    break;
            }
        }


        public void SetWeeklyStatus(int status, object sender)
        {
            SolidColorBrush foregroundSolidColorBrush;
            SolidColorBrush backgroundStatusTextSolidColorBrush;
            Button obj = (Button)sender;
            if (obj.Tag.ToString() == "1")
            {
                backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush = new SolidColorBrush();
                backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);

                obj.Background = btnUnknown.BorderBrush = backgroundStatusTextSolidColorBrush;
                obj.Foreground = foregroundSolidColorBrush;
                obj.Tag = "2";
            }
            else
            {
                backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush = new SolidColorBrush();
                backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);

                obj.Background = backgroundStatusTextSolidColorBrush;
                obj.Foreground = foregroundSolidColorBrush;
                obj.Tag = "1";
            }
            if (btnMon.Tag.ToString() == "1" && btnTue.Tag.ToString() == "1" && btnWed.Tag.ToString() == "1" && btnThu.Tag.ToString() == "1" && btnFri.Tag.ToString() == "1" && btnSat.Tag.ToString() == "1" && btnSun.Tag.ToString() == "1")
            {
                btnMorning.IsHitTestVisible = btnLunch.IsHitTestVisible = btnEvening.IsHitTestVisible = btnAfternoon.IsHitTestVisible = btnNight.IsHitTestVisible = false;
                btnMorning.Tag = btnLunch.Tag = btnEvening.Tag = btnAfternoon.Tag = btnNight.Tag = 1;
                backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush = new SolidColorBrush();
                backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 220, 220, 220);
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);

                btnMorning.Background = btnLunch.Background = btnEvening.Background = btnAfternoon.Background = btnNight.Background = backgroundStatusTextSolidColorBrush;
                btnMorning.Foreground = btnLunch.Foreground = btnEvening.Foreground = btnAfternoon.Foreground = btnNight.Foreground = foregroundSolidColorBrush;

            }
            else
            {
                btnMorning.IsHitTestVisible = btnLunch.IsHitTestVisible = btnEvening.IsHitTestVisible = btnAfternoon.IsHitTestVisible = btnNight.IsHitTestVisible = true;
                backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush = new SolidColorBrush();
                if (btnMorning.Tag.ToString() == "1" && btnLunch.Tag.ToString() == "1" && btnEvening.Tag.ToString() == "1" && btnAfternoon.Tag.ToString() == "1" && btnNight.Tag.ToString() == "1")
                {
                    backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);
                    btnMorning.Background = btnLunch.Background = btnEvening.Background = btnAfternoon.Background = btnNight.Background = backgroundStatusTextSolidColorBrush;
                    btnMorning.Foreground = btnLunch.Foreground = btnEvening.Foreground = btnAfternoon.Foreground = btnNight.Foreground = foregroundSolidColorBrush;


                }
              
            }
        }

        public void SetTimeSlotStatus(int status,object sender)
        {
            SolidColorBrush foregroundSolidColorBrush;
            SolidColorBrush backgroundStatusTextSolidColorBrush;
            Button obj = (Button)sender;
            if (obj.Tag.ToString() == "1")
            {
                backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush = new SolidColorBrush();
                backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 191, 181);
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);

                obj.Background = backgroundStatusTextSolidColorBrush;
                obj.Foreground = foregroundSolidColorBrush;
                obj.Tag = "2";

            }
            else
            {
                backgroundStatusTextSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush = new SolidColorBrush();
                backgroundStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);

                obj.Background = backgroundStatusTextSolidColorBrush;
                obj.Foreground = foregroundSolidColorBrush;
                obj.Tag = "1";
            }
          
        }
        private void btnReset_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ResetFilters();
        }

        private void ResetFilters()
        {
            SetGenderStatus(3);
            SetEmployerStatus(3);
            btnAvailable.Tag = "2";
            btnMaybeavailable.Tag = "2";
            btnUnavailable.Tag = "2";
            btnUnknown.Tag = "2";
            
            SetStatus(1);
            SetStatus(2);
            SetStatus(3);
            SetStatus(4);

            btnMon.Tag = "2";
            btnTue.Tag = "2";
            btnWed.Tag = "2";
            btnThu.Tag = "2";
            btnFri.Tag = "2";
            btnSat.Tag = "2";
            btnSun.Tag = "2";

            SetWeeklyStatus(1, btnMon);
            SetWeeklyStatus(2, btnTue);
            SetWeeklyStatus(3, btnWed);
            SetWeeklyStatus(4, btnThu);
            SetWeeklyStatus(5, btnFri);
            SetWeeklyStatus(6, btnSat);
            SetWeeklyStatus(7, btnSun);

            btnMorning.Tag = "2";
            btnLunch.Tag = "2";
            btnAfternoon.Tag = "2";
            btnEvening.Tag = "2";
            btnNight.Tag = "2";

            SetWeeklyStatus(1, btnMorning);
            SetWeeklyStatus(2, btnLunch);
            SetWeeklyStatus(3, btnAfternoon);
            SetWeeklyStatus(4, btnEvening);
            SetWeeklyStatus(5, btnNight);

            cbLocalAuthority.SelectedIndex = -1;
            cbSpeciality.SelectedIndex = -1;
            cbLanguage.SelectedIndex = -1;
            sliderDistance.Value = 10;
        }

        private void btnApply_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (btnMon.Tag.ToString() == "2" || btnTue.Tag.ToString() == "2" || btnWed.Tag.ToString() == "2" || btnThu.Tag.ToString() == "2" || btnFri.Tag.ToString() == "2" || btnSat.Tag.ToString() == "2" || btnSun.Tag.ToString() == "2")
            {
                    if (btnMorning.Tag.ToString() == "1" && btnLunch.Tag.ToString() == "1" && btnEvening.Tag.ToString() == "1" && btnAfternoon.Tag.ToString() == "1" && btnNight.Tag.ToString() == "1")
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Please select a time slot in which you would like to search for the doctors on the selected day of the week.");
                    } else
                    {
                    spFilters.Visibility = Visibility.Collapsed;

                    try
                    {
                        if (IsAddressBookVisible)
                            GetOffsetDataForAddressBook(0, isReload: true);
                        else
                            GetOffsetDataForLocation(0, isReload: true);

                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).RemoveUserControlFromHeader();
                    }
                    catch { }

                    if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                    }
                }
            } else
            {
                spFilters.Visibility = Visibility.Collapsed;

                try
                {
                    if (IsAddressBookVisible)
                        GetOffsetDataForAddressBook(0, isReload: true);
                    else
                        GetOffsetDataForLocation(0, isReload: true);

                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).RemoveUserControlFromHeader();
                }
                catch { }

                if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                }
            }
            
        }

        private void imgFilter_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            spFilters.Visibility = Visibility.Visible;
            try
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetUserControlOnHeader();
            }
            catch { }
        }

        private void btnResetSortBy_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ResetSortby();
        }

        private void ResetSortby()
        {
            chkAvailability.IsChecked = false;
            chkDistance.IsChecked = false;
            rbtnTtoI.IsChecked = false;
            rbtnItoT.IsChecked = false;
            rbtnAtoZ.IsChecked = false;
            rbtnZtoA.IsChecked = false;
        }

        private void btnApplySortBy_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            GridSortBy.Visibility = Visibility.Collapsed;
            try
            {
                if (IsAddressBookVisible)
                    GetOffsetDataForAddressBook(0, isReload: true);
                else
                    GetOffsetDataForLocation(0, isReload: true);

                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).RemoveUserControlFromHeader();
            }
            catch { }
        }

        private void imgSort_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            GridSortBy.Visibility = Visibility.Visible;
            try
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetUserControlOnHeader();
            }
            catch { }
        }

        private void txtsearchs12address_KeyUp(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (IsAddressBookVisible)
            {
                if (!string.IsNullOrEmpty(txtsearchs12address.Text))
                {
                    //      if (addressBookSearchList == null)
                    addressBookSearchList = new ObservableCollection<AddressBookDoctorData>();

                    var data = addressBookList.Where(x => x.Name.ToLower().Contains(txtsearchs12address.Text.ToLower())).ToList();
                    foreach (var item in data)
                    {
                        addressBookSearchList.Add(item);
                    }
                    IsSearch = true;
                    listAddressBook.ItemsSource = addressBookSearchList;//addressBookList.Where(x => x.Name.ToLower().Contains(txtsearchs12address.Text.ToLower())).ToList();
                }
                else
                {
                    listAddressBook.ItemsSource = addressBookList;
                    IsSearch = false;
                }
                


                if (listAddressBook.Items.Count <= 0)
                    txtNoResultsFound.Visibility = Visibility.Visible;
                else
                    txtNoResultsFound.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtsearchs12address.Text))
                {
                    locationSearchListSearchData = new ObservableCollection<AddressBookDoctorData>();

                    var data = locationSearchList.Where(x => x.Name.ToLower().Contains(txtsearchs12address.Text.ToLower())).ToList();
                    foreach (var item in data)
                    {
                        locationSearchListSearchData.Add(item);
                    }
                    IsLocationSearch = true;

                    listAddressBook.ItemsSource = locationSearchListSearchData;// locationSearchList.Where(x => x.Name.ToLower().Contains(txtsearchs12address.Text.ToLower())).ToList();
                }
                else
                {
                    listAddressBook.ItemsSource = locationSearchList;
                    IsLocationSearch = false;
                }
                    

                if (listAddressBook.Items.Count <= 0)
                    txtNoResultsFound.Visibility = Visibility.Visible;
                else
                    txtNoResultsFound.Visibility = Visibility.Collapsed;
            }
        }

        private void bdrMapView_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ListBoxMultipleSelectionMode(false);
            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).MultiSelectButtonCollabpsed();

            IsAddressBookVisible = false;
            SolidColorBrush foregroundSolidColorBrushForMap = new SolidColorBrush();
            foregroundSolidColorBrushForMap.Color = Color.FromArgb(255, 71, 191, 181);
            txtMapView.Foreground = foregroundSolidColorBrushForMap;
            imgMapView.Source =
            new BitmapImage(new Uri("ms-appx:///Assets/AMHP/LocationSearch/map_select.png", UriKind.RelativeOrAbsolute));

            SolidColorBrush foregroundSolidColorBrushForList = new SolidColorBrush();
            foregroundSolidColorBrushForList.Color = Color.FromArgb(255, 79, 93, 96);
            txtListView.Foreground = foregroundSolidColorBrushForList;
            imgListView.Source =
            new BitmapImage(new Uri("ms-appx:///Assets/AMHP/LocationSearch/list_unselect.png", UriKind.RelativeOrAbsolute));

            imgSort.Visibility = Visibility.Collapsed;

            autoSuggerstBoxLocationSearch.Visibility = Visibility.Visible;
            txtsearchs12address.Visibility = Visibility.Collapsed;

            //gridMap.Visibility = Visibility.Visible;
            gridAddressBookData.Visibility = Visibility.Collapsed;
        }

        private void bdrListView_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ListBoxMultipleSelectionMode(false);
            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).MultiSelectButtonVisible();

            listAddressBook.ItemsSource = locationSearchList;

            IsAddressBookVisible = false;
            SolidColorBrush foregroundSolidColorBrushForList = new SolidColorBrush();
            foregroundSolidColorBrushForList.Color = Color.FromArgb(255, 71, 191, 181);
            txtListView.Foreground = foregroundSolidColorBrushForList;
            imgListView.Source =
            new BitmapImage(new Uri("ms-appx:///Assets/AMHP/LocationSearch/list_select.png", UriKind.RelativeOrAbsolute));

            SolidColorBrush foregroundSolidColorBrushForMap = new SolidColorBrush();
            foregroundSolidColorBrushForMap.Color = Color.FromArgb(255, 79, 93, 96);
            txtMapView.Foreground = foregroundSolidColorBrushForMap;
            imgMapView.Source =
            new BitmapImage(new Uri("ms-appx:///Assets/AMHP/LocationSearch/map_unselect.png", UriKind.RelativeOrAbsolute));

            imgSort.Visibility = Visibility.Visible;

            autoSuggerstBoxLocationSearch.Visibility = Visibility.Collapsed;
            txtsearchs12address.Visibility = Visibility.Visible;

            //gridMap.Visibility = Visibility.Collapsed;
            gridAddressBookData.Visibility = Visibility.Visible;
        }

        private void lblCallMobile_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            amhpServices.contactDoctor(addressBookData.DoctorId.ToString(), "2");
            Windows.ApplicationModel
            .Calls.PhoneCallManager
            .ShowPhoneCallUI(addressBookData.MobileNumber, addressBookData.Name);
        }

        private void lblCallLandline_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            amhpServices.contactDoctor(addressBookData.DoctorId.ToString(), "3");
            Windows.ApplicationModel
           .Calls.PhoneCallManager
           .ShowPhoneCallUI(addressBookData.LandlineNumber, addressBookData.Name);
        }

        private void btnCallCancel_Click(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gridCallPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void gridCallBack_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gridCallPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void gridActionCallBack_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gridAction.Visibility = Visibility.Collapsed;
            listAddressBook.SelectedIndex = -1;
        }

        private void lblActionCall_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(addressBookData.MobileNumber) && !string.IsNullOrWhiteSpace(addressBookData.LandlineNumber))
                gridCallPopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
            else if (!string.IsNullOrWhiteSpace(addressBookData.MobileNumber))
            {
                lblCallMobile_Tapped(null, null);
            }
            else if (!string.IsNullOrWhiteSpace(addressBookData.LandlineNumber))
            {
                lblCallLandline_Tapped(null, null);
            }
            else
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Sorry, no contact numbers are available for this doctor.");
            }
            gridAction.Visibility = Visibility.Collapsed;
            listAddressBook.SelectedIndex = -1;
        }

        private async void lblActionSMS_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(addressBookData.MobileNumber))
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Sorry, no mobile number is available for this doctor.");
            else
            {
                amhpServices.contactDoctor(addressBookData.DoctorId.ToString(), "4");

                Windows.ApplicationModel.Chat.ChatMessage msg
                         = new Windows.ApplicationModel.Chat.ChatMessage();
                msg.Body = string.Format("Dear Dr {0}," + Environment.NewLine + "" + Environment.NewLine + "I am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]." + Environment.NewLine + "Are you available to attend? if so, please reply to this message or phone [number] for more information." + Environment.NewLine + Environment.NewLine + "Kind regards," + Environment.NewLine + Environment.NewLine + Utilities.SessionData.CurrntAppUser.Data.Name, addressBookData.Name);
                msg.Recipients.Add(addressBookData.MobileNumber);

                await Windows.ApplicationModel.Chat.ChatMessageManager.ShowComposeSmsMessageAsync(msg);
            }
            gridAction.Visibility = Visibility.Collapsed;
            listAddressBook.SelectedIndex = -1;
        }

        private async void lblActionEmail_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(addressBookData.Email))
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Sorry, no email address is available for this doctor.");
            else
            {
                try
                {
                    amhpServices.contactDoctor(addressBookData.DoctorId.ToString(), "1");
                    EmailRecipient sendTo = new EmailRecipient()
                    {
                        Address = addressBookData.Email,
                    };

                    //generate mail object
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = "AMHP request for MHA ax";
                    mail.Body = string.Format("Dear Dr {0},"+ Environment.NewLine + "" + Environment.NewLine+ "I am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]." + Environment.NewLine + "Are you available to attend? if so, please reply to this message or phone [number] for more information." + Environment.NewLine + Environment.NewLine + "Kind regards," + Environment.NewLine + Environment.NewLine + Utilities.SessionData.CurrntAppUser.Data.Name, addressBookData.Name);
                    //add recipients to the mail object
                    mail.To.Add(sendTo);
                    //mail.Bcc.Add(sendTo);
                    //mail.CC.Add(sendTo);

                    //open the share contract with Mail only:
                    await EmailManager.ShowComposeNewEmailAsync(mail);
                }
                catch (Exception ex)
                {
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }

            }
            gridAction.Visibility = Visibility.Collapsed;
            listAddressBook.SelectedIndex = -1;
        }

        private void ListAddressBook_Loaded(object sender, RoutedEventArgs e)
        {
            ScrollViewer viewer = GetScrollViewer(this.listAddressBook);
            viewer.ViewChanged += MainPage_ViewChanged;
        }

        private void listAddressBook_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void MainPage_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer view = (ScrollViewer)sender;
            double progress = view.VerticalOffset / view.ScrollableHeight;
            System.Diagnostics.Debug.WriteLine(progress);

            if (IsAddressBookVisible)
            {
                if (progress == 1 & !incallAddressBook && !endoflistAddressBook)
                {
                    incallAddressBook = true;
                    fetchCountries(offsetAddressBook);
                }
            }
            else
            {
                if (progress == 1 & !incallLocation && !endoflistLocation)
                {
                    incallLocation = true;
                    fetchCountries(offsetLocation);
                }
            }
        }

        private void listAddressBook_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (listAddressBook.SelectionMode == ListViewSelectionMode.Multiple)
                {
                    if (listAddressBook.SelectedItems.Count > 0)
                    {
                        AddressBookDoctorData item = ((AddressBookDoctorData)listAddressBook.SelectedItems[listAddressBook.SelectedItems.Count - 1]);
                        if (item != null && string.IsNullOrEmpty(item.MobileNumber))
                        {
                            listAddressBook.SelectedItems.Remove(item);
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("There is no mobile number associated with this doctor.");
                        }
                    }
                }
            }
            catch (Exception)
            { }
        }

        private void btnMon_Click(object sender, RoutedEventArgs e)
        {
            SetWeeklyStatus(1,sender);
          //  SetSlotTimeDisableEnableStatus(1);
        }

        private void btnTue_Click(object sender, RoutedEventArgs e)
        {
            SetWeeklyStatus(2,sender);
        }

        private void btnWed_Click(object sender, RoutedEventArgs e)
        {
            SetWeeklyStatus(3,sender);
        }

        private void btnThu_Click(object sender, RoutedEventArgs e)
        {
            SetWeeklyStatus(4,sender);
        }

        private void btnFri_Click(object sender, RoutedEventArgs e)
        {
            SetWeeklyStatus(5,sender);
        }

        private void btnSat_Click(object sender, RoutedEventArgs e)
        {
            SetWeeklyStatus(6,sender);
        }

        private void btnSun_Click(object sender, RoutedEventArgs e)
        {
            SetWeeklyStatus(7,sender);
        }

        private void btnMorning_Click(object sender, RoutedEventArgs e)
        {
            SetTimeSlotStatus(1,sender);
        }

        private void btnLunch_Click(object sender, RoutedEventArgs e)
        {
            SetTimeSlotStatus(2,sender);
        }

        private void btnAfternoon_Click(object sender, RoutedEventArgs e)
        {
            SetTimeSlotStatus(3,sender);
        }

        private void btnEvening_Click(object sender, RoutedEventArgs e)
        {
            SetTimeSlotStatus(4,sender);
        }

        private void btnNight_Click(object sender, RoutedEventArgs e)
        {
            SetTimeSlotStatus(5,sender);
        }

        private void CloseInfobox_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            //Infobox.Visibility = Visibility.Collapsed;
        }

        private void gridMapPinViewerCallBack_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gridMapPinViewer.Visibility = Visibility.Collapsed;
        }

        private void gridMapPinViewerData_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            Grid grid = (Grid)sender;
            if (grid != null)
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenDoctorDetailPage(Convert.ToInt32(grid.Tag));
            }
            gridMapPinViewer.Visibility = Visibility.Collapsed;
        }

        private void fetchCountries(int offset)
        {
            if (IsAddressBookVisible)
            {
                if (!fatchDataAddressBook)
                {
                    if (offset > 0)
                    {
                        GetOffsetDataForAddressBook(offset);
                        fatchDataAddressBook = true;
                    }
                    else
                    {
                        endoflistAddressBook = true;
                    }
                }
                incallAddressBook = false;
            }
            else
            {
                if (!fatchDataLocation)
                {
                    if (offset > 0)
                    {
                        GetOffsetDataForLocation(offset);
                        fatchDataLocation = true;
                    }
                    else
                    {
                        endoflistLocation = true;
                    }
                }
                incallLocation = false;
            }
        }

        // method to pull out a ScrollViewer
        public static ScrollViewer GetScrollViewer(DependencyObject depObj)
        {
            if (depObj is ScrollViewer) return depObj as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);
                var result = GetScrollViewer(child);
                if (result != null) return result;
            }

            return null;
        }

        async private void lblActionMark_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                if (listAddressBook.SelectedIndex != -1)
                {
                    var data = ((AddressBookDoctorData)listAddressBook.SelectedItem);
                    object isFavourite = await amhpServices.favouriteUnfavouriteDoctor(data.DoctorId.ToString(), data.IsFavourite ? "0" : "1");

                    if (isFavourite is DoctorDetailMeta)
                    {
                        if (((DoctorDetailMeta)isFavourite).Status == 200)
                        {
                            if (data.IsFavourite == true)
                            {
                                ((AddressBookDoctorData)listAddressBook.SelectedItem).IsFavourite = false;
                            }
                            else
                            {
                                ((AddressBookDoctorData)listAddressBook.SelectedItem).IsFavourite = true;
                            }
                        }
                        else
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(isFavourite != null ? ((DoctorDetailMeta)isFavourite).Message : "An unknown error has occurred.");
                    }
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(isFavourite != null ? isFavourite.ToString() : "An unknown error has occurred.");
                }

                gridAction.Visibility = Visibility.Collapsed;
                listAddressBook.SelectedIndex = -1;
            }
            catch { }
        }

        private void btnActionCancel_Click(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gridAction.Visibility = Visibility.Collapsed;
            listAddressBook.SelectedIndex = -1;
        }
    }
}
