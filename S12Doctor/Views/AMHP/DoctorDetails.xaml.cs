﻿using S12Doctor.Model;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using System;
using Windows.ApplicationModel.Email;
using S12Doctor.Model.AMHP;
using S12Doctor.Model.S12Doctor;
using S12Doctor.WebService;
using S12Doctor.Model.Common;
using System.Diagnostics;

namespace S12Doctor.Views.AMHP
{
    public sealed partial class DoctorDetails : UserControl
    {
        AMHPServices amhpServices;
        DoctorDetailData doctorDetailData;

        public DoctorDetails()
        {
            this.InitializeComponent();
        }

        public async void SetData(long doctorID)
        {
            this.DataContext = null;

            if (amhpServices == null)
                amhpServices = new AMHPServices();

            try
            {
                //if (calanderResult is CalanderAvailibility)
                //{
                //    CalanderAvailibility = (CalanderAvailibility)calanderResult;
                //    Debug.WriteLine("Ans:=" + am);
                //}
            }
            catch (Exception) { }

            object result = await amhpServices.doctorDetail(doctorID.ToString());
            if (result is DoctorDetail)
            {
                doctorDetailData = ((DoctorDetail)result).Data;

                this.DataContext = doctorDetailData;
                SolidColorBrush foregroundSolidColorBrush;
                txtUpdatedStatusTimestamp.Visibility = Visibility.Visible;

                if (doctorDetailData.StatusText.ToLower() == "unavailable")
                {
                    foregroundSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 72, 88);
                    txtStatus.Foreground = foregroundSolidColorBrush;
                }
                else if (doctorDetailData.StatusText.ToLower() == "may be available")
                {
                    foregroundSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 186, 0);
                    txtStatus.Foreground = foregroundSolidColorBrush;
                }
                else if (doctorDetailData.StatusText.ToLower() == "available")
                {
                    foregroundSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 0, 216, 0);
                    txtStatus.Foreground = foregroundSolidColorBrush;
                }
                else
                {
                    doctorDetailData.AvailabilityMessage = "N/A";
                    txtUpdatedStatusTimestamp.Visibility = Visibility.Collapsed;
                    foregroundSolidColorBrush = new SolidColorBrush();
                    foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);
                    txtStatus.Foreground = foregroundSolidColorBrush;
                }

                string am = doctorDetailData.WeeklyAvailabilityStatus.am;
                string[] morn = am.Split(',');

                stk_mon_mor.Visibility = (morn[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_mor_tue.Visibility = (morn[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_mor_wed.Visibility = (morn[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_mor_thu.Visibility = (morn[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_mor_fri.Visibility = (morn[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_mor_sat.Visibility = (morn[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_mor_sun.Visibility = (morn[6] == "1" ? Visibility.Visible : Visibility.Collapsed);

                txt_mon_mor.Visibility = (morn[0] == "1" ? Visibility.Collapsed : Visibility.Visible); 
                txt_mor_tue.Visibility = (morn[1] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_mor_wed.Visibility = (morn[2] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_mor_thu.Visibility = (morn[3] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_mor_fri.Visibility = (morn[4] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_mor_sat.Visibility = (morn[5] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_mor_sun.Visibility = (morn[6] == "1" ? Visibility.Collapsed : Visibility.Visible);

                string evening = doctorDetailData.WeeklyAvailabilityStatus.evening;
                string[] even = evening.Split(',');

                txt_mon_eve.Visibility = (even[0] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_eve_tue.Visibility = (even[1] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_eve_wed.Visibility = (even[2] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_eve_thu.Visibility = (even[3] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_eve_fri.Visibility = (even[4] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_eve_sat.Visibility = (even[5] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_eve_sun.Visibility = (even[6] == "1" ? Visibility.Collapsed : Visibility.Visible);

                stk_mon_eve.Visibility = (even[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_eve_tue.Visibility = (even[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_eve_wed.Visibility = (even[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_eve_thu.Visibility = (even[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_eve_fri.Visibility = (even[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_eve_sat.Visibility = (even[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_eve_sun.Visibility = (even[6] == "1" ? Visibility.Visible : Visibility.Collapsed);

                string lunch = doctorDetailData.WeeklyAvailabilityStatus.lunch;
                string[] lun = lunch.Split(',');

                txt_mon_lunch.Visibility = (lun[0] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_lunch_tue.Visibility = (lun[1] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_lunch_wed.Visibility = (lun[2] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_lunch_thu.Visibility = (lun[3] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_lunch_fri.Visibility = (lun[4] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_lunch_sat.Visibility = (lun[5] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_lunch_sun.Visibility = (lun[6] == "1" ? Visibility.Collapsed : Visibility.Visible);

                stk_mon_lunch.Visibility = (lun[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_lunch_tue.Visibility = (lun[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_lunch_wed.Visibility = (lun[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_lunch_thu.Visibility = (lun[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_lunch_fri.Visibility = (lun[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_lunch_sat.Visibility = (lun[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_lunch_sun.Visibility = (lun[6] == "1" ? Visibility.Visible : Visibility.Collapsed);
                string night = doctorDetailData.WeeklyAvailabilityStatus.night;
                string[] nig = night.Split(',');

                txt_mon_night.Visibility = (nig[0] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_night_tue.Visibility = (nig[1] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_night_wed.Visibility = (nig[2] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_night_thu.Visibility = (nig[3] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_night_fri.Visibility = (nig[4] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_night_sat.Visibility = (nig[5] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_night_sun.Visibility = (nig[6] == "1" ? Visibility.Collapsed : Visibility.Visible);

                stk_mon_night.Visibility = (nig[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_night_tue.Visibility = (nig[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_night_wed.Visibility = (nig[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_night_thu.Visibility = (nig[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_night_fri.Visibility = (nig[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_night_sat.Visibility = (nig[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_night_sun.Visibility = (nig[6] == "1" ? Visibility.Visible : Visibility.Collapsed);
                string pm = doctorDetailData.WeeklyAvailabilityStatus.pm;
                string[] pms = pm.Split(',');

                txt_mon_aftn.Visibility = (pms[0] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_aftn_tue.Visibility = (pms[1] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_aftn_wed.Visibility = (pms[2] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_aftn_thu.Visibility = (pms[3] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_aftn_fri.Visibility = (pms[4] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_aftn_sat.Visibility = (pms[5] == "1" ? Visibility.Collapsed : Visibility.Visible);
                txt_aftn_sun.Visibility = (pms[6] == "1" ? Visibility.Collapsed : Visibility.Visible);

                stk_mon_aftn.Visibility = (pms[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_aftn_tue.Visibility = (pms[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_aftn_wed.Visibility = (pms[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_aftn_thu.Visibility = (pms[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_aftn_fri.Visibility = (pms[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_aftn_sat.Visibility = (pms[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
                stk_aftn_sun.Visibility = (pms[6] == "1" ? Visibility.Visible : Visibility.Collapsed);
                
            }
            else
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
        }

        private void imgCall_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtMobileNumber.Text.Replace("N/A", "")) &&
                !string.IsNullOrWhiteSpace(txtLandlineNumber.Text.Replace("N/A", "")))
                gridCallPopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
            else if (!string.IsNullOrWhiteSpace(txtMobileNumber.Text.Replace("N/A", "")))
            {
                lblCallMobile_Tapped(null, null);
            }
            else if (!string.IsNullOrWhiteSpace(txtLandlineNumber.Text.Replace("N/A", "")))
            {
                lblCallLandline_Tapped(null, null);
            }
            else
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Sorry, no contact numbers are available for this doctor.");
            }
        }

        private async void imgMsg_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
           
            if (string.IsNullOrWhiteSpace(txtMobileNumber.Text.Replace("N/A", "")))
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Sorry, no mobile number is available for this doctor.");
            else
            {
                try
                {
                    amhpServices.contactDoctor(doctorDetailData.DoctorId.ToString(), "4");

                }
                catch (Exception ex) { }

                Windows.ApplicationModel.Chat.ChatMessage msg
                         = new Windows.ApplicationModel.Chat.ChatMessage();
                
                msg.Body = string.Format("Dear Dr {0}," + Environment.NewLine + "" + Environment.NewLine + "I am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]." + Environment.NewLine + "Are you available to attend? if so, please reply to this message or phone [number] for more information." + Environment.NewLine + Environment.NewLine + "Kind regards," + Environment.NewLine + Environment.NewLine + Utilities.SessionData.CurrntAppUser.Data.Name, txtName.Text);
                msg.Recipients.Add(txtMobileNumber.Text);

                await Windows.ApplicationModel.Chat.ChatMessageManager.ShowComposeSmsMessageAsync(msg);
            }
        }

        private async void imgEmail_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                amhpServices.contactDoctor(doctorDetailData.DoctorId.ToString(), "1");
            }
            catch (Exception ex) { }

            if (string.IsNullOrWhiteSpace(txtEmail.Text.Replace("N/A", "")))
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Sorry, no email address is available for this doctor.");
            else
            {
                try
                {
                    EmailRecipient sendTo = new EmailRecipient()
                    {
                        Address = txtEmail.Text,
                    };

                    //generate mail object
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = "AMHP request for MHA ax";
                    mail.Body = string.Format("Dear Dr {0},"+Environment.NewLine+"" +Environment.NewLine +"I am arranging a Mental Health Act assessment [day] ideally at [time] at this location: [postcode]." + Environment.NewLine + "Are you available to attend? if so, please reply to this message or phone [number] for more information." + Environment.NewLine + Environment.NewLine + "Kind Regards," + Environment.NewLine + Environment.NewLine + Utilities.SessionData.CurrntAppUser.Data.Name, txtName.Text);
                    //add recipients to the mail object
                    mail.To.Add(sendTo);
                    //mail.Bcc.Add(sendTo);
                    //mail.CC.Add(sendTo);

                    //open the share contract with Mail only:
                    await EmailManager.ShowComposeNewEmailAsync(mail);
                }
                catch (Exception ex)
                {
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }

            }

        }

        private void lblCallMobile_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            amhpServices.contactDoctor(doctorDetailData.DoctorId.ToString(),"2");
            Windows.ApplicationModel
            .Calls.PhoneCallManager
            .ShowPhoneCallUI(txtMobileNumber.Text, txtName.Text);
        }

        private void lblCallLandline_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                amhpServices.contactDoctor(doctorDetailData.DoctorId.ToString(), "3");
                
            }
            catch (Exception ex) { }
            Windows.ApplicationModel
            .Calls.PhoneCallManager
            .ShowPhoneCallUI(txtLandlineNumber.Text, txtName.Text);
        }

        private void gridCallBack_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gridCallPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void btnCallCancel_Click(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gridCallPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
    }
}
