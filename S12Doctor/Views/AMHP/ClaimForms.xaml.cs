﻿using S12Doctor.Model.AMHP;
using S12Doctor.Model.Common;
using S12Doctor.WebService;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Views.AMHP
{
    public sealed partial class ClaimForms : UserControl
    {
        private ObservableCollection<ClaimFormForApprovalData> claimFormForApprovalData;
        public AMHP.ViewClaimForm viewClaimForm;
        private AMHPServices amhpServices;

        private bool incall = false, endoflist = false, fatchData = false;
        private int offset = 0;

        public ClaimForms()
        {
            this.InitializeComponent();
            this.Loaded += ClaimForms_Loaded;

        }

        private void ClaimForms_Loaded(object sender, RoutedEventArgs e)
        {
            if (amhpServices == null)
                amhpServices = new AMHPServices();

            claimFormForApprovalData = new ObservableCollection<ClaimFormForApprovalData>();
            listClaimForms.ItemsSource = claimFormForApprovalData;
        }

        private async void GetOffsetData(int _offset, int count = 10, bool isReload = false)
        {
            object claimFormForApproval = await amhpServices.claimFormForApproval(_offset.ToString(), count.ToString());

            if (claimFormForApproval is ClaimFormForApprovalList)
            {
                var data = new ObservableCollection<ClaimFormForApprovalData>(((ClaimFormForApprovalList)claimFormForApproval).Data);

                offset = 0;
                if (claimFormForApprovalData == null)
                    claimFormForApprovalData = new ObservableCollection<ClaimFormForApprovalData>();

                if (isReload)
                    claimFormForApprovalData.Clear();

                int i = offset + 1;
                foreach (var item in data)
                {
                    claimFormForApprovalData.Add(item);
                }
                
                if (claimFormForApprovalData.Count <= 0)
                    txtNoResultsFound.Visibility = Visibility.Visible;
                else
                    txtNoResultsFound.Visibility = Visibility.Collapsed;

                offset = (int)((ClaimFormForApprovalList)claimFormForApproval).Meta.NewOffset;
                fatchData = false;

                if (isReload)
                    incall = endoflist = fatchData = false;
            }
            else
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(claimFormForApproval != null ? claimFormForApproval.ToString() : "An unknown error has occurred.");
        }

        public void ReloadData()
        {
            GetOffsetData(0, isReload: true);
        }

        private void ViewClaimFrom_Click(object sender, RoutedEventArgs e)
        {
            string id = ((Button)sender).Tag.ToString();

            if (viewClaimForm == null)
            {
                viewClaimForm = new AMHP.ViewClaimForm();
                gridViewClaimForm.Children.Add(viewClaimForm);
                viewClaimForm.btnCCGToSubmitConfirm.Click += BtnCCGToSubmitConfirm_Click; ;
                viewClaimForm.btnRejectionReasonConfirm.Click += BtnRejectionReasonConfirm_Click;
            }

            try
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetUserControlOnHeader();

                viewClaimForm.SetData(claimFormForApprovalData.Where(i => i.ClaimId.ToString() == id).FirstOrDefault().ClaimId);
                gridViewClaimForm.Visibility = Visibility.Visible;
            }
            catch { }
        }

        async private void BtnRejectionReasonConfirm_Click(object sender, RoutedEventArgs e)
        {
            string info = await viewClaimForm.RejectionReasonConfirm();

            if (!string.IsNullOrWhiteSpace(info))
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(info);
            else
            {
                gridViewClaimForm.Visibility = Visibility.Collapsed;
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).RemoveUserControlFromHeader();
                GetOffsetData(0, isReload: true);
            }
        }

        async private void BtnCCGToSubmitConfirm_Click(object sender, RoutedEventArgs e)
        {
            string info = await viewClaimForm.CCGToSubmitConfirm();

            if (!string.IsNullOrWhiteSpace(info))
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(info);
            else
            {
                gridViewClaimForm.Visibility = Visibility.Collapsed;
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).RemoveUserControlFromHeader();
                GetOffsetData(0, isReload: true);
            }
        }
        
        async private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;

            if (string.IsNullOrWhiteSpace(txtPassword.Password))
                error = "Password cannot be blank.";

            if (!string.IsNullOrWhiteSpace(error))
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
                try
                {
                    object result = await amhpServices.verifyPassword(txtPassword.Password);

                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            gridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                            GetOffsetData(0);
                        }
                        else
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                catch (Exception ex)
                {
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }
        }

        private void listClaimForms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listClaimForms.SelectedIndex != -1)
            {
                string id = claimFormForApprovalData[listClaimForms.SelectedIndex].ClaimId.ToString();

                if (viewClaimForm == null)
                {
                    viewClaimForm = new AMHP.ViewClaimForm();
                    gridViewClaimForm.Children.Add(viewClaimForm);
                    viewClaimForm.btnCCGToSubmitConfirm.Click += BtnCCGToSubmitConfirm_Click; ;
                    viewClaimForm.btnRejectionReasonConfirm.Click += BtnRejectionReasonConfirm_Click;
                }

                try
                {
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetUserControlOnHeader();

                    viewClaimForm.SetData(claimFormForApprovalData.Where(i => i.ClaimId.ToString() == id).FirstOrDefault().ClaimId);
                    gridViewClaimForm.Visibility = Visibility.Visible;
                }
                catch { }

                listClaimForms.SelectedIndex = -1;
            }
        }

        private void listClaimForms_Loaded(object sender, RoutedEventArgs e)
        {
            ScrollViewer viewer = GetScrollViewer(this.listClaimForms);
            viewer.ViewChanged += MainPage_ViewChanged;
        }

        private void MainPage_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer view = (ScrollViewer)sender;
            double progress = view.VerticalOffset / view.ScrollableHeight;
            System.Diagnostics.Debug.WriteLine(progress);

            if (progress == 1 & !incall && !endoflist)
            {
                incall = true;
                fetchCountries(offset);
            }
        }

        private void fetchCountries(int offset)
        {
            if (!fatchData)
            {
                if (offset > 0)
                {
                    GetOffsetData(offset);
                    fatchData = true;
                }
                else
                {
                    endoflist = true;
                }
            }
            incall = false;
        }

        // method to pull out a ScrollViewer
        private ScrollViewer GetScrollViewer(DependencyObject depObj)
        {
            if (depObj is ScrollViewer) return depObj as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);
                var result = GetScrollViewer(child);
                if (result != null) return result;
            }

            return null;
        }

    }
}
