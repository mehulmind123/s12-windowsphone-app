﻿using S12Doctor.Model.Common;
using System;
using System.Text.RegularExpressions;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace S12Doctor.Views.AMHP
{
    public sealed partial class ChangePassword : UserControl
    {

        WebService.AMHPServices amhpServices;
        public ChangePassword()
        {
            this.InitializeComponent();
            this.Loaded += ChangePassword_Loaded;
        }
        
        private async void ChangePassword_Loaded(object sender, RoutedEventArgs e)
        {

            if (amhpServices == null)
                amhpServices = new WebService.AMHPServices();

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
             txtOldPassword.Focus(FocusState.Keyboard));
        }

        async private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;
            
            if (string.IsNullOrWhiteSpace(txtOldPassword.Password))
                error = "Old Password cannot be blank.";
            else if (string.IsNullOrWhiteSpace(txtNewPassword.Password))
                error = "New Password cannot be blank.";
            else if (!Regex.IsMatch(txtNewPassword.Password, @"^(((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]))|((?=.*[0-9])(?=.*[a-z])(?=.*[!()@#.*=_?$%^&+=]))|((?=.*[0-9])(?=.*[A-Z])(?=.*[!()@#.*=_?$%^&+=]))|((?=.*[0-9])(?=.*[A-Z])(?=.*[!()@#.*=_?$%^&+=])(?=\\S+$))|((?=.*[a-z])(?=.*[A-Z])(?=.*[!()@#.*=_?$%^&+=]))).{8,}$"))
                error = "Passwords should be at least 8 characters long and use three of four of the following four types of characters: uppercase, lowercase, numbers and special characters such as !@#$%^&*(){}[]";
            else if (txtOldPassword.Password.Equals(txtNewPassword.Password))
                error = "Old Password and New Password can not be same.";
            else if (string.IsNullOrWhiteSpace(txtConfirmPassword.Password))
                error = "Please confirm your password.";
            else if (!txtConfirmPassword.Password.Equals(txtNewPassword.Password))
                error = "New Password and Confirm Password do not match.";

            if (!string.IsNullOrWhiteSpace(error))
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
            {
                object result = await amhpServices.changePassword(txtOldPassword.Password, txtNewPassword.Password);
                if (result is MetaInfo)
                {
                    if (((MetaInfo)result).Meta.Status == 200)
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Password changed successfully");
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).RedirectToHomePage();
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Visible);

                        Helpers.SettingHelper.EmailId = Utilities.SessionData.CurrntAppUser.Data.Email;
                        Helpers.SettingHelper.Password = txtNewPassword.Password;
                        Helpers.SettingHelper.LoginType = "2";
                    }
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                }
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
            }
        }

        private void stPassword_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                FocusManager.TryMoveFocus(FocusNavigationDirection.Next);

                // Make sure to set the Handled to true, otherwise the RoutedEvent might fire twice
                e.Handled = true;
            }
        }
    }
}
