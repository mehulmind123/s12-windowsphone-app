﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System;
using S12Doctor.WebService;
using S12Doctor.Model.Common;
using S12Doctor.Model.AMHP;

namespace S12Doctor.Views.AMHP
{
    public sealed partial class EditProfile : UserControl
    {
        AMHPServices amhpServices;
        public EditProfile()
        {
            this.InitializeComponent();
            this.Loaded += EditProfile_Loaded;
        }

        private void EditProfile_Loaded(object sender, RoutedEventArgs e)
        {
            if (amhpServices == null)
                amhpServices = new AMHPServices();
            txtName.Text = Utilities.SessionData.CurrntAppUser.Data.Name;
            txtMobileNumber.Text = Utilities.SessionData.CurrntAppUser.Data.MobileNumber;
            txtLocalAuthority.Text = Utilities.SessionData.CurrntAppUser.Data.SelectedLocalAuthorityByName;
            txtEmailAddress.Text = Utilities.SessionData.CurrntAppUser.Data.Email;
        }

        async private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string Error = string.Empty;

            if (string.IsNullOrWhiteSpace(txtName.Text))
                Error = "Please provide your name.";

            if (!string.IsNullOrWhiteSpace(Error))
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(Error);
            else
            {
                object result = await amhpServices.editProfile(
                    txtName.Text,
                    txtMobileNumber.Text,
                    txtEmailAddress.Text,
                    Utilities.SessionData.CurrntAppUser.Data.SelectedLocalAuthorityById);

                if (result is AppUser)
                {
                    if (((AppUser)result).Meta.Status == 200)
                    {
                        Utilities.SessionData.CurrntAppUser = ((AppUser)result);
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Profile edited successfully");
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).RedirectToHomePage();
                    }
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                }
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
            }
        }
    }
}
