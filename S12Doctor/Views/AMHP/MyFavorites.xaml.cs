﻿using S12Doctor.Model;
using S12Doctor.Model.AMHP;
using S12Doctor.WebService;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Views.AMHP
{
    public sealed partial class MyFavorites : UserControl
    {
        ObservableCollection<FavouriteDoctorData> myFavouriteDoctorsData;

        AMHPServices amhpServices;

        private bool incall = false, endoflist = false, fatchData = false;
        private int offset = 0;

        public MyFavorites()
        {
            this.InitializeComponent();
            this.Loaded += MyFavorites_Loaded;
        }

        private async void GetOffsetData(int _offset, int count = 10)
        {
            object myFavouriteDoctorsListData = await amhpServices.myFavouriteDoctorsList(_offset.ToString(), count.ToString());

            if (myFavouriteDoctorsListData is FavouriteDoctorList)
            {
                var data = new ObservableCollection<FavouriteDoctorData>(((FavouriteDoctorList)myFavouriteDoctorsListData).Data);

                offset = 0;
                if (myFavouriteDoctorsData == null)
                    myFavouriteDoctorsData = new ObservableCollection<FavouriteDoctorData>();

                //myFavouriteDoctorsData.Clear();
                int i = offset + 1;
                foreach (var item in data)
                {
                    myFavouriteDoctorsData.Add(item);
                }

                if (myFavouriteDoctorsData.Count <= 0)
                {
                    txtNoResultsFound.Visibility = Visibility.Visible;
                }
                else
                {
                    txtNoResultsFound.Visibility = Visibility.Collapsed;
                }

                offset = (int)((FavouriteDoctorList)myFavouriteDoctorsListData).Meta.NewOffset;
                fatchData = false;
                //isDeleteMode = false; incall = endoflist = fatchData = false;
            }
            else
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(myFavouriteDoctorsListData != null ? myFavouriteDoctorsListData.ToString() : "An unknown error has occurred.");
        }

        private void MyFavorites_Loaded(object sender, RoutedEventArgs e)
        {
            if (amhpServices == null)
                amhpServices = new AMHPServices();

            if (myFavouriteDoctorsData == null)
                myFavouriteDoctorsData = new ObservableCollection<FavouriteDoctorData>();

            listMyFavorites.ItemsSource = myFavouriteDoctorsData;

            GetOffsetData(0);
        }

        private void listMyFavorites_Loaded(object sender, RoutedEventArgs e)
        {
            ScrollViewer viewer = GetScrollViewer(this.listMyFavorites);
            viewer.ViewChanged += MainPage_ViewChanged;
        }

        private void MainPage_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer view = (ScrollViewer)sender;
            double progress = view.VerticalOffset / view.ScrollableHeight;
            System.Diagnostics.Debug.WriteLine(progress);

            if (progress == 1 & !incall && !endoflist)
            {
                incall = true;
                fetchCountries(offset);
            }
        }

        private void fetchCountries(int offset)
        {
            if (!fatchData)
            {
                if (offset > 0)
                {
                    GetOffsetData(offset);
                    fatchData = true;
                }
                else
                {
                    endoflist = true;
                }
            }
            incall = false;
        }

        // method to pull out a ScrollViewer
        private ScrollViewer GetScrollViewer(DependencyObject depObj)
        {
            if (depObj is ScrollViewer) return depObj as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);
                var result = GetScrollViewer(child);
                if (result != null) return result;
            }

            return null;
        }

        private void btnOpenDoctorDetailView_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenDoctorDetailPage(((FavouriteDoctorData)listMyFavorites.SelectedItem).DoctorId);
                listMyFavorites.SelectedIndex = -1;
            }
            catch { }
        }

        private async void Favourite_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                if (listMyFavorites.SelectedIndex != -1)
                {
                    var data = ((FavouriteDoctorData)listMyFavorites.SelectedItem);
                    object isFavourite = await amhpServices.favouriteUnfavouriteDoctor(data.DoctorId.ToString(), data.IsDoctorFavourite ? "0" : "1");

                    if (isFavourite is DoctorDetailMeta)
                    {
                        if (((DoctorDetailMeta)isFavourite).Status == 200)
                        {
                            if (data.IsDoctorFavourite == true)
                            {
                                ((FavouriteDoctorData)listMyFavorites.SelectedItem).IsDoctorFavourite = false;
                            }
                            else
                            {
                                ((FavouriteDoctorData)listMyFavorites.SelectedItem).IsDoctorFavourite = true;
                            }
                        }
                        else
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(isFavourite != null ? ((DoctorDetailMeta)isFavourite).Message : "An unknown error has occurred.");
                    }
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(isFavourite != null ? isFavourite.ToString() : "An unknown error has occurred.");
                }

                listMyFavorites.SelectedIndex = -1;
            }
            catch { }
        }

    }
}
