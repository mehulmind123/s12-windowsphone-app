﻿using S12Doctor.Model;
using S12Doctor.Model.AMHP;
using S12Doctor.Utilities;
using S12Doctor.WebService;
using System;
using System.IO;
using System.Net.Http;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace S12Doctor.Views.AMHP
{
    public sealed partial class UsefulInfo : UserControl
    {
        public UsefulInfo()
        {
            this.InitializeComponent();
            this.Loaded += UsefulInfo_Loaded;
        }

        private async void UsefulInfo_Loaded(object sender, RoutedEventArgs e)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                string url = string.Format("https://s12solutions.com/application/useful-info/{0} {1}", Utilities.SessionData.ApiHeaderScheme, Utilities.SessionData.ApiHeaderParameter);
           
                //string url = string.Format("https://www.itrainacademy.in/doctorApp/useful-info/{0} {1}", Utilities.SessionData.ApiHeaderScheme, Utilities.SessionData.ApiHeaderParameter);
                webViewer.Navigate(new Uri(url));
              // webViewer.CacheMode=CacheMode
            }
            else
            {
                try
                {
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                    StorageFolder local = ApplicationData.Current.LocalFolder;
                    StorageFile file = await local.GetFileAsync("offline-web.html");
                    if (file != null)
                    {
                        using (StreamReader reader = new StreamReader(await file.OpenStreamForReadAsync()))
                        {
                            var htmlstring = await reader.ReadToEndAsync();
                            webViewer.NavigateToString(htmlstring);
                        }
                    }
                    else
                    {
                        MessageDialog dlg = new MessageDialog("Please check your internet", "Warning");
                        await dlg.ShowAsync();
                    }
                } catch(Exception) { }

            }

        }

        private async void webView_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            if (args.IsSuccess == true)
            {
                try
                {
                    using (HttpClient client = new HttpClient())
                    {
                        string url = string.Format("https://s12solutions.com/application/useful-info/{0} {1}", Utilities.SessionData.ApiHeaderScheme, Utilities.SessionData.ApiHeaderParameter);

                        if (args.Uri != null && args.Uri.ToString() == url)
                        {
                            var htmlstring = await client.GetStringAsync(args.Uri);
                            StorageFolder local = ApplicationData.Current.LocalFolder;
                            StorageFile file = await local.CreateFileAsync("offline-web.html", CreationCollisionOption.ReplaceExisting);
                            using (StreamWriter writer = new StreamWriter(await file.OpenStreamForWriteAsync()))
                            {
                                writer.Write(htmlstring);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                StorageFolder local = ApplicationData.Current.LocalFolder;
                StorageFile file = await local.GetFileAsync("offline-web.html");
                if (file != null)
                {
                    using (StreamReader reader = new StreamReader(await file.OpenStreamForReadAsync()))
                    {
                        var htmlstring = await reader.ReadToEndAsync();
                        sender.NavigateToString(htmlstring);
                    }
                }
                else
                {
                    MessageDialog dlg = new MessageDialog("Please check your internet", "Warning");
                    await dlg.ShowAsync();
                }
            }
        }
    }
}
