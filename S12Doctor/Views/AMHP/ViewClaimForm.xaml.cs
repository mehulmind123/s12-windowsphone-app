﻿using S12Doctor.Model.AMHP;
using S12Doctor.Model.Common;
using S12Doctor.WebService;
using System;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace S12Doctor.Views.AMHP
{
    public sealed partial class ViewClaimForm : UserControl
    {
        private AMHPServices amhpServices;
        private ClaimFormData claimFormData;
        public ViewClaimForm()
        {
            this.InitializeComponent();
        }

        async public void SetData(long claimId)
        {
            try
            {
                this.DataContext = null;
                bdrSpecifiedLocationType.Visibility = Visibility.Collapsed;
                bdrSpecifiedSection.Visibility = Visibility.Collapsed;

                if (amhpServices == null)
                    amhpServices = new AMHPServices();

                object result = await amhpServices.viewClaimForm(claimId.ToString());

                if (result is ClaimForm)
                    claimFormData = ((ClaimForm)result).Data;
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");

                this.DataContext = claimFormData;

                if (claimFormData.SectionImplementedText.Contains("Other"))
                    bdrSpecifiedSection.Visibility = Visibility.Visible;

                if (claimFormData.AssessmentLocationTypeText.Contains("Other"))
                    bdrSpecifiedLocationType.Visibility = Visibility.Visible;

                getCCGListData();
            }
            catch (Exception ex)
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        async private void getCCGListData()
        {
            object result = await amhpServices.ccgList(claimFormData.LocalAuthorityId.ToString());

            if (result is CCGList)
                cbCCGToSubmit.DataContext = ((CCGList)result).Data;
            else
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
        }
        private void gridBack_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gridRejectionReasonPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridCCGToSubmitPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        async public Task<string> RejectionReasonConfirm()
        {
            string info = string.Empty;
            try
            {

                if (string.IsNullOrWhiteSpace(txtRejectionInfo.Text))
                    info = "Reason can not be blank.";
                else
                {
                    object result = await amhpServices.acceptOrRejectClaimForm(claimFormData.ClaimId.ToString(), "2", "", txtRejectionInfo.Text);

                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            gridRejectionReasonPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Claim form request has been rejected successfully.");
                        }
                        else
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
            }
            catch (Exception ex)
            {
                info = ex.Message;
            }

            return info;
        }

        async public Task<string> CCGToSubmitConfirm()
        {
            string info = string.Empty;

            try
            {
                if (cbCCGToSubmit.SelectedIndex < 0)
                    info = "Please select CCG.";
                else
                {
                    object result = await amhpServices.acceptOrRejectClaimForm(claimFormData.ClaimId.ToString(), "1", ((CCG)cbCCGToSubmit.SelectedItem).OrganisationId.ToString(), "");

                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            gridCCGToSubmitPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Claim form request has been accepted successfully.");
                        }
                        else
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");


                }
            }
            catch (Exception ex)
            {
                info = ex.Message;
            }

            return info;
        }

        private void imgRight_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            cbCCGToSubmit.SelectedIndex = -1;
            gridCCGToSubmitPopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void imgClose_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            txtRejectionInfo.Text = string.Empty;
            gridRejectionReasonPopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }
    }
}
