﻿using S12Doctor.Model.Common;
using S12Doctor.Model.S12Doctor;
using S12Doctor.Utilities;
using S12Doctor.WebService;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace S12Doctor.Views
{
    public sealed partial class Home : UserControl
    {
        public CreateClaimForm createClaimForm;
        public EditProfile EditProfile;
        public CalanderAvailibility CalanderAvailibility;
        private S12DoctorServices s12DoctorServices;
        public long CurrentStatus = 4;
        long newStatus = 4;
        
        public Home()
        {
            this.InitializeComponent();

            if (s12DoctorServices == null)
                s12DoctorServices = new S12DoctorServices();

            EditStauts.Tapped += EditStauts_Tapped;
            gridOpenPopUp.Tapped += EditStauts_Tapped;
            btnSaveStatusPopUp.Click += BtnSaveStatusPopUp_Click;
            btnAvailable.Click += btnAvailable_Click;
            btnMaybeAvailable.Click += btnMaybeAvailable_Click;
            btnUnavailable.Click += btnUnavailable_Click;
            btnCreateClaimForm.Click += btnCreateClaimForm_Click;
            this.Loaded += Home_Loaded;
        }

        private async void Home_Loaded(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            try
            {
                if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable){
                    object result = await s12DoctorServices.getWeeklyAvailability();
                    if (result is CalanderAvailibility)
                    {
                        CalanderAvailibility = (CalanderAvailibility)result;
                        Windows.Storage.ApplicationDataCompositeValue composite = new Windows.Storage.ApplicationDataCompositeValue();
                        composite["am"] = CalanderAvailibility.Data.WeeklyAvailabilityStatus.am;
                        composite["lunch"] = CalanderAvailibility.Data.WeeklyAvailabilityStatus.lunch;
                        composite["pm"] = CalanderAvailibility.Data.WeeklyAvailabilityStatus.pm;
                        composite["evening"] = CalanderAvailibility.Data.WeeklyAvailabilityStatus.evening;
                        composite["night"] = CalanderAvailibility.Data.WeeklyAvailabilityStatus.night;
                        localSettings.Values["calanderAvailibility"] = composite;
                        string am = CalanderAvailibility.Data.WeeklyAvailabilityStatus.am;
                        string evening = CalanderAvailibility.Data.WeeklyAvailabilityStatus.evening;
                        string lunch = CalanderAvailibility.Data.WeeklyAvailabilityStatus.lunch;
                        string night = CalanderAvailibility.Data.WeeklyAvailabilityStatus.night;
                        string pm = CalanderAvailibility.Data.WeeklyAvailabilityStatus.pm;
                        availibilityCalander(am, lunch, evening , night, pm);
                    }
                } else
                {
                    Windows.Storage.ApplicationDataCompositeValue composite =
   (Windows.Storage.ApplicationDataCompositeValue)localSettings.Values["calanderAvailibility"];
                    //object value = localSettings.Values["calanderAvailibility"];
                    
                    if (composite != null)
                    {
                        string am = composite["am"].ToString();
                        string lunch = composite["lunch"].ToString();
                        string evening = composite["evening"].ToString();
                        string night = composite["night"].ToString();
                        string pm = composite["pm"].ToString();
                        availibilityCalander(am, lunch, evening, night, pm);
                    } else
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(composite != null ? composite.ToString() : "Not Updated");
                    }
                    //Debug.WriteLine("Ans:="+ composite["am"]);
                }
                
            }
            catch (Exception) { }



            //Canvas canvas = new Canvas();
            //// canvas.Children.Add(button);
            //canvas.Width = 200;
            //canvas.Height = 20;
            //canvas.Background = new SolidColorBrush(Colors.Blue);
            //canvas.Margin = 10;
            //Canvas.SetLeft(button, 100); //X
            //Canvas.SetTop(button, 10); //Y


            //LayoutRoot.Children.Add(canvas);



            if (string.IsNullOrEmpty(Helpers.SettingHelper.IsS12LoginFirstTime))
            {
                Helpers.SettingHelper.IsS12LoginFirstTime = "1";
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("S12 uses your location to allow AMHPs to find you when setting up MHA assessments. Go to the Settings screen within S12 to view the current GPS status for your device. If you'd prefer not to disclose your location, please make sure that your base post code is added to your profile.");
            }

            if (SessionData.CurrntAppUser != null)
            {
                CurrentStatus = SessionData.CurrntAppUser.Data.AvailabilityStatus;
                if (string.IsNullOrWhiteSpace(SessionData.CurrntAppUser.Data.AvailabilityMessage))
                {
                    StatusText.Text = "Please write further details of availability";
                }
                else
                {
                    StatusText.Text = SessionData.CurrntAppUser.Data.AvailabilityMessage;
                    StatusEditTextFild.Text = StatusText.Text;
                }
                s12WorkDetailsText.Text = SessionData.CurrntAppUser.Data.GeneralAvailability;
            }

            SetStatus(CurrentStatus);
        }
        
        public void availibilityCalander(string am, string lunch, string evening, string night, string pm)
        {
            
            string[] morn = am.Split(',');

            stk_mon_mor.Visibility = (morn[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_mor_tue.Visibility = (morn[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_mor_wed.Visibility = (morn[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_morn_thu.Visibility = (morn[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_morn_fri.Visibility = (morn[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_morn_sat.Visibility = (morn[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_morn_sun.Visibility = (morn[6] == "1" ? Visibility.Visible : Visibility.Collapsed);

            mor_mon.IsChecked = (morn[0] == "1" ? true : false);
            mor_tue.IsChecked = (morn[1] == "1" ? true : false);
            mor_wed.IsChecked = (morn[2] == "1" ? true : false);
            mor_thu.IsChecked = (morn[3] == "1" ? true : false);
            mor_fri.IsChecked = (morn[4] == "1" ? true : false);
            mor_sat.IsChecked = (morn[5] == "1" ? true : false);
            mor_sun.IsChecked = (morn[6] == "1" ? true : false);

            
            string[] even = evening.Split(',');

            stk_eve_mon.Visibility = (even[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_eve_tue.Visibility = (even[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_eve_wed.Visibility = (even[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_eve_thu.Visibility = (even[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_eve_fri.Visibility = (even[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_eve_sat.Visibility = (even[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_eve_sun.Visibility = (even[6] == "1" ? Visibility.Visible : Visibility.Collapsed);

            eve_mon.IsChecked = (even[0] == "1" ? true : false);
            eve_tue.IsChecked = (even[1] == "1" ? true : false);
            eve_wed.IsChecked = (even[2] == "1" ? true : false);
            eve_thu.IsChecked = (even[3] == "1" ? true : false);
            eve_fri.IsChecked = (even[4] == "1" ? true : false);
            eve_sat.IsChecked = (even[5] == "1" ? true : false);
            eve_sun.IsChecked = (even[6] == "1" ? true : false);
            
            string[] lun = lunch.Split(',');

            stk_lunch_mon.Visibility = (lun[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_lunch_tue.Visibility = (lun[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_lunch_wed.Visibility = (lun[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_lunch_thu.Visibility = (lun[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_lunch_fri.Visibility = (lun[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_lunch_sat.Visibility = (lun[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_lunch_sun.Visibility = (lun[6] == "1" ? Visibility.Visible : Visibility.Collapsed);

            lunch_mon.IsChecked = (lun[0] == "1" ? true : false);
            lunch_tue.IsChecked = (lun[1] == "1" ? true : false);
            lunch_wed.IsChecked = (lun[2] == "1" ? true : false);
            lunch_thu.IsChecked = (lun[3] == "1" ? true : false);
            lunch_fri.IsChecked = (lun[4] == "1" ? true : false);
            lunch_sat.IsChecked = (lun[5] == "1" ? true : false);
            lunch_sun.IsChecked = (lun[6] == "1" ? true : false);

            
            string[] nig = night.Split(',');

            stk_night_mon.Visibility = (nig[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_night_tue.Visibility = (nig[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_night_wed.Visibility = (nig[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_night_thu.Visibility = (nig[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_night_fri.Visibility = (nig[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_night_sat.Visibility = (nig[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_night_sun.Visibility = (nig[6] == "1" ? Visibility.Visible : Visibility.Collapsed);

            night_mon.IsChecked = (nig[0] == "1" ? true : false);
            night_tue.IsChecked = (nig[1] == "1" ? true : false);
            night_wed.IsChecked = (nig[2] == "1" ? true : false);
            night_thu.IsChecked = (nig[3] == "1" ? true : false);
            night_fri.IsChecked = (nig[4] == "1" ? true : false);
            night_sat.IsChecked = (nig[5] == "1" ? true : false);
            night_sun.IsChecked = (nig[6] == "1" ? true : false);

            
            string[] pms = pm.Split(',');

            stk_aftn_mon.Visibility = (pms[0] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_aftn_tue.Visibility = (pms[1] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_aftn_wed.Visibility = (pms[2] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_aftn_thu.Visibility = (pms[3] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_aftn_fri.Visibility = (pms[4] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_aftn_sat.Visibility = (pms[5] == "1" ? Visibility.Visible : Visibility.Collapsed);
            stk_aftn_sun.Visibility = (pms[6] == "1" ? Visibility.Visible : Visibility.Collapsed);

            aftn_mon.IsChecked = (pms[0] == "1" ? true : false);
            aftn_tue.IsChecked = (pms[1] == "1" ? true : false);
            aftn_wed.IsChecked = (pms[2] == "1" ? true : false);
            aftn_thu.IsChecked = (pms[3] == "1" ? true : false);
            aftn_fri.IsChecked = (pms[4] == "1" ? true : false);
            aftn_sat.IsChecked = (pms[5] == "1" ? true : false);
            aftn_sun.IsChecked = (pms[6] == "1" ? true : false);
        }
        private void EditStauts_Tapped(object sender, TappedRoutedEventArgs e)
        {
            newStatus = CurrentStatus;
            gridEditPopUp.Visibility = Visibility.Visible;
        }

        private void gridBackShedow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridEditPopUp.Visibility = Visibility.Collapsed;
            SetStatus(CurrentStatus);
        }

        private void btnAvailable_Click(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Windows.Storage.ApplicationDataCompositeValue composite = new Windows.Storage.ApplicationDataCompositeValue();
                composite["status"] = "1";
                composite["isStatusSynced"] = "0";
                localSettings.Values["Avaibility"] = composite;
                CurrentStatus = 1;
                SetStatus(CurrentStatus);

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("The update will sync when regained internet connection");
            }
            else
            {
                CurrentStatus = 1;
                SetStatus(CurrentStatus);
                Windows.Storage.ApplicationDataCompositeValue composite = new Windows.Storage.ApplicationDataCompositeValue();

                composite["status"] = "1";
                composite["isStatusSynced"] = "1";
                localSettings.Values["Avaibility"] = composite;

                updateStatusOnHomeButtonsClick();

            }
        }

        private void btnMaybeAvailable_Click(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Windows.Storage.ApplicationDataCompositeValue composite = new Windows.Storage.ApplicationDataCompositeValue();
                composite["status"] = "2";
                composite["isStatusSynced"] = "0";
                localSettings.Values["Avaibility"] = composite;
                CurrentStatus = 2;
                SetStatus(CurrentStatus);

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("The update will sync when regained internet connection");
            } else
            {
                CurrentStatus = 2;
                SetStatus(CurrentStatus);
                updateStatusOnHomeButtonsClick();
            }
            
        }

        private void btnUnavailable_Click(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Windows.Storage.ApplicationDataCompositeValue composite = new Windows.Storage.ApplicationDataCompositeValue();
                composite["status"] = "3";
                composite["isStatusSynced"] = "0";
                localSettings.Values["Avaibility"] = composite;
                CurrentStatus = 3;
                SetStatus(CurrentStatus);

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("The update will sync when regained internet connection");
            } else
            {
                CurrentStatus = 3;
                SetStatus(CurrentStatus);
                updateStatusOnHomeButtonsClick();
            }
            
        }

        async private void updateStatusOnHomeButtonsClick()
        {
            StatusText.Text = "Please write further details of availability";
            StatusEditTextFild.Text = "";
            await UpdateStatusInService(CurrentStatus, "");
        }

        private void imgAvailable_Tapped(object sender, TappedRoutedEventArgs e)
        {
            newStatus = 1;
            SetStatus(newStatus);
        }

        private void imgMaybeAvailable_Tapped(object sender, TappedRoutedEventArgs e)
        {
            newStatus = 2;
            SetStatus(newStatus);
        }

        private void imgUnavailable_Tapped(object sender, TappedRoutedEventArgs e)
        {
            newStatus = 3;
            SetStatus(newStatus);
        }

        private async void BtnSaveStatusPopUp_Click(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Windows.Storage.ApplicationDataCompositeValue composite = new Windows.Storage.ApplicationDataCompositeValue();
                composite["status"] = newStatus;
                composite["EditStatusText"] = StatusEditTextFild.Text;
                composite["isStatusSynced"] = "0";
                localSettings.Values["Avaibility"] = composite;
                gridEditPopUp.Visibility = Visibility.Collapsed;
                CurrentStatus = newStatus;
                if (!string.IsNullOrEmpty(StatusEditTextFild.Text))
                {
                    StatusText.Text = StatusEditTextFild.Text;
                }
                SetStatus(CurrentStatus);

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("The update will sync when regained internet connection");
            } else
            {
                gridEditPopUp.Visibility = Visibility.Collapsed;
                CurrentStatus = newStatus;
                SetStatus(CurrentStatus);

                if (!string.IsNullOrEmpty(StatusEditTextFild.Text))
                {
                    StatusText.Text = StatusEditTextFild.Text;
                    await UpdateStatusInService(CurrentStatus, StatusText.Text);
                }
                else
                    updateStatusOnHomeButtonsClick();
            }
        }

        private async System.Threading.Tasks.Task UpdateStatusInService(long currentStatus, string statusText)
        {
            object result = await s12DoctorServices.changeAvailabilityStatus(currentStatus.ToString(), statusText,false);

            if (result is AvailabilityStatus) { }
                //((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((AvailabilityStatus)result).Meta.Message);
            else
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "Not Updated");
        }

        private void btnCreateClaimForm_Click(object sender, RoutedEventArgs e)
        {
            //if (gridCreateClaimForm.Visibility != Visibility)
            //{
            //    if (createClaimForm == null)
            //    {
            //        createClaimForm = new CreateClaimForm();
            //        createClaimForm.BtnPasswordSubmit.Click += BtnPasswordSubmit_Click;
            //        gridCreateClaimForm.Children.Add(createClaimForm);
            //    }

            //    try
            //    {
            //        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("Create Claim Form");
            //        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Visible);
            //        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
            //    }
            //    catch { }

            //    createClaimForm.SetDefaultData();
            //    gridCreateClaimForm.Visibility = Visibility.Visible;
            //}
        }

        async private void BtnPasswordSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;

            if (string.IsNullOrWhiteSpace(createClaimForm.txtPassword.Password))
                error = "Password cannot be blank.";

            if (!string.IsNullOrWhiteSpace(error))
            {
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            }
            else
            {
                try
                {
                    object result = await s12DoctorServices.verifyPassword(createClaimForm.txtPassword.Password);

                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;

                            string saveClaimFormresult = await createClaimForm.SaveClaimForm(isDraft: false);
                            if ("error".Equals(saveClaimFormresult)) { ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Visible); }
                            else
                            {
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Claim form submitted successfully.");
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("Section 12 doctor home");
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Collapsed);
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Visible);
                                createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                                gridCreateClaimForm.Visibility = Visibility.Collapsed;
                            }
                        }
                        else
                        {
                            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                        }
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                catch (Exception ex)
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }
            }
        }

        private void gridOpenEditProfile_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (EditProfile == null)
            {
                EditProfile = new EditProfile();
                EditProfile.btnSubmit.Click += BtnSubmit_Click;
                gridEditProfile.Children.Add(EditProfile);
            }

            try
            {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("Edit Profile");
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
            }
            catch { }

            gridEditProfile.Visibility = Visibility.Visible;
            EditProfile.SetDefaultData(true);
        }

        private async void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(EditProfile.Error))
            {
                gridEditProfile.Visibility = Visibility.Collapsed;
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("Section 12 doctor home");
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Visible);
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                            btnAvailable.Focus(FocusState.Pointer));
            }
        }

        public void SetStatus(long status)
        {
            try
            {
                SolidColorBrush foregroundSolidColorBrush;
                SolidColorBrush headerStatusTextSolidColorBrush;

                btnAvailable.Visibility = Visibility.Visible;
                btnMaybeAvailable.Visibility = Visibility.Visible;
                btnUnavailable.Visibility = Visibility.Visible;

                imgAvailable.Source = new BitmapImage(new Uri(
                               "ms-appx:///Assets/HomeScreen/available_unselect.png", UriKind.Absolute));
                imgMaybeAvailable.Source = new BitmapImage(new Uri(
                               "ms-appx:///Assets/HomeScreen/may_be_available_unselect.png", UriKind.Absolute));
                imgUnavailable.Source = new BitmapImage(new Uri(
                               "ms-appx:///Assets/HomeScreen/unavailable_unselect.png", UriKind.Absolute));
                SessionData.CurrntAppUser.Data.AvailabilityStatus = CurrentStatus;
                SessionData.CurrntAppUser.Data.AvailabilityMessage = StatusText.Text;

                switch (status)
                {
                    case 1:

                        HeaderStatusText.Text = "Available:";
                        foregroundSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 0, 216, 0);
                        HeaderStatusText.Foreground = foregroundSolidColorBrush;

                        btnAvailable.Visibility = Visibility.Collapsed;
                        Grid.SetRow(btnMaybeAvailable, 0);
                        Grid.SetRow(btnUnavailable, 1);

                        headerStatusTextSolidColorBrush = new SolidColorBrush();
                        headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 199, 255, 199);
                        StatusFill.Fill = headerStatusTextSolidColorBrush;
                        StatusImage.Source = new BitmapImage(new Uri(
                                "ms-appx:///Assets/HomeScreen/available.png", UriKind.Absolute));

                        imgAvailable.Source = new BitmapImage(new Uri(
                               "ms-appx:///Assets/HomeScreen/available_select.png", UriKind.Absolute));

                        break;
                    case 2:

                        HeaderStatusText.Text = "May be available:";
                        foregroundSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 186, 0);
                        HeaderStatusText.Foreground = foregroundSolidColorBrush;

                        btnMaybeAvailable.Visibility = Visibility.Collapsed;
                        Grid.SetRow(btnUnavailable, 1);
                        Grid.SetRow(btnAvailable, 0);

                        headerStatusTextSolidColorBrush = new SolidColorBrush();
                        headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 253, 181);
                        StatusFill.Fill = headerStatusTextSolidColorBrush;
                        StatusImage.Source = new BitmapImage(new Uri(
                                "ms-appx:///Assets/HomeScreen/may_be_available.png", UriKind.Absolute));

                        imgMaybeAvailable.Source = new BitmapImage(new Uri(
                               "ms-appx:///Assets/HomeScreen/may_be_available_select.png", UriKind.Absolute));

                        break;
                    case 3:

                        HeaderStatusText.Text = "Unavailable:";
                        foregroundSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 72, 88);
                        HeaderStatusText.Foreground = foregroundSolidColorBrush;

                        btnUnavailable.Visibility = Visibility.Collapsed;
                        Grid.SetRow(btnAvailable, 0);
                        Grid.SetRow(btnMaybeAvailable, 1);

                        headerStatusTextSolidColorBrush = new SolidColorBrush();
                        headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 255, 216, 219);
                        StatusFill.Fill = headerStatusTextSolidColorBrush;
                        StatusImage.Source = new BitmapImage(new Uri(
                                "ms-appx:///Assets/HomeScreen/unavailable.png", UriKind.Absolute));

                        imgUnavailable.Source = new BitmapImage(new Uri(
                               "ms-appx:///Assets/HomeScreen/unavailable_select.png", UriKind.Absolute));
                        break;
                    default:

                        HeaderStatusText.Text = "Unknown:";
                        foregroundSolidColorBrush = new SolidColorBrush();
                        foregroundSolidColorBrush.Color = Color.FromArgb(255, 165, 167, 185);
                        HeaderStatusText.Foreground = foregroundSolidColorBrush;

                        Grid.SetRow(btnAvailable, 0);
                        Grid.SetRow(btnMaybeAvailable, 1);
                        Grid.SetRow(btnUnavailable, 2);

                        headerStatusTextSolidColorBrush = new SolidColorBrush();
                        headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 237, 237, 241);
                        StatusFill.Fill = headerStatusTextSolidColorBrush;
                        StatusImage.Source = new BitmapImage(new Uri(
                                "ms-appx:///Assets/HomeScreen/unknown.png", UriKind.Absolute));

                        break;
                }
            }
            catch
            {

            }
            
        }

        public void DisposeMemory()
        {
            try
            {
                if (imgUnavailable != null)
                {
                    imgUnavailable.Resources.Clear();
                    imgUnavailable.Resources = null;
                    imgUnavailable.Source = null;
                    imgUnavailable = null;
                }
                if (imgMaybeAvailable != null)
                {
                    imgMaybeAvailable.Resources.Clear();
                    imgMaybeAvailable.Resources = null;
                    imgMaybeAvailable.Source = null;
                    imgMaybeAvailable = null;
                }
                if (imgAvailable != null)
                {
                    imgAvailable.Resources.Clear();
                    imgAvailable.Resources = null;
                    imgAvailable.Source = null;
                    imgAvailable = null;
                }
                if (EditStauts != null)
                {
                    EditStauts.Tapped -= EditStauts_Tapped;
                    EditStauts.Resources.Clear();
                    EditStauts.Resources = null;
                    EditStauts.Source = null;
                    EditStauts = null;
                }
                if (StatusImage != null)
                {
                    StatusImage.Resources.Clear();
                    StatusImage.Resources = null;
                    StatusImage.Source = null;
                    StatusImage = null;
                }
                if (StatusEditTextFild != null)
                {
                    StatusEditTextFild.Text = null;
                    StatusEditTextFild = null;
                }
                if (lblUnavailable != null)
                {
                    lblUnavailable.Text = null;
                    lblUnavailable = null;
                }
                if (lblMaybeAvailable != null)
                {
                    lblMaybeAvailable.Text = null;
                    lblMaybeAvailable = null;
                }
                if (lblAvailable != null)
                {
                    lblAvailable.Text = null;
                    lblAvailable = null;
                }
                if (s12WorkHeader != null)
                {
                    s12WorkHeader.Text = null;
                    s12WorkHeader = null;
                }
                if (s12WorkDetailsText != null)
                {
                    s12WorkDetailsText.Text = null;
                    s12WorkDetailsText = null;
                }
                if (StatusText != null)
                {
                    StatusText.Text = null;
                    StatusText = null;
                }
                if (StatusFill != null)
                {
                    StatusFill.Resources.Clear();
                    StatusFill.Resources = null;
                    StatusFill = null;
                }
                if (Rect3 != null)
                {
                    Rect3.Resources.Clear();
                    Rect3.Resources = null;
                    Rect3 = null;
                }
                if (Rect2 != null)
                {
                    Rect2.Resources.Clear();
                    Rect2.Resources = null;
                    Rect2 = null;
                }
                if (Rect1 != null)
                {
                    Rect1.Resources.Clear();
                    Rect1.Resources = null;
                    Rect1 = null;
                }
                if (brGeneral != null)
                {
                    brGeneral.Resources.Clear();
                    brGeneral.Resources = null;
                    brGeneral = null;
                }
                if (btnSaveStatusPopUp != null)
                {
                    btnSaveStatusPopUp.Click -= BtnSaveStatusPopUp_Click;
                    btnSaveStatusPopUp.Resources.Clear();
                    btnSaveStatusPopUp.Resources = null;
                    btnSaveStatusPopUp = null;
                }
                if (gridStatusAvailabilityButtons != null)
                {
                    //btnLogin.Click -= BtnLogin_Click;
                    gridStatusAvailabilityButtons.Resources.Clear();
                    gridStatusAvailabilityButtons.Resources = null;
                    gridStatusAvailabilityButtons = null;
                }
                if (btnAvailable != null)
                {
                    btnAvailable.Click -= btnAvailable_Click;
                    btnAvailable.Resources.Clear();
                    btnAvailable.Resources = null;
                    btnAvailable = null;
                }
                if (btnMaybeAvailable != null)
                {
                    btnMaybeAvailable.Click -= btnMaybeAvailable_Click;
                    btnMaybeAvailable.Resources.Clear();
                    btnMaybeAvailable.Resources = null;
                    btnMaybeAvailable = null;
                }
                if (btnUnavailable != null)
                {
                    btnUnavailable.Click -= btnUnavailable_Click;
                    btnUnavailable.Resources.Clear();
                    btnUnavailable.Resources = null;
                    btnUnavailable = null;
                }
                if (btnCreateClaimForm != null)
                {
                    btnCreateClaimForm.Click -= btnCreateClaimForm_Click;
                    btnCreateClaimForm.Resources.Clear();
                    btnCreateClaimForm.Resources = null;
                    btnCreateClaimForm = null;
                }
                if (gridStatusDetailView != null)
                {
                    gridStatusDetailView.Resources.Clear();
                    gridStatusDetailView.Resources = null;
                    gridStatusDetailView.Children.Clear();
                    gridStatusDetailView = null;
                }
                if (stkUnavailable != null)
                {
                    stkUnavailable.Resources.Clear();
                    stkUnavailable.Resources = null;
                    stkUnavailable.Children.Clear();
                    stkUnavailable = null;
                }
                if (stkMaybeAvailable != null)
                {
                    stkMaybeAvailable.Resources.Clear();
                    stkMaybeAvailable.Resources = null;
                    stkMaybeAvailable.Children.Clear();
                    stkMaybeAvailable = null;
                }
                if (stkAvailable != null)
                {
                    stkAvailable.Resources.Clear();
                    stkAvailable.Resources = null;
                    stkAvailable.Children.Clear();
                    stkAvailable = null;
                }
                if (gridInner != null)
                {
                    gridInner.Resources.Clear();
                    gridInner.Resources = null;
                    gridInner.Children.Clear();
                    gridInner = null;
                }
                if (gridBackShedow != null)
                {
                    gridBackShedow.Resources.Clear();
                    gridBackShedow.Resources = null;
                    gridBackShedow.Children.Clear();
                    gridBackShedow = null;
                }
                if (gridEditPopUp != null)
                {
                    gridEditPopUp.Resources.Clear();
                    gridEditPopUp.Resources = null;
                    gridEditPopUp.Children.Clear();
                    gridEditPopUp = null;
                }
                if (gridGeneralAvailability != null)
                {
                    gridGeneralAvailability.Resources.Clear();
                    gridGeneralAvailability.Resources = null;
                    gridGeneralAvailability.Children.Clear();
                    gridGeneralAvailability = null;
                }
                if (stkStatusGroup != null)
                {
                    stkStatusGroup.Resources.Clear();
                    stkStatusGroup.Resources = null;
                    stkStatusGroup.Children.Clear();
                    stkStatusGroup = null;
                }
                if (gridContaint != null)
                {
                    gridContaint.Resources.Clear();
                    gridContaint.Resources = null;
                    gridContaint.Children.Clear();
                    gridContaint = null;
                }
                if (scroll != null)
                {
                    scroll.Resources.Clear();
                    scroll.Resources = null;
                    scroll = null;
                }
                if (LayoutRoot != null)
                {
                    LayoutRoot.Resources.Clear();
                    LayoutRoot.Resources = null;
                    LayoutRoot.Children.Clear();
                    LayoutRoot = null;
                }
                if (HomeUserControl != null)
                {
                    HomeUserControl.Resources.Clear();
                    HomeUserControl.Resources = null;
                    HomeUserControl = null;
                }
                GC.Collect();
            }
            catch (Exception)
            { }
        }

        private async void btn_save(object sender, RoutedEventArgs e)
        {
            try
            {

                int mon_morning = stk_mon_mor.Visibility == Visibility.Visible ? 1 : 0;
                int tue_morning = stk_mor_tue.Visibility == Visibility.Visible ? 1 : 0;
                int wed_morning = stk_mor_wed.Visibility == Visibility.Visible ? 1 : 0;
                int thu_morning = stk_morn_thu.Visibility == Visibility.Visible ? 1 : 0;
                int fri_morning = stk_morn_fri.Visibility == Visibility.Visible ? 1 : 0;
                int sat_morning = stk_morn_sat.Visibility == Visibility.Visible ? 1 : 0;
                int sun_morning = stk_morn_sun.Visibility == Visibility.Visible ? 1 : 0;
                string moring = mon_morning + "," + tue_morning + "," + wed_morning + "," + thu_morning + "," + fri_morning + "," + sat_morning + "," + sun_morning;

                int mon_lunch = stk_lunch_mon.Visibility == Visibility.Visible ? 1 : 0;
                int tue_lunch = stk_lunch_tue.Visibility == Visibility.Visible ? 1 : 0;
                int wed_lunch = stk_lunch_wed.Visibility == Visibility.Visible ? 1 : 0;
                int thu_lunch = stk_lunch_thu.Visibility == Visibility.Visible ? 1 : 0;
                int fri_lunch = stk_lunch_fri.Visibility == Visibility.Visible ? 1 : 0;
                int sat_lunch = stk_lunch_sat.Visibility == Visibility.Visible ? 1 : 0;
                int sun_lunch = stk_lunch_sun.Visibility == Visibility.Visible ? 1 : 0;
                string lunch = mon_lunch + "," + tue_lunch + "," + wed_lunch + "," + thu_lunch + "," + fri_lunch + "," + sat_lunch + "," + sun_lunch;

                int mon_aftn = stk_aftn_mon.Visibility == Visibility.Visible ? 1 : 0;
                int tue_aftn = stk_aftn_tue.Visibility == Visibility.Visible ? 1 : 0;
                int wed_aftn = stk_aftn_wed.Visibility == Visibility.Visible ? 1 : 0;
                int thu_aftn = stk_aftn_thu.Visibility == Visibility.Visible ? 1 : 0;
                int fri_aftn = stk_aftn_fri.Visibility == Visibility.Visible ? 1 : 0;
                int sat_aftn = stk_aftn_sat.Visibility == Visibility.Visible ? 1 : 0;
                int sun_aftn = stk_aftn_sun.Visibility == Visibility.Visible ? 1 : 0;
                string aftn = mon_aftn + "," + tue_aftn + "," + wed_aftn + "," + thu_aftn + "," + fri_aftn + "," + sat_aftn + "," + sun_aftn;

                int mon_eve = stk_eve_mon.Visibility == Visibility.Visible ? 1 : 0;
                int tue_eve = stk_eve_tue.Visibility == Visibility.Visible ? 1 : 0;
                int wed_eve = stk_eve_wed.Visibility == Visibility.Visible ? 1 : 0;
                int thu_eve = stk_eve_thu.Visibility == Visibility.Visible ? 1 : 0;
                int fri_eve = stk_eve_fri.Visibility == Visibility.Visible ? 1 : 0;
                int sat_eve = stk_eve_sat.Visibility == Visibility.Visible ? 1 : 0;
                int sun_eve = stk_eve_sun.Visibility == Visibility.Visible ? 1 : 0;
                string eve = mon_eve + "," + tue_eve + "," + wed_eve + "," + thu_eve + "," + fri_eve + "," + sat_eve + "," + sun_eve;

                int mon_night = stk_night_mon.Visibility == Visibility.Visible ? 1 : 0;
                int tue_night = stk_night_tue.Visibility == Visibility.Visible ? 1 : 0;
                int wed_night = stk_night_wed.Visibility == Visibility.Visible ? 1 : 0;
                int thu_night = stk_night_thu.Visibility == Visibility.Visible ? 1 : 0;
                int fri_night = stk_night_fri.Visibility == Visibility.Visible ? 1 : 0;
                int sat_night = stk_night_sat.Visibility == Visibility.Visible ? 1 : 0;
                int sun_night = stk_night_sun.Visibility == Visibility.Visible ? 1 : 0;
                string night = mon_night + "," + tue_night + "," + wed_night + "," + thu_night + "," + fri_night + "," + sat_night + "," + sun_night;

                //store calendar locally
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

                if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    Windows.Storage.ApplicationDataCompositeValue composite = new Windows.Storage.ApplicationDataCompositeValue();
                    composite["am"] = moring;
                    composite["lunch"] = lunch;
                    composite["pm"] = aftn;
                    composite["evening"] = eve;
                    composite["night"] = night;
                    composite["isSynced"] = "0";
                    localSettings.Values["calanderData"] = composite;
                    localSettings.Values["calanderAvailibility"] = composite;

                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("The update will sync when regained internet connection");
                }
                else
                {
                    object result = await s12DoctorServices.availabilityCalander(moring, lunch, aftn, eve, night,true);
                    ///save data locally for sync
                    //Windows.Storage.ApplicationDataCompositeValue composite = (Windows.Storage.ApplicationDataCompositeValue)localSettings.Values["calanderData"];
                    Windows.Storage.ApplicationDataCompositeValue composite = new Windows.Storage.ApplicationDataCompositeValue();
                    composite["am"] = moring;
                    composite["lunch"] = lunch;
                    composite["pm"] = aftn;
                    composite["evening"] = eve;
                    composite["night"] = night;
                    composite["isSynced"] = "1";
                    localSettings.Values["calanderData"] = composite;

                   
                        if (result is CalanderAvailibility)
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Weekly available status updated successfully.");
                    }
                    else
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }

                }
               
            } catch (Exception ex) {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        private void mor_mon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (mor_mon.IsChecked == true)
            {
                stk_mon_mor.Visibility = Visibility.Visible;
            }
            else
            {
                stk_mon_mor.Visibility = Visibility.Collapsed;
            }
        }

        private void lunch_mon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (lunch_mon.IsChecked == true)
                stk_lunch_mon.Visibility = Visibility.Visible;
            else
                stk_lunch_mon.Visibility = Visibility.Collapsed;
        }

        private void aftn_mon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (aftn_mon.IsChecked == true)
                stk_aftn_mon.Visibility = Visibility.Visible;
            else
                stk_aftn_mon.Visibility = Visibility.Collapsed;
        }

        private void eve_mon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (eve_mon.IsChecked == true)
                stk_eve_mon.Visibility = Visibility.Visible;
            else
                stk_eve_mon.Visibility = Visibility.Collapsed;
        }

        private void night_mon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (night_mon.IsChecked == true)
                stk_night_mon.Visibility = Visibility.Visible;
            else
                stk_night_mon.Visibility = Visibility.Collapsed;
        }

        private void mor_tue_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (mor_tue.IsChecked == true)
                stk_mor_tue.Visibility = Visibility.Visible;
            else
                stk_mor_tue.Visibility = Visibility.Collapsed;
        }

        private void lunch_tue_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (lunch_tue.IsChecked == true)
                stk_lunch_tue.Visibility = Visibility.Visible;
            else
                stk_lunch_tue.Visibility = Visibility.Collapsed;
        }

        private void aftn_tue_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (aftn_tue.IsChecked == true)
                stk_aftn_tue.Visibility = Visibility.Visible;
            else
                stk_aftn_tue.Visibility = Visibility.Collapsed;
        }

        private void eve_tue_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (eve_tue.IsChecked == true)
                stk_eve_tue.Visibility = Visibility.Visible;
            else
                stk_eve_tue.Visibility = Visibility.Collapsed;
        }

        private void night_tue_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (night_tue.IsChecked == true)
                stk_night_tue.Visibility = Visibility.Visible;
            else
                stk_night_tue.Visibility = Visibility.Collapsed;
        }

        private void mor_wed_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (mor_wed.IsChecked == true)
                stk_mor_wed.Visibility = Visibility.Visible;
            else
                stk_mor_wed.Visibility = Visibility.Collapsed;
        }

        private void lunch_wed_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (lunch_wed.IsChecked == true)
                stk_lunch_wed.Visibility = Visibility.Visible;
            else
                stk_lunch_wed.Visibility = Visibility.Collapsed;
        }

        private void aftn_wed_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (aftn_wed.IsChecked == true)
                stk_aftn_wed.Visibility = Visibility.Visible;
            else
                stk_aftn_wed.Visibility = Visibility.Collapsed;
        }

        private void eve_wed_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (eve_wed.IsChecked == true)
                stk_eve_wed.Visibility = Visibility.Visible;
            else
                stk_eve_wed.Visibility = Visibility.Collapsed;
        }

        private void night_wed_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (night_wed.IsChecked == true)
                stk_night_wed.Visibility = Visibility.Visible;
            else
                stk_night_wed.Visibility = Visibility.Collapsed;
        }

        private void mor_thu_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (mor_thu.IsChecked == true)
                stk_morn_thu.Visibility = Visibility.Visible;
            else
                stk_morn_thu.Visibility = Visibility.Collapsed;
        }

        private void lunch_thu_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (lunch_thu.IsChecked == true)
                stk_lunch_thu.Visibility = Visibility.Visible;
            else
                stk_lunch_thu.Visibility = Visibility.Collapsed;
        }

        private void aftn_thu_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (aftn_thu.IsChecked == true)
                stk_aftn_thu.Visibility = Visibility.Visible;
            else
                stk_aftn_thu.Visibility = Visibility.Collapsed;
        }

        private void eve_thu_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (eve_thu.IsChecked == true)
                stk_eve_thu.Visibility = Visibility.Visible;
            else
                stk_eve_thu.Visibility = Visibility.Collapsed;
        }

        private void night_thu_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (night_thu.IsChecked == true)
                stk_night_thu.Visibility = Visibility.Visible;
            else
                stk_night_thu.Visibility = Visibility.Collapsed;
        }

        private void morn_fri_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (mor_fri.IsChecked == true)
                stk_morn_fri.Visibility = Visibility.Visible;
            else
                stk_morn_fri.Visibility = Visibility.Collapsed;
        }

        private void lunch_fri_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (lunch_fri.IsChecked == true)
                stk_lunch_fri.Visibility = Visibility.Visible;
            else
                stk_lunch_fri.Visibility = Visibility.Collapsed;
        }

        private void aftn_fri_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (aftn_fri.IsChecked == true)
                stk_aftn_fri.Visibility = Visibility.Visible;
            else
                stk_aftn_fri.Visibility = Visibility.Collapsed;
        }

        private void eve_fri_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (eve_fri.IsChecked == true)
                stk_eve_fri.Visibility = Visibility.Visible;
            else
                stk_eve_fri.Visibility = Visibility.Collapsed;
        }

        private void night_fri_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (night_fri.IsChecked == true)
                stk_night_fri.Visibility = Visibility.Visible;
            else
                stk_night_fri.Visibility = Visibility.Collapsed;
        }

        private void mor_sat_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (mor_sat.IsChecked == true)
                stk_morn_sat.Visibility = Visibility.Visible;
            else
                stk_morn_sat.Visibility = Visibility.Collapsed;
        }

        private void lunch_sat_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (lunch_sat.IsChecked == true)
                stk_lunch_sat.Visibility = Visibility.Visible;
            else
                stk_lunch_sat.Visibility = Visibility.Collapsed;
        }

        private void aftn_sat_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (aftn_sat.IsChecked == true)
                stk_aftn_sat.Visibility = Visibility.Visible;
            else
                stk_aftn_sat.Visibility = Visibility.Collapsed;
        }

        private void eve_sat_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (eve_sat.IsChecked == true)
                stk_eve_sat.Visibility = Visibility.Visible;
            else
                stk_eve_sat.Visibility = Visibility.Collapsed;
        }

        private void night_sat_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (night_sat.IsChecked == true)
                stk_night_sat.Visibility = Visibility.Visible;
            else
                stk_night_sat.Visibility = Visibility.Collapsed;
        }

        private void mor_sun_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (mor_sun.IsChecked == true)
                stk_morn_sun.Visibility = Visibility.Visible;
            else
                stk_morn_sun.Visibility = Visibility.Collapsed;
        }

        private void lunch_sun_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (lunch_sun.IsChecked == true)
                stk_lunch_sun.Visibility = Visibility.Visible;
            else
                stk_lunch_sun.Visibility = Visibility.Collapsed;
        }

        private void aftn_sun_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (aftn_sun.IsChecked == true)
                stk_aftn_sun.Visibility = Visibility.Visible;
            else
                stk_aftn_sun.Visibility = Visibility.Collapsed;
        }

        private void eve_sun_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (eve_sun.IsChecked == true)
                stk_eve_sun.Visibility = Visibility.Visible;
            else
                stk_eve_sun.Visibility = Visibility.Collapsed;
        }

        private void night_sun_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (night_sun.IsChecked == true)
                stk_night_sun.Visibility = Visibility.Visible;
            else
                stk_night_sun.Visibility = Visibility.Collapsed;
        }
    }
}
