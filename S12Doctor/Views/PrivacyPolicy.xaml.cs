﻿using S12Doctor.Model.Common;
using S12Doctor.Utilities;
using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace S12Doctor.Views
{
    public sealed partial class PrivacyPolicy : UserControl
    {
        bool willNavigate;
        private WebService.CommonServices common;
        public PrivacyPolicy()
        {
            this.InitializeComponent();
            this.Loaded += PrivacyPolicy_Loaded;
        }

        private async void PrivacyPolicy_Loaded(object sender, RoutedEventArgs e)
        {
            if (common == null)
                common = new WebService.CommonServices();

            if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                webViewer.NavigateToString(S12Doctor.Utilities.SessionData.CMSData.Data.TermsAndConditionsContent);
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                }
                else
                {
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                }
            }
            else
            {
                object result = await common.cms();
                if (result is CMS)
                {
                    SessionData.CMSTermsConditionData = ((CMS)result);
                }
                else
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                if (S12Doctor.Utilities.SessionData.CMSTermsConditionData != null)
                {
                    string strHTML = "<html><head><style type=\"text/css\"> li{ font-size:45px; } td {font-size:30px;} </style></head><body>" +
                        S12Doctor.Utilities.SessionData.CMSTermsConditionData.Data.PrivacyContent + "<body></html>";
                    strHTML = strHTML.Replace("width:605px", "width:95%");
                    strHTML= strHTML.Replace("<p style=\"margin-left:36pt; margin-right:0cm\" style=\"color:red;font-size:30px\">", "<span style=\"margin-left:36pt; margin-right:0cm\" style=\"font-size:40px\">");
                    webViewer.NavigateToString(strHTML);
                }
            }
        }

        private async void webViewer_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            if (!willNavigate)
            {
                willNavigate = true;
                return;
            }

            // cancel navigation to the clicked link in the webBrowser control
            args.Cancel = true;
            string str = args.Uri.ToString();
            var uriLink = new Uri(str);

            // Launch the URI
            var success = await Windows.System.Launcher.LaunchUriAsync(uriLink);

            if (success)
            {
                // URI launched
            }
            else
            {
                // URI launch failed
            }
            
        }
        
    }
}
