﻿using S12Doctor.Utilities;
using S12Doctor.Model.Common;
using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace S12Doctor.Views
{
    public sealed partial class AboutUs : UserControl
    {
        private WebService.CommonServices common;
        public AboutUs()
        {
            this.InitializeComponent();
            this.Loaded += AboutUs_Loaded;
        }

        private async void AboutUs_Loaded(object sender, RoutedEventArgs e)
        {
            if (common == null)
                common = new WebService.CommonServices();

            if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                webViewer.NavigateToString(S12Doctor.Utilities.SessionData.CMSData.Data.TermsAndConditionsContent);
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                }
                else
                {
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                }
            }
            else
            {
                object result = await common.cms();
                if (result is CMS)
                {
                    SessionData.CMSTermsConditionData = ((CMS)result);
                }
                else
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                if (S12Doctor.Utilities.SessionData.CMSTermsConditionData != null)
                {
                    string aboutUs = "<html><head><style type=\"text/css\"> li{ font-size:45px; }</style></head><body>" +
                            S12Doctor.Utilities.SessionData.CMSTermsConditionData.Data.AboutUsContent +
                            "<body></html>";
                    webViewer.NavigateToString(aboutUs);
                }
            }
        }
    }
}
