﻿using S12Doctor.Model.Common;
using S12Doctor.Model.S12Doctor;
using S12Doctor.WebService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Views
{
    public sealed partial class DraftClaimForm : UserControl
    {
        ObservableCollection<ClaimFormData> draftData;
        private S12DoctorServices s12DoctorServices;

        public CreateClaimForm createClaimForm;
        private bool isDeleteMode = false;
        private bool incall = false, endoflist = false, fatchData = false;
        private int offset = 0;

        public DraftClaimForm()
        {
            this.InitializeComponent();
            this.Loaded += DraftClaimForm_Loaded;
        }

        private void DraftClaimForm_Loaded(object sender, RoutedEventArgs e)
        {
            if (s12DoctorServices == null)
                s12DoctorServices = new S12DoctorServices();

            if (draftData == null)
                draftData = new ObservableCollection<ClaimFormData>();

            listViewDraft.ItemsSource = draftData;
        }

        async public void ReloadData()
        {
            object draftClaimFoprmData = await s12DoctorServices.completedClaimForm("1", "0", "10");

            if (draftClaimFoprmData is CompletedClaimForm)
            {
                var data = new ObservableCollection<ClaimFormData>(((CompletedClaimForm)draftClaimFoprmData).Data);

                offset = 0;
                if (draftData == null)
                    draftData = new ObservableCollection<ClaimFormData>();

                draftData.Clear();
                foreach (var item in data)
                {
                    if (!string.IsNullOrEmpty(item.AmhpName))
                        item.Title = "AMHP: " + item.AmhpName;
                    else if (!string.IsNullOrEmpty(item.AssessmentPostcode))
                        item.Title = "Postcode: " + item.AssessmentPostcode;
                    else item.Title = "Draft";

                    draftData.Add(item);
                }

                if (draftData.Count <= 0)
                {
                    imgNoDraft.Visibility = Visibility.Visible;
                    gridSelection.Visibility = Visibility.Collapsed;
                }

                offset = (int)((CompletedClaimForm)draftClaimFoprmData).Meta.NewOffset;
                isDeleteMode = false; incall = endoflist = fatchData = false;
            }
            else
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(draftClaimFoprmData != null ? draftClaimFoprmData.ToString() : "An unknown error has occurred.");
        }

        async public void GetOffsetData(int _offset, int count = 10)
        {
            object draftClaimFoprmData = await s12DoctorServices.completedClaimForm("1", _offset.ToString(), count.ToString());

            if (draftClaimFoprmData is CompletedClaimForm)
            {
                var data = new ObservableCollection<ClaimFormData>(((CompletedClaimForm)draftClaimFoprmData).Data);

                if (draftData == null)
                    draftData = new ObservableCollection<ClaimFormData>();
                
                foreach (var item in data)
                {
                    if (!string.IsNullOrEmpty(item.AmhpName))
                        item.Title = "AMHP: " + item.AmhpName;
                    else if (!string.IsNullOrEmpty(item.AssessmentPostcode))
                        item.Title = "Postcode: " + item.AssessmentPostcode;
                    else item.Title = "Draft";

                    draftData.Add(item);
                }

                if (draftData.Count <= 0)
                {
                    imgNoDraft.Visibility = Visibility.Visible;
                    gridSelection.Visibility = Visibility.Collapsed;
                }

                offset = (int)((CompletedClaimForm)draftClaimFoprmData).Meta.NewOffset;
                fatchData = false;
            }
            else
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(draftClaimFoprmData != null ? draftClaimFoprmData.ToString() : "An unknown error has occurred.");
        }

        async private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;

            if (string.IsNullOrWhiteSpace(txtPassword.Password))
                error = "Password cannot be blank.";

            if (!string.IsNullOrWhiteSpace(error))
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
            {
                try
                {
                    object result = await s12DoctorServices.verifyPassword(txtPassword.Password);

                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            gridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                            GetOffsetData(offset);
                        }
                        else
                            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                catch (Exception ex)
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }
            }
        }

        private void listViewDraft_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!isDeleteMode)
            {
                if (gridCreateClaimForm.Visibility != Visibility.Visible)
                {
                    if (createClaimForm == null)
                    {
                        createClaimForm = new CreateClaimForm();
                        createClaimForm.BtnPasswordSubmit.Click += BtnPasswordSubmit_Click;
                        gridCreateClaimForm.Children.Add(createClaimForm);
                    }

                    try
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("Create Claim Form");
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Visible);
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                    }
                    catch { }

                    ClaimFormData data = ((ClaimFormData)listViewDraft.SelectedItem);

                    createClaimForm.SetDefaultData(data);
                    gridCreateClaimForm.Visibility = Visibility.Visible;
                    listViewDraft.SelectedIndex = -1;
                }
            }
            else
            {
                try
                {
                    if (listViewDraft.SelectedIndex != -1)
                    {
                        if (draftData[listViewDraft.SelectedIndex].IsDeleteMode == true)
                        {
                            draftData[listViewDraft.SelectedIndex].IsDeleteMode = false;
                        }
                        else
                        {
                            draftData[listViewDraft.SelectedIndex].IsDeleteMode = true;
                        }
                    }
                    listViewDraft.SelectedIndex = -1;
                }
                catch { }
            }
        }

        async private void BtnPasswordSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;

            if (string.IsNullOrWhiteSpace(createClaimForm.txtPassword.Password))
                error = "Password cannot be blank.";

            if (!string.IsNullOrWhiteSpace(error))
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
            {
                try
                {
                    object result = await s12DoctorServices.verifyPassword(createClaimForm.txtPassword.Password);

                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;

                            string saveClaimFormresult = await createClaimForm.SaveClaimForm(isDraft: false);
                            if ("error".Equals(saveClaimFormresult)) { ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Visible); }
                            else
                            {
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Claim form submitted successfully.");
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("Drafts");
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Collapsed);
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Visible);
                                createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                                gridCreateClaimForm.Visibility = Visibility.Collapsed;
                                ReloadData();
                            }
                        }
                        else
                        {
                            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                        }
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                catch (Exception ex)
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }
            }
        }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            isDeleteMode = true;
            btnSelect.Visibility = Visibility.Collapsed;
            btnDelete.Visibility = btnCancel.Visibility = Visibility.Visible;
        }

        async private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<ClaimFormData> data = draftData.Where(i => i.IsDeleteMode == true).ToList();

                foreach (var item in data)
                {
                    object result = await s12DoctorServices.deleteClaimForm(item.ClaimId.ToString());

                    if (result is ClaimForm) draftData.Remove(item);
                    else ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }

                isDeleteMode = false;
                if (draftData.Count <= 0)
                {
                    imgNoDraft.Visibility = Visibility.Visible;
                    gridSelection.Visibility = Visibility.Collapsed;
                }
                else
                    btnSelect.Visibility = Visibility.Visible;
                btnDelete.Visibility = btnCancel.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < draftData.Count; i++)
            {
                draftData[i].IsDeleteMode = false;
            }

            isDeleteMode = false;
            btnSelect.Visibility = Visibility.Visible;
            btnDelete.Visibility = btnCancel.Visibility = Visibility.Collapsed;
        }

        private void Draftlist_Loaded(object sender, RoutedEventArgs e)
        {
            ScrollViewer viewer = GetScrollViewer(this.listViewDraft);
            viewer.ViewChanged += MainPage_ViewChanged;
        }

        private void MainPage_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer view = (ScrollViewer)sender;
            double progress = view.VerticalOffset / view.ScrollableHeight;
            System.Diagnostics.Debug.WriteLine(progress);

            if (progress == 1 & !incall && !endoflist)
            {
                incall = true;
                fetchCountries(offset);
            }
        }

        private void fetchCountries(int offset)
        {
            if (!fatchData)
            {
                if (offset > 0)
                {
                    GetOffsetData(offset);
                    fatchData = true;
                }
                else
                {
                    endoflist = true;
                }
            }
            incall = false;
        }

        // method to pull out a ScrollViewer
        private ScrollViewer GetScrollViewer(DependencyObject depObj)
        {
            if (depObj is ScrollViewer) return depObj as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);
                var result = GetScrollViewer(child);
                if (result != null) return result;
            }

            return null;
        }
    }
}
