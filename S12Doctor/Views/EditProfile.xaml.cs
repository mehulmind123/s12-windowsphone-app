﻿using S12Doctor.Model;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using System.Linq;
using System.Text.RegularExpressions;
using Windows.UI.Core;
using System;
using S12Doctor.Model.S12Doctor;
using S12Doctor.WebService;
using S12Doctor.Utilities;
using S12Doctor.Model.Common;

namespace S12Doctor.Views
{
    public sealed partial class EditProfile : UserControl
    {
        public Views.Home HomeControl;
        private LocalAuthorityList localAuthorities;
        private OrganisationList employerList;
        private LanguageList languages;
        private SpecialityList specialty;
        private OrganisationList employer;
        private S12DoctorServices s12DoctorServices;

        public EditProfile()
        {
            this.InitializeComponent();
        }

        public async void SetDefaultData(bool isGeneralAvailabilityOnFocus)
        {
            try
            {
                if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    if (s12DoctorServices == null)
                        s12DoctorServices = new S12DoctorServices();

                    object userInfo = await s12DoctorServices.userDetail();
                    if (userInfo is AppUser)
                    {
                        SessionData.CurrntAppUser = (AppUser)userInfo;
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(userInfo != null ? userInfo.ToString() : "An unknown error has occurred.");


                    object localAuthoritiesresult = await s12DoctorServices.localAuthorityList("all", SessionData.CurrntAppUser.Data.RegionId.ToString());
                    if (localAuthoritiesresult is LocalAuthorityList)
                    {
                        localAuthorities = (LocalAuthorityList)localAuthoritiesresult;
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(localAuthoritiesresult != null ? localAuthoritiesresult.ToString() : "An unknown error has occurred.");
//// Employer

                    object employerresult = await s12DoctorServices.organisationList();
                    if (employerresult is OrganisationList)
                    {
                        employerList = (OrganisationList)employerresult;
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(employerresult != null ? employerresult.ToString() : "An unknown error has occurred.");



                    object languagesresult = await s12DoctorServices.languageList();
                    if (languagesresult is LanguageList)
                    {
                        languages = (LanguageList)languagesresult;
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(languagesresult != null ? languagesresult.ToString() : "An unknown error has occurred.");

                    object specialtyresult = await s12DoctorServices.specialityList();
                    if (specialtyresult is SpecialityList)
                    {
                        specialty = (SpecialityList)specialtyresult;
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(specialtyresult != null ? specialtyresult.ToString() : "An unknown error has occurred.");


                    listEmployer.ItemsSource = employerList.Data;
                    listLocalAuthorities.ItemsSource = localAuthorities.Data;
                    listLanguages.ItemsSource = languages.Data;
                    listSpecialities.ItemsSource = specialty.Data;
                   
                    this.DataContext = null;
                    this.DataContext = SessionData.CurrntAppUser.Data;

                    setEmployer();
                    setLocalAuthorities();
                    setLanguages();
                    setSpecialtie();

                    if (isGeneralAvailabilityOnFocus)
                        SetGeneralAvailabilityOnFocus();

                    this.UpdateLayout();
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Please check your internet connection or try again later.");
            }
            catch (Exception ex)
            {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        private void setEmployer()
        {
            try
            {
                if (SessionData.CurrntAppUser.Data.Employer != null && employerList.Data != null)
                {
                    foreach (var item in SessionData.CurrntAppUser.Data.Employer)
                    {
                        if (employerList.Data.Where(em => em.OrganisationId == item.OrganisationId).FirstOrDefault() != null)
                            employerList.Data.Where(em => em.OrganisationId == item.OrganisationId).FirstOrDefault().IsChecked = true;
                    }
                }
                txtEmployer.Text = employerList.SelectedOrganisationByName;
            }
            catch (Exception)
            { }
        }

        private void setLocalAuthorities()
        {
            try
            {
                if (SessionData.CurrntAppUser.Data.LocalAuthority != null && localAuthorities.Data != null)
                {
                    foreach (var item in SessionData.CurrntAppUser.Data.LocalAuthority)
                    {
                        if (localAuthorities.Data.Where(la => la.AuthorityId == item.AuthorityId).FirstOrDefault() != null)
                            localAuthorities.Data.Where(la => la.AuthorityId == item.AuthorityId).FirstOrDefault().IsChecked = true;
                    }
                }
                txtLocalAuthoroty.Text = localAuthorities.SelectedLocalAuthorityByName;
            }
            catch (Exception)
            { }
        }

        private void setLanguages()
        {
            try
            {
                if (SessionData.CurrntAppUser.Data.LanguageSpoken != null && languages.Data != null)
                {
                    foreach (var item in SessionData.CurrntAppUser.Data.LanguageSpoken)
                    {
                        if (languages.Data.Where(la => la.LanguageId == item.LanguageId).FirstOrDefault() != null)
                            languages.Data.Where(la => la.LanguageId == item.LanguageId).FirstOrDefault().IsChecked = true;
                    }
                }
                txtLanguages.Text = languages.SelectedLanguageByName;
            }
            catch (Exception)
            { }
        }

        private void setSpecialtie()
        {
            try
            {
                if (SessionData.CurrntAppUser.Data.Specialties != null && specialty.Data != null)
                {
                    foreach (var item in SessionData.CurrntAppUser.Data.Specialties)
                    {
                        if (specialty.Data.Where(la => la.SpecialtiesId == item.SpecialtiesId).FirstOrDefault() != null)
                            specialty.Data.Where(la => la.SpecialtiesId == item.SpecialtiesId).FirstOrDefault().IsChecked = true;
                    }
                }
                txtSpecialities.Text = specialty.SelectedSpecialityByName;
            }
            catch (Exception)
            { }
        }

        public async void SetGeneralAvailabilityOnFocus()
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
              txtGeneralAvailability.Focus(FocusState.Keyboard));
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                gridLocalAuthorities.Visibility = Visibility.Collapsed;
                gridEmployerList.Visibility = Visibility.Collapsed;

                gridLanguages.Visibility = Visibility.Collapsed;
                gridSpecialities.Visibility = Visibility.Collapsed;
                txtLocalAuthoroty.Text = localAuthorities.SelectedLocalAuthorityByName;
                txtEmployer.Text = employerList.SelectedOrganisationByName;
                txtLanguages.Text = languages.SelectedLanguageByName;
                txtSpecialities.Text = specialty.SelectedSpecialityByName;

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).RemoveUserControlFromHeader();
            }
            catch { }
        }
        
        private void brEmployer_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtEmployer.Text))
                {
                    string[] localEmployerNames = txtEmployer.Text.Split(',');
                    if (localEmployerNames.Length > 0)
                    {
                        for (int i = 0; i < localEmployerNames.Length; i++)
                        {
                            employerList.Data.Where(em => em.OrganisationName.Contains(localEmployerNames[i].Trim())).FirstOrDefault().IsChecked = true;
                        }
                    }
                }
                else
                    foreach (var item in employerList.Data)
                    {
                        if (item.IsChecked)
                            item.IsChecked = false;
                    }

                gridEmployerList.Visibility = Visibility.Visible;

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetUserControlOnHeader();
            }
            catch { }
        }


        private void brLocalAuthoroty_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtLocalAuthoroty.Text))
                {
                    string[] localAuthorotyNames = txtLocalAuthoroty.Text.Split(',');
                    if (localAuthorotyNames.Length > 0)
                    {
                        for (int i = 0; i < localAuthorotyNames.Length; i++)
                        {
                            localAuthorities.Data.Where(la => la.AuthorityName.Contains(localAuthorotyNames[i].Trim())).FirstOrDefault().IsChecked = true;
                        }
                    }
                }
                else
                    foreach (var item in localAuthorities.Data)
                    {
                        if (item.IsChecked)
                            item.IsChecked = false;
                    }

                gridLocalAuthorities.Visibility = Visibility.Visible;

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetUserControlOnHeader();
            }
            catch { }
        }

        private void brLanguages_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtLanguages.Text))
                {
                    string[] languageNames = txtLanguages.Text.Split(',');
                    if (languageNames.Length > 0)
                    {
                        for (int i = 0; i < languageNames.Length; i++)
                        {
                            languages.Data.Where(len => len.LanguageName == languageNames[i].Trim()).FirstOrDefault().IsChecked = true;
                        }
                    }
                }
                else
                    foreach (var item in languages.Data)
                    {
                        if (item.IsChecked)
                            item.IsChecked = false;
                    }

                gridLanguages.Visibility = Visibility.Visible;

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetUserControlOnHeader();
            }
            catch { }
        }

        private void brSpecialities_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtSpecialities.Text))
                {
                    string[] specialitieNames = txtSpecialities.Text.Split(',');
                    if (specialitieNames.Length > 0)
                    {
                        for (int i = 0; i < specialitieNames.Length; i++)
                        {
                            specialty.Data.Where(len => len.SpecialtiesName == specialitieNames[i].Trim()).FirstOrDefault().IsChecked = true;
                        }
                    }
                }
                else
                    foreach (var item in specialty.Data)
                    {
                        if (item.IsChecked)
                            item.IsChecked = false;
                    }

                gridSpecialities.Visibility = Visibility.Visible;

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetUserControlOnHeader();
            }
            catch { }
        }

        private void btnAddLanguage_Click(object sender, RoutedEventArgs e)
        {
            gridAddLanguagePopUp.Visibility = Visibility.Visible;
        }

        private void gridBack_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridAddLanguagePopUp.Visibility = Visibility.Collapsed;
        }

        private void btnAddNewLanguage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtLanguage.Text) &&
                    languages.Data.Where(i => i.LanguageName == txtLanguage.Text).FirstOrDefault() == null)
                {
                    languages.AddLanguage(txtLanguage.Text);
                    txtLanguage.Text = string.Empty;
                    gridAddLanguagePopUp.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Language name can not be blank.");
                }
            }
            catch (Exception)
            { }
        }
        private void listEmployer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (listEmployer.SelectedIndex != -1)
                {
                    if (employerList.Data[listEmployer.SelectedIndex].IsChecked == true)
                    {
                        employerList.Data[listEmployer.SelectedIndex].IsChecked = false;
                    }
                    else
                    {
                        employerList.Data[listEmployer.SelectedIndex].IsChecked = true;
                    }
                }

                listEmployer.SelectedIndex = -1;
            }
            catch { }
        }

        private void listLocalAuthorities_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (listLocalAuthorities.SelectedIndex != -1)
                {
                    if (localAuthorities.Data[listLocalAuthorities.SelectedIndex].IsChecked == true)
                    {
                        localAuthorities.Data[listLocalAuthorities.SelectedIndex].IsChecked = false;
                    }
                    else
                    {
                        localAuthorities.Data[listLocalAuthorities.SelectedIndex].IsChecked = true;
                    }
                }

                listLocalAuthorities.SelectedIndex = -1;
            }
            catch { }
        }

        private void listLanguages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (listLanguages.SelectedIndex != -1)
                {
                    if (languages.Data[listLanguages.SelectedIndex].IsChecked == true)
                    {
                        languages.Data[listLanguages.SelectedIndex].IsChecked = false;
                    }
                    else
                    {
                        languages.Data[listLanguages.SelectedIndex].IsChecked = true;
                    }
                }
                listLanguages.SelectedIndex = -1;
            }
            catch { }
        }

        private void listSpecialities_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (listSpecialities.SelectedIndex != -1)
                {
                    if (specialty.Data[listSpecialities.SelectedIndex].IsChecked == true)
                    {
                        specialty.Data[listSpecialities.SelectedIndex].IsChecked = false;
                    }
                    else
                    {
                        specialty.Data[listSpecialities.SelectedIndex].IsChecked = true;
                    }
                }
                listSpecialities.SelectedIndex = -1;
            }
            catch { }
        }

        private void btn_OfficeBasePostCodeInfo_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridOfficeBasePostCodeInfoMsg.Visibility = Visibility.Visible;
        }

        private void btnCloseInfoWindows_Click(object sender, TappedRoutedEventArgs e)
        {
            gridOfficeBasePostCodeInfoMsg.Visibility = Visibility.Collapsed;
        }

        private void stContaint_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            //if (e.Key == Windows.System.VirtualKey.Enter)
            //{
            //    FocusManager.TryMoveFocus(FocusNavigationDirection.Next);

            //    // Make sure to set the Handled to true, otherwise the RoutedEvent might fire twice
            //    e.Handled = true;
            //}
        }

        public string Error = string.Empty;

        async private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            Error = string.Empty;

            if (string.IsNullOrWhiteSpace(txtName.Text))
                Error = "Please provide your name.";
            else if (string.IsNullOrWhiteSpace(employerList.SelectedOrganisationByName))
                Error = "Please select employer.";
            else if (string.IsNullOrWhiteSpace(localAuthorities.SelectedLocalAuthorityByName))
                Error = "Please select local authority.";
            else if (txtMobileNumber.Text.Length > 0 && txtMobileNumber.Text.Length != 11)
                Error = "Please enter valid 11 digit mobile number.";
            else if (txtLandlineNumber.Text.Length > 0 && txtLandlineNumber.Text.Length != 11)
                Error = "Please enter valid 11 digit landline number.";
            else if (!string.IsNullOrWhiteSpace(txtOfficeBasePostcode.Text) &&
                !Regex.IsMatch(txtOfficeBasePostcode.Text, @"^[a-zA-Z0-9 ]{5,9}$"))
                Error = "Office base postcode must be alphanumeric and between 5 and 9 characters only.";

            if (!string.IsNullOrWhiteSpace(Error))
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(Error);
            else
            {
                //if (cbEmployer.SelectedItem != null)
                //    appUser.Data.EmployerId = ((Organisation)cbEmployer.SelectedItem).OrganisationId;
                //else
                //    appUser.Data.EmployerId = 0;

                //appUser.Data.au
                string empID = string.Empty;
                //if (cbEmployer.SelectedItem != null)
                //    empID = ((Organisation)cbEmployer.SelectedItem).OrganisationId.ToString();
                try
                {
                    object appUser = await s12DoctorServices.editProfile(
                         txtName.Text, employerList.SelectedOrganisationByID,
                         SessionData.CurrntAppUser.Data.RegionId.ToString(),
                         localAuthorities.SelectedLocalAuthorityByID,
                         txtGMCRefrenceNumber.Text, txtGeneralAvailability.Text,
                         txtMobileNumber.Text, txtLandlineNumber.Text, txtOfficeBasePostcode.Text,
                         txtOfficeBaseTeam.Text, languages.SelectedLanguageByID, specialty.SelectedSpecialityByID,
                         txtDefaultClaimForm.Text, txtCarModel.Text, txtCarMake.Text, txtCarRegistrationPlate.Text,
                         txtEngineSize.Text, languages.NewLanguageByName);

                    if (appUser is AppUser)
                    {
                        SessionData.CurrntAppUser.Data.GeneralAvailability = txtGeneralAvailability.Text;
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Profile edited successfully");
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).RedirectToHomePage();
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(appUser != null ? appUser.ToString() : "An unknown error has occurred.");
                }
                catch (Exception ex)
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }
            }
        }

        private void txtOfficeBasePostcode_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            TextBox currentContainer = ((TextBox)sender);
            int caretPosition = currentContainer.SelectionStart;

            currentContainer.Text = currentContainer.Text.ToUpper();
            currentContainer.SelectionStart = caretPosition++;
        }
    }
}

