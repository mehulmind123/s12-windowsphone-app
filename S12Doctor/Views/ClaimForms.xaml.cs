﻿using S12Doctor.Model.Common;
using S12Doctor.Model.S12Doctor;
using S12Doctor.WebService;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Views
{
    public partial class ClaimForms : UserControl
    {
        //Items items = new Items();
        ObservableCollection<ClaimFormData> approvedItems;
        ObservableCollection<ClaimFormData> pendingItems;
        ObservableCollection<ClaimFormData> rejectedItems;

        ViewClaimForm viewClaimForm;
        public CreateClaimForm createClaimForm;
        private S12DoctorServices s12DoctorServices;

        public ClaimForms()
        {
            this.InitializeComponent();
            this.Loaded += ClaimForms_Loaded;
        }

        async public void ReloadData()
        {
            if (listViewPending.Visibility == Visibility.Visible)
            {
                object pendingData = await s12DoctorServices.completedClaimForm("2", "0", "10");

                if (pendingData is CompletedClaimForm)
                {
                    pendingOffset = 0;

                    if (pendingItems == null)
                        pendingItems = new ObservableCollection<ClaimFormData>();

                    var data = new ObservableCollection<ClaimFormData>(((CompletedClaimForm)pendingData).Data);

                    pendingItems.Clear();
                    int i = pendingOffset + 1;

                    foreach (var item in data)
                    {
                        pendingItems.Add(item);
                    }

                    if (pendingItems.Count <= 0)
                        txtNoResultsFound.Visibility = Visibility.Visible;
                    else
                        txtNoResultsFound.Visibility = Visibility.Collapsed;

                    pendingOffset = (int)((CompletedClaimForm)pendingData).Meta.NewOffset;
                    pendingIncall = pendingEndoflist = pendingFatchData = false;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(pendingData != null ? pendingData.ToString() : "An unknown error has occurred.");
            }

            if (listViewRejected.Visibility == Visibility.Visible)
            {
                object rejectedData = await s12DoctorServices.completedClaimForm("4", "0", "10");

                if (rejectedData is CompletedClaimForm)
                {
                    rejectedOffset = 0;

                    if (rejectedItems == null)
                        rejectedItems = new ObservableCollection<ClaimFormData>();

                    var data = new ObservableCollection<ClaimFormData>(((CompletedClaimForm)rejectedData).Data);

                    rejectedItems.Clear();
                    int i = rejectedOffset + 1;

                    foreach (var item in data)
                    {
                        rejectedItems.Add(item);
                    }

                    if (rejectedItems.Count <= 0)
                        txtNoResultsFound.Visibility = Visibility.Visible;
                    else
                        txtNoResultsFound.Visibility = Visibility.Collapsed;

                    rejectedOffset = (int)((CompletedClaimForm)rejectedData).Meta.NewOffset;
                    rejectedIncall = rejectedEndoflist = rejectedFatchData = false;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(rejectedData != null ? rejectedData.ToString() : "An unknown error has occurred.");
            }

            if (listViewApproved.Visibility == Visibility.Visible)
            {
                object approvedData = await s12DoctorServices.completedClaimForm("3", "0", "10");

                if (approvedData is CompletedClaimForm)
                {
                    approvedOffset = 0;

                    if (approvedItems == null)
                        approvedItems = new ObservableCollection<ClaimFormData>();

                    var data = new ObservableCollection<ClaimFormData>(((CompletedClaimForm)approvedData).Data);

                    approvedItems.Clear();
                    int i = approvedOffset + 1;

                    foreach (var item in data)
                    {
                        approvedItems.Add(item);
                    }

                    if (approvedItems.Count <= 0)
                        txtNoResultsFound.Visibility = Visibility.Visible;
                    else
                        txtNoResultsFound.Visibility = Visibility.Collapsed;

                    approvedOffset = (int)((CompletedClaimForm)approvedData).Meta.NewOffset;
                    approvedIncall = approvedEndoflist = approvedFatchData = false;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(approvedData != null ? approvedData.ToString() : "An unknown error has occurred.");
            }
        }

        async public void GetOffsetData(int _offset, int count = 10, bool forAll = false)
        {
            if (listViewPending.Visibility == Visibility.Visible || forAll)
            {
                object pendingData = await s12DoctorServices.completedClaimForm("2", _offset.ToString(), count.ToString());

                if (pendingData is CompletedClaimForm)
                {
                    if (pendingItems == null)
                        pendingItems = new ObservableCollection<ClaimFormData>();

                    var data = new ObservableCollection<ClaimFormData>(((CompletedClaimForm)pendingData).Data);

                    int i = pendingOffset + 1;

                    foreach (var item in data)
                    {
                        pendingItems.Add(item);
                    }

                    if (pendingItems.Count <= 0)
                        txtNoResultsFound.Visibility = Visibility.Visible;
                    else
                        txtNoResultsFound.Visibility = Visibility.Collapsed;

                    pendingOffset = (int)((CompletedClaimForm)pendingData).Meta.NewOffset;
                    pendingFatchData = false;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(pendingData != null ? pendingData.ToString() : "An unknown error has occurred.");
            }

            if (listViewRejected.Visibility == Visibility.Visible || forAll)
            {
                object rejectedData = await s12DoctorServices.completedClaimForm("4", _offset.ToString(), count.ToString());

                if (rejectedData is CompletedClaimForm)
                {
                    if (rejectedItems == null)
                        rejectedItems = new ObservableCollection<ClaimFormData>();

                    var data = new ObservableCollection<ClaimFormData>(((CompletedClaimForm)rejectedData).Data);

                    int i = rejectedOffset + 1;

                    foreach (var item in data)
                    {
                        rejectedItems.Add(item);
                    }

                    if (rejectedItems.Count <= 0)
                        txtNoResultsFound.Visibility = Visibility.Visible;
                    else
                        txtNoResultsFound.Visibility = Visibility.Collapsed;

                    rejectedOffset = (int)((CompletedClaimForm)rejectedData).Meta.NewOffset;
                    rejectedFatchData = false;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(rejectedData != null ? rejectedData.ToString() : "An unknown error has occurred.");
            }

            if (listViewApproved.Visibility == Visibility.Visible || forAll)
            {
                object approvedData = await s12DoctorServices.completedClaimForm("3", _offset.ToString(), count.ToString());

                if (approvedData is CompletedClaimForm)
                {
                    if (approvedItems == null)
                        approvedItems = new ObservableCollection<ClaimFormData>();

                    var data = new ObservableCollection<ClaimFormData>(((CompletedClaimForm)approvedData).Data);

                    int i = approvedOffset + 1;

                    foreach (var item in data)
                    {
                        approvedItems.Add(item);
                    }

                    if (approvedItems.Count <= 0)
                        txtNoResultsFound.Visibility = Visibility.Visible;
                    else
                        txtNoResultsFound.Visibility = Visibility.Collapsed;

                    approvedOffset = (int)((CompletedClaimForm)approvedData).Meta.NewOffset;
                    approvedFatchData = false;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(approvedData != null ? approvedData.ToString() : "An unknown error has occurred.");
            }
        }

        private void ClaimForms_Loaded(object sender, RoutedEventArgs e)
        {
            txtNoResultsFound.Visibility = Visibility.Collapsed;

            if (s12DoctorServices == null)
                s12DoctorServices = new S12DoctorServices();

            if (approvedItems == null)
                approvedItems = new ObservableCollection<ClaimFormData>();

            if (rejectedItems == null)
                rejectedItems = new ObservableCollection<ClaimFormData>();

            if (pendingItems == null)
                pendingItems = new ObservableCollection<ClaimFormData>();

            listViewApproved.ItemsSource = approvedItems;
            listViewRejected.ItemsSource = rejectedItems;
            listViewPending.ItemsSource = pendingItems;

            gridApproved_Tapped(null, null);
        }

        private void Retry_Click(object sender, RoutedEventArgs e)
        {
            if (gridCreateClaimForm.Visibility != Visibility.Visible)
            {
                if (createClaimForm == null)
                {
                    createClaimForm = new CreateClaimForm();
                    createClaimForm.BtnPasswordSubmit.Click += BtnPasswordSubmit_Click;
                    gridCreateClaimForm.Children.Add(createClaimForm);
                }

                try
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("Create Claim Form");
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Visible);
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);

                    string id = ((Button)sender).Tag.ToString();
                    ClaimFormData data = rejectedItems.Where(c => c.ClaimId.ToString() == id).FirstOrDefault();
                    if (data != null)
                    {
                        data.Disclaimer = 1;
                        createClaimForm.SetDefaultData(data);
                        gridCreateClaimForm.Visibility = Visibility.Visible;
                    }
                }
                catch (Exception ex) { ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message); }
            }
        }

        async private void BtnPasswordSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;

            if (string.IsNullOrWhiteSpace(createClaimForm.txtPassword.Password))
                error = "Password cannot be blank.";

            if (!string.IsNullOrWhiteSpace(error))
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
            {
                try
                {
                    object result = await s12DoctorServices.verifyPassword(createClaimForm.txtPassword.Password);

                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;

                            string saveClaimFormresult = await createClaimForm.SaveClaimForm(isDraft: false);
                            if ("error".Equals(saveClaimFormresult)) { ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Visible); }
                            else
                            {
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("Claim form submitted successfully.");
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("Claim Forms");
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Collapsed);
                                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Visible);
                                createClaimForm.GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
                                gridCreateClaimForm.Visibility = Visibility.Collapsed;
                                ReloadData();
                            }
                        }
                        else
                        {
                            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                        }
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                catch (Exception ex)
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }
            }
        }

        private void Rejected_Click(object sender, RoutedEventArgs e)
        {
            //string id = ((Button)sender).Tag.ToString();
            //items.ObjData.Remove(items.ObjData.Where(i => i.ID == id).FirstOrDefault());
        }

        private void Approved_Click(object sender, RoutedEventArgs e)
        {
            //string id = ((Button)sender).Tag.ToString();
            //items.ObjData.Remove(items.ObjData.Where(i => i.ID == id).FirstOrDefault());
        }

        private void Pending_Click(object sender, RoutedEventArgs e)
        {
            //string id = ((Button)sender).Tag.ToString();
            //items.ObjData.Remove(items.ObjData.Where(i => i.ID == id).FirstOrDefault());
        }

        private void View_Click(object sender, RoutedEventArgs e)
        {
            string id = ((Button)sender).Tag.ToString();

            if (viewClaimForm == null)
            {
                viewClaimForm = new ViewClaimForm();
                gridViewClaimForm.Children.Add(viewClaimForm);
            }

            try
            {
                if (listViewApproved.Visibility == Visibility.Visible)
                    viewClaimForm.SetData(approvedItems.Where(i => i.ClaimId.ToString() == id).FirstOrDefault());
                if (listViewPending.Visibility == Visibility.Visible)
                    viewClaimForm.SetData(pendingItems.Where(i => i.ClaimId.ToString() == id).FirstOrDefault());
                if (listViewRejected.Visibility == Visibility.Visible)
                    viewClaimForm.SetData(rejectedItems.Where(i => i.ClaimId.ToString() == id).FirstOrDefault());

                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHeaderText("View Claim Form");

                gridViewClaimForm.Visibility = Visibility.Visible;

            }
            catch (Exception ex) { ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message); }

        }

        private void gridApproved_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (approvedItems.Count <= 0)
                txtNoResultsFound.Visibility = Visibility.Visible;
            else
                txtNoResultsFound.Visibility = Visibility.Collapsed;

            txtApproved.Foreground = new SolidColorBrush(Color.FromArgb(255, 19, 113, 185));
            brdApproved.BorderThickness = new Thickness(0, 0, 0, 2);
            listViewApproved.Visibility = Visibility.Visible;

            txtRejected.Foreground = new SolidColorBrush(Color.FromArgb(255, 165, 167, 185));
            brdRejected.BorderThickness = new Thickness(0, 0, 0, 1);
            listViewRejected.Visibility = Visibility.Collapsed;

            txtPending.Foreground = new SolidColorBrush(Color.FromArgb(255, 165, 167, 185));
            brdPending.BorderThickness = new Thickness(0, 0, 0, 1);
            listViewPending.Visibility = Visibility.Collapsed;
        }

        private void gridRejected_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (rejectedItems.Count <= 0)
                txtNoResultsFound.Visibility = Visibility.Visible;
            else
                txtNoResultsFound.Visibility = Visibility.Collapsed;

            txtRejected.Foreground = new SolidColorBrush(Color.FromArgb(255, 19, 113, 185));
            brdRejected.BorderThickness = new Thickness(0, 0, 0, 2);
            listViewRejected.Visibility = Visibility.Visible;

            txtApproved.Foreground = new SolidColorBrush(Color.FromArgb(255, 165, 167, 185));
            brdApproved.BorderThickness = new Thickness(0, 0, 0, 1);
            listViewApproved.Visibility = Visibility.Collapsed;

            txtPending.Foreground = new SolidColorBrush(Color.FromArgb(255, 165, 167, 185));
            brdPending.BorderThickness = new Thickness(0, 0, 0, 1);
            listViewPending.Visibility = Visibility.Collapsed;
        }

        private void gridPending_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (pendingItems.Count <= 0)
                txtNoResultsFound.Visibility = Visibility.Visible;
            else
                txtNoResultsFound.Visibility = Visibility.Collapsed;

            txtPending.Foreground = new SolidColorBrush(Color.FromArgb(255, 19, 113, 185));
            brdPending.BorderThickness = new Thickness(0, 0, 0, 2);
            listViewPending.Visibility = Visibility.Visible;

            txtRejected.Foreground = new SolidColorBrush(Color.FromArgb(255, 165, 167, 185));
            brdRejected.BorderThickness = new Thickness(0, 0, 0, 1);
            listViewRejected.Visibility = Visibility.Collapsed;

            txtApproved.Foreground = new SolidColorBrush(Color.FromArgb(255, 165, 167, 185));
            brdApproved.BorderThickness = new Thickness(0, 0, 0, 1);
            listViewApproved.Visibility = Visibility.Collapsed;
        }

        async private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;

            if (string.IsNullOrWhiteSpace(txtPassword.Password))
                error = "Password cannot be blank.";

            if (!string.IsNullOrWhiteSpace(error))
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
                else
                    ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
                try
                {
                    object result = await s12DoctorServices.verifyPassword(txtPassword.Password);

                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            gridEnterPasswordPopUp.Visibility = Visibility.Collapsed;

                            GetOffsetData(0, forAll: true);
                        }
                        else
                            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                }
                catch (Exception ex)
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                }
        }

        private void listViewRejected_Loaded(object sender, RoutedEventArgs e)
        {
            ScrollViewer viewer = GetRejectedScrollViewer(this.listViewRejected);
            viewer.ViewChanged += Rejected_ViewChanged;
        }

        private bool rejectedIncall = false,
            rejectedEndoflist = false, rejectedFatchData = false;
        private int rejectedOffset = 0;

        private void Rejected_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer view = (ScrollViewer)sender;
            double progress = view.VerticalOffset / view.ScrollableHeight;
            System.Diagnostics.Debug.WriteLine(progress);

            if (progress == 1 & !rejectedIncall && !rejectedEndoflist)
            {
                rejectedIncall = true;
                fetchRejectedCountries(rejectedOffset);
            }
        }

        private void fetchRejectedCountries(int rejectedOffset)
        {
            if (!rejectedFatchData)
            {
                if (rejectedOffset > 0)
                {
                    GetOffsetData(rejectedOffset);
                    rejectedFatchData = true;
                }
                else
                {
                    rejectedEndoflist = true;
                }
            }
            rejectedIncall = false;
        }

        private ScrollViewer GetRejectedScrollViewer(DependencyObject depObj)
        {
            if (depObj is ScrollViewer) return depObj as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);
                var result = GetRejectedScrollViewer(child);
                if (result != null) return result;
            }

            return null;
        }

        private void listViewApproved_Loaded(object sender, RoutedEventArgs e)
        {
            ScrollViewer viewer = GetApprovedScrollViewer(this.listViewApproved);
            viewer.ViewChanged += Approved_ViewChanged;
        }

        private bool approvedIncall = false,
            approvedEndoflist = false, approvedFatchData = false;
        private int approvedOffset = 0;

        private void Approved_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer view = (ScrollViewer)sender;
            double progress = view.VerticalOffset / view.ScrollableHeight;
            System.Diagnostics.Debug.WriteLine(progress);

            if (progress == 1 & !approvedIncall && !approvedEndoflist)
            {
                approvedIncall = true;
                fetchApprovedCountries(approvedOffset);
            }
        }

        private void fetchApprovedCountries(int approvedOffset)
        {
            if (!approvedFatchData)
            {
                if (approvedOffset > 0)
                {
                    GetOffsetData(approvedOffset);
                    approvedFatchData = true;
                }
                else
                {
                    approvedEndoflist = true;
                }
            }
            approvedIncall = false;
        }

        private ScrollViewer GetApprovedScrollViewer(DependencyObject depObj)
        {
            if (depObj is ScrollViewer) return depObj as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);
                var result = GetApprovedScrollViewer(child);
                if (result != null) return result;
            }

            return null;
        }

        private void listViewPending_Loaded(object sender, RoutedEventArgs e)
        {
            ScrollViewer viewer = GetPendingScrollViewer(this.listViewPending);
            viewer.ViewChanged += Pending_ViewChanged;
        }

        private bool pendingIncall = false,
            pendingEndoflist = false, pendingFatchData = false;
        private int pendingOffset = 0;

        private void Pending_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer view = (ScrollViewer)sender;
            double progress = view.VerticalOffset / view.ScrollableHeight;
            System.Diagnostics.Debug.WriteLine(progress);

            if (progress == 1 & !pendingIncall && !pendingEndoflist)
            {
                pendingIncall = true;
                fetchPendingCountries(pendingOffset);
            }
        }

        private void fetchPendingCountries(int pendingOffset)
        {
            if (!pendingFatchData)
            {
                if (pendingOffset > 0)
                {
                    GetOffsetData(pendingOffset);
                    pendingFatchData = true;
                }
                else
                {
                    pendingEndoflist = true;
                }
            }
            pendingIncall = false;
        }

        private ScrollViewer GetPendingScrollViewer(DependencyObject depObj)
        {
            if (depObj is ScrollViewer) return depObj as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);
                var result = GetPendingScrollViewer(child);
                if (result != null) return result;
            }

            return null;
        }
    }
}
