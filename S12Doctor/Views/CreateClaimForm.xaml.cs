﻿using S12Doctor.Model.Common;
using S12Doctor.Model.S12Doctor;
using S12Doctor.Utilities;
using S12Doctor.WebService;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace S12Doctor.Views
{
    public sealed partial class CreateClaimForm : UserControl
    {
        private S12DoctorServices s12DoctorServices;
        private ClaimFormData claimFormData;
        public CreateClaimForm()
        {
            this.InitializeComponent();
        }

        private void stContaint_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                FocusManager.TryMoveFocus(FocusNavigationDirection.Next);

                // Make sure to set the Handled to true, otherwise the RoutedEvent might fire twice
                e.Handled = true;
            }
        }

        async public void SetDefaultData(ClaimFormData data)
        {
            claimFormData = data;

            try
            {
                cbAHMP.IsEnabled = true;
                cbAHMP.DataContext = new AMHPUserList().Data;
                cbAHMP.IsEnabled = false;

                cbAssessmentType.DataContext = new AssessmentLocationTypeList().Data;
                cbLocalAuthority.DataContext = new LocalAuthorityList().Data;
                cbSectionImplemented.DataContext = new SectionImplementList().Data;

                txtDoctorName.Text = txtDoctorAddress.Text = txtAssessmentLocationPostcode.Text =
                    txtPatientNHSNumber.Text = txtPatientPostcode.Text = txtAdditionalInformation.Text =
                    txtMileageFromPostCode.Text = txtMileageToPostCode.Text = txtMilesTraveled.Text =
                    txtCarMake.Text = txtCarModel.Text = txtEngineSize.Text = txtCarRegistrationPlate.Text =
                    txtNotes.Text = string.Empty;

                chkDisclaimer.IsChecked = false;

                try
                {
                    AssessmentDate.Date = Convert.ToDateTime(data.AssessmentDateTime);
                    AssessmentTime.Time = Convert.ToDateTime(data.AssessmentDateTime).TimeOfDay;
                }
                catch (Exception ex)
                { }

                if (s12DoctorServices == null)
                    s12DoctorServices = new S12DoctorServices();

                //object userInfo = await s12DoctorServices.userDetail();
                //if (userInfo is AppUser)
                //{
                //    SessionData.CurrntAppUser = (AppUser)userInfo;
                //}
                //else
                //    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(userInfo != null ? userInfo.ToString() : "An unknown error has occurred.");


                txtDoctorName.Text = data.DoctorName;
                txtDoctorAddress.Text = data.DoctorAddress;
                txtAssessmentLocationPostcode.Text = data.AssessmentPostcode;
                txtPatientNHSNumber.Text = data.PatientNhsNumber;
                txtPatientPostcode.Text = data.PatientPostcode;
                txtAdditionalInformation.Text = data.AdditionalInformation;
                txtMileageFromPostCode.Text = data.FromPostcode;
                txtMileageToPostCode.Text = data.ToPostcode;
                txtMilesTraveled.Text = data.MilesTraveled;
                txtCarMake.Text = data.CarMake;
                txtCarModel.Text = data.CarModel;
                txtEngineSize.Text = data.EngineSize.ToString();
                txtCarRegistrationPlate.Text = data.CarRegistrationPlate;
                txtNotes.Text = data.Notes;
                txtSpecifyAssessmentLocationType.Text = data.AssessmentLocationTypeDescription;
                txtOtherOutcome.Text = data.SectionTypeDescription;

                chkDisclaimer.IsChecked = data.Disclaimer == 1 ? true : false;

                object localAuthoritiesresult = await s12DoctorServices.localAuthorityList("s12", SessionData.CurrntAppUser.Data.RegionId.ToString());
                if (localAuthoritiesresult is LocalAuthorityList)
                {
                    cbLocalAuthority.DataContext = ((LocalAuthorityList)localAuthoritiesresult).Data;
                    cbLocalAuthority.SelectedItem = ((LocalAuthorityList)localAuthoritiesresult).Data.
                        Where(emp => emp.AuthorityId == data.LocalAuthorityId).FirstOrDefault();
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(localAuthoritiesresult != null ? localAuthoritiesresult.ToString() : "An unknown error has occurred.");


                object assessmentLocationTypeListresult = await s12DoctorServices.assessmentLocationTypeList();
                if (assessmentLocationTypeListresult is AssessmentLocationTypeList)
                {
                    cbAssessmentType.DataContext = ((AssessmentLocationTypeList)assessmentLocationTypeListresult).Data;
                    cbAssessmentType.SelectedItem = ((AssessmentLocationTypeList)assessmentLocationTypeListresult).Data.
                        Where(emp => emp.AssessmentLocationTypeId == data.AssessmentLocationType).FirstOrDefault();
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(assessmentLocationTypeListresult != null ? assessmentLocationTypeListresult.ToString() : "An unknown error has occurred.");


                object sectionImplementListresult = await s12DoctorServices.sectionImplementList();
                if (sectionImplementListresult is SectionImplementList)
                {
                    cbSectionImplemented.DataContext = ((SectionImplementList)sectionImplementListresult).Data;
                    cbSectionImplemented.SelectedItem = ((SectionImplementList)sectionImplementListresult).Data.
                        Where(emp => emp.SectionImplementName == data.SectionImplementedText).FirstOrDefault();
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(sectionImplementListresult != null ? sectionImplementListresult.ToString() : "An unknown error has occurred.");
            }
            catch (Exception ex)
            {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        async public void SetDefaultData()
        {
            try
            {
                cbAHMP.IsEnabled = true;
                cbAHMP.DataContext = new AMHPUserList().Data;
                cbAHMP.IsEnabled = false;

                txtDoctorName.Text = txtDoctorAddress.Text = txtAssessmentLocationPostcode.Text =
                    txtPatientNHSNumber.Text = txtPatientPostcode.Text = txtAdditionalInformation.Text =
                    txtMileageFromPostCode.Text = txtMileageToPostCode.Text = txtMilesTraveled.Text =
                    txtCarMake.Text = txtCarModel.Text = txtEngineSize.Text = txtCarRegistrationPlate.Text =
                    txtNotes.Text = string.Empty;

                chkDisclaimer.IsChecked = false;

                AssessmentDate.Date = DateTime.Now;
                AssessmentTime.Time = DateTime.Now.TimeOfDay;

                if (s12DoctorServices == null)
                    s12DoctorServices = new S12DoctorServices();

                object userInfo = await s12DoctorServices.userDetail();
                if (userInfo is AppUser)
                {
                    SessionData.CurrntAppUser = (AppUser)userInfo;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(userInfo != null ? userInfo.ToString() : "An unknown error has occurred.");

                txtDoctorName.Text = SessionData.CurrntAppUser.Data.Name;
                txtDoctorAddress.Text = SessionData.CurrntAppUser.Data.DefaultClaimAddress;
                txtCarMake.Text = SessionData.CurrntAppUser.Data.CarMake;
                txtCarModel.Text = SessionData.CurrntAppUser.Data.CarModel;
                txtEngineSize.Text = SessionData.CurrntAppUser.Data.EngineSize;
                txtCarRegistrationPlate.Text = SessionData.CurrntAppUser.Data.CarRegistrationPlate;

                object localAuthoritiesresult = await s12DoctorServices.localAuthorityList("s12", SessionData.CurrntAppUser.Data.RegionId.ToString());
                if (localAuthoritiesresult is LocalAuthorityList)
                {
                    cbLocalAuthority.DataContext = ((LocalAuthorityList)localAuthoritiesresult).Data;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(localAuthoritiesresult != null ? localAuthoritiesresult.ToString() : "An unknown error has occurred.");


                object assessmentLocationTypeListresult = await s12DoctorServices.assessmentLocationTypeList();
                if (assessmentLocationTypeListresult is AssessmentLocationTypeList)
                {
                    cbAssessmentType.DataContext = ((AssessmentLocationTypeList)assessmentLocationTypeListresult).Data;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(assessmentLocationTypeListresult != null ? assessmentLocationTypeListresult.ToString() : "An unknown error has occurred.");


                object sectionImplementListresult = await s12DoctorServices.sectionImplementList();
                if (sectionImplementListresult is SectionImplementList)
                {
                    cbSectionImplemented.DataContext = ((SectionImplementList)sectionImplementListresult).Data;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(sectionImplementListresult != null ? sectionImplementListresult.ToString() : "An unknown error has occurred.");
            }
            catch (Exception ex)
            {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        public async System.Threading.Tasks.Task<string> SaveClaimForm(bool isDraft = true)
        {
            try
            {
                string localAuthorityId = string.Empty, localAuthorityName = string.Empty;
                string amhpId = string.Empty, amhpName = string.Empty;
                string assessmentTypeId = string.Empty, assessmentTypeName = string.Empty;
                string sectionImplementedId = string.Empty, sectionImplementedName = string.Empty;

                if (cbLocalAuthority.SelectedItem != null)
                {
                    localAuthorityId = ((LocalAuthority)cbLocalAuthority.SelectedItem).AuthorityId.ToString();
                    localAuthorityName = ((LocalAuthority)cbLocalAuthority.SelectedItem).AuthorityName.ToString();
                }

                if (cbAHMP.SelectedItem != null)
                {
                    amhpId = ((AMHPUser)cbAHMP.SelectedItem).AmhpId.ToString();
                    amhpName = ((AMHPUser)cbAHMP.SelectedItem).AmhpName.ToString();
                }

                if (cbAssessmentType.SelectedItem != null)
                {
                    assessmentTypeId = ((AssessmentLocationType)cbAssessmentType.SelectedItem).AssessmentLocationTypeId.ToString();
                    assessmentTypeName = ((AssessmentLocationType)cbAssessmentType.SelectedItem).AssessmentLocationTypeName.ToString();
                }

                if (cbSectionImplemented.SelectedItem != null)
                {
                    sectionImplementedId = ((SectionImplement)cbSectionImplemented.SelectedItem).SectionImplementId.ToString();
                    sectionImplementedName = ((SectionImplement)cbSectionImplemented.SelectedItem).SectionImplementName.ToString();
                }

                string date = AssessmentDate.Date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                string time = AssessmentTime.Time.ToString();

                //DateTime dt = Convert.ToDateTime(date + " " + time);
                //DateTime dt = DateTime.ParseExact(string.Format("{0} {1}", date, time), "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);

                string dateTime = string.Format("{0} {1}", date, time);// dt.ToString();
                string id = string.Empty;

                if (claimFormData != null)
                    id = claimFormData.ClaimId.ToString();

                object claimFormresult = await s12DoctorServices.createEditClaimForm(localAuthorityName, localAuthorityId,
                    amhpName, amhpId, txtDoctorName.Text, assessmentTypeId, txtAssessmentLocationPostcode.Text,
                    dateTime, txtPatientNHSNumber.Text, txtPatientPostcode.Text,
                    sectionImplementedId, txtCarMake.Text, txtCarModel.Text, txtCarRegistrationPlate.Text,
                    txtEngineSize.Text, txtMilesTraveled.Text, txtNotes.Text, txtSpecifyAssessmentLocationType.Text,
                    txtOtherOutcome.Text, id, txtAdditionalInformation.Text, txtMileageFromPostCode.Text,
                    txtMileageToPostCode.Text, isDraft ? "1" : "2", "",
                    chkDisclaimer.IsChecked.HasValue ? chkDisclaimer.IsChecked.Value ? "1" : "0" : "0",
                    txtDoctorAddress.Text);

                if (claimFormresult is ClaimForm) { }
                else
                {
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(claimFormresult != null ? claimFormresult.ToString() : "An unknown error has occurred.");
                    return "error";
                }
            }
            catch (Exception ex)
            {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
                return "error";
            }

            return "success";
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string error = string.Empty;

            if (cbLocalAuthority.SelectedIndex < 0)
                error = "Please select local authority.";
            else if (cbAHMP.SelectedIndex < 0)
                error = "Please select an AMHP.";
            else if (string.IsNullOrWhiteSpace(txtDoctorName.Text))
                error = "Please provide your name.";
            else if (string.IsNullOrWhiteSpace(txtDoctorAddress.Text))
                error = "Please provide your address.";
            else if (cbAssessmentType.SelectedIndex < 0)
                error = "Please select assessment location type.";
            else if (cbAssessmentType.SelectedIndex < 0)
                error = "Please select assessment location type.";
            else if (string.IsNullOrWhiteSpace(txtSpecifyAssessmentLocationType.Text) &&
                gridSpecifyAssessmentLocationType.Visibility == Visibility.Visible)
                error = "Please provide assessment location type description.";
            else if (string.IsNullOrWhiteSpace(txtAssessmentLocationPostcode.Text))
                error = "Please provide assessment location postcode.";
            else if (!Regex.IsMatch(txtAssessmentLocationPostcode.Text, "^[a-zA-Z0-9 ]{5,9}$"))
                error = "Assessment location postcode must be alphanumeric and between 5 and 9 characters only.";
            //else if (string.IsNullOrWhiteSpace(txtPatientNHSNumber.Text))
            //    error = "Please provide patient NHS number.";
            else if (!string.IsNullOrWhiteSpace(txtPatientNHSNumber.Text) &&
                txtPatientNHSNumber.Text.Count() != 12)
                error = "Please enter patient's 10 digit NHS number.";
            //else if (string.IsNullOrWhiteSpace(txtPatientPostcode.Text))
            //    error = "Please provide patient postcode.";
            else if (!string.IsNullOrWhiteSpace(txtPatientPostcode.Text) &&
                !Regex.IsMatch(txtPatientPostcode.Text, "^[a-zA-Z0-9]{5,9}$"))
                error = "Patient postcode must be alphanumeric and between 5 and 9 characters only.";
            else if (!string.IsNullOrWhiteSpace(txtMileageFromPostCode.Text) &&
                !Regex.IsMatch(txtMileageFromPostCode.Text, "^[a-zA-Z0-9 ]{5,9}$"))
                error = "Mileage from postcode must be alphanumeric and between 5 and 9 characters only.";
            else if (!string.IsNullOrWhiteSpace(txtMileageToPostCode.Text) &&
                !Regex.IsMatch(txtMileageToPostCode.Text, "^[a-zA-Z0-9 ]{5,9}$"))
                error = "Mileage to postcode must be alphanumeric and between 5 and 9 characters only.";
            else if (cbSectionImplemented.SelectedIndex < 0)
                error = "Please select section implemented.";
            else if (string.IsNullOrWhiteSpace(txtOtherOutcome.Text) &&
                gridOtherOutcome.Visibility == Visibility.Visible)
                error = "Please provide other outcome.";
            else if (!chkDisclaimer.IsChecked.HasValue || !chkDisclaimer.IsChecked.Value)
                error = "You can't submit claim request without accepting disclaimer.";

            if (!string.IsNullOrWhiteSpace(error))
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(error);
            else
            {
                ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Collapsed);
                txtPassword.Password = string.Empty;
                GridEnterPasswordPopUp.Visibility = Visibility.Visible;
            }
        }

        private void gridBack_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetSaveButtonVisibility(Visibility.Visible);
            GridEnterPasswordPopUp.Visibility = Visibility.Collapsed;
        }

        private void btn_AdditionalInformation_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridAdditionalInformationMsg.Visibility = Visibility.Visible;
        }

        private void btnCloseInfoWindows_Click(object sender, TappedRoutedEventArgs e)
        {
            gridAdditionalInformationMsg.Visibility = Visibility.Collapsed;
        }

        private void gridBackOfficeBasePostCodeInfoMsg_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridAdditionalInformationMsg.Visibility = Visibility.Collapsed;
        }

        async private void cbLocalAuthority_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string id = string.Empty;

            if (cbLocalAuthority.SelectedItem != null)
                id = ((LocalAuthority)cbLocalAuthority.SelectedItem).AuthorityId.ToString();

            if (!string.IsNullOrWhiteSpace(id))
            {
                if (cbAHMP.Items.Count > 0)
                    cbAHMP.SelectedIndex = -1;

                cbAHMP.IsEnabled = false;

                object amhpUserList = await s12DoctorServices.amhpUserList(id);
                if (amhpUserList is AMHPUserList)
                {
                    cbAHMP.DataContext = ((AMHPUserList)amhpUserList).Data;

                    if (claimFormData != null && id.Equals(claimFormData.LocalAuthorityId.ToString()))
                    {
                        cbAHMP.SelectedItem = ((AMHPUserList)amhpUserList).Data.
                        Where(emp => emp.AmhpId == claimFormData.AmhpId).FirstOrDefault();
                    }

                    cbAHMP.IsEnabled = true;
                }
                else
                    ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(amhpUserList != null ? amhpUserList.ToString() : "An unknown error has occurred.");
            }
            else
            {
                if (cbAHMP.Items.Count > 0)
                    cbAHMP.SelectedIndex = -1;
                cbAHMP.IsEnabled = false;
            }

        }

        private void cbAssessmentType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string name = string.Empty;

            if (cbAssessmentType.SelectedItem != null)
                name = ((AssessmentLocationType)cbAssessmentType.SelectedItem).AssessmentLocationTypeName.ToString();

            if (!string.IsNullOrWhiteSpace(name) && name.Contains("Other"))
                gridSpecifyAssessmentLocationType.Visibility = Visibility.Visible;
            else
            {
                txtSpecifyAssessmentLocationType.Text = string.Empty;
                gridSpecifyAssessmentLocationType.Visibility = Visibility.Collapsed;
            }
        }

        private void cbSectionImplemented_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string name = string.Empty;

            if (cbSectionImplemented.SelectedItem != null)
                name = ((SectionImplement)cbSectionImplemented.SelectedItem).SectionImplementName.ToString();

            if (!string.IsNullOrWhiteSpace(name) && name.Contains("Other"))
                gridOtherOutcome.Visibility = Visibility.Visible;
            else
            {
                txtOtherOutcome.Text = string.Empty;
                gridOtherOutcome.Visibility = Visibility.Collapsed;
            }
        }

        private void txtPatientNHSNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string s = txtPatientNHSNumber.Text;

            if (s.Length == 10)
            {
                s = s.Replace("-", "");
                double sAsD = double.Parse(s);
                if (s.Length == 10)
                    txtPatientNHSNumber.Text = string.Format("{0:###-###-####}", sAsD).ToString();
                else
                    txtPatientNHSNumber.Text = s;
            }

            if (txtPatientNHSNumber.Text.Length > 1)
                txtPatientNHSNumber.SelectionStart = txtPatientNHSNumber.Text.Length;
            txtPatientNHSNumber.SelectionLength = 0;
        }

        private void btnSaveAsDraftNo_Click(object sender, TappedRoutedEventArgs e)
        {
            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SaveAsDraftClaimFormOnBackKeyPress(false);
            GridSaveAsDraftPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void btnSaveAsDraftYes_Click(object sender, TappedRoutedEventArgs e)
        {
            ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SaveAsDraftClaimFormOnBackKeyPress(true);
            GridSaveAsDraftPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void txtPostcode_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            TextBox currentContainer = ((TextBox)sender);
            int caretPosition = currentContainer.SelectionStart;

            currentContainer.Text = currentContainer.Text.ToUpper();
            currentContainer.SelectionStart = caretPosition++;
        }
    }
}
