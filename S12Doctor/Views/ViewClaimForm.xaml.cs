﻿using S12Doctor.Model.S12Doctor;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Views
{
    public sealed partial class ViewClaimForm : UserControl
    {
        public ViewClaimForm()
        {
            this.InitializeComponent();
        }

        public void SetData(ClaimFormData itemData)
        {
            bdrSpecifiedLocationType.Visibility = Visibility.Collapsed;
            bdrSpecifiedSection.Visibility = Visibility.Collapsed;

            this.DataContext = itemData;
            SolidColorBrush foregroundSolidColorBrush;
            bdrCCGName.Visibility = Visibility.Collapsed;
            bdrRejectionReason.Visibility = Visibility.Collapsed;

            if (itemData.SectionImplementedText.Contains("Other"))
                bdrSpecifiedSection.Visibility = Visibility.Visible;

            if (itemData.AssessmentLocationTypeText.Contains("Other"))
                bdrSpecifiedLocationType.Visibility = Visibility.Visible;

            if (itemData.ApprovalStatus == 3)
            {
                bdrCCGName.Visibility = Visibility.Visible;

                foregroundSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 0, 216, 0);
                txtApproved.Foreground = foregroundSolidColorBrush;
            }
            else if (itemData.ApprovalStatus == 4)
            {
                bdrRejectionReason.Visibility = Visibility.Visible;

                foregroundSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 75, 88);
                txtApproved.Foreground = foregroundSolidColorBrush;
            }
            else
            {
                foregroundSolidColorBrush = new SolidColorBrush();
                foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 186, 0);
                txtApproved.Foreground = foregroundSolidColorBrush;
            }
        }
    }
}
