﻿using S12Doctor.Model.Common;
using S12Doctor.Utilities;
using S12Doctor.WebService;
using System;
using System.Diagnostics;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace S12Doctor.Views
{
  public sealed partial class TermsAndConditions : UserControl
    {
        private S12DoctorServices s12DoctorServices;
        private AMHPServices amhpServices;
        private WebService.CommonServices common;
        bool willNavigate;

        public TermsAndConditions()
        {
            this.InitializeComponent();
            this.Loaded += TermsAndConditions_Loaded;
        }

        private async void TermsAndConditions_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (s12DoctorServices == null)
                s12DoctorServices = new S12DoctorServices();

            if (amhpServices == null)
                amhpServices = new AMHPServices();
            if (common == null)
                common = new WebService.CommonServices();

            if (S12Doctor.Utilities.SessionData.CurrntAppUser.Data.AcceptTermsCondition == 0)
            {
                webViewer.NavigateToString(S12Doctor.Utilities.SessionData.CMSData.Data.TermsAndConditionsContent);
            } else
            {
                if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    webViewer.NavigateToString(S12Doctor.Utilities.SessionData.CMSData.Data.TermsAndConditionsContent);
                    if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                    }
                    else
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox("This information can only be viewed offline if it has been loaded prior to losing internet connection.");
                    }
                }
                else
                {
                    object result = await common.cms();
                    if (result is CMS)
                    {
                        SessionData.CMSTermsConditionData = ((CMS)result);
                    }
                    else
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }
                    if (S12Doctor.Utilities.SessionData.CMSTermsConditionData != null)
                    {
                        string termsCondition = "<html><head><style type=\"text/css\"> li{ font-size:45px; }</style></head><body>" +
                            S12Doctor.Utilities.SessionData.CMSTermsConditionData.Data.TermsAndConditionsContent+
                            "<body></html>";
                        webViewer.NavigateToString(termsCondition);
                    }
                }
               
            }

            SolidColorBrush headerStatusTextSolidColorBrush;
            headerStatusTextSolidColorBrush = new SolidColorBrush();

            if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
            {
                headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 19, 113, 185);
                btnAccept.Background = headerStatusTextSolidColorBrush;
            }
            else
            {
                headerStatusTextSolidColorBrush.Color = Color.FromArgb(255, 71, 192, 181);
                btnAccept.Background = headerStatusTextSolidColorBrush;
            }
            
           
        }

        public void SetTermsConditionsVisibility(Visibility visibility)
        {
            btnAccept.Visibility = visibility;
            txtText.Visibility=Visibility;
            termsCondText.Visibility = Visibility.Visible;
            webViewer.Margin = new Thickness(10, 0, 0, 177);
        }

        async private void btnAccept_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            try
            {
                if (S12Doctor.Utilities.SessionData.CurrentApplication == "S12")
                {
                    object result = await s12DoctorServices.acceptTermsConditions();
                    if (result == null)
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).AddControl(Utilities.Pages.ChangePassword);
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                    }
                    else
                    {
                        ((S12Doctor.MainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }

                }
                else
                {
                    object result = await amhpServices.acceptTermsConditions();
                    if (result is MetaInfo)
                    {
                        if (((MetaInfo)result).Meta.Status == 200)
                        {
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).AddControl(Utilities.Pages.ChangePassword);
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).SetHamburgerVisibility(Visibility.Collapsed);
                        }
                        else
                            ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(((MetaInfo)result).Meta.Message);
                    }
                    else
                    {
                        ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(result != null ? result.ToString() : "An unknown error has occurred.");
                    }
                }
            }
            catch { }
        }

        private async void webViewer_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            if (!willNavigate)
            {
                willNavigate = true;
                return;
            }

            // cancel navigation to the clicked link in the webBrowser control
            args.Cancel = true;
            string str = args.Uri.ToString();
            var uriLink = new Uri(str);

            // Launch the URI
            var success = await Windows.System.Launcher.LaunchUriAsync(uriLink);

            if (success)
            {
                // URI launched
            }
            else
            {
                // URI launch failed
            }
            
        }

       
    }
   
}
