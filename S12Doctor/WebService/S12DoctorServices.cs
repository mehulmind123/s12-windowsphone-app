﻿using Newtonsoft.Json;
using S12Doctor.Common;
using S12Doctor.Model;
using S12Doctor.Model.Common;
using S12Doctor.Model.S12Doctor;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace S12Doctor.WebService
{
    public class S12DoctorServices
    {
        #region //Fields
        private HttpClient client;
        public string BaseUrl { get; set; }
        public string GoogleAPIKey { get; set; }

        private const string NetworkError = "Please check your internet connection or try again later.";
        #endregion

        #region //Constructor
        public S12DoctorServices()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            client.DefaultRequestHeaders.Authorization = new
                    System.Net.Http.Headers.AuthenticationHeaderValue(Utilities.SessionData.ApiHeaderScheme, Utilities.SessionData.ApiHeaderParameter);

            client.DefaultRequestHeaders.Add("Accept", "application/x.api.v2+json");
             //BaseUrl = "https://www.itrainacademy.in/doctorApp/api";
            BaseUrl = "https://s12solutions.com/application/api";
            GoogleAPIKey = "AIzaSyB6bUuk7bH2CPU106EbdFYe6ndY_Buo9mI";
        }
        #endregion

        #region //Methods
        //login_type = 1/2
        //1 - S12 Doctor User
        //2 - AMHP User
        //availability_status:
        //1- Available
        //2- May Be Available
        //3 Not Available
        public async Task<object> changeAvailabilityStatus(string availability_status, string availability_message, bool isSync)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                if (!isSync)
                    Loader.Show();

                string RestUrl = BaseUrl + "/change-availability-status";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    availability_status = availability_status,
                    availability_message = availability_message
                };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    AvailabilityStatus Items = AvailabilityStatus.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        //type(Optional) s12 - Get only assigned LA to this login s12 doctor user
        //if type value is blank, it will return all LA from database
        public async Task<object> localAuthorityList(string type,string region_id)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/local-authority-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, type = type ,region_id = region_id };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    LocalAuthorityList Items = LocalAuthorityList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }



      
        /// <summary>
        /// Get all AMHP users who are associated with given local_authority_id.
        /// </summary>
        /// <param name="local_authority_id"></param>
        /// <returns></returns>
        public async Task<object> amhpUserList(string local_authority_id)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/amhp-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, local_authority_id = local_authority_id };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    AMHPUserList Items = AMHPUserList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /// <summary>
        /// get all assessment location type list
        /// </summary>
        /// <returns>assessment location type list</returns>
        public async Task<object> assessmentLocationTypeList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/assessment-location-type-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    AssessmentLocationTypeList Items = AssessmentLocationTypeList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /// <summary>
        /// get all section implement list
        /// </summary>
        /// <returns>section implement list</returns>
        public async Task<object> sectionImplementList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/section-implement-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    SectionImplementList Items = SectionImplementList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /// <summary>
        /// get all language list
        /// </summary>
        /// <returns>language list</returns>
        public async Task<object> languageList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/language-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    LanguageList Items = LanguageList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /// <summary>
        /// get all speciality list
        /// </summary>
        /// <returns>speciality list</returns>
        public async Task<object> specialityList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/specialties-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    SpecialityList Items = SpecialityList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /// <summary>
        /// get all organisation list
        /// </summary>
        /// <returns>organisation list</returns>
        public async Task<object> organisationList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/organisation-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    OrganisationList Items = OrganisationList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> employerList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/organisation-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    EmployerList Items = EmployerList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /// <summary>
        /// settings
        /// </summary>
        /// <param name="gps_enable"></param>
        /// <param name="notification_reminder"></param>
        /// <returns></returns>
        public async Task<object> settings(string gps_enable, string notification_reminder)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/setting";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, gps_enable = gps_enable, notification_reminder = notification_reminder };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    Settings Items = Settings.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /*
            claim_id: pass claim id at edit time
            claim_type : 1/2
                1 - Save as draft
                2 - Submit Claim
         */
        public async Task<object> createEditClaimForm(string local_authority_name,
            string authority_id, string amhp_name, string amhp_id, string doctor_name,
            string assessment_location_type, string assessment_postcode, string assessment_date_time, string patient_nhs_number,
            string patient_postcode, string section_implemented, string car_make, string car_model,
            string car_registration_plate, string engine_size, string miles_traveled, string notes,
            string assessment_location_type_description, string section_type_description, string claim_id, string additional_information,
            string from_postcode, string to_postcode, string claim_type, string sync_id,
            string disclaimer, string doctor_address)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/create-claim-form";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    local_authority_name = local_authority_name,
                    authority_id = authority_id,
                    amhp_name = amhp_name,
                    amhp_id = amhp_id,
                    doctor_name = doctor_name,
                    assessment_location_type = assessment_location_type,
                    assessment_postcode = assessment_postcode,
                    assessment_date_time = assessment_date_time,
                    patient_nhs_number = patient_nhs_number,
                    patient_postcode = patient_postcode,
                    section_implemented = section_implemented,
                    car_make = car_make,
                    car_model = car_model,
                    car_registration_plate = car_registration_plate,
                    engine_size = engine_size,
                    miles_traveled = miles_traveled,
                    notes = notes,
                    assessment_location_type_description = assessment_location_type_description,
                    section_type_description = section_type_description,
                    claim_id = claim_id,
                    additional_information = additional_information,
                    from_postcode = from_postcode,
                    to_postcode = to_postcode,
                    claim_type = claim_type,
                    sync_id = sync_id,
                    disclaimer = disclaimer,
                    doctor_address = doctor_address
                };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    ClaimForm Items = ClaimForm.FromJson(Jsoncontent);

                    if (Items != null && Items.Meta != null &&
                        Utilities.SessionData.CurrntAppUser != null && Utilities.SessionData.CurrntAppUser.Meta != null)
                        Utilities.SessionData.CurrntAppUser.Meta.DraftCount = Items.Meta.DraftCount;

                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /*
           form_type:: Pass approval status values form below:
                3 - For Approved
                2 - For Pending
                4 - For Rejected
        */
        public async Task<object> completedClaimForm(string form_type, string offset, string limit)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/completed-claim-forms";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, form_type = form_type, offset = offset, limit = limit };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    CompletedClaimForm Items = CompletedClaimForm.FromJson(Jsoncontent);

                    if (Items != null && Items.Meta != null &&
                        Utilities.SessionData.CurrntAppUser != null && Utilities.SessionData.CurrntAppUser.Meta != null)
                        Utilities.SessionData.CurrntAppUser.Meta.DraftCount = Items.Meta.DraftCount;

                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> usefulInformation(string offset, string limit)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/s12-useful-information";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, offset = offset, limit = limit };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    UsefulInformationList Items = UsefulInformationList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        //language_name (comma separated)
        public async Task<object> editProfile(string name, string employer,string region_id, string local_authorities, string gmc_referance_number,
            string general_availability, string mobile_number, string landline_number, string office_base_postcode,
            string office_base_team, string language_spoken, string specialties, string default_claim_form_address,
            string car_model, string car_make, string car_registration_plate, string engine_size, string language_name)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/s12-edit-profile";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    name = name,
                    employer = employer,
                    region_id = region_id,
                    local_authorities = local_authorities,
                    gmc_referance_number = gmc_referance_number,
                    general_availability = general_availability,
                    mobile_number = mobile_number,
                    landline_number = landline_number,
                    office_base_postcode = office_base_postcode,
                    office_base_team = office_base_team,
                    language_spoken = language_spoken,
                    specialties = specialties,
                    default_claim_form_address = default_claim_form_address,
                    car_model = car_model,
                    car_make = car_make,
                    car_registration_plate = car_registration_plate,
                    engine_size = engine_size,
                    language_name = language_name
                };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                    
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    AppUser Items = AppUser.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> acceptTermsConditions()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/s12-accept-term-condition";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    //AppUser Items = JsonConvert.DeserializeObject<AppUser>(Jsoncontent);
                    //Debug.WriteLine(@"TodoItem successfully saved.");
                    //return Items;
                    Loader.Hode();
                    return null;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> verifyPassword(string password)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/verify-password";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, password = password };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    MetaInfo Items = JsonConvert.DeserializeObject<MetaInfo>(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> setCurrentLatLong(string latitude, string longitude)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                //Loader.Show();
                string RestUrl = BaseUrl + "/user-lat-lng";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, latitude = latitude, longitude = longitude };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    //AppUser Items = JsonConvert.DeserializeObject<AppUser>(Jsoncontent);
                    //Debug.WriteLine(@"TodoItem successfully saved.");
                    //return Items;
                    //Loader.Hode();
                    return null;
                }
                else
                {
                    //Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> userDetail()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/user-details";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    AppUser Items = AppUser.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> viewClaimForm(string claim_id)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/view-claim-form";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, claim_id = claim_id };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    ClaimForm Items = ClaimForm.FromJson(Jsoncontent);

                    if (Items != null && Items.Meta != null &&
                        Utilities.SessionData.CurrntAppUser != null && Utilities.SessionData.CurrntAppUser.Meta != null)
                        Utilities.SessionData.CurrntAppUser.Meta.DraftCount = Items.Meta.DraftCount;

                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> deleteClaimForm(string claim_id)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/delete-claim-form";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, claim_id = claim_id };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    ClaimForm Items = ClaimForm.FromJson(Jsoncontent);

                    if (Items != null && Items.Meta != null &&
                        Utilities.SessionData.CurrntAppUser != null && Utilities.SessionData.CurrntAppUser.Meta != null)
                        Utilities.SessionData.CurrntAppUser.Meta.DraftCount = Items.Meta.DraftCount;

                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> changePassword(string old_password, string new_password)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/change-password";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { old_password = old_password, new_password = new_password, login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    MetaInfo Items = MetaInfo.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> availabilityCalander(string am, string lunch, string pm, string evening, string night,bool isShow)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                if (isShow)
                    Loader.Show();
                string RestUrl = BaseUrl + "/change-weekly-availability";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;
                var weekly_availability_status = new
                {
                    am = am,
                    lunch = lunch,
                    pm = pm,
                    evening = evening,
                    night = night
                };
                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    weekly_availability_status = weekly_availability_status
                };

                //var jsonRequest = new { login_type = Utilities.SessionData.LoginType , weekly_availability_status = weekly_availability_status};
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    CalanderAvailibility Items = CalanderAvailibility.FromJson(Jsoncontent);

                    if (Items != null && Items.Meta != null &&
                        Utilities.SessionData.CurrntAppUser != null && Utilities.SessionData.CurrntAppUser.Meta != null)
                        Utilities.SessionData.CurrntAppUser.Meta.DraftCount = Items.Meta.DraftCount;

                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
            {
                return NetworkError;
            }
        }


        public async Task<object> getWeeklyAvailability()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                //Loader.Show();
                string RestUrl = BaseUrl + "/get-weekly-availability";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType};
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    CalanderAvailibility Items = CalanderAvailibility.FromJson(Jsoncontent);

                    if (Items != null && Items.Meta != null &&
                        Utilities.SessionData.CurrntAppUser != null && Utilities.SessionData.CurrntAppUser.Meta != null)
                        Utilities.SessionData.CurrntAppUser.Meta.DraftCount = Items.Meta.DraftCount;

                    //Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
            {
                return NetworkError;
            }
        }
        #endregion
    }
}