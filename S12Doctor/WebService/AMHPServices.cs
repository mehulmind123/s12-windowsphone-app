﻿using Newtonsoft.Json;
using S12Doctor.Common;
using S12Doctor.Model.AMHP;
using S12Doctor.Model.Common;
using S12Doctor.Model.S12Doctor;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace S12Doctor.WebService
{
    public class AMHPServices
    {
        #region //Fields
        private HttpClient client;
        public string BaseUrl { get; set; }
        public string GoogleAPIKey { get; set; }

        private const string NetworkError = "Please check your internet connection or try again later.";
        #endregion

        #region //Constructor
        public AMHPServices()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            client.DefaultRequestHeaders.Authorization = new
                    System.Net.Http.Headers.AuthenticationHeaderValue(Utilities.SessionData.ApiHeaderScheme, Utilities.SessionData.ApiHeaderParameter);
            client.DefaultRequestHeaders.Add("Accept", "application/x.api.v2+json");
            //BaseUrl = "https://www.itrainacademy.in/doctorApp/api";
            BaseUrl = "https://s12solutions.com/application/api";
            GoogleAPIKey = "AIzaSyB6bUuk7bH2CPU106EbdFYe6ndY_Buo9mI";
        }
        #endregion

        #region //Methods
        //login_type = 1/2
        //1 - S12 Doctor User
        //2 - AMHP User
        public async Task<object> ccgList(string local_authority_id)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/ccg-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, local_authority_id = local_authority_id };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    CCGList Items = CCGList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> usefulInformation()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/amhp-useful-information";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    Model.AMHP.UsefulInformationList Items = Model.AMHP.UsefulInformationList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> myFavouriteDoctorsList(string offset, string limit)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/my-favourite";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, offset = offset, limit = limit };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    FavouriteDoctorList Items = FavouriteDoctorList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> favouriteUnfavouriteDoctor(string doctor_id, string favourite_status)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/favourite-unfavourite-doctor";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    doctor_id = doctor_id,
                    favourite_status = favourite_status
                };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    DoctorDetail Items = DoctorDetail.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items.Meta;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> doctorDetail(string doctor_id)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/doctor-detail";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, doctor_id = doctor_id };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    DoctorDetail Items = DoctorDetail.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> claimFormForApproval(string offset, string limit)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/claim-form-for-approval";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, offset = offset, limit = limit };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    ClaimFormForApprovalList Items = ClaimFormForApprovalList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> acceptTermsConditions()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/s12-accept-term-condition";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();
                    MetaInfo Items = JsonConvert.DeserializeObject<MetaInfo>(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> verifyPassword(string password)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/verify-password";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType, password = password };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    MetaInfo Items = JsonConvert.DeserializeObject<MetaInfo>(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /*
         status: 1/2
            1 - Approve Claim Form. At approval claim you have to pass ccg_id.
            2 - Reject Claim Form. At reject claim you have to pass rejection_reason.
         */
        public async Task<object> acceptOrRejectClaimForm(string form_id, string status,
            string ccg_id, string rejection_reason)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/accept-reject-claim-form";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    form_id = form_id,
                    status = status,
                    ccg_id = ccg_id,
                    rejection_reason = rejection_reason
                };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    MetaInfo Items = MetaInfo.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /*
         type:1/2 (1-Address Book Tab, 2-Location Tab)
            search_text:
                filter_availability:comma separated availability status id(1,2,3,4)
                filter_local_authority:Local authority id
                filter_distance: pass selected distance range
                filter_employer: 1/2 (1-trust, 2-independent)
                filter_specialty:speciality id
                filter_gender: 1/2 (1-female, 2-male)
                filter_language: language id
                sortby_availability: 1 (1-sort by availability --> Unknown ASC)
                sortby_employer: 1/2 (1- Sort by Trust, 2-Sort by independent)
                sortby_distance: 1/0 (1- Nearest First, 0- DESC by distance)
                sortby_name:1/2 (1- A to Z, 2- Z to A)
                longitude: Pass current longitude (for location tab)
                latitude: Pass current latitude (for location tab)
         */
        public async Task<object> homeScreen(string type, string offset, string limit, string search_text, string filter_availability, string filter_local_authority,
            string filter_distance, string filter_employer, string filter_specialty, string filter_gender,
            string filter_language, string sortby_availability, string sortby_employer, string sortby_distance,
            string sortby_name, string longitude, string latitude, string week_day, string time_slot)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/amhp-home";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    type = type,
                    offset = offset,
                    limit = limit,
                    search_text = search_text,
                    filter_availability = filter_availability,
                    filter_local_authority = filter_local_authority,
                    filter_distance = filter_distance,
                    filter_employer = filter_employer,
                    filter_specialty = filter_specialty,
                    filter_gender = filter_gender,
                    filter_language = filter_language,
                    sortby_availability = sortby_availability,
                    sortby_employer = sortby_employer,
                    sortby_distance = sortby_distance,
                    sortby_name = sortby_name,
                    longitude = longitude,
                    latitude = latitude,
                    week_day = week_day,
                    time_slot = time_slot
                };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    AddressBookDoctorList Items = AddressBookDoctorList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> editProfile(string name, string mobile_number, string email, string local_authority)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/amhp-edit-profile";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    name = name,
                    mobile_number = mobile_number,
                    email = email,
                    local_authority = local_authority
                };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    AppUser Items = AppUser.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> localAuthorityList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/local-authority-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType
                };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    Model.AMHP.LocalAuthorityList Items = Model.AMHP.LocalAuthorityList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /// <summary>
        /// get all language list
        /// </summary>
        /// <returns>language list</returns>
        public async Task<object> languageList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/language-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    LanguageList Items = LanguageList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        /// <summary>
        /// get all speciality list
        /// </summary>
        /// <returns>speciality list</returns>
        public async Task<object> specialityList()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/specialties-list";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    SpecialityList Items = SpecialityList.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> viewClaimForm(string claim_id)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/view-claim-form";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new
                {
                    login_type = Utilities.SessionData.LoginType,
                    claim_id = claim_id
                };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    Model.AMHP.ClaimForm Items = Model.AMHP.ClaimForm.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }
        public async void contactDoctor(string DID, string CType)
        {
                string RestUrl = BaseUrl + "/contact-doctor";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { doctor_id = DID, contact_type = CType, login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }
            
           
        }

        public async Task<object> changePassword(string old_password, string new_password)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/change-password";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { old_password = old_password, new_password = new_password, login_type = Utilities.SessionData.LoginType };
                var json = JsonConvert.SerializeObject(jsonRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    MetaInfo Items = MetaInfo.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> GetPlacesAutocompleteAsync(string search)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                // from: https://developers.google.com/places/documentation/autocomplete
                // e.g. https://maps.googleapis.com/maps/api/place/autocomplete/xml?input=Kirk&key=AddYourOwnKeyHere
                string request = string.Format("https://maps.googleapis.com/maps/api/place/autocomplete/xml?input={0}&key={1}", search, GoogleAPIKey);
                var xml = await (new HttpClient()).GetStringAsync(request);
                var results = XDocument.Parse(xml).Element("AutocompletionResponse").Elements("prediction");

                var suggestions = new List<string>();
                List<Place> PlacesArray = new List<Place>();
                foreach (var result in results)
                {
                    var suggestion = result.Element("description").Value;
                    Place place = new Place(suggestion);
                    PlacesArray.Add(place);
                    suggestions.Add(suggestion);
                }

                return PlacesArray;
            }
            else
                return NetworkError;
        }

        #endregion
    }
}
