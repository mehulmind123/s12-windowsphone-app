﻿using Newtonsoft.Json;
using S12Doctor.Common;
using S12Doctor.Model.Common;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace S12Doctor.WebService
{
    public class CommonServices
    {
        #region //Fields
        private HttpClient client;
        public string BaseUrl { get; set; }
        public string GoogleAPIKey { get; set; }

        private const string NetworkError = "Please check your internet connection or try again later.";
        #endregion

        #region //Constructor
        public CommonServices()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            client.DefaultRequestHeaders.Add("Accept", "application/x.api.v2+json");
            //BaseUrl = "https://www.itrainacademy.in/doctorApp/api";
            BaseUrl = "https://s12solutions.com/application/api";
            GoogleAPIKey = "AIzaSyB6bUuk7bH2CPU106EbdFYe6ndY_Buo9mI";
        }
        #endregion

        #region //Methods
        //login_type = 1/2
        //1 - S12 Doctor User
        //2 - AMHP User
        public async Task<object> login(string email, string password, string login_type)
        {
            try
            {
                if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    Loader.Show();

                    Utilities.SessionData.CurrntAppUser = null;

                    string RestUrl = BaseUrl + "/login";
                    var uri = new Uri(string.Format(RestUrl));
                    HttpResponseMessage response = null;

                    var jsonRequest = new { email = email, password = password, login_type = login_type };

                    var json = JsonConvert.SerializeObject(jsonRequest);

                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    try
                    {
                        response = await client.PostAsync(uri, content);
                    }
                    catch (Exception ex)
                    { }

                    if (response.IsSuccessStatusCode)
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();

                        AppUser Items = JsonConvert.DeserializeObject<AppUser>(Jsoncontent);

                        if (Items != null && Items.Data != null)
                        {
                            Utilities.SessionData.LoginType = Items.Data.LoginType.ToString();
                            Utilities.SessionData.CurrntAppUser = Items;

                            if (login_type == "1")
                                Helpers.SettingHelper.S12UserInfo = Jsoncontent;
                            else
                                Helpers.SettingHelper.AmhpUserInfo = Jsoncontent;
                        }
                        if (Items.Meta != null)
                        {
                            string[] auth = Items.Meta.Token.Split(' ');
                            if (auth.Length > 1)
                            {
                                Utilities.SessionData.ApiHeaderScheme = auth[0];
                                Utilities.SessionData.ApiHeaderParameter = auth[1];
                            }
                        }
                        Loader.Hode();
                        return Items;
                    }
                    else
                    {
                        Loader.Hode();
                        try
                        {
                            var Jsoncontent = await response.Content.ReadAsStringAsync();
                            MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                            return Item.Message;
                        }
                        catch (Exception) { }
                        return response.ToString();
                    }
                }
                else
                    return NetworkError;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public async Task<object> forgotPassword(string email, string login_type)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/forgot-password";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { email = email, login_type = login_type };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    MetaInfo Items = MetaInfo.FromJson(Jsoncontent);
                    Loader.Hode();
                    return Items;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> cms()
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                string RestUrl = BaseUrl + "/cms";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    CMS Items = JsonConvert.DeserializeObject<CMS>(Jsoncontent);
                    return Items;
                }
                else
                {
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        public async Task<object> changePassword(string old_password, string new_password, string login_type)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                Loader.Show();
                string RestUrl = BaseUrl + "/change-password";
                var uri = new Uri(string.Format(RestUrl));
                HttpResponseMessage response = null;

                var jsonRequest = new { old_password = old_password, new_password = new_password, login_type = login_type };

                var json = JsonConvert.SerializeObject(jsonRequest);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    response = await client.PostAsync(uri, content);
                }
                catch (Exception ex)
                { }

                if (response.IsSuccessStatusCode)
                {
                    var Jsoncontent = await response.Content.ReadAsStringAsync();

                    //AppUser Items = JsonConvert.DeserializeObject<AppUser>(Jsoncontent);
                    //Debug.WriteLine(@"TodoItem successfully saved.");
                    //return Items;
                    Loader.Hode();
                    return null;
                }
                else
                {
                    Loader.Hode();
                    try
                    {
                        var Jsoncontent = await response.Content.ReadAsStringAsync();
                        MetaMeta Item = MetaMeta.FromJson(Jsoncontent);
                        return Item.Message;
                    }
                    catch (Exception) { }
                    return response.ToString();
                }
            }
            else
                return NetworkError;
        }

        #endregion
    }
}
