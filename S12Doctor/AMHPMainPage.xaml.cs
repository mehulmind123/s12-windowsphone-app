﻿using S12Doctor.Model;
using S12Doctor.Model.AMHP;
using S12Doctor.Utilities;
using System;
using Windows.ApplicationModel.Email;
using Windows.Phone.UI.Input;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media;
using Windows.UI;
using Windows.UI.Popups;

namespace S12Doctor
{
    public sealed partial class AMHPMainPage : Page
    {
        private bool isLeftMenuOpen = false;

        DispatcherTimer dptTimer;
        DispatcherTimer amhpdptTimer;

        public Login LoginControl;
        public Views.AMHP.Home HomeControl;
        public Views.AMHP.UsefulInfo UsefulInfoControl;
        public Views.TermsAndConditions TermsAndConditionsControl;
        public Views.AMHP.ChangePassword ChangePasswordControl;
        public Views.AMHP.MyFavorites MyFavoritesControl;
        public Views.AMHP.EditProfile EditProfileControl;
        public Views.AMHP.ClaimForms ClaimFormsControl;
        public Views.UserGuide userGuideControl;
        public Views.AMHP.DoctorDetails DoctorDetailsControl;
        public Views.AboutUs AboutUsControl;
        public Views.PrivacyPolicy PrivacyPolicyControl;

        public AMHPMainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Disabled;
            this.Loaded += MainPage_Loaded;
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //Show the status bar
            var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
            ////statusBar.BackgroundColor = Windows.UI.Colors.White;
            statusBar.ForegroundColor = Windows.UI.Colors.Black;
            //statusBar.BackgroundOpacity = 1;
            await statusBar.ShowAsync();

            dptTimer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(500) };
            dptTimer.Tick += DptTimer_Tick;
            dptTimer.Start();

            amhpdptTimer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(100) };
            amhpdptTimer.Tick += amhpDptTimer_Tick;
            amhpdptTimer.Start();

            RightMenuStorybord_Close.Completed += RightMenuStorybord_Close_Completed;

            AddControl(Pages.Login);

            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }
        private void amhpDptTimer_Tick(object sender, object e)
        {
            if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
            {
                enableLinks();
            }
            else
            {
                disableLinks();
            }
        }
        private void DptTimer_Tick(object sender, object e)
        {
            try
            {
                // Gets the app's current memory usage   
                ulong AppMemoryUsageUlong = MemoryManager.AppMemoryUsage;

                // Gets the app's memory usage limit   
                ulong AppMemoryUsageLimitUlong = MemoryManager.AppMemoryUsageLimit;

                AppMemoryUsageUlong /= 1024 * 1024;
                AppMemoryUsageLimitUlong /= 1024 * 1024;
                AppMemoryUsage.Text = "App memory uage - " + AppMemoryUsageUlong.ToString();
                AppMemoryUsageLimit.Text = "App memory usage limit - " + AppMemoryUsageLimitUlong.ToString();

                // Gets the app's memory usage level whether low or medium or high   
                AppMemoryUsageLevels.Text = "App memory usage level - " + MemoryManager.AppMemoryUsageLevel.ToString();

                GC.Collect();
            }
            catch { }
        }

        private void RightMenuStorybord_Close_Completed(object sender, object e)
        {
            RightMenuStorybord.Stop();
            RightMenuStorybord_Close.Stop();
        }

        private void Hamburger_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MenubarAction();
        }

        private void CloseLeftMenu_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MenubarAction();
        }

        public void RedirectToHomePage()
        {
            AddControl(Pages.Home);
        }
        public void disableLinks()
        {
            SolidColorBrush foregroundSolidColorBrush;
            foregroundSolidColorBrush = new SolidColorBrush();
            foregroundSolidColorBrush.Color = Color.FromArgb(255, 105, 105, 105);

            textMyFavorites.Foreground = foregroundSolidColorBrush;
            textDraftClaimForms.Foreground = foregroundSolidColorBrush;
            textEditProfile.Foreground = foregroundSolidColorBrush;
            textChangePassword.Foreground = foregroundSolidColorBrush;
            if(SessionData.CurrentView == Pages.Login)
            {
                AmhpInternetCon.Visibility = Visibility.Collapsed;
            } else
            {
                AmhpInternetCon.Visibility = Visibility.Visible;
            }

        }
        public void enableLinks()
        {
            SolidColorBrush foregroundSolidColorBrush;
            foregroundSolidColorBrush = new SolidColorBrush();
            foregroundSolidColorBrush.Color = Color.FromArgb(255, 255, 255, 255);

            textMyFavorites.Foreground = foregroundSolidColorBrush;
            textDraftClaimForms.Foreground = foregroundSolidColorBrush;
            textEditProfile.Foreground = foregroundSolidColorBrush;
            textChangePassword.Foreground = foregroundSolidColorBrush;
            AmhpInternetCon.Visibility = Visibility.Collapsed;

        }
        private void MenubarAction()
        {
            isLeftMenuOpen = !isLeftMenuOpen;

            if (isLeftMenuOpen)
            {
                RightMenuStorybord.Begin();
                CloseLeftMenu.Visibility = Visibility.Visible;
                if (!Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                {
                    disableLinks();
                }
                else
                {
                    enableLinks();

                }
            }
            else
            {
                RightMenuStorybord_Close.Begin();
                CloseLeftMenu.Visibility = Visibility.Collapsed;
            }
        }

        public void LoaderVisibility(Visibility visibility)
        {
            LoadingWindow.Visibility = visibility;
        }

        public void OpenPopUpBox(string msg)
        {
            txtPopUpMessage.Text = msg;
            GridPopUpWindow.Visibility = Visibility.Visible;
        }

        public void SetHeaderText(string text)
        {
            HeaderText.Text = text;
        }

        public void SetHamburgerVisibility(Visibility visibility)
        {
            Hamburger.Visibility = visibility;
        }

        public void SetUserControlOnHeader()
        {
            Grid.SetRow(ViewContainer, 0);
            Grid.SetRowSpan(ViewContainer, 2);
        }

        public void RemoveUserControlFromHeader()
        {
            Grid.SetRow(ViewContainer, 1);
        }

        public void OpenDoctorDetailPage(long doctorID)
        {
            if (DoctorDetailsControl == null)
            {
                DoctorDetailsControl = new Views.AMHP.DoctorDetails();
                DoctorDetailsView.Children.Add(DoctorDetailsControl);
            }
            DoctorDetailsView.Visibility = Visibility.Visible;
            DoctorDetailsControl.SetData(doctorID);
        }

        #region ADD & DISPOSE Applications Functions...

        #region Pages

        public void AddControl(Pages view)
        {
            if (SessionData.CurrentView != view)
            {
                SessionData.DisposableView = SessionData.CurrentView;
                SessionData.CurrentView = view;

                DisposeControl(SessionData.DisposableView);

                switch (view)
                {
                    case Pages.Login:

                        if (LoginControl == null)
                        {
                            LoginControl = new Login();
                            SetUserControlOnHeader();
                            ViewContainer.Children.Add(LoginControl);
                        }

                        break;
                    case Pages.Home:

                        if (HomeControl == null)
                        {
                            HeaderText.Text = "AMHP Home";
                            HomeControl = new Views.AMHP.Home();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(HomeControl);
                            MultiSelectButtonVisible();
                        }

                        break;

                    case Pages.UsefulInfo:

                        if (UsefulInfoControl == null)
                        {
                            HeaderText.Text = "Useful Information";
                            UsefulInfoControl = new Views.AMHP.UsefulInfo();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(UsefulInfoControl);
                        }

                        break;

                    case Pages.MyFavorites:

                        if (MyFavoritesControl == null)
                        {
                            HeaderText.Text = "My Favourites";
                            MyFavoritesControl = new Views.AMHP.MyFavorites();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(MyFavoritesControl);
                        }

                        break;

                    case Pages.ClaimForms:

                        if (ClaimFormsControl == null)
                        {
                            HeaderText.Text = "Claim Forms for Review";
                            ClaimFormsControl = new Views.AMHP.ClaimForms();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(ClaimFormsControl);
                        }

                        break;

                    case Pages.EditProfile:

                        if (EditProfileControl == null)
                        {
                            HeaderText.Text = "Edit Profile";
                            EditProfileControl = new Views.AMHP.EditProfile();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(EditProfileControl);
                        }

                        break;

                    case Pages.ChangePassword:

                        if (ChangePasswordControl == null)
                        {
                            HeaderText.Text = "Change Password";
                            ChangePasswordControl = new Views.AMHP.ChangePassword();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(ChangePasswordControl);
                        }

                        break;

                    case Pages.TermsAndConditions:

                        if (TermsAndConditionsControl == null)
                        {
                            HeaderText.Text = "Terms & Conditions";
                            TermsAndConditionsControl = new Views.TermsAndConditions();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(TermsAndConditionsControl);
                        }

                        break;

                    case Pages.UserGuide:
                        if (userGuideControl == null)
                        {
                            HeaderText.Text = "User Guide";
                            userGuideControl = new Views.UserGuide();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(userGuideControl);
                        }
                        break;

                    case Pages.AboutUs:

                        if (AboutUsControl == null)
                        {
                            HeaderText.Text = "About Us";
                            AboutUsControl = new Views.AboutUs();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(AboutUsControl);
                        }

                        break;

                    case Pages.PrivacyPolicy:

                        if (PrivacyPolicyControl == null)
                        {
                            HeaderText.Text = "Privacy Policy";
                            PrivacyPolicyControl = new Views.PrivacyPolicy();
                            RemoveUserControlFromHeader();
                            ViewContainer.Children.Add(PrivacyPolicyControl);
                        }

                        break;

                    default:
                        break;
                }
            }
        }

        public void DisposeControl(Pages view)
        {
            switch (view)
            {
                case Pages.Login:

                    if (LoginControl != null)
                    {
                        LoginControl.DisposeMemory();
                        LoginControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.Home:

                    if (HomeControl != null)
                    {
                        HomeControl.DisposeMemory();
                        HomeControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                        MultiSelectButtonCollabpsed();
                    }

                    break;

                case Pages.UsefulInfo:

                    if (UsefulInfoControl != null)
                    {
                        //UsefulInfoControl.DisposeMemory();
                        UsefulInfoControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.MyFavorites:

                    if (MyFavoritesControl != null)
                    {
                        //MyFavoritesControl.DisposeMemory();
                        MyFavoritesControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.ClaimForms:

                    if (ClaimFormsControl != null)
                    {
                        //ClaimFormsControl.DisposeMemory();
                        ClaimFormsControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.EditProfile:

                    if (EditProfileControl != null)
                    {
                        //EditProfileControl.DisposeMemory();
                        EditProfileControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.ChangePassword:

                    if (ChangePasswordControl != null)
                    {
                        //ChangePasswordControl.DisposeMemory();
                        ChangePasswordControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.TermsAndConditions:

                    if (TermsAndConditionsControl != null)
                    {
                        //TermsAndConditionsControl.DisposeMemory();
                        TermsAndConditionsControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;
                case Pages.UserGuide:

                    if (userGuideControl != null)
                    {
                        //TermsAndConditionsControl.DisposeMemory();
                        userGuideControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.AboutUs:

                    if (AboutUsControl != null)
                    {
                        //AboutUsControl.DisposeMemory();
                        AboutUsControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                case Pages.PrivacyPolicy:

                    if (PrivacyPolicyControl != null)
                    {
                        //PrivacyPolicyControl.DisposeMemory();
                        PrivacyPolicyControl = null;
                        ViewContainer.Children.Clear();
                        this.UpdateLayout();
                    }

                    break;

                default:
                    break;
            }
        }

        #endregion

        #endregion

        private void HeaderText_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MemoryWindow.Visibility = Visibility.Visible;
        }

        private void btnOk_Click(object sender, TappedRoutedEventArgs e)
        {
            GridPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void gridShadow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GridPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void gridHalpSupportBack_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridHalpSupportPopUp.Visibility = Visibility.Collapsed;
        }

        private void btnHalpSupportCancel_Click(object sender, TappedRoutedEventArgs e)
        {
            gridHalpSupportPopUp.Visibility = Visibility.Collapsed;
        }

        private void lblUserGuide_Tapped(object sender, TappedRoutedEventArgs e)
        {
            gridHalpSupportPopUp.Visibility = Visibility.Collapsed;
            AddControl(Pages.UserGuide);
            if (gridHalpSupportPopUp.Visibility != Visibility.Visible)
                MenubarAction();
        }

        private async void lblEmailUs_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                gridHalpSupportPopUp.Visibility = Visibility.Collapsed;

                EmailRecipient sendTo = new EmailRecipient()
                {
                    Address = "support@s12solutions.com",
                };

                //generate mail object
                EmailMessage mail = new EmailMessage();
                mail.Subject = "";
                mail.Body = "";
                //add recipients to the mail object
                mail.To.Add(sendTo);

                //open the share contract with Mail only:
                await EmailManager.ShowComposeNewEmailAsync(mail);
            }
            catch (Exception ex)
            {
                ((S12Doctor.AMHPMainPage)(Window.Current.Content as Frame).Content).OpenPopUpBox(ex.Message);
            }
        }

        private void gridLogOutShadow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GridLogOutPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void btnLogOutNo_Click(object sender, TappedRoutedEventArgs e)
        {
            GridLogOutPopUpWindow.Visibility = Visibility.Collapsed;
        }

        private void btnLogOutYes_Click(object sender, TappedRoutedEventArgs e)
        {
            GridLogOutPopUpWindow.Visibility = Visibility.Collapsed;
            DisposeControl(SessionData.DisposableView);
            DisposeControl(SessionData.CurrentView);
            MenubarAction();

            SessionData.ClearSessionData();

            var lst = new System.Collections.Generic.List<string>();
            lst.Add("iss12loginfirsttime_key");
            lst.Add("isamhploginfirsttime_key");

            Helpers.SettingHelper.Purge(lst);
            
            if (Frame.CanGoBack)
                Frame.GoBack();
            else
                Frame.Navigate(typeof(WelcomePage));
        }

        private void MemoryWindow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MemoryWindow.Visibility = Visibility.Collapsed;
        }

        #region SidebarMenuButtons

        private void gridLogout_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GridLogOutPopUpWindow.Visibility = Visibility.Visible;
        }

        private void SideMenuButton_Click(object sender, TappedRoutedEventArgs e)
        {
            switch (((Grid)sender).Name)
            {
                case "gridHome":
                    AddControl(Pages.Home);
                    break;
                case "gridUserInfo":
                    AddControl(Pages.UsefulInfo);
                    break;
                case "gridEditProfile":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.EditProfile);
                        }
                        else
                            return;
                    }
                    
                    break;
                case "gridMyFavorites":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.MyFavorites);
                        }
                        else
                            return;
                    }
                    break;
                case "gridChangePassword":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.ChangePassword);
                        }
                        else
                            return;

                    }

                    break;
                case "gridClaimForms":
                    {
                        if (Common.NetworkAvailabilty.Instance.IsNetworkAvailable)
                        {
                            AddControl(Pages.ClaimForms);
                        }
                        else
                            return;

                    }

                    break;
                case "gridAboutUs":
                    AddControl(Pages.AboutUs);
                    break;
                case "gridTermsConditions":
                    AddControl(Pages.TermsAndConditions);
                    break;
                case "gridPrivecyPolicy":
                    AddControl(Pages.PrivacyPolicy);
                    break;
                case "gridHelpSupport":
                    gridHalpSupportPopUp.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }

            if (gridHalpSupportPopUp.Visibility != Visibility.Visible)
                MenubarAction();
        }

        #endregion

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (GridPopUpWindow.Visibility == Visibility.Visible)
            {
                GridPopUpWindow.Visibility = Visibility.Collapsed;
                e.Handled = true;
            }
            else if (gridHalpSupportPopUp.Visibility == Visibility.Visible)
            {
                gridHalpSupportPopUp.Visibility = Visibility.Collapsed;
                e.Handled = true;
            }
            else if (GridLogOutPopUpWindow.Visibility == Visibility.Visible)
            {
                GridLogOutPopUpWindow.Visibility = Visibility.Collapsed;
                e.Handled = true;
            }
            else if (DoctorDetailsView.Visibility == Visibility.Visible)
            {
                if (DoctorDetailsControl.gridCallPopUp.Visibility == Visibility.Visible)
                {
                    DoctorDetailsControl.gridCallPopUp.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else if (DoctorDetailsView.Visibility == Visibility.Visible)
                {
                    DoctorDetailsView.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
            }
            else if (SessionData.CurrentView == Pages.Login)
            {
                if (LoginControl.gridForgotPassword.Visibility == Visibility.Visible)
                {
                    LoginControl.txtFPEmailAddress.Text = string.Empty;
                    LoginControl.gridForgotPassword.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else
                {
                    e.Handled = true;
                    btnLogOutYes_Click(null, null);
                }
            }
            else if (SessionData.CurrentView == Pages.Home)
            {
                if (HomeControl.gridCallPopUp.Visibility == Visibility.Visible)
                {
                    HomeControl.gridCallPopUp.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else if (HomeControl.gridMapPinViewer.Visibility == Visibility.Visible)
                {
                    HomeControl.gridMapPinViewer.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else if (HomeControl.gridAction.Visibility == Visibility.Visible)
                {
                    HomeControl.gridAction.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else if (HomeControl.spFilters.Visibility == Visibility.Visible)
                {
                    HomeControl.spFilters.Visibility = Visibility.Collapsed;
                    if (HomeControl.IsAddressBookVisible)
                        HomeControl.SetDefaultAddressBookFilter();
                    else
                        HomeControl.SetDefaultLocationFilter();
                    RemoveUserControlFromHeader();
                    e.Handled = true;
                }
                else if (HomeControl.GridSortBy.Visibility == Visibility.Visible)
                {
                    HomeControl.GridSortBy.Visibility = Visibility.Collapsed;
                    if (HomeControl.IsAddressBookVisible)
                        HomeControl.SetDefaultAddressBookSort();
                    else
                        HomeControl.SetDefaultLocationSort();
                    RemoveUserControlFromHeader();
                    e.Handled = true;
                }
            }
            else if (SessionData.CurrentView == Pages.EditProfile)
            {

            }
            else if (SessionData.CurrentView == Pages.ClaimForms)
            {
                if (ClaimFormsControl.viewClaimForm != null && ClaimFormsControl.viewClaimForm.gridRejectionReasonPopUp.Visibility == Visibility.Visible)
                {
                    ClaimFormsControl.viewClaimForm.gridRejectionReasonPopUp.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else if (ClaimFormsControl.viewClaimForm != null && ClaimFormsControl.viewClaimForm.gridCCGToSubmitPopUp.Visibility == Visibility.Visible)
                {
                    ClaimFormsControl.viewClaimForm.gridCCGToSubmitPopUp.Visibility = Visibility.Collapsed;
                    e.Handled = true;
                }
                else if (ClaimFormsControl.gridViewClaimForm.Visibility == Visibility.Visible)
                {
                    ClaimFormsControl.gridViewClaimForm.Visibility = Visibility.Collapsed;
                    RemoveUserControlFromHeader();
                    e.Handled = true;
                }
            }
            //else if (Frame.CanGoBack)
            //{
            //    e.Handled = true;
            //    Frame.GoBack();
            //}
        }

        public void MultiSelectButtonVisible()
        {
            imgMultiSelect.Visibility = Visibility.Visible;
            lblMultiSelectDone.Visibility = Visibility.Collapsed;
        }

        public void MultiSelectButtonCollabpsed()
        {
            imgMultiSelect.Visibility = Visibility.Collapsed;
            lblMultiSelectDone.Visibility = Visibility.Collapsed;
        }

        private void imgMultiSelect_Tapped(object sender, TappedRoutedEventArgs e)
        {
            imgMultiSelect.Visibility = Visibility.Collapsed;
            lblMultiSelectDone.Visibility = Visibility.Visible;
            if (HomeControl != null)
                HomeControl.ListBoxMultipleSelectionMode(true);
        }

        private void lblMultiSelectDone_Tapped(object sender, TappedRoutedEventArgs e)
        {
            imgMultiSelect.Visibility = Visibility.Visible;
            lblMultiSelectDone.Visibility = Visibility.Collapsed;
            if (HomeControl != null)
            {
                HomeControl.SendSelectedContectsSMS();
            }
        }
    }
}

