﻿using System;
using WPGoogleApi.Entities.Common.Enums;
using WPGoogleApi.Entities.Common.Enums.Extensions;
using WPGoogleApi.Entities.Interfaces;

namespace WPGoogleApi.Entities.Places.Details.Request
{
    /// <summary>
    /// Places Details Request.
    /// </summary>
    public class PlacesDetailsRequest : BasePlacesRequest, IRequestQueryString
    {
        /// <summary>
        /// Base Url.
        /// </summary>
        protected internal override string BaseUrl => base.BaseUrl + "details/json";

        /// <summary>
        /// A textual identifier that uniquely identifies a place, returned from a Place Search.
        /// </summary>
        public virtual string PlaceId { get; set; }

        /// <summary>
        /// Language (optional) — The language code, indicating in which language the results should be returned, if possible. 
        /// See the list of supported languages and their codes. Note that we often update supported languages so this list may not be exhaustive.
        /// </summary>
        public virtual Language Language { get; set; } = Language.English;

        /// <summary>
        /// Extensions (optional) — Indicates if the Place Details response should include additional fields. 
        /// Additional fields may include Premium data, requiring an additional license, or values that are not commonly requested. 
        /// Supported values for the extensions parameter are: ◦review_summary includes a rich and concise review curated by Google's editorial staff.
        /// </summary>
        public virtual Enums.Extensions Extensions { get; set; } = Enums.Extensions.None;

        /// <summary>
        /// See <see cref="BasePlacesRequest.GetQueryStringParameters()"/>.
        /// </summary>
        /// <returns>The <see cref="QueryStringParameters"/> collection.</returns>
        public override QueryStringParameters GetQueryStringParameters()
        {
            if (string.IsNullOrWhiteSpace(this.PlaceId))
                throw new ArgumentException("PlaceId is required");

            var parameters = base.GetQueryStringParameters();

            parameters.Add("placeid", this.PlaceId);
            parameters.Add("language", this.Language.ToCode());

            if (this.Extensions != Enums.Extensions.None)
                parameters.Add("extensions", this.Extensions.ToString().ToLower());

            return parameters;
        }
    }
}