﻿using WPGoogleApi.Entities.Places.Search.Common;

namespace WPGoogleApi.Entities.Places.Search.Radar.Response
{
    /// <summary>
    /// Radar Result.
    /// </summary>
    public class RadarResult : BaseResult
    {

    }
}